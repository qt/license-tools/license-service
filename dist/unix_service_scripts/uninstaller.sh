#!/usr/bin/env bash
# Copyright (C) 2023 The Qt Company Ltd.
#
# SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

name="qtlicd"
install_dir="/opt/${name}/"

function exit_fail {
    echo "ERROR! $1: Aborting uninstall"
    exit 1
}

if [ $(whoami) != "root" ]; then
    exit_fail "You are not root"
fi

# Get the operating system name
OS=$(uname)
echo "Operating system: ${OS}"
if [ "$OS" == "Linux" ]; then
    service=${name}.service
    service_dir="/etc/systemd/system/"
    service_stop="systemctl stop ${service}"
    service_disable="systemctl disable ${service}"
    service_alive="systemctl is-active --quiet ${service}"
elif [ "$OS" == "Darwin" ]; then
    service=io.qt.${name}.plist
    service_dir="/Library/LaunchDaemons/"
    service_disable="launchctl remove ${service_dir}${service}"
    service_stop="launchctl unload -w ${service_dir}${service}"
    service_alive="launchctl list| grep -q '${service}'"
else
    exit_fail "OS not supported: ${OS}"
fi

function validate_paths {
    # Refusing to delete everything from under /opt or evan / (!)
    # in case required variables are not defined
    if [[ -z "${install_dir}" || -z "${name}" || -z "${service_dir}" || -z "${service}" ]]; then
        exit_fail "Incorrect uninstall parameters"
    fi
}


function stop_daemon {
    eval "${service_stop}"
    eval "${service_alive}"
    if [ $? != 0 ]; then
        echo "Qt License Service not running"
        return
    fi

    retries=0
    running=false
    while [ ${running} != "false" ]
    do
        running=false
        eval "${service_stop}"
        if [ $? != 0 ]; then
            running=true
            echo "Qt License Service running, trying to stop ${service}"
            retries=$((retries+1))
            if [ $retries -gt 2 ]; then
                exit_fail "Failed to terminate ${service}"
            fi
        fi
    done
    eval "${service_disable}"
    echo "Stopped $service"
}

function remove_files() {
    # Remove installation files.
    rm ${service_dir}${service}
    rm /usr/local/bin/qtlicensetool
    rm -rf ${install_dir}
    if [ "${OS}" == "Linux" ]; then
        systemctl daemon-reload
    fi
}

validate_paths

stop_daemon

remove_files

echo "Done uninstalling the Qt License Service"
