#!/usr/bin/env bash
# Copyright (C) 2023 The Qt Company Ltd.
#
# SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

name="qtlicd"
install_dir="/opt/${name}/"

function exit_fail {
    echo "ERROR! $1: Aborting installation"
    exit 1
}

function check_prerequisites {
    if [ $(whoami) != "root" ]; then
        exit_fail "You are not root"
    fi
    # Get the operating system name
    OS=$(uname)
    echo "Operating system: ${OS}"
    if [ "$OS" == "Linux" ]; then
        service=${name}.service
        service_dir="/etc/systemd/system/"
        service_enable="systemctl daemon-reload; systemctl enable ${service}"
        service_start="systemctl start ${service}"
        service_stop="systemctl stop ${service}"
        service_alive="systemctl is-active --quiet ${service}"
    elif [ "$OS" == "Darwin" ]; then
        service=io.qt.${name}.plist
        service_dir="/Library/LaunchDaemons/"
        service_enable="launchctl load -w ${service_dir}${service}"
        service_start="" # "launchctl start ${service}"
        service_stop="launchctl unload -w ${service_dir}${service}"
        service_alive="launchctl list| grep -q '${service}'"
    else
        exit_fail "OS not supported: ${OS}"
    fi
}


function create_install_dir {
    umask u=rwx,go=rx;mkdir -p -m 755 ${install_dir}
    if [ $? != 0 ]; then
        exit_fail "Could not create directory ${install_dir}"
    fi

}

function copy_files {
    # Copy executables, unistall script, docs and .ini
    cp ${name} ${name}.ini installer.sh uninstaller.sh README.md CHANGELOG INSTALL_unix.txt qtlicensetool mocwrapper licheck ${install_dir}/
    if [ $? != 0 ]; then
        exit_fail "Failed to copy files to ${install_dir}"
    fi

    cp ${service} ${service_dir}
    if [ $? != 0 ]; then
        exit_fail "Failed to copy ${service} file into ${service_dir}"
    fi

    # Create a symlink for qtlicensetool binary if not already there
    if ! [ -f "/usr/local/bin/qtlicensetool" ]; then
        ln -s ${install_dir}/qtlicensetool /usr/local/bin/qtlicensetool
    fi

    sync
}


function stop_daemon {
    eval "${service_stop}"
    eval "${service_alive}"
    if [ $? != 0 ]; then
        echo "Qt License Service not running"
        return
    fi

    retries=0
    running=false
    while [ ${running} != "false" ]
    do
        running=false
        eval "${service_stop}"
        if [ $? != 0 ]; then
            running=true
            echo "Qt License Service running, trying to stop ${service}"
            retries=$((retries+1))
            if [ $retries -gt 2 ]; then
                exit_fail "Failed to terminate ${service}"
            fi
        fi
    done
    echo "Stopped $service"
}

function enable_and_start_daemon {
    # Enable the daemon
    eval "${service_enable}"
    if [ $? != 0 ]; then
        exit_fail "Could not enable the Qt License Sarvice"
    fi
    # Start the Qt License Service
    eval "${service_start}"
    if [ $? != 0 ]; then
        exit_fail "Could not start the Qt License Service"
    fi
}

check_prerequisites

stop_daemon

create_install_dir

copy_files

enable_and_start_daemon

echo "Installed Qt License Service"
