:: Copyright (C) 2023 The Qt Company Ltd.
::
:: SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
::

@echo off

set workDir=C:\Users\qt\work
set deployDir=%workDir%\license-tools\license-service\deploy
call :check_error_level

set cut="C:\Program Files\Git\usr\bin\cut.exe"
echo %COIN_DOWNLOAD_URL% | "C:\Program Files\Git\usr\bin\cut.exe" -d'/' -f3 | "C:\Program Files\Git\usr\bin\cut.exe" -d':' -f1 > %TEMP%\ip.txt
call :check_error_level
set /p coin_ip=<%TEMP%\ip.txt
set sources="http://%coin_ip%:8080/coin/api/results/license-tools/license-service/%TESTED_MODULE_REVISION_COIN%/sources_win.tar.gz"
curl -o %deployDir%\license_service_sources_win.tar.gz %sources% || powershell Invoke-WebRequest -UseBasicParsing %sources% -OutFile %deployDir%\license_service_sources_win.tar.gz
call :check_error_level
del %TEMP%\ip.txt

call :check_error_level
:check_error_level
IF %ERRORLEVEL% NEQ 0 (
    echo Error: Operation failed with error code %ERRORLEVEL%.
    exit %ERRORLEVEL%
)
EXIT /B 0
