#!/usr/bin/env bash
# Copyright (C) 2023 The Qt Company Ltd.
#
# SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
#


name=qtlicd
deploy_dir=deploy
INSTALL=false
DEBUG=false

# Get the operating system name
OS=$(uname)

function exit_fail {
    echo "ERROR! $1: Aborting build"
    exit 1
}

function helptext {
    echo "Supported CLI arguments:"
    echo " --debug    :  Toggle debug build"
    echo " --help     :  This help"
}

# Read command line options
function parse_cli_arguments {
    ret=0
    for arg in "$@"
    do
        case ${arg} in
        --help)
            helptext
            exit 0
            ;;
        --debug)
            DEBUG=true
          ;;
        *)
            echo "Invalid CLI argument ${arg} given"
            helptext
            exit 1
            ;;
        esac
    done

    return ${ret}
}

function build {
    # Create build dir (if not there)
    mkdir -p build
    if [ $? != 0 ]; then
        exit_fail "Failed to create build dir"
    fi
    cd build

    # build
    if ( ${DEBUG} ) then
        cmake .. -DDEBUG=True
    else
        cmake .. -DCONFIG=Release
    fi

    if [ $? != 0 ]; then
        exit_fail "Cmake failed"
    fi

    make clean
    make
    if [ $? != 0 ]; then
        exit_fail "Build failed"
    fi

    chmod +x bin/${name} bin/licheck bin/qtlicensetool bin/mocwrapper
    cd ..
    # Copy files for deployment dir (first create it)
    mkdir -p ${deploy_dir}
    if [ $? != 0 ]; then
        exit_fail "Failed to create deploy dir"
    fi
}

function copy_files {
    cp build/bin/${name} ${deploy_dir}/ || exit_fail "failed to copy ${name}"
    cp build/bin/licheck ${deploy_dir}/ || exit_fail "failed to copy licheck"
    cp build/bin/qtlicensetool ${deploy_dir}/ || exit_fail "failed to copy qtlicensetool"
    cp build/bin/mocwrapper ${deploy_dir}/ || exit_fail "failed to copy mocwrapper"
    cp dist/${name}.ini ${deploy_dir}/ || exit_fail "failed to copy ${name}.ini"
    cp README.md ${deploy_dir}/ || exit_fail "failed to copy README.md"
    cp CHANGELOG ${deploy_dir}/ || exit_fail "failed to copy CHANGELOG"
    cp INSTALL_unix.txt ${deploy_dir}/ || exit_fail "failed to copy INSTALL_unix.txt"
    cp dist/unix_service_scripts/uninstaller.sh ${deploy_dir}/ || exit_fail "failed to copy uninstaller.sh"
    cp dist/unix_service_scripts/installer.sh ${deploy_dir}/ || exit_fail "failed to copy installer.sh"
    if [ "$OS" == "Linux" ]; then
        cp dist/unix_service_scripts/${name}.service ${deploy_dir}/ || exit_fail "failed to copy ${name}.service"
    elif [ "$OS" == "Darwin" ]; then
        cp dist/unix_service_scripts/io.qt.${name}.plist ${deploy_dir}/ || exit_fail "failed to copy io.qt.${name}.plist"
    fi
    chmod +x ${deploy_dir}/installer.sh ${deploy_dir}/uninstaller.sh || exit_fail "failed to chmod installer/uninstaller.h scripts"
}

parse_cli_arguments "$@"

build

copy_files

echo "Done."

#!/bin/bash
