// Copyright (C) 2024 The Qt Company Ltd.


Qt License Service for Linux and macOS

This document describes how to build and install the Qt License Service by admins if there is
a need to do this by yourself.

Note, that the Qt (Online) Installer will install the Qt License Service automatically as an
on-demand service on behalf of the user.


----------  Prerequisites (Linux): ----------

cmake, libcurl-openssl-dev, build-essential, libuuid:

    In ubuntu, install them with command:
    > sudo apt install cmake build-essential uuid-dev

RHEL and other rpm-based systems are different, but equivalent packages are available.


----------  Prerequisites (macOS): ----------

Xcode:
    Downloadable from App Store or from: https://developer.apple.com/download/

CMake:
    Binaries are available from: https://cmake.org/download/


----------  Qt License Service operating modes: ----------

The Qt License Service can operate in the following modes:

- Option 1: As a system service
  - Running always on the background and available for all users on the machine

- Option 2: As a user-space process
  - The user is responsible starting and shutting down the service. Mainly for testing purposes.

- Option 3: On-demand start (user-space)
  - The service is launched automatically when a license reservation is required


----------  Note about SSL certificates: ----------

The Qt License Service uses libcurl for performing network requests.

Linux:
    libcurl has its own build time detection for a CA bundle file, or a CA path containing multiple
    certificates that will be used for verifying the SSL certificate presented by the Qt License Server.

    As the path for CA bundle file can differ from distribution to distribution, the Qt License Service
    overrides the build time auto detection with run time lookup for a valid certificate:

    1. First a CA bundle file is looked up from predefined search paths
    2. If no CA bundle file was found, individual certificates are searched from "/etc/ssl/certs"


----------  Build: ----------

1. Export OPENSSL_ROOT_DIR to point to OpenSSL libraries that should be used for linking the service

    $ export OPENSSL_ROOT_DIR=</path/to/your/openssl/installation>

2. Run the build script: (from the project root)

    $ ./build_unix.sh


----------  Configure server address: ----------

1. Edit qtlicd.ini file

    1.1 $ cd deploy (created by the build_unix.sh)
    1.2 Edit the qtlicd.ini file
      - server_addr         # The address and port of the Qt License Server. The default one
                            # is the cloud instance hosted by Qt. If using on-prem server then
                            # set the address accordingly.
      - ca_bundle_path      # Set the absolute path to CA bundle file, or "auto" for automatic detection.
                            # The automatic detection tries to first find a CA bundle file from predefined
                            # search paths, and if that fails, "/etc/ssl/certs" will be used as a fallback
                            # for looking for an individual certificate file to verify the peer with.
      - tcp_listening_port  # Change if the default value is not suitable when using the
                            # on-prem server
      - request_timeout     # Set the timeout for network requests in seconds, or 0 to never time out


----------  Option 1: Install it as a system service: ----------

2. Run the installation script as root and start the service:

    $ sudo ./installer.sh

    (will now start automatically after reboot)

    If you want to run the service in user space then refer to chapter 3.

2.1. Check if the service is running:
    Linux:
        $ sudo systemctl status qtlicd
    macOS:
        $ sudo launchctl list | grep qtlicd

2.2. To stop it:
    Linux:
        $ sudo systemctl stop qtlicd
    macOS:
        $ sudo launchctl stop io.qt.qtlicd
            -- Note! service is still visible with the 'launchctl list' command, but there's no PID.

2.3. To disable it:
    Linux:
        $ sudo systemctl disable qtlicd
    macOS:
        $ sudo launchctl unload -w /Library/LaunchDaemons/io.qt.qtlicd

2.4 To view service status changes
    Linux:
        $ journalctl -u qtlicd -n 100  # Displays last 100 lines
    macOS:
        $ log show --predicate 'process == "io.qt.qtlicd"'
          and/or
        $ launchctl print system/io.qt.qtlicd

2.5. To view logs:

     See the logs file: /opt/qtlicd/qtlicd_log.txt

2.6 Accept Terms and Conditions of the service

    Use the qtlicensetool to accept the Terms and Conditions of the service.
    This is a prerequisite for enabling the license reservation functionality.

    2.6.1 To view and accept the Terms and Conditions from an interactive prompt:
        $ /opt/qtlicd/qtlicensetool --show-terms-and-conditions
    2.6.2 To accept the Terms and Conditions without interaction:
        $ /opt/qtlicd/qtlicensetool --accept-terms-and-conditions

2.7. To completely uninstall, run 'uninstaller.sh' from the deploy directory (as root)

     In addition, you need to remove symlinks from your Qt installation path and restore the
     original binaries there:

     2.7.1 $ cd <your Qt installation path>/bin
     2.7.2 $ mv qtmoc moc
     2.7.3 $ mv licheck.bak licheck    <-- or whatever you named your backup


----------  Option 2: Install it as a user space service: ----------

This is recommended only for testing purposes or if it is not possible to install it as a
system service.

(3. Run the service in user space)

    You also can 'manually' start qtlicd(.exe) in non-daemon mode (as normal CLI app):
    - First, make sure the service is not running
    - From the command line, cd to the directory where the Qt License Service is installed,
      then give a command:
        3.1 > ./qtlicd --mode cli (Linux/Mac)
        3.2 > qtlicd.exe --mode cli (Windows)
    - You now can see immediately what happens in there. Stop like any other app with 'Ctrl-C'
    - Note that where ever you start qtlicd(.exe), it will always read settings from the
      installation folder.


----------  Option 3: Install it as an on-demand service: ----------

4. Copy the qtlicd executables to preferred destination folder

    4.1 $ cd deploy (created by the build_unix.sh)
        $ mkdir /home/<user>/qtlicd  # choose suitable destination folder for your
        $ cp qtlicd mocwrapper licheck qtlicensetool /home/<user>/qtlicd/

5. Copy the service configuration file qtlicd.ini in place for on-demand launch

    5.1 Linux:
        $ mkdir -p /home/<user>/.local/share/Qt/qtlicd
        $ cp qtlicd.ini /home/<user>/.local/share/Qt/qtlicd
    5.2 macOS:
        $ mkdir -p $HOME/Library/Application Support/Qt/qtlicd
        $ cp qtlicd.ini $HOME/Library/Application Support/Qt/qtlicd

6. Register the qtlicd executable for on-demand start

    6.1 $ cd /home/<user>/qtlicd/  # detination folder defined by you
        $ ./qtlicd --register <optional arg to specify the source, e.g. "admin" or "john">

    6.2 You can verify that the registration succeeded

        6.2.1 Linux:
            $ cat /home/<user>/.local/share/Qt/qtlicd/installations.ini

        6.2.2 macOS:
            $ cat $HOME/Library/Application Support/Qt/qtlicd/installations.ini

7 Accept Terms and Conditions of the service

    Use the qtlicensetool to accept the Terms and Conditions of the service.
    This is a prerequisite for enabling the license reservation functionality.

    7.1 To view and accept the Terms and Conditions from an interactive prompt:
        $ ./qtlicensetool --show-terms-and-conditions
    7.2 To accept the Terms and Conditions without interaction:
        $ ./qtlicensetool --accept-terms-and-conditions

8. To view logs:

     See the logs file: /home/<user>/<path-to-qtlicd-installation>/qtlicd_log.txt


----------  Enable license reservations for Qt5 and Qt6: ----------

4. Install MOC wrapper (Qt5 and Qt6)

   Because the installation script does not know your Qt installation paths, you must set up the
   MOC wrapper (Something like /home/user/Qt/6.3.1/gcc_64/) for each Qt install path you need the
   license check enabled for.

   4.1. Qt5: $ cd <your Qt installation path>/bin
        Qt6: $ cd <your Qt installation path>/libexec
   4.2. $ mv moc qtmoc   <--- Note: has to be exactly 'qtmoc'
   4.3.1. When using Qt License Service as a system service
        $ ln -s /opt/qtlicd/mocwrapper moc
   4.3.2. When using Qt License Service as an on-demand service
        $ ln -s /home/<user>/qtlicd/mocwrapper moc
   4.4. Try it by building any Qt-enabled app with any IDE or from the command line

5. Disable legacy license check (Qt5)

   In case you're using Qt5, you need to disable legacy support for its qmake licensing system by
   replacing the 'licheck' binary with the provided one:

   (Still in the <your Qt installation path>/bin):
   5.1. $ cp licheck licheck.bak   <--- Back up the original, you can name it freely
   5.2.1. When using Qt License Service as a system service
        $ ln -s /opt/qtlicd/licheck licheck
   5.2.2. When using Qt License Service as an on-demand service
        $ ln -s /home/<user>/qtlicd/licheck licheck
