#!/usr/bin/env bash
# Copyright (C) 2023 The Qt Company Ltd.
#
# SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
#

set -ex

function exit_fail {
    echo "ERROR! $1: Aborting installation"
    exit 1
}

serviceArtifactsFolder="${HOME}/work/license-tools/license-service/artifacts/service_artifacts"
cipArtifactsFolder="${HOME}/work/license-tools/license-service/artifacts/cip_artifacts"
precheckArtifactsFolder="${HOME}/work/license-tools/license-service/artifacts/precheck_artifacts"
corelibArtifactsFolder="${HOME}/work/license-tools/license-service/artifacts/corelib_artifacts"

mkdir -p ${serviceArtifactsFolder} || exit_fail "Failed to create Service artifacts folders"
mkdir -p ${cipArtifactsFolder} || exit_fail "Failed to create CIP API artifacts folders"
mkdir -p ${precheckArtifactsFolder} || exit_fail "Failed to create precheck API artifacts folders"
mkdir -p ${corelibArtifactsFolder} || exit_fail "Failed to create core library artifacts folders"

cp ${HOME}/work/license-tools/license-service/deploy/* ${serviceArtifactsFolder} || exit_fail "Failed to move deploy folder to artifacts"
cp ${HOME}/work/license-tools/license-service/build/bin/testapp ${serviceArtifactsFolder} || exit_fail "Failed to move testapp to artifacts"

cp ${HOME}/work/license-tools/license-service/build/lib/libqlicenseclient*.* ${cipArtifactsFolder} || exit_fail "Failed to move CIP API lib to artifacts"
cp ${HOME}/work/license-tools/license-service/build/include/licenseclient.h ${cipArtifactsFolder} || exit_fail "Failed to move CIP API header to artifacts"
cp ${HOME}/work/license-tools/license-service/build/include/licenseinfo.h ${cipArtifactsFolder} || exit_fail "Failed to move LicenseInfo header to artifacts"

cp ${HOME}/work/license-tools/license-service/build/lib/libqlicenseprecheck*.* ${precheckArtifactsFolder} || exit_fail "Failed to move precheck API lib to artifacts"
cp ${HOME}/work/license-tools/license-service/build/lib/libcurl.a ${precheckArtifactsFolder} || exit_fail "Failed to move curl lib to artifacts"
cp ${HOME}/work/license-tools/license-service/build/include/licenseprecheck.h ${precheckArtifactsFolder} || exit_fail "Failed to move precheck API header to artifacts"
cp ${HOME}/work/license-tools/license-service/build/include/licenseclient.h ${precheckArtifactsFolder} || exit_fail "Failed to move CIP API header to artifacts"
cp ${HOME}/work/license-tools/license-service/build/include/licenseinfo.h ${precheckArtifactsFolder} || exit_fail "Failed to move LicenseInfo header to artifacts"

cp ${HOME}/work/license-tools/license-service/build/lib/libqlicensecore*.* ${corelibArtifactsFolder} || exit_fail "Failed to move core lib to artifacts"
cp ${HOME}/work/license-tools/license-service/src/libs/qlicensecore/*.h ${corelibArtifactsFolder} || exit_fail "Failed to move core library headers to artifacts"
cp ${HOME}/work/license-tools/license-service/src/libs/3rdparty/nlohmann/json.hpp ${corelibArtifactsFolder} || exit_fail "Failed to move nlohmann header to artifacts"
cp ${HOME}/work/license-tools/license-service/build/include/commonsetup.h ${corelibArtifactsFolder} || exit_fail "Failed to move constants header to artifacts"
cp ${HOME}/work/license-tools/license-service/build/include/version.h ${corelibArtifactsFolder} || exit_fail "Failed to move version header to artifacts"

echo "List artifacts folder"
ls -la ${serviceArtifactsFolder}
echo "List CIP artifacts folder"
ls -la ${cipArtifactsFolder}
echo "List precheck artifacts folder"
ls -la ${precheckArtifactsFolder}
echo "List corelib artifacts folder"
ls -la ${corelibArtifactsFolder}
