/* Copyright (C) 2025 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "settingsoperation.h"

#include <abstractfactory.h>
#include <licdsetup.h>

namespace QLicenseService {

class SettingsOperationFactory
    : public QLicenseCore::AbstractFactory<SettingsOperation, std::string, QLicenseCore::LicdSetup &>
{
public:
    static SettingsOperationFactory &instance();

    template <typename T>
    void registerOperation(const std::string &key)
    {
        registerProduct<T>(key);
    }
    void registerDefaultCallback(const std::string &key, SettingsOperation::Callback callback);

    SettingsOperation *create(const std::string &key, QLicenseCore::LicdSetup &settings) const override;
    SettingsOperation::Callback defaultCallback(const std::string &key) const;

private:
    SettingsOperationFactory() = default;

private:
    std::unordered_map<std::string, SettingsOperation::Callback> m_callbacks;
};

} // nomespace QLicenseService
