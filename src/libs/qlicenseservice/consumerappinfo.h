/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "jsonhandler.h"

#include <string>

namespace QLicenseService {

struct ConsumerAppInfo
{
    ConsumerAppInfo() = default;
    ConsumerAppInfo(const nlohmann::json &jsonObject);
    ConsumerAppInfo(const std::string &consumerId, const std::string &consumerVersion,
        const std::string &consumerProcessId);

    nlohmann::json toJson() const;

    std::string id;
    std::string version;
    std::string installationSchema;
    std::string supportedFeatures;
    std::string processId;
    std::string buildTimestamp;

    bool operator==(const ConsumerAppInfo &other) const;
    bool operator!=(const ConsumerAppInfo &other) const;
};

} // namespace QLicenseService
