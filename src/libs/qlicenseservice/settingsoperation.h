/* Copyright (C) 2025 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <licdsetup.h>

#include <functional>
#include <string>

namespace QLicenseService {

class SettingsOperation
{
public:
    using Callback = std::function<bool(const std::string &value, std::string &error)>;

    SettingsOperation(QLicenseCore::LicdSetup &settings);
    virtual ~SettingsOperation() = 0;

    virtual bool validate(const std::string &value) const;
    virtual bool apply(const std::string &value, QLicenseCore::SettingsType type);
    virtual bool postProcess(Callback callback);

    std::string error() const;

protected:
    mutable std::string m_error;
    std::string m_key;

    QLicenseCore::LicdSetup *const m_settings;
};

class RequestTimeoutOperation : public SettingsOperation
{
public:
    RequestTimeoutOperation(QLicenseCore::LicdSetup &settings);

    bool validate(const std::string &value) const override;
};

class ServerAddressOperation : public SettingsOperation
{
public:
    ServerAddressOperation(QLicenseCore::LicdSetup &settings);
};

class TermsAndConditionsOperation : public SettingsOperation
{
public:
    TermsAndConditionsOperation(QLicenseCore::LicdSetup &settings);

    bool validate(const std::string &value) const override;
};

} // nomespace QLicenseService
