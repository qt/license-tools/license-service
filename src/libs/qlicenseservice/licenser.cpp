/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "licenser.h"

#include "commonsetup.h"
#include "clienthandler.h"
#include "cipcommandparser.h"
#include "licenseimport.h"
#include "version.h"
#include "settingsoperationfactory.h"

#include <constants.h>
#include <jsonhandler.h>
#include <utils.h>
#include <errors.h>
#include <pipe.h>
#include <logger.h>
#include <installationmanager.h>

#include <regex>

using namespace QLicenseCore;

namespace QLicenseService {

static std::string replaceOptionValue(const std::string &request, const std::string &option)
{
    std::string replaced = request;

    const size_t pos = request.find(option);
    if (pos != std::string::npos) {
        // Find the start of the value
        const size_t valueStart = request.find(' ', pos);
        if (valueStart != std::string::npos) {
            // Find the end of the value
            size_t valueEnd = request.find(' ', valueStart + 1);
            if (valueEnd == std::string::npos)
                valueEnd = request.length();

            // Replace the value with asterisks
            replaced.replace(valueStart + 1, valueEnd - valueStart - 1, "*****");
        }
    }

    return replaced;
}


Licenser::Licenser(DaemonRunMode runMode, uint16_t tcpPort, const std::string &workDir)
    : m_runMode(runMode)
    , m_state(DaemonState::Starting)
    , m_workDir(workDir)
    , m_cache(new LicenseCache(runMode == DaemonRunMode::Daemon
                                   ? LicenseCache::Context::System
                                   : LicenseCache::Context::User))
    , m_idleWatchdog(nullptr)
    , m_importWatchdog(nullptr)
    , m_finished(false)
    , m_termsAndConditionsAccepted(false)
{
    initSettings();
    initHttpClient(HttpSettings(m_settings));

    // Get server version as HTTP should be up now
    if ((m_state != DaemonState::Offline) && getServerVersionAndTime())
        logInfo("%s", m_serverVersion.c_str());

    // Initialize license cache
    if (!m_cache->init())
        throw Error("Failure while initializing license cache:" + m_cache->error());

    // Import local license files if present (when not running as system service)
    if (runMode != DaemonRunMode::Daemon && runMode != DaemonRunMode::TestMode) {
        const char* importPathEnv = std::getenv("QTLICD_LICENSE_IMPORT_PATH");
        m_licenseImportPath = importPathEnv ? importPathEnv : utils::getUserHomeDir();
        logInfo("Importing local licenses from: %s", m_licenseImportPath.c_str());

        if (importLocalLicenses()) {
            // init periodical reimport
            m_importWatchdog.reset(new Watchdog(std::chrono::seconds(IMPORT_INTERVAL_SECS)));
            m_importWatchdog->setCallback([this] {
                Pipe::globalObject()->write(PIPE_MSG_TIMEOUT, Pipe::Type::Watchdog, std::to_string(m_importWatchdog->id()));
            });
            m_importWatchdog->start();
        }
    }

    // Purge all hanging leftover reservations from server in case we have server connection.
    if (m_state == DaemonState::Online)
        releaseExpiredReservations();

    // Initialize periodical usage reporting
    if (runMode != DaemonRunMode::TestMode && m_state == DaemonState::Online) {
        int timeout = utils::strToInt(m_settings->get(sc_executionStatUploadInterval));
        timeout = (timeout <= 0) ? DEFAULT_USAGE_SEND_INTERVAL : timeout;

        m_usageStatisticsWatchdog.reset(new Watchdog(std::chrono::seconds(timeout)));
        m_usageStatisticsWatchdog->setCallback([this] {
            Pipe::globalObject()->write(PIPE_MSG_UPLOAD_USAGE, Pipe::Type::Watchdog,
                std::to_string(m_usageStatisticsWatchdog->id()));
        });
        m_usageStatisticsWatchdog->start();
        sendUsageStatistics();
    }

    // Start the TCP/IP server
    if (tcpPort == 0) {
        tcpPort = utils::strToInt(m_settings->get(sc_tcpListeningPort));
    }
    m_tcpServer = new TcpServer();
    if (!m_tcpServer->init(tcpPort)) {
        logError("Error while initializing TCP server in port: %d", tcpPort);
        throw Error("Error while initializing TCP server in port: "
            + std::to_string(tcpPort));
    }

    m_tcpServer->start();
    logDebug("Starting Daemon TCP server");

    initServiceLockFile();
    initPortFile(tcpPort);

    // Start the watchdog timer in on-demand mode. The service is automatically
    // shut down on timeout between external events
    if (m_runMode == DaemonRunMode::OnDemand) {
        int timeout = utils::strToInt(m_settings->get(sc_onDemandShutdownTimeout));
        timeout = (timeout <= 0) ? DEFAULT_SHUTDOWN_TIMEOUT : timeout;

        m_idleWatchdog.reset(new Watchdog(std::chrono::seconds(timeout)));
        m_idleWatchdog->setCallback([this] {
            Pipe::globalObject()->write(PIPE_MSG_TIMEOUT, Pipe::Type::Watchdog, std::to_string(m_idleWatchdog->id()));
        });
        m_idleWatchdog->start();
    }

    registerSettingsOperations();
}

Licenser::~Licenser()
{
    if (m_runMode == DaemonRunMode::OnDemand || m_runMode == DaemonRunMode::CLIMode) {
        // Purge non-existent service installations from installations.ini for on-demand start.
        // The user may have removed installations without unregistering the service installation(s)
        // so let's try to keep the config file up-to-date.
        InstallationManager().purgeObsoleteInstallations();
    }

    // Remove port number information
    utils::deleteFile(m_portFilePath);

    m_tcpServer->stop();
    delete(m_settings);
    delete(m_tcpServer);
    delete(m_http);
}

bool Licenser::listen()
{
    // Reset the shutdown timeout
    if (m_idleWatchdog && !m_clients.empty())
        m_idleWatchdog->reset();

    if ((Pipe::globalObject()->queuedMessagesCount() == 0)
            && !Pipe::globalObject()->waitForReadyRead(1)) {
        return true; // No data yet in this cycle
    }

    Pipe::Type pipeType;
    std::string pipeId;
    const std::string message = Pipe::globalObject()->read(&pipeType, &pipeId);

    if (message.empty()) {
        logWarn("No data to read from pipe");
        return false;
    }

    switch (pipeType) {
    case Pipe::Type::TcpServer:
        return onTcpReadyRead(utils::strToInt(pipeId));
    case Pipe::Type::HttpClient:
        return onHttpReadyRead(utils::strToInt(pipeId));
    case Pipe::Type::Watchdog:
        return onTimeoutEvent(pipeId, message);
    default:
        return false; // Unknown sender
    }
}

void Licenser::shutdown()
{
    logInfo("Shutting down...");
    m_state = DaemonState::Stopped;

    m_idleWatchdog.reset(nullptr);
    m_importWatchdog.reset(nullptr);
    m_usageStatisticsWatchdog.reset(nullptr);

    // Copy the keys as the elements get removed from map
    std::vector<uint16_t> clients;
    for (auto& client : m_clients)
        clients.push_back(client.first);

    // Release all clients
    for (auto &client : clients)
        onTcpDisconnected(client);
}

bool Licenser::isFinished() const
{
    return m_finished;
}

bool Licenser::hasPendingRequests() const
{
    // Check for any pending Http requests on shutdown
    return !m_httpCallbacks.empty();
}

/*
    Returns the state of the license service.
*/
Licenser::DaemonState Licenser::state() const
{
    return m_state;
}

bool Licenser::onTcpReadyRead(uint16_t socketId)
{
    // ignore new TCP messages on shutdown
    if (m_state == DaemonState::Stopped)
        return false;

    ServiceResponse response;
    std::string input = m_tcpServer->receive(socketId);
    utils::trimStr(input);
    if (input.empty()) {
        response.setServiceResponse(false, statusString(Status::BAD_CLIENT_REQUEST),
            static_cast<int>(Status::BAD_CLIENT_REQUEST));
        sendTcpResponse(socketId, response);
        return false;
    }

    if (input == SOCKET_CLOSED_MSG) {
        onTcpDisconnected(socketId);
        return true;
    }

    if (Logger::getInstance()->logLevel() >= LogLevel::LOGLEVEL_DEBUG)
        logDebug("Got a request: %s", replaceOptionValue(input, "-jwt").c_str());

    CipCommandParser parser(input, DAEMON_VERSION);
    if (!parser.parseInput()) {
        logError("Request parsing failed: %s", parser.error().c_str());
        response.setServiceResponse(false, statusString(Status::BAD_CLIENT_REQUEST),
            static_cast<int>(Status::BAD_CLIENT_REQUEST));
        sendTcpResponse(socketId, response);
        return false;
    }

    const Status versionStatus = isCompatibleCIPVersion(parser.clientLibraryVersion());
    if (versionStatus != Status::SUCCESS) {
        response.setServiceResponse(false, statusString(versionStatus),
            static_cast<int>(versionStatus));
        sendTcpResponse(socketId, response);
        return false;
    }

    RequestInfo request;
    request.socketId = socketId;
    request.userId = parser.userId();

    if (parser.command() == LICENSE_REQUEST_CMD)
        return processLicenseRequest(request, input);
    else
        return processOperationRequest(request, parser);
}

bool Licenser::processReservation(ClientHandler *client, uint32_t delay)
{
    uint16_t socketId = client->socketId();
    if (m_state == DaemonState::Offline) {
        // No need to bother with the request if we are in offline mode
        const ServiceResponse resp = ServiceResponse(false,
            statusString(Status::BAD_CONNECTION), static_cast<int>(Status::BAD_CONNECTION));

        sendTcpResponse(socketId, resp);
        return false;
    }

    if (!m_termsAndConditionsAccepted) {
        const ServiceResponse resp = ServiceResponse(false,
            statusString(Status::TERMS_AND_CONDITIONS_NOT_ACCEPTED),
            static_cast<int>(Status::TERMS_AND_CONDITIONS_NOT_ACCEPTED));
        sendTcpResponse(socketId, resp);
        logWarn("Rejected license request for client ID %d due to not being able "
            "to verify user acceptance of terms and conditions of the service.", socketId);
        return false;
    }

    auto handleLicenseResponse = [this, socketId](uint16_t requestId) {
        HttpResponse reply;
        Status httpResult = m_http->receive(requestId, reply);
        Status parseResult = Status::UNKNOWN_ERROR;

        ClientHandler *client = getClient(socketId);
        if (!client) {
            // Client disconnected before getting reply
            logWarn("No client stored with id: %d", requestId);
            return false;
        }

        // No further actions on shutdown
        if (m_state == DaemonState::Stopped)
            return false;

        ServiceResponse response;
        if (httpResult != Status::SUCCESS) {
            response.setServiceResponse(false, statusString(httpResult), static_cast<int>(httpResult));
            logWarn("License request for client id %d failed: %s",
                    socketId, statusString(httpResult).c_str());
        } else if (serverIsBusy(reply)) {
            const uint64_t retryDelay = m_retryCounter->nextRetryDelay();
            logWarn("Server is busy, retrying license request for client id %d after %d ms",
                    socketId, retryDelay);
            // No reply to client yet
            processReservation(client, retryDelay);
            return false;
        } else {
            parseResult = client->parseResponse(reply.body);
            if (parseResult == Status::SUCCESS) {
                response.setServiceResponse(true, statusString(parseResult), static_cast<int>(parseResult));
                response.setLicenseInfo(reply.body);
                const std::string licenseId = client->license()->id();
                logInfo("Granted new license. License id: %s, user: \"%s\", reservation id: %s, client id: %d",
                        client->license()->id().c_str(), client->request().userId.c_str(),
                        client->reservationId().c_str(), socketId);
            } else {
                response.setServiceResponse(false, statusString(parseResult), static_cast<int>(parseResult));
                logWarn("Rejected license request for client id %d: \"%s\"",
                        socketId, statusString(parseResult).c_str());
            }
        }
        m_retryCounter->reset();

        // Respond back to client
        sendTcpResponse(socketId, response);

        return (parseResult == Status::SUCCESS);
    };

    const uint16_t requestId = utils::generateUniqueShortId();
    if (!registerHttpCallback(requestId, handleLicenseResponse))
        return -1;

    logDebug("Initiating a request to the server, clientId %d", socketId);
    sendHttpRequest(client->request(), requestId, delay);
    return 0;
}

void Licenser::processRelease(Reservation &reservation, uint32_t delay)
{
    const bool isRetry = (delay > 0);
    if (!isRetry) {
        // Prevent sending release request from multiple trigger sources. This can happen if
        // release is triggered for a process based floating license due to client disconnection,
        // but the server is in high load and we don't get a response before the lease time for
        // the reservation is expired, which would trigger another release request.
        if (reservation.isPendingForRelease())
            return;

        // Reservation status of clients is reset immediately when the reservation expires,
        // as the release request may not succeed in a timely manner
        resetClientsForReservation(&reservation);
        reservation.setPendingForRelease();
    }

    const std::string reservationId = reservation.id();
    if (m_state == DaemonState::Offline) {
        // No need to bother with the request if we already know there's no connection
        logDebug("Skipping releasing reservation with id %s due to no server connection",
            reservationId.c_str());
        return;
    }

    if (!m_termsAndConditionsAccepted) {
        logWarn("Skipping releasing reservation with id %s due to not being able "
            "to verify user acceptance of terms and conditions of the service.",
            reservationId.c_str());
        return;
    }

    logDebug("Releasing reservation with id %s", reservationId.c_str());

    auto handleReleaseResponse = [this, reservationId](uint16_t requestId) {
        Reservation *reservation = m_cache->reservation(reservationId);
        if (!reservation) {
            logWarn("No reservation to be released stored with id: %s", reservationId.c_str());
            return false;
        }

        HttpResponse reply;
        Status result = m_http->receive(requestId, reply);
        if (result != Status::SUCCESS) {
            logWarn("Failed to release reservation id %s: %s",
                    reservationId.c_str(), statusString(result).c_str());
            // Something went wrong with the request, keep the reservation as we
            // can try to remove it during next startup
            return false;
        }

        if (serverIsBusy(reply)) {
            const uint64_t retryDelay = m_retryCounter->nextRetryDelay();
            logWarn("Server is busy, retrying release of reservation id %s after %d ms",
                    reservationId.c_str(), retryDelay);
            processRelease(*reservation, retryDelay);
            return false;
        } else {
            m_retryCounter->reset();
        }

        result = preParseResponse(reply.body);
        if (result != Status::SUCCESS) {
            // Server didn't accept release request, but remove the reservation locally
            logWarn("Could not release reservation id: %s: %s Removing from cache.", reservationId.c_str(),
                statusString(result).c_str()); // statusString ends in period

            removeReservation(*reservation);
            return false;
        }

        logInfo("Released reservation with ID %s", reservationId.c_str());
        removeReservation(*reservation);

        return true;
    };

    const RequestInfo request = prepareRequest(reservation, sc_reservationReleaseAccessPoint);
    if (request.payload.empty()) {
        logError("Could not construct request payload for releasing reservation: %s for license :%s",
            reservationId.c_str(), reservation.license()->id().c_str());
        return;
    }

    const uint16_t requestId = utils::generateUniqueShortId();
    if (!registerHttpCallback(requestId, handleReleaseResponse))
        return;

    sendHttpRequest(request, requestId, delay);
}

void Licenser::processRenewal(Reservation &reservation, uint32_t delay)
{
    const std::string reservationId = reservation.id();
    if (reservation.isPendingForRelease()) {
        logDebug("Skipping renewal of reservation with id %s due to being marked for release",
            reservationId.c_str());
        return;
    }
    if (m_state == DaemonState::Offline) {
        // No need to bother with the request if we already know there's no connection
        logDebug("Skipping renewal of reservation with id %s due to no server connection",
            reservationId.c_str());
        return;
    }

    if (!m_termsAndConditionsAccepted) {
        logWarn("Skipping renewal of reservation with id %s due to not being able "
            "to verify user acceptance of terms and conditions of the service.",
            reservationId.c_str());
        return;
    }

    logDebug("Renewing reservation with id %s", reservationId.c_str());

    auto handleRenewalResponse = [this, reservationId](uint16_t requestId) {
        Reservation *reservation = m_cache->reservation(reservationId);
        if (!reservation) {
            logWarn("No reservation to be renewed stored with id: %s", reservationId.c_str());
            return false;
        }

        HttpResponse reply;
        Status result = m_http->receive(requestId, reply);
        if (result != Status::SUCCESS) {
            logWarn("Failed to renew reservation id %s: %s Retrying at later time.",
                    reservationId.c_str(), statusString(result).c_str());
            reservation->resetRenewAndExpiryTimers();
            return false; // Something went wrong with the request
        }

        if (serverIsBusy(reply)) {
            const uint64_t retryDelay = m_retryCounter->nextRetryDelay();
            logWarn("Server is busy, retrying renewal of reservation id %s after %d ms",
                    reservationId.c_str(), retryDelay);
            processRenewal(*reservation, retryDelay);
            return false;
        } else {
            m_retryCounter->reset();
        }

        result = preParseResponse(reply.body);
        if (result == Status::SUCCESS) {
            // Test that the renewed data is valid before updating the existing cached reservation
            std::unique_ptr<Reservation> tmpReservation(new Reservation(reservation->license()));
            result = tmpReservation->parseFromJson(reply.body);
        }

        if (result != Status::SUCCESS) {
            // Something was wrong with the renewed license. Remove existing reservation
            // and perform a new license request for all current consumers
            logWarn("Could not renew reservation id: %s: %s", reservationId.c_str(),
                statusString(result).c_str());

            resetClientsForReservation(reservation);
            removeReservation(*reservation);
            return false;
        }

        // valid response data, update the existing reservation
        reservation->parseFromJson(reply.body);
        reservation->resetRenewAndExpiryTimers();
        m_cache->flush(reservation);

        logInfo("Reservation renewed for reservation id: %s", reservationId.c_str());
        return true;
    };

    const RequestInfo request = prepareRequest(reservation, sc_reservationRenewalAccessPoint);
    if (request.payload.empty()) {
        logError("Could not construct request payload for renewing reservation: %s for license :%s",
            reservationId.c_str(), reservation.license()->id().c_str());
        return;
    }

    const uint16_t requestId = utils::generateUniqueShortId();
    if (!registerHttpCallback(requestId, handleRenewalResponse))
        return;

    sendHttpRequest(request, requestId, delay);
}

void Licenser::processUsageStatistics(Reservation &reservation, uint32_t delay)
{
    const std::string reservationId = reservation.id();
    if (reservation.statistics().empty()) {
        logDebug("No pending usage statistics for reservation %s", reservationId.c_str());
        return;
    }
    if (reservation.isPendingForRelease()) {
        logDebug("Skipping sending usage statistics of reservation with ID %s "
            "due to being marked for release.", reservationId.c_str());
        return;
    }
    if (m_state == DaemonState::Offline) {
        // No need to bother with the request if we already know there's no connection
        logDebug("Skipping sending usage statistics of reservation with ID %s "
            "due to no server connection.", reservationId.c_str());
        return;
    }

    if (!m_termsAndConditionsAccepted) {
        logWarn("Skipping sending usage statistics of reservation with ID %s due to not being able "
            "to verify user acceptance of terms and conditions of the service.",
            reservationId.c_str());
        return;
    }

    logDebug("Sending usage statistics for reservation with ID %s", reservationId.c_str());

    auto handleStatisticsResponse = [this, reservationId](uint16_t requestId) {
        Reservation *reservation = m_cache->reservation(reservationId);
        if (!reservation) {
            logWarn("No reservation for usage statistics stored with ID: %s", reservationId.c_str());
            return false;
        }

        HttpResponse response;
        Status result = m_http->receive(requestId, response);
        constexpr int sc_statusCodeNotFound(404);
        if (response.code == sc_statusCodeNotFound) {
            // We need to resort to a hackish solution to detect a non-existent route
            // in omission of a proper API discovery service
            logWarn("The endpoint for sending usage statistics does not exist. Skipping further requests...");
            if (m_usageStatisticsWatchdog)
                m_usageStatisticsWatchdog->stop();
            return false;
        }

        if (result != Status::SUCCESS) {
            logWarn("Failed to send usage statistics for reservation ID %s: %s.",
                reservationId.c_str(), statusString(result).c_str());
            return false;
        }

        if (serverIsBusy(response)) {
            const uint64_t retryDelay = m_retryCounter->nextRetryDelay();
            logWarn("Server is busy. Retrying sending usage statistics of reservation ID %s after %d ms",
                reservationId.c_str(), retryDelay);
            processUsageStatistics(*reservation, retryDelay);
            return false;
        } else {
            m_retryCounter->reset();
        }

        result = preParseResponse(response.body);
        if (result != Status::SUCCESS) {
            logWarn("Could not send usage statistics of reservation ID: %s: %s",
                reservationId.c_str(), statusString(result).c_str());

            return false;
        }

        logInfo("Sent usage statistics of reservation with ID %s", reservationId.c_str());
        return true;
    };

    const RequestInfo request = prepareRequest(reservation, sc_usageAccessPoint);
    if (request.payload.empty()) {
        logError("Could not construct request payload for sending usage statistics "
            "of reservation: %s for license :%s", reservationId.c_str(),
            reservation.license()->id().c_str());
        return;
    }

    const uint16_t requestId = utils::generateUniqueShortId();
    if (!registerHttpCallback(requestId, handleStatisticsResponse))
        return;

    sendHttpRequest(request, requestId, delay);
}

bool Licenser::processOperationRequest(const RequestInfo &request, const CipCommandParser &parser)
{
    ServiceResponse response;

    if (parser.command() == DAEMON_VERSION_CMD) {
        response.setServiceResponse(true, getDaemonVersion(), static_cast<int>(Status::SUCCESS));
    } else if (parser.command() == RESERVATION_QUERY_CMD) {
        response.setServiceResponse(true, getReservations(request.userId), static_cast<int>(Status::SUCCESS));
    } else if (parser.command() == CLEAR_CACHE_CMD) {
        std::vector<License *> licenses = m_cache->licenses(request.userId);
        uint64_t count = 0;
        for (auto *license : licenses) {
            for (auto *reservation : license->reservations()) {
                if (m_state == DaemonState::Offline || !reservation->isReleasable()) {
                    disconnectClientsForReservation(reservation);
                    removeReservation(*reservation);
                } else {
                    // If there's network connectivity, we can attempt releasing the reservation from server
                    processRelease(*reservation);
                }
                count++;
            }
        }
        std::stringstream ss;
        if (count > 0)
            ss << "Removed " << count << " reservation(s) for user " << request.userId;
        else
            ss << "No reservations to remove for user " << request.userId;

        response.setServiceResponse(true, ss.str(), static_cast<int>(Status::SUCCESS));
    } else if (parser.command() == CURRENT_RESERVATION_CMD) {
        // Kept for 3.0.x compatibility. Since 3.1, the client API will make a periodical
        // license request which may return immediately for valid existing reservation,
        // or request a new one in case the existing reservation was revoked
        ClientHandler *client = getClient(request.socketId);
        if (!client) {
            response.setServiceResponse(false, statusString(Status::BAD_CLIENT_REQUEST),
                static_cast<int>(Status::BAD_CLIENT_REQUEST));
            sendTcpResponse(request.socketId, response);
            return false;
        }
        License *license = client->license();
        if (!license) {
            response.setServiceResponse(false, statusString(Status::LICENSE_REJECTED),
                static_cast<int>(Status::LICENSE_REJECTED));
            sendTcpResponse(request.socketId, response);
            return false;
        }
        std::string reservationInfo;
        Reservation *reservation = client->findCachedReservation(reservationInfo, state());
        if (!reservation) {
            response.setServiceResponse(false, statusString(Status::LICENSE_REJECTED), static_cast<int>(Status::LICENSE_REJECTED));
            sendTcpResponse(request.socketId, response);
            return false;
        }
        response.setServiceResponse(true, statusString(Status::SUCCESS), static_cast<int>(Status::SUCCESS));
        response.setLicenseInfo(reservationInfo);
    } else if (parser.command() == SERVER_VERSION_CMD) {
        getServerVersionAndTime();
        response = ServiceResponse(true, m_serverVersion, static_cast<int>(Status::SUCCESS));
    } else if (parser.command() == SET_SERVICE_SETTING_CMD) {
        const std::string key = parser.settingsKey();
        const std::string value = parser.settingsValue();

        std::string message;
        if (setServiceSettingByKey(key, value, parser.settingsType(), message)) {
            message = "Service setting has been successfully updated for key: '"
                + key + "' with value: " + value;
            response.setServiceResponse(true, message, static_cast<int>(Status::SUCCESS));
        } else {
            response.setServiceResponse(false, message, static_cast<int>(Status::SETTINGS_ERROR));
            logError("%s", message.c_str());
        }
    } else if (parser.command() == GET_SERVICE_SETTING_CMD) {
        const std::string key = parser.settingsKey();
        std::string value;
        if (getServiceSettingByKey(key, value)) {
            response.setServiceResponse(true, value, static_cast<int>(Status::SUCCESS));
        } else {
            response.setServiceResponse(false, "Could not retrieve service settings for key: " + key,
                static_cast<int>(Status::SETTINGS_ERROR));
        }
    } else {
        response.setServiceResponse(false, "Unknown operation requested", static_cast<int>(Status::BAD_REQUEST));
        sendTcpResponse(request.socketId, response);
        return false;
    }

    sendTcpResponse(request.socketId, response);
    return true;
}

/*
    Parses the status code from the server \a response. Returns
    Status::SUCCESS on success, otherwise a result indicating the failure.
*/
Status Licenser::preParseResponse(const std::string &response) const
{
    JsonHandler json(response);

    std::string code;
    if (!json.get("code", code) || code.empty())
        return Status::INVALID_RESPONSE;

    return ServerResponseCode::codeToStatus(code);
}

bool Licenser::storeClient(ClientHandler *client)
{
    const uint16_t clientId = client->socketId();
    if (clientInStorage(clientId)) {
        logWarn("Client with a same id already exists in cache: %d", clientId);
        return false;
    }
    logDebug("Storing client in cache");
    m_clients.emplace(clientId, std::move(std::unique_ptr<ClientHandler>(client)));

    return true;
}

void Licenser::sendHttpRequest(const RequestInfo &request, uint16_t requestId, uint32_t delay)
{
    std::string auth;
    if (!request.jwt.empty()) {
        auth += "Bearer ";
        auth += request.jwt;
    }

    // Send the request
    m_http->send(requestId, request.payload, request.accessPoint, request.serverAddr, auth, delay);
}

void Licenser::sendTcpResponse(uint16_t socketId, const ServiceResponse &response)
{
    const std::string responseString = response.toString();
    if (m_tcpServer->sendResponse(socketId, (responseString + "\n")) == 0)
        logDebug("Replied to socket %d: %s", socketId, responseString.c_str());
}

bool Licenser::onHttpReadyRead(uint16_t requestId)
{
    auto it = m_httpCallbacks.find(requestId);
    if (it == m_httpCallbacks.end()) {
        logWarn("No callback registered with id: %d", requestId);
        return false;
    }
    // Call the callback
    const int success = it->second(requestId);
    if (!removeHttpCallback(requestId))
        return false;

    return success;
}

bool Licenser::onTimeoutEvent(const std::string &eventId, const std::string &message)
{
    if (m_importWatchdog && (eventId == std::to_string(m_importWatchdog->id()))) {
        importLocalLicenses();
        m_importWatchdog->restart();
    } else if (m_idleWatchdog && (eventId == std::to_string(m_idleWatchdog->id()))) {
        if (!m_clients.empty()) {
            // Could be that the host was resumed from sleep, and the timeout
            // was triggered before the watchdog was reset in the event queue
            m_idleWatchdog->restart();
        } else {
            logInfo("Idle timeout for on-demand service reached");
            m_finished = true;
        }
    } else if (message == PIPE_MSG_LICENSE_EXPIRED) {
        // eventId is the reservationId of the tracked reservation
        License *license = m_cache->licenseByReservation(eventId);
        if (!license) {
            logInfo("License expiry triggered, but no such license found with reservation id: %s", eventId.c_str());
            return false;
        }
        Reservation *reservation = license->reservation(eventId);
        if (!reservation) {
            logInfo("License expiry triggered, but no such reservation found with id: %s", eventId.c_str());
            return false;
        }

        processRelease(*reservation);
    } else if (message == PIPE_MSG_LICENSE_RENEWAL) {
        // eventId is the reservationId of the tracked reservation
        License *license = m_cache->licenseByReservation(eventId);
        if (!license) {
            logInfo("License renewal triggered, but no such license found with reservation id: %s", eventId.c_str());
            return false;
        }
        Reservation *reservation = license->reservation(eventId);
        if (!reservation) {
            logInfo("License renewal triggered, but no such reservation found with id: %s", eventId.c_str());
            return false;
        }

        if (reservation->clients().size() > 0)
            processRenewal(*reservation);
    } else if (m_usageStatisticsWatchdog && (message == PIPE_MSG_UPLOAD_USAGE)) {
        sendUsageStatistics();
        m_usageStatisticsWatchdog->restart();
    }

    return true;
}

bool Licenser::registerHttpCallback(uint16_t requestId, HttpCallback callback)
{
    auto it = m_httpCallbacks.find(requestId);
    if (it != m_httpCallbacks.end()) {
        logWarn("Callback already registered with same id: %d", requestId);
        return false;
    }

    m_httpCallbacks.insert(std::make_pair<const uint16_t &, HttpCallback &>
        (requestId, callback));

    return true;
}

bool Licenser::removeHttpCallback(uint16_t requestId)
{
    auto it = m_httpCallbacks.find(requestId);
    if (it == m_httpCallbacks.end()) {
        logWarn("No callback registered with id: %d", requestId);
        return false;
    }

    m_httpCallbacks.erase(it);
    return true;
}

bool Licenser::processLicenseRequest(const RequestInfo &request, const std::string &input)
{
    ServiceResponse response;

    if (!m_termsAndConditionsAccepted) {
        response.setServiceResponse(false, statusString(Status::TERMS_AND_CONDITIONS_NOT_ACCEPTED),
            static_cast<int>(Status::TERMS_AND_CONDITIONS_NOT_ACCEPTED));
        sendTcpResponse(request.socketId, response);
        logWarn("Rejected license request for client ID %d due to not being able "
            "to verify user acceptance of terms and conditions of the service.", request.socketId);
        return false;
    }

    std::string timeError;
    if (!m_timeChecker.allowedTimeDelta(timeError)) {
        response.setServiceResponse(false, timeError, static_cast<int>(Status::TIME_MISMATCH));
        sendTcpResponse(request.socketId, response);
        return false;
    }

    // Find an existing client handler or create new one
    ClientHandler *client = getClient(request.socketId);
    std::unique_ptr<ClientHandler> newClient;

    if (!client) {
        newClient.reset(new ClientHandler(request, *m_settings, *m_cache.get()));
        client = newClient.get();
    }

    std::string parseError;
    const Status parseStatus = client->parseRequest(input, parseError);
    if (parseStatus != Status::SUCCESS) {
        logError("Request parsing failed: %s", parseError.c_str());
        response.setServiceResponse(false, parseError, static_cast<int>(parseStatus));
        sendTcpResponse(request.socketId, response);
        return false;
    }

    if (newClient) {
        client = newClient.release();
        storeClient(client);
    }

    // Otherwise it's a license reservation request, check if we can skip contacting server
    std::string reservationInfo;
    if (Reservation *reservation = client->findCachedReservation(reservationInfo, state())) {
        logInfo("Granted cached license. License id: %s, user: \"%s\", reservation id: %s, client id: %d",
                client->license()->id().c_str(), client->request().userId.c_str(),
                client->reservationId().c_str(), request.socketId);

        response.setServiceResponse(true, statusString(Status::SUCCESS), static_cast<int>(Status::SUCCESS));
        response.setLicenseInfo(reservationInfo);
        sendTcpResponse(request.socketId, response);

        // If the current client is the only consumer for reservation, check if
        // there was a missed renew request since the previous client disconnect.
        if ((reservation->clients().size() == 1)
                && reservation->isRenewable() && reservation->isRenewDue()) {
            processRenewal(*reservation);
        }
        return true;
    }

    // We need to perform the full request.
    return processReservation(client);
}

std::string Licenser::getReservations(const std::string &user)
{
    std::vector<License *> licenses = m_cache->licenses(user);

    std::stringstream reply;
    uint32_t count = 0;
    reply << "Current reservations: \n";
    for (auto *license : licenses) {
        reply << "\n--- License ID " << license->id() << " ---\n";
        reply << " User ID: " << license->localUser() << std::endl;
        reply << " Valid to date: " << license->validToDateString() << std::endl;
        for (auto *reservation : license->reservations()) {
            reply << " --- Reservation ID: " << reservation->id() << " ---\n";
            reply << "  Valid from: " << reservation->validFromUtcString() << std::endl;
            reply << "  Valid to: " << reservation->validToUtcString() << std::endl;
            ++count;
        }
    }

    if (count == 0)
        return std::string("No reservations for user: \"" + user + "\"");

    return reply.str();
}


void Licenser::onTcpDisconnected(uint16_t socketId)
{
    logDebug("Client disconnected, socket id %d", socketId);

    ClientHandler *client = getClient(socketId);
    if (client == nullptr)
        return;

    bool release = false;
    Reservation *reservation = nullptr;

    if (License *license = client->license()) {
        if ((reservation = license->reservation(client->reservationId()))) {
            // Check if the license model dictates release on client disconnection
            release = reservation->requiresReleaseOnDisconnect();
        }
    }

    removeClient(socketId);

    if (release && reservation)
        processRelease(*reservation);
}

void Licenser::removeClient(uint16_t clientId)
{
    auto it = m_clients.find(clientId);
    if (it == m_clients.end()) {
        logDebug("No client stored with id: %d", clientId);
        return;
    }
    m_clients.erase(clientId);
}

bool Licenser::clientInStorage(uint16_t socketId) const
{
    for (const auto &client : m_clients) {
        if (client.first == socketId) {
            return true;
        }
    }
    return false;
}

ClientHandler *Licenser::getClient(uint16_t clientId)
{
    // Find and return client pointer from the main cache (excluding children)
    auto it = m_clients.find(clientId);
    if (it == m_clients.end())
        return nullptr;

    return it->second.get();
}

void Licenser::disconnectClientsForReservation(Reservation *reservation)
{
    for (const auto &client : m_clients) {
        if (client.second->reservationId() == reservation->id()) {
            logDebug("Disconnecting client %d for reservation %s", client.first, reservation->id().c_str());
            client.second->revokeCurrentReservation();
            m_tcpServer->closeClientSocket(client.second->socketId());
        }
    }
}

void Licenser::resetClientsForReservation(Reservation *reservation)
{
    for (const auto &client : m_clients) {
        if (client.second->reservationId() == reservation->id()) {
            logDebug("Removing reservation %s from client %d", reservation->id().c_str(), client.first);
            client.second->revokeCurrentReservation();
        }
    }
}

std::string Licenser::getDaemonVersion()
{
    return std::string(DAEMON_VERSION_DESC) + DAEMON_VERSION;
}

bool Licenser::getServiceSettingByKey(const std::string &key, std::string &value)
{
    if (!key.empty()) {
        if (m_settings->contains(key)) {
            value = m_settings->get(key);
            return true;
        }
    }
    return false;
}

bool Licenser::setServiceSettingByKey(const std::string &key, const std::string &value,
    QLicenseCore::SettingsType type, std::string &error)
{
    auto *operation = SettingsOperationFactory::instance().create(key, *m_settings);
    if (!operation) {
        error = "Settings change is not supported for key: '" + key + "'";
        return false;
    }

    if (!operation->validate(value)) {
        error = "Invalid value '" + value + "' for setting '"
            + key + "': " + operation->error();
        return false;
    }

    if (!operation->apply(value, type)) {
        error = "Could not apply value '" + value + "' for setting '"
            + key + "': " + operation->error();
        return false;
    }

    if (!operation->postProcess(SettingsOperationFactory::instance().defaultCallback(key))) {
        error = "Could not perform post processing after changing setting '"
            + key + "': " + operation->error();
        return false;
    }

    logDebug("Set service settings for key: %s with value: %s", key.c_str(), value.c_str());
    return true;
}

bool Licenser::getServerVersionAndTime()
{
    if (m_runMode == DaemonRunMode::TestMode) // Disable for (unit) tests
        return true;

    m_timeChecker.reset();

    std::string accessPoint = m_settings->get(sc_versionQueryAccessPoint);
    std::string serverAddr = m_settings->get(sc_serverAddr);
    std::string payload;
    HttpResponse reply; // server reply JSON
    m_serverVersion = "Server version not available";

    logInfo("Fetching the License Server version");

    if (!m_http->sendReceive(reply, payload, accessPoint, serverAddr)) {
        logError("Cannot connect to server");
        return false;
    }

    auto it = reply.headers.find("Date");
    if (it != reply.headers.end()) {
        const std::string serverTime = it->second;
        const time_t nowUtc = utils::utcTime();

        logInfo("Server UTC time: %s", serverTime.c_str());
        logInfo("Local UTC time: %s", utils::utcTimeToString(nowUtc).c_str());
    }

    m_state = DaemonState::Online;

    std::string version;
    JsonHandler data(reply.body);
    if (data.get("version", version) && !version.empty()) {
        m_serverVersion = SERVER_VERSION_DESC + version;
    } else {
        logError("Failed to parse server version");
        return false;
    }
    return true;
}

bool Licenser::updateRequestTimeout(const uint64_t timeoutSecs)
{
    m_http->setRequestTimeout(timeoutSecs);
    return true;
}

void Licenser::removeReservation(Reservation &reservation)
{
    const std::string reservationId = reservation.id();

    License *license = reservation.license();
    license->removeReservation(reservationId);

    if (license->reservations().empty()) {
        // No more reservations for this license, remove from cache
        if (!m_cache->remove(license->localUser(), license->id()))
            logWarn(m_cache->error().c_str());
    } else {
        // Remove reservation data from cache
        if (!m_cache->remove(license->localUser(), license->id(), reservationId))
            logWarn(m_cache->error().c_str());
    }
}

void Licenser::releaseExpiredReservations()
{
    if (m_runMode == DaemonRunMode::TestMode) // Disable for unit tests
        return;

    std::set<Reservation *> reservations = m_cache->reservations();
    if (reservations.empty()) {
        logDebug("No leftover reservations found, no release needed");
        return;
    }

    uint16_t count = 0;
    for (auto *res : reservations) {
        if (!res->isLeaseExpired() && (!res->license()->isExpired() || res->license()->isPerpetual()))
            continue;

        if (!res->isReleasable())
            continue;

        processRelease(*res);
        count++;
    }

    logInfo("Found %d expired reservations to be released", count);
}

void Licenser::sendUsageStatistics()
{
    if (m_runMode == DaemonRunMode::TestMode)
        return;

    logDebug("Looking for usage statistics to upload...");
    std::set<Reservation *> reservations = m_cache->reservations();
    if (reservations.empty())
        return;

    for (auto *res : reservations) {
        if (res->isLocalOnly())
            continue;

        processUsageStatistics(*res);
    }
}

RequestInfo Licenser::prepareRequest(Reservation &reservation, const std::string &accessPoint) const
{
    try {
        nlohmann::json json;
        json["service_version"] = DAEMON_VERSION;
        json["reservation_id"] = reservation.id();
        json["user_id"] = reservation.license()->localUser();
        json["hw_id"] = m_settings->get(sc_hwId);

        buildStatisticsJson(reservation, json);
        if (accessPoint == sc_reservationRenewalAccessPoint)
            buildConsumersJson(reservation, json);

        RequestInfo request;
        request.jwt = reservation.license()->jwt();
        request.payload = json.dump(-1, ' ', false, nlohmann::json::error_handler_t::replace);
        request.accessPoint = m_settings->get(accessPoint);

        return request;

    } catch (const nlohmann::json::exception& e) {
        logError("Error while building request: %s", e.what());
        return RequestInfo();
    }
}

void Licenser::buildStatisticsJson(Reservation &reservation, nlohmann::json &json) const
{
    const std::vector<UsageStatistics> usageStatistics = reservation.takeStatistics();
    if (usageStatistics.empty())
        return;

    nlohmann::json consumerRecordArray;
    for (const auto &statistic : usageStatistics)
        consumerRecordArray.push_back(statistic.toJson());

    json["consumer_runtime_records"] = consumerRecordArray;
}

void Licenser::buildConsumersJson(Reservation &reservation, nlohmann::json &json) const
{
    const std::vector<ConsumerAppInfo> consumers = reservation.attachedConsumers();
    if (consumers.empty())
        return;

    nlohmann::json consumersArray;
    for (const auto &consumer : consumers)
        consumersArray.push_back(consumer.toJson());

    json["attached_consumers"] = consumersArray;
}

bool Licenser::serverIsBusy(const HttpResponse &response) const
{
    static constexpr long sc_statusCodeServiceUnavailable(503);
    if (response.code == sc_statusCodeServiceUnavailable)
        return true;

    JsonHandler resp(response.body);

    std::string code;
    if (!resp.get("code", code) || code.empty())
        return false;

    return (ServerResponseCode::codeToStatus(code) == Status::SERVER_BUSY);
}

QLicenseCore::Status Licenser::isCompatibleCIPVersion(const std::string &clientLibraryVersion) const
{
    if (!clientLibraryVersion.empty()
            && utils::compareVersion(clientLibraryVersion, DAEMON_VERSION, 1) != 0) {
        logWarn("Incompatible client library major version in request: \"%s\", the major version must "
                "match that of the Qt License Service: \"%s\"", clientLibraryVersion.c_str(), DAEMON_VERSION);

        if (clientLibraryVersion > DAEMON_VERSION)
            return Status::SERVICE_VERSION_TOO_LOW;
        else
            return Status::SERVICE_VERSION_TOO_NEW;
    }
    return Status::SUCCESS;
}

void Licenser::initSettings()
{
    logInfo("%s", getDaemonVersion().c_str());
    logInfo("PID: %d", utils::getPID());

    // Make sure work dir has a dir separator in the end
    if (m_workDir[m_workDir.length() - 1] != DIR_SEPARATOR) {
        m_workDir += DIR_SEPARATOR;
    }
    std::string settingFile = m_workDir + std::string(DAEMON_SETTINGS_FILE);
    m_settings = new LicdSetup(settingFile);
    if (!m_settings->init())
        throw Error("Not able to read settings");

    if (utils::strToInt(m_settings->get(sc_queueMinRetryInterval)) < QUEUE_MIN_RETRY_TIME) {
        logWarn("Too short queue retry time in settings: Setting it to minimum: %d ms", QUEUE_MIN_RETRY_TIME);
        m_settings->set(sc_queueMinRetryInterval, std::to_string(QUEUE_MIN_RETRY_TIME));
    }

    // To avoid calculating this possibly at each round again:
    //m_maxQueueTime = utils::strToInt(m_settings->get(sc_queueMaxWaitingTime)) * SECS_IN_HOUR;

    const uint64_t maxRetryInterval = utils::strToInt(m_settings->get(sc_queueMaxRetryInterval));
    const uint64_t minRetryInterval = utils::strToInt(m_settings->get(sc_queueMinRetryInterval));
    const uint64_t retryIncrement = utils::strToInt(m_settings->get(sc_queueIntervalIncrement));

    m_retryCounter.reset(new RetryCounter(minRetryInterval, maxRetryInterval, retryIncrement));

    // Statistics collection settings
    const uint64_t leeway = utils::strToInt(m_settings->get(sc_executionStatLeewayTime));
    if (leeway <= sc_maxExecutionStatLeeway) {
        auto leewayMs = std::chrono::milliseconds(leeway);
        UsageStatistics::setAllowedLeeway(leewayMs);
    } else {
        logWarn("Too high execution statistics leeway time: %d ms, maximum supported is %d ms. "
            "Setting it to defalt value: %d ms.", leeway, sc_maxExecutionStatLeeway, USAGE_STATS_LEEWAY_MS);
    }

    const std::string tcAccepted = m_settings->get(sc_termsAndConditionsAccepted);
    if (tcAccepted != sc_true && tcAccepted != sc_false) {
        logWarn("Invalid value for setting '%s'. Supported values are '%s' and '%s'",
                sc_termsAndConditionsAccepted, sc_true, sc_false);
    }
    m_termsAndConditionsAccepted = (tcAccepted == sc_true);
}

void Licenser::initHttpClient(HttpSettings settings)
{
    const std::string serverAddr = settings.serverAddress;
    if (serverAddr.empty()) {
        logInfo("No server address defined, starting in offline mode.");
        m_state = DaemonState::Offline;
    }

    m_http = new HttpClient(serverAddr);
    m_http->setCABundlePath(settings.caBundlePath);
    m_http->setCertRevokeBehavior(settings.certRevokeBehavior);
    m_http->setRequestTimeout(settings.requestTimeout);
}

void Licenser::initPortFile(uint16_t tcpPort)
{
    m_portFilePath = m_runMode == DaemonRunMode::TestMode
        ? LicdSetup::getQtAppDataLocation() + std::string(USER_SETTINGS_FOLDER_NAME)
            + DIR_SEPARATOR + std::string(DAEMON_TCP_PORT_FILE)
        : m_workDir + std::string(DAEMON_TCP_PORT_FILE);
    // Write the assigned main port number to a file to let the CIP know it
    if (!utils::writeToFile(m_portFilePath, std::to_string(tcpPort)))
        throw Error("Failed to write port info to file: \"" + m_portFilePath);
}

void Licenser::initServiceLockFile()
{
    const std::string lockFilePath = m_workDir + std::string(DAEMON_LOCK_FILE);
    m_lockFile.reset(new QLicenseCore::LockFile(lockFilePath));
    if (!m_lockFile->tryLock()) {
        throw Error("Failed to create lock file for license service: "
            + m_lockFile->error() + ". Is another instance running?");
    }
}

void Licenser::registerSettingsOperations()
{
    SettingsOperationFactory::instance().registerOperation<RequestTimeoutOperation>(sc_requestTimeout);
    SettingsOperationFactory::instance().registerDefaultCallback(sc_requestTimeout,
        [&](const std::string &value, std::string &error) {
            uint64_t timeoutSecs;
            if (!utils::strToUint64(value, timeoutSecs)) {
                error = "Could not convert timeout value '" + value + "' to int.";
                return false;
            }
            m_http->setRequestTimeout(timeoutSecs);
            return true;
        }
    );

    SettingsOperationFactory::instance().registerOperation<ServerAddressOperation>(sc_serverAddr);
    SettingsOperationFactory::instance().registerDefaultCallback(sc_serverAddr,
        [&](const std::string &value, std::string &error) {
            m_http->setServerUrl(value);
            return true;
        }
    );

    SettingsOperationFactory::instance().registerOperation<TermsAndConditionsOperation>(sc_termsAndConditionsAccepted);
    SettingsOperationFactory::instance().registerDefaultCallback(sc_termsAndConditionsAccepted,
        [&](const std::string &value, std::string &error) {
            m_termsAndConditionsAccepted = (value == sc_true);
            return true;
        }
    );
}

bool Licenser::importLocalLicenses()
{
    std::string currentUser;
    if (!utils::getCurrentUserName(currentUser) || currentUser.empty()) {
        logWarn("Unable to import local license files, current username not available?");
        return false;
    }

    LicenseImport importer(*m_cache.get(), currentUser);
    importer.sync({ m_licenseImportPath });
    return true;
}


} // namespace QLicenseService
