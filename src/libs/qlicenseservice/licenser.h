/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/
#pragma once

#include <string>
#include <unordered_map>

#if _WIN32
    // Windows
    #define WIN32_LEAN_AND_MEAN
    //#include <windows.h>
#else
#endif
#include "httpclient.h"
#include "httpsettings.h"
#include "licensecache.h"
#include "retrycounter.h"
#include "tcpserver.h"
#include "timedeltachecker.h"

#include <cipcommandparser.h>
#include <licdsetup.h>
#include <lockfile.h>
#include <response.h>
#include <watchdog.h>

namespace QLicenseService {

enum class DaemonRunMode {
    Daemon      = 0,
    OnDemand    = 1,
    CLIMode     = 2,
    TestMode    = 3
};

class Licenser
{
public:
    enum class DaemonState {
        Stopped       = -2,
        Starting      = -1,
        Online        = 0,
        Offline       = 1
    };

    explicit Licenser(DaemonRunMode runMode, uint16_t tcpPort = 0, const std::string &settingsPath = "");
    ~Licenser();

    bool listen();

    void shutdown();

    bool isFinished() const;
    bool hasPendingRequests() const;
    DaemonState state() const;

private:
    using HttpCallback = std::function<bool(uint16_t)>;

    void initSettings();
    void initHttpClient(HttpSettings settings);
    void initPortFile(uint16_t tcpPort);
    void initServiceLockFile();
    void registerSettingsOperations();
    bool importLocalLicenses();

    void sendHttpRequest(const RequestInfo &request, uint16_t requestId, uint32_t delay = 0);
    void sendTcpResponse(uint16_t socketId, const QLicenseCore::ServiceResponse &response);

    bool onTcpReadyRead(uint16_t socketId);
    bool onHttpReadyRead(uint16_t requestId);
    bool onTimeoutEvent(const std::string &eventId, const std::string &message);

    void onTcpDisconnected(uint16_t socketId);

    bool processReservation(ClientHandler *client, uint32_t delay = 0);
    void processRelease(Reservation &reservation, uint32_t delay = 0);
    void processRenewal(Reservation &reservation, uint32_t delay = 0);
    void processUsageStatistics(Reservation &reservation, uint32_t delay = 0);

    bool registerHttpCallback(uint16_t requestId, HttpCallback callback);
    bool removeHttpCallback(uint16_t requestId);

    bool processLicenseRequest(const RequestInfo &request, const std::string &input);
    bool processOperationRequest(const RequestInfo &request, const CipCommandParser &operation);

    QLicenseCore::Status preParseResponse(const std::string &response) const;

    bool storeClient(ClientHandler *client);
    void removeClient(uint16_t clientId);
    bool clientInStorage(uint16_t socketId) const;
    ClientHandler *getClient(uint16_t clientId);
    void disconnectClientsForReservation(Reservation *reservation);
    void resetClientsForReservation(Reservation *reservation);

    std::string getDaemonVersion();
    bool getServiceSettingByKey(const std::string &key, std::string &value);
    bool setServiceSettingByKey(const std::string &key, const std::string &value,
        QLicenseCore::SettingsType type, std::string &error);
    bool updateRequestTimeout(uint64_t timeoutSecs);
    bool getServerVersionAndTime();

    void removeReservation(Reservation &reservation);
    std::string getReservations(const std::string &user);

    void releaseExpiredReservations();
    void sendUsageStatistics();

    RequestInfo prepareRequest(Reservation &reservation, const std::string &accessPoint) const;
    void buildStatisticsJson(Reservation &reservation, nlohmann::json &json) const;
    void buildConsumersJson(Reservation &reservation, nlohmann::json &json) const;

    bool serverIsBusy(const HttpResponse &response) const;
    QLicenseCore::Status isCompatibleCIPVersion(const std::string &clientLibraryVersion) const;

private:
    HttpClient *m_http;
    TcpServer *m_tcpServer;
    QLicenseCore::LicdSetup *m_settings;
    TimeDeltaChecker m_timeChecker;

    DaemonState m_state;
    DaemonRunMode m_runMode;
    bool m_finished;
    bool m_termsAndConditionsAccepted;

    std::string m_serverVersion;
    uint64_t m_maxQueueTime;
    std::string m_workDir;
    std::string m_portFilePath;
    std::string m_licenseImportPath;

    std::unique_ptr<LicenseCache> m_cache;
    std::unique_ptr<QLicenseCore::Watchdog> m_idleWatchdog;
    std::unique_ptr<QLicenseCore::Watchdog> m_importWatchdog;
    std::unique_ptr<QLicenseCore::Watchdog> m_usageStatisticsWatchdog;
    std::unique_ptr<QLicenseCore::LockFile> m_lockFile;

    std::unordered_map<uint16_t, std::unique_ptr<ClientHandler>> m_clients;
    std::unordered_map<uint16_t, HttpCallback> m_httpCallbacks;

    std::unique_ptr<RetryCounter> m_retryCounter;
};

} // namespace QLicenseService
