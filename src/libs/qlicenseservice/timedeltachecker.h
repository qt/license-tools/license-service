/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <string>

namespace QLicenseService {

class TimeDeltaChecker
{
public:
    TimeDeltaChecker();

    void reset();
    void setTimes(time_t localTimeUtc, time_t serverTimeUtc);
    bool allowedTimeDelta(std::string &error) const;

private:
    time_t m_localTimeUtc;
    time_t m_serverTimeUtc;
};

} // namespace QLicenseService
