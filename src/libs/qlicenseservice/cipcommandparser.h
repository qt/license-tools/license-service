/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/
#pragma once

#include "commonsetup.h"

#include "licdsetup.h"

#include <string>
#include <map>
#include <unordered_set>
#include <functional>
#include <map>

namespace QLicenseService {

using ParseCipFunction = std::function<bool()>;

class CipCommandParser
{
public:
    CipCommandParser(const std::string &input, const std::string &daemonVersion);

    bool parseInput();

    std::string error() const;

    std::string command() const;
    std::string protocolVersion() const;
    std::string clientLibraryVersion() const;
    std::string consumerId() const;
    std::string consumerVersion() const;
    std::string consumerInstallationSchema() const;
    std::string consumerSupportedFeatures() const;
    std::string consumerProcessId() const;
    std::string consumerBuildTimestamp() const;
    std::string jwtToken() const;
    std::string userId() const;
    std::string settingsKey() const;
    std::string settingsValue() const;
    QLicenseCore::SettingsType settingsType() const;

private:
    bool findProtocolVersion();
    ParseCipFunction findParser() const;

    bool parseCipV10();

private:
    struct {
        std::string protocolVersion;
        std::string clientLibraryVersion;
        std::string consumerId;
        std::string consumerVersion;
        std::string consumerInstallationSchema;
        std::string consumerSupportedFeatures;
        std::string consumerProcessId;
        std::string consumerBuildTimestamp;
        std::string userId;
        std::string jwtToken;
        std::string command;
        std::string settingsKey;
        std::string settingsValue;
        std::string settingsType;
    } m_params;

    std::string m_error;
    std::string m_inputStr;
    std::string m_daemonVersion;

    const std::map<std::string, ParseCipFunction> m_cipParsers {
        {"1.0", std::bind(&CipCommandParser::parseCipV10, this)}
    };

    const std::unordered_set<std::string> supportedCommands {
        LICENSE_REQUEST_CMD,
        CURRENT_RESERVATION_CMD,
        DAEMON_VERSION_CMD,
        SERVER_VERSION_CMD,
        RESERVATION_QUERY_CMD,
        CLEAR_CACHE_CMD,
        SET_SERVICE_SETTING_CMD,
        GET_SERVICE_SETTING_CMD
    };

};

} // namespace QLicenseService
