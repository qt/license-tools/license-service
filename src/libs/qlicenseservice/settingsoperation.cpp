/* Copyright (C) 2025 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "settingsoperation.h"

#include <constants.h>
#include <utils.h>

using namespace QLicenseCore;

namespace QLicenseService {

SettingsOperation::SettingsOperation(QLicenseCore::LicdSetup &settings)
    : m_settings(&settings)
{
}

SettingsOperation::~SettingsOperation()
{
}

bool SettingsOperation::validate(const std::string &value) const
{
    if (m_key.empty()) {
        m_error = "No settings key set for the operation.";
        return false;
    }

    return true;
}

bool SettingsOperation::apply(const std::string &value, QLicenseCore::SettingsType type)
{
    if (!m_settings->set(m_key, value, type)) {
        m_error = "Could not apply setting '" + m_key + "' with value '" + value + "'";
        return false;
    }

    return true;
}

bool SettingsOperation::postProcess(Callback callback)
{
    if (!callback)
        return true;

    if (!m_settings->contains(m_key)) {
        m_error = "No settings value for key '" + m_key + "'";
        return false;
    }

    return callback(m_settings->get(m_key), m_error);
}

std::string SettingsOperation::error() const
{
    return m_error;
}


RequestTimeoutOperation::RequestTimeoutOperation(LicdSetup &settings)
    : SettingsOperation(settings)
{
    m_key = sc_requestTimeout;
}

bool RequestTimeoutOperation::validate(const std::string &value) const
{
    uint64_t timeoutSecs;
    if (!utils::strToUint64(value, timeoutSecs)) {
        m_error = "Could not convert timeout value '" + value + "' to int.";
        return false;
    }

    return SettingsOperation::validate(value);
}


ServerAddressOperation::ServerAddressOperation(QLicenseCore::LicdSetup &settings)
    : SettingsOperation(settings)
{
    m_key = sc_serverAddr;
}


TermsAndConditionsOperation::TermsAndConditionsOperation(QLicenseCore::LicdSetup &settings)
    : SettingsOperation(settings)
{
    m_key = sc_termsAndConditionsAccepted;
}

bool TermsAndConditionsOperation::validate(const std::string &value) const
{
    if (value != sc_true && value != sc_false) {
        m_error = "Value '" + value + "' must be either '" + sc_true + "' or '" + sc_false + "'";
        return false;
    }

    return SettingsOperation::validate(value);
}

} // namespace QLicenseService
