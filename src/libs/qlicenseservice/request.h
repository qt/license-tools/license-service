/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "consumerappinfo.h"
#include "license.h"

#include <string>

namespace QLicenseService {

struct RequestInfo
{
    uint16_t socketId = 0;

    std::string userId;
    std::string jwt;

    ConsumerAppInfo consumerInfo;

    std::string payload;
    std::string serverAddr;
    std::string accessPoint;
};

} // namespace QLicenseService
