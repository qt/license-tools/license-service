/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "status.h"
#include "reservationjsonparser.h"
#include "usagestatistics.h"

#include <jsonhandler.h>
#include <watchdog.h>

#include <set>

namespace QLicenseService {

class License;
class ClientHandler;

class Reservation
{
public:
    enum class Flags {
        None = 0,
        Renewable = 1,
        Releasable = 2,
        ServerManaged = Renewable | Releasable,
        LocalOnly = ~(Renewable | Releasable),
        Default = ServerManaged
    };

    Reservation(License *const license);
    Reservation(License *const license, const std::string &id, time_t validFrom, time_t validTo,
        Flags flags, uint64_t leaseRenewalSecsUntilExpiry = 0);

    QLicenseCore::Status parseFromJson(const std::string &data);
    bool copyReservationData(const Reservation *const other);

    std::string toString(uint8_t indent = 0) const;

    std::string id() const;

    License *license() const;

    void resetRenewAndExpiryTimers();

    bool isLeaseExpired() const;
    bool isRenewDue() const;

    std::string validFromUtcString() const;
    std::string validToUtcString() const;

    bool isRenewable() const;
    bool isReleasable() const;
    bool isLocalOnly() const;

    void setPendingForRelease();
    bool isPendingForRelease() const;

    bool requiresReleaseOnDisconnect() const;

    void addClient(ClientHandler *client);
    void removeClient(ClientHandler *client);
    std::set<ClientHandler *> clients() const;

    void addStatistic(const UsageStatistics &statistic);
    std::vector<UsageStatistics> statistics() const;
    std::vector<UsageStatistics> takeStatistics();

    void attachConsumer(const ConsumerAppInfo &consumer);
    std::vector<ConsumerAppInfo> attachedConsumers() const;
    bool isAttachedToConsumer(const ConsumerAppInfo &consumer) const;

    const QLicenseCore::JsonHandler *data() const;

public:
    friend inline Flags operator|(Flags lhs, Flags rhs)
    {
        return static_cast<Flags>(static_cast<int>(lhs) | static_cast<int>(rhs));
    }

    friend inline Flags operator&(Flags lhs, Flags rhs)
    {
        return static_cast<Flags>(static_cast<int>(lhs) & static_cast<int>(rhs));
    }

    friend inline Flags &operator|=(Flags &lhs, Flags rhs)
    {
        return lhs = static_cast<Flags>(static_cast<int>(lhs) | static_cast<int>(rhs));
    }

private:
    void completeStatistics(ClientHandler *client);
    std::vector<UsageStatistics>::iterator findExistingStatistic(const ConsumerAppInfo &consumer);

private:
    friend class ReservationJsonParser;

    ReservationJsonParser m_parser;

    License *const m_license;

    Flags m_flags;
    std::string m_id;
    bool m_pendingForRelease;

    std::set<ClientHandler *> m_clients;
    std::vector<UsageStatistics> m_usageStatistics;
    std::vector<ConsumerAppInfo> m_attachedConsumers;

    time_t m_leaseValidFromUtc;
    time_t m_leaseValidToUtc;
    time_t m_nextRenewTimeUtc;

    uint64_t m_leaseRenewalSecsUntilExpiry;

    std::unique_ptr<QLicenseCore::Watchdog> m_expiryWatchdog;
    std::unique_ptr<QLicenseCore::Watchdog> m_renewalWatchdog;

    QLicenseCore::JsonHandler m_data;
};

} // namespace QLicenseService
