/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/
#pragma once

#include "status.h"

#include <jsonhandler.h>

#include <string>
#include <map>
#include <memory>
#include <functional>

namespace QLicenseService {

class License;
struct RequestInfo;

using ParseLicenseFunction = std::function<QLicenseCore::Status(License &license)>;
using ParseLicenseResponseFunction = std::function<QLicenseCore::Status(const RequestInfo *request,
    const License &license, std::string &response)>;

class LicenseJsonParser
{
public:
    LicenseJsonParser() = default;

    QLicenseCore::Status parse(const std::string &data, License &license);
    QLicenseCore::Status parseResponse(const std::string &data, const RequestInfo *request,
        const License &license, std::string &response);

    std::string error() const;

protected:
    std::string preParseVersion() const;
    ParseLicenseFunction findParser(const std::string &version) const;
    ParseLicenseResponseFunction findResponseParser(const std::string &version) const;

    QLicenseCore::Status parseLicenseV10(License &license);
    QLicenseCore::Status parseLicenseResponseV10(const RequestInfo *request, const License &license, std::string &response);

private:
    std::unique_ptr<QLicenseCore::JsonHandler> m_json;
    std::string m_error;

    const std::map<std::string, ParseLicenseFunction> m_serverResponseParsers {
        {"1.0", std::bind(&LicenseJsonParser::parseLicenseV10, this, std::placeholders::_1)}
    };

    const std::map<std::string, ParseLicenseResponseFunction> m_clientResponseParsers {
        {"1.0", std::bind(&LicenseJsonParser::parseLicenseResponseV10, this,
            std::placeholders::_1,
            std::placeholders::_2,
            std::placeholders::_3)}
    };
};

} // namespace QLicenseService
