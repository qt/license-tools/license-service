/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "consumerappinfo.h"
#include "jsonhandler.h"

#include <sys/types.h>
#include <chrono>

namespace QLicenseService {

struct Consumer;

class UsageStatistics
{
public:
    using time_point = std::chrono::system_clock::time_point;

    UsageStatistics(const ConsumerAppInfo &consumerInfo, time_point startTime = time_point{});
    UsageStatistics(nlohmann::json jsonObject);

    ConsumerAppInfo consumerInfo() const;
    nlohmann::json toJson() const;

    void setComplete(bool complete, time_point stopTime = time_point{});
    bool isComplete() const;
    bool isWithinAllowedLeeway(time_point currentTime) const;

    time_point startTime() const;
    time_point stopTime() const;

public:
    static std::chrono::milliseconds allowedLeeway();
    static void setAllowedLeeway(std::chrono::milliseconds ms);

private:
    ConsumerAppInfo m_consumerInfo;

    bool m_complete;
    time_point m_startTime;
    time_point m_stopTime;
};

} // namespace QLicenseService
