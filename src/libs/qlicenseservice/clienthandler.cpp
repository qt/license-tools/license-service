
/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "clienthandler.h"

#include "version.h"
#include "licensecache.h"
#include "cipcommandparser.h"
#include "crypto.h"

#include <constants.h>
#include <errors.h>

using namespace QLicenseCore;

namespace QLicenseService {

ClientHandler::ClientHandler(const RequestInfo &request, const LicdSetup &settings, LicenseCache &cache)
    : m_cache(cache)
    , m_license(nullptr)
    , m_request(request)
    , m_settings(settings)
{
}

ClientHandler::~ClientHandler()
{
    if (!m_license)
        return;

    if (Reservation *res = m_license->reservation(m_reservationId))
        res->removeClient(this);
}

/*
    Attempts to find a cached license reservation for the current license handler. If found,
    returns a pointer to the reservation object and the JSON representation of the reservation
    with the \a response return parameter. Returns \c nullptr otherwise.

    Cached reservation can be used only if an application with similar consumer properties
    has used the reservation previously. Otherwise a new reservation is requested from the server,
    to ensure most suitable license for each unique consumer application. If the daemon's \a state
    is \c Offline, reservation is allowed to be used regardless of the attached consumer information.

    Example case:
    - user has single license that supports QtC and QDS
    - QtC makes the initial reservation and the res. is attached for it (consumerInfo())
    - QDS is started later, as it's consumerInfo() is not attached to it we bail out here,
      which triggers a fresh reservation request to be sent (Licenser::processReservation())
    - as there is only one license the server returns the same reservation + extends it
*/
Reservation *ClientHandler::findCachedReservation(std::string &response, Licenser::DaemonState state)
{
    if (m_license) {
        // Already have a license object for this client
        if (Reservation *reservation = m_license->reservation(m_reservationId)) {
            if (buildResponseJson(reservation, response) != Status::SUCCESS)
                return nullptr;

            return reservation;
        }
    }

    m_license = m_cache.licenseByConsumer(m_request.userId, m_request.consumerInfo);
    if (!m_license) // No compatible license in cache
        return nullptr;

    if (!m_license->isUsable()) {
        m_license = nullptr;
        return nullptr;
    }

    for (auto *reservation : m_license->reservations()) {
        const bool offlineReservation = reservation->isLocalOnly()
            || state == Licenser::DaemonState::Offline;

        if (reservation->isLeaseExpired())
            continue;

        if (reservation->isPendingForRelease())
            continue;

        if (!offlineReservation && !reservation->isAttachedToConsumer(consumerInfo()))
            continue;

        const bool isUserReservation = (m_license->reservationType() == License::ReservationType::User);
        bool isMatchingProcessReservation = false;
        if (m_license->reservationType() == License::ReservationType::Process) {
            for (auto *client : reservation->clients()) {
                if (client->consumerInfo() != consumerInfo())
                    continue;
                // Needs to match with processId as well, could be that this is some other
                // module in client application requesting a license
                if (client->consumerInfo().processId == consumerInfo().processId) {
                    isMatchingProcessReservation = true;
                    break;
                }
            }
        }

        if (isUserReservation || isMatchingProcessReservation) {
            if (buildResponseJson(reservation, response) != Status::SUCCESS)
                return nullptr;

            reservation->addClient(this);
            if (!offlineReservation)
                reservation->attachConsumer(consumerInfo());

            m_reservationId = reservation->id();

            return reservation;
        }
    }

    // default
    m_license = nullptr;
    return nullptr;
}

Status ClientHandler::parseRequest(const std::string &input, std::string &error)
{
    CipCommandParser cipParser(input, DAEMON_VERSION);
    if (!cipParser.parseInput()) {
        error = cipParser.error();
        return Status::BAD_CLIENT_REQUEST;
    }
    m_request.consumerInfo.id = cipParser.consumerId();
    m_request.consumerInfo.version = cipParser.consumerVersion();
    m_request.consumerInfo.installationSchema = cipParser.consumerInstallationSchema();
    m_request.consumerInfo.supportedFeatures = cipParser.consumerSupportedFeatures();
    m_request.consumerInfo.processId = cipParser.consumerProcessId();
    m_request.consumerInfo.buildTimestamp = cipParser.consumerBuildTimestamp();
    m_request.userId = cipParser.userId();
    m_request.jwt = cipParser.jwtToken();

    if (!buildRequestJson()) {
        // TODO: consider adding a separate error code for this.
        error = "Could not build request payload";
        return Status::BAD_REQUEST;
    }

    return Status::SUCCESS;
}

void ClientHandler::revokeCurrentReservation()
{
    if (!m_license)
        return;

    if (Reservation *res = m_license->reservation(m_reservationId))
        res->removeClient(this);

    m_license = nullptr;
    m_reservationId.clear();
}

RequestInfo ClientHandler::request()
{
    return m_request;
}

/*
    Returns the consumer information provided by application associated
    with this client handler.
*/
ConsumerAppInfo ClientHandler::consumerInfo() const
{
    return m_request.consumerInfo;
}

int ClientHandler::socketId() const
{
    return m_request.socketId;
}

std::string ClientHandler::reservationId() const
{
    return m_reservationId;
}

License *ClientHandler::license() const
{
    return m_license;
}

bool ClientHandler::buildRequestJson()
{
    // TODO This needs to be versioned, like CIP command parsers
    JsonHandler json;
    json.set("service_version", DAEMON_VERSION);
    json.set("consumer_id", m_request.consumerInfo.id);
    json.set("consumer_version", m_request.consumerInfo.version);
    json.set("consumer_installation_schema", m_request.consumerInfo.installationSchema);
    json.set("consumer_supported_features", m_request.consumerInfo.supportedFeatures);
    json.set("consumer_process_id", m_request.consumerInfo.processId);
    if (!m_request.consumerInfo.buildTimestamp.empty())
        json.set(sc_consumerBuildTimestamp, m_request.consumerInfo.buildTimestamp);
    json.set("user_id", m_request.userId);
    json.set("hw_id", m_settings.get(sc_hwId));
    json.set("host_os", m_settings.get(sc_hostOs));

    m_request.accessPoint = m_settings.get(sc_reservationAccessPoint);
    m_request.payload = json.dump();

    return !m_request.payload.empty();
}

Status ClientHandler::buildResponseJson(Reservation *reservation, std::string &response)
{
    LicenseJsonParser responseParser;
    const Status result = responseParser.parseResponse(reservation->toString(), &m_request, *m_license, response);
    if (result != Status::SUCCESS) {
        logError("Error while constructing response to client: %s", responseParser.error().c_str());
        m_license = nullptr;
    }

    return result;
}

/*
    Parses the status code from the server \a response. Returns
    Status::SUCCESS on success, otherwise a result indicating the failure.
*/
Status ClientHandler::preParseResponse(std::string &response)
{
    JsonHandler json(response);

    std::string code;
    if (!json.get("code", code) || code.empty())
        return Status::INVALID_RESPONSE;

    return ServerResponseCode::codeToStatus(code);
}

License *ClientHandler::updateExistingLicense(std::string &jsonData)
{
    License *existingLicense = m_cache.license(m_request.userId, m_license->id());
    if (!existingLicense)
        return nullptr;

    std::unique_ptr<Reservation> tmpReservation(new Reservation(existingLicense));
    if (tmpReservation->parseFromJson(jsonData) != Status::SUCCESS) {
        // Something went wrong while parsing the json..
        return nullptr;
    }

    // Update existing license data in cache
    if (!existingLicense->copyLicenseData(m_license))
        return nullptr;

    Reservation *reservation = existingLicense->reservation(tmpReservation->id());
    if (reservation) {
        // Found existing reservation, update reservation data
        if (!reservation->copyReservationData(tmpReservation.get()))
            return nullptr;
    } else {
        // No existing reservation, add to license
        reservation = tmpReservation.release();
        existingLicense->addReservation(reservation);
    }

    reservation->addClient(this);
    reservation->attachConsumer(consumerInfo());
    m_reservationId = reservation->id();

    m_cache.flush(reservation);
    return existingLicense;
}

Status ClientHandler::addNewReservation(License *license, std::string &jsonData)
{
    std::unique_ptr<Reservation> reservation(new Reservation(license));

    const Status result = reservation->parseFromJson(jsonData);
    if (result != Status::SUCCESS)
        return result;

    reservation->addClient(this);
    reservation->attachConsumer(consumerInfo());
    m_reservationId = reservation->id();
    license->addReservation(reservation.release());

    return result;
}

Status ClientHandler::parseResponse(std::string &response)
{
    Status result = preParseResponse(response);
    if (result != Status::SUCCESS)
        return result;

    m_license = new License(m_request.userId);
    std::unique_ptr<License> guard(m_license);

    result = m_license->parseFromJson(response);
    if (result != Status::SUCCESS) {
        m_license = nullptr;
        return result;
    }

    // Check if there's already a compatible license in cache
    License *existingLicense = updateExistingLicense(response);
    if (existingLicense) {
        m_license = existingLicense;
    } else {
        // No? Add new reservation information to license
        result = addNewReservation(m_license, response);
        if (result != Status::SUCCESS) {
            m_license = nullptr;
            return result;
        }
    }

    m_license->setJwt(m_request.jwt);

    if (!existingLicense && !m_cache.insert(m_license)) {
        logError("Error while storing license id %s: %s", m_license->id().c_str(), m_cache.error().c_str());
        m_license = nullptr;
        return Status::LICENSE_REJECTED;
    }

    Reservation *reservation = m_license->reservation(m_reservationId);
    // Start/restart event timers
    reservation->resetRenewAndExpiryTimers();
    // Construct a response to client
    result = buildResponseJson(reservation, response);
    if (result != Status::SUCCESS) {
        m_license = nullptr;
        return result;
    }

    // Cache now manages the object. In case we use an existing license,
    // let the guard delete the temporary license object.
    if (!existingLicense)
        guard.release();

    return result;
}

} // namespace QLicenseService
