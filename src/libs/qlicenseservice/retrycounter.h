/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <cstdint>

namespace QLicenseService {

class RetryCounter
{
public:
    RetryCounter(uint64_t minInterval, uint64_t maxInterval, uint64_t increment);

    uint64_t nextRetryDelay();
    void reset();

private:
    uint64_t m_minInterval;
    uint64_t m_maxInterval;
    uint64_t m_increment;

    uint64_t m_delay;
    uint32_t m_timesTried;
};

} // namespace QLicenseService
