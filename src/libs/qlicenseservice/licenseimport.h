/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
 */

#pragma once

#include "licensecache.h"

#include <string>

namespace QLicenseService {

class LicenseImport
{
public:
    LicenseImport(LicenseCache &cache, const std::string &localUser);

    void sync(const std::vector<std::string> &searchPaths);

private:
    std::vector<License *> localLicenses() const;
    bool licenseInUse(const std::string &licenseFile) const;
    bool licenseInUse(License *license) const;

private:
    LicenseCache &m_licenseCache;
    std::string m_localUser;
};

} // namespace QLicenseService
