/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <tcpmessageprotocol.h>

#include <atomic>
#include <sys/types.h>
#include <unordered_map>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <deque>
#include <string>
#include <vector>

#define BUFFER_SIZE 4096
#define MAX_CONN 4096
#define TRUE 1
#define FALSE 0
#define SOCKET_CLOSED_MSG  "closed"

#if __APPLE__ || __MACH__ || __linux__
    #include <sys/types.h>
    #include <arpa/inet.h>
    #include <unistd.h>
    #include <netdb.h>
    #include <sys/time.h>
    #include <poll.h>
#else
    #define WIN32_LEAN_AND_MEAN
    #define NOMINMAX
    #include <windows.h>
    #include <ws2tcpip.h>
    // Need to link with Ws2_32.lib
    #pragma comment (lib, "Ws2_32.lib")
#endif

namespace QLicenseService {

struct TcpMessage
{
    uint16_t socketId;
    std::string message;
};

struct ClientSocket
{
    ClientSocket() = default;
    ClientSocket(type_socket sock)
        : fd(sock)
        , capabilities(0)
    {}

    type_socket fd;
    uint32_t capabilities;
};

class TcpServer : public QLicenseCore::TcpMessageProtocol
{
public:
    TcpServer();
    ~TcpServer();

    bool init(uint16_t &serverPort);
    void start();
    void stop();

    int sendResponse(uint16_t socketId, const std::string &message);
    void closeClientSocket(uint16_t socketId);
    bool dataAvailable();
    std::string receive(uint16_t &socketId);

    ClientSocket clientSocket(uint16_t socketId);

private:
    void run();
    void doCloseSocket(type_socket &socketFD);
    bool generateSocketId(uint16_t &index);

    bool createListeningSocket(type_socket &listeningSocket, uint16_t &port);
    std::string getLastSocketError() const;

private:
    type_socket m_masterSocket;
    // <(unique)socket_id, ClientSocket>, descriptor numbers can be reused
    std::unordered_map<uint16_t, ClientSocket> m_clientSockets;
#ifdef _WIN32
    std::vector<WSAPOLLFD> m_readfds;
#else
    std::vector<struct pollfd> m_readfds;
#endif
    struct sockaddr_in m_address;

    std::atomic<bool> m_stopRequested;
    bool m_running;

    char m_buffer[BUFFER_SIZE];

    std::deque<TcpMessage> m_messages;

    std::thread m_thread;
    std::mutex m_mutex;
    std::condition_variable m_condvar;
};

} // namespace QLicenseService
