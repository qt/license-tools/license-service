/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
 */

#pragma once

#include "status.h"

namespace QLicenseService {

class Reservation;

class ReservationJsonParser
{
public:
    ReservationJsonParser() = default;

    QLicenseCore::Status parse(const std::string &data, Reservation &reservation);

    std::string error() const;

protected:
    QLicenseCore::Status parseReservation(Reservation &reservation);

private:
    std::string m_jsonData;
    std::string m_error;
};

} // namespace QLicenseService
