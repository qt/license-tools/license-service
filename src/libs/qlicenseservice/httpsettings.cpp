/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "httpsettings.h"

#include "constants.h"
#include "logger.h"

using namespace QLicenseCore;

namespace QLicenseService {

HttpSettings::HttpSettings(QLicenseCore::LicdSetup *settings)
    : requestTimeout(-1)
{
    serverAddress = settings->get(sc_serverAddr);

    caBundlePath = settings->get(sc_caBundlePath);
    caBundlePath = ((caBundlePath == "auto") ? std::string() : caBundlePath);

    std::string certRevokeCheck = settings->get(sc_certificateRevokeCheck);
    certRevokeBehavior = HttpRequest::CertRevokeOption::Default;
    if (certRevokeCheck == "strict")
        certRevokeBehavior = HttpRequest::CertRevokeOption::Strict;
    else if (certRevokeCheck == "auto")
        certRevokeBehavior = HttpRequest::CertRevokeOption::BestEffort;
    else if (certRevokeCheck == "off")
        certRevokeBehavior = HttpRequest::CertRevokeOption::NoRevoke;
    else
        logWarn("Unknown value \"%s\" for option %s", certRevokeCheck.c_str(), sc_certificateRevokeCheck);

    const std::string timeoutStr = settings->get(sc_requestTimeout);
    try {
        requestTimeout = stoi(timeoutStr);
    } catch (...) {
        logWarn("Invalid setting value for request timeout: \"%s\"", timeoutStr.c_str());
    }
}

} // namespace QLicenseService
