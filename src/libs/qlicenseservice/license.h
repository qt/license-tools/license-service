/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "licensejsonparser.h"
#include "status.h"
#include "reservation.h"

#include <jsonhandler.h>

#include <string>
#include <set>

namespace QLicenseService {

class License
{
public:
    enum class Model {
        None         = 0,
        Site         = 1,
        Float        = 2,
        Named        = 3
    };

    enum class ReservationType {
        None         = 0,
        Process      = 1,
        User         = 2
    };

    enum class RenewalPolicy {
        Passive      = 0,
        Active       = 1
    };

    struct Consumer;

public:
    explicit License(const std::string &user);
    License(const std::string &user, const std::string &data);
    ~License();

    QLicenseCore::Status parseFromJson(const std::string &data);
    bool copyLicenseData(const License *const other);

    bool verifySignature() const;
    bool isVerified() const;

    bool isUsable() const;

    std::string id() const;
    Model model() const;
    ReservationType reservationType() const;
    RenewalPolicy renewalPolicy() const;
    int priority() const;
    std::string installationSchema() const;
    std::string localUser() const;

    void setJwt(const std::string &jwt);
    std::string jwt();

    bool isValidFromStarted() const;
    bool isExpired() const;

    bool isPerpetual() const;

    std::string validFromDateString() const;

    time_t validToDate() const;
    std::string validToDateString() const;

    uint64_t leaseTimeSeconds() const;

    const std::set<Consumer> &consumers() const;
    const Consumer *consumer(const std::string &consumerId, const std::string &consumerVersion,
        const std::string &installationSchema = std::string()) const;

    const std::set<Reservation *> reservations() const;
    Reservation *reservation(const std::string &reservationId);
    void addReservation(Reservation *reservation);
    bool removeReservation(const std::string &reservationId);

    const QLicenseCore::JsonHandler *data() const;

public:
    bool operator==(const License &other) const;

private:
    friend class LicenseJsonParser;
    friend class Reservation;

    LicenseJsonParser m_parser;

    mutable bool m_verified;

    std::string m_id;
    Model m_model;
    bool m_perpetual;
    ReservationType m_reservationType;
    RenewalPolicy m_renewalPolicy;
    int m_priority;

    time_t m_licenseValidToDate;
    time_t m_licenseValidFromDate;
    uint64_t m_leaseTimeSecs;

    std::string m_localUser;
    std::string m_jwt;
    std::string m_cs_p;
    std::string m_cs_s;

    std::map<std::string, Reservation *> m_reservations;
    std::set<Consumer> m_consumers;

    QLicenseCore::JsonHandler m_data;
};

struct License::Consumer
{
    Consumer() = default;
    Consumer(QLicenseCore::JsonHandler consumerData, const std::string &consumerId,
             const std::string &consumerVersion, const std::string &consumerInstallationSchema)
        : id(consumerId)
        , version(consumerVersion)
        , data(consumerData)
    {}

    std::string id;
    std::string version;
    QLicenseCore::JsonHandler data;

    bool operator<(const Consumer &other) const;
    bool operator==(const Consumer &other) const;
};

} // namespace QLicenseService
