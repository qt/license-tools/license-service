/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <localqtaccount.h>

#include <string>

namespace QLicenseService {

class QtAccount : public QLicenseCore::LocalQtAccount
{
public:
    QtAccount() = default;

    bool performLogin(const std::string &email, const std::string &password);
    bool writeToDisk();
};

} // namespace QLicenseService
