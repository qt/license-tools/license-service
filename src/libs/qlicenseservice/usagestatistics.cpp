/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "usagestatistics.h"

#include <commonsetup.h>

#include <constants.h>
#include <utils.h>

using namespace QLicenseCore;

namespace QLicenseService {

static std::chrono::milliseconds s_allowedLeeway(std::chrono::milliseconds(USAGE_STATS_LEEWAY_MS));

/*
    Constructs a new usage statistic object for \a consumerInfo. Optionally set the
    starting time of the usage statistics to \a startTime. Otherwise the current UTC
    time (with milliseconds) is set as the starting time of this usage.
*/
UsageStatistics::UsageStatistics(const ConsumerAppInfo &consumerInfo, time_point startTime)
    : m_consumerInfo(consumerInfo)
    , m_complete(false)
    , m_startTime((startTime == time_point{}) ? utils::utcTimeMs() : startTime)
    , m_stopTime(time_point{})
{
}

/*
    Parses and constructs a new usage statistic from \a jsonObject.
*/
UsageStatistics::UsageStatistics(nlohmann::json jsonObject)
    : m_complete(false)
    , m_startTime(time_point{})
    , m_stopTime(time_point{})
{
    try {
        m_consumerInfo.id = jsonObject["consumer_id"] ;
        m_consumerInfo.version = jsonObject["consumer_version"];
        m_startTime = utils::stringToUtcTimeMs(std::string(jsonObject["start_time"]));
    } catch (const nlohmann::json::exception &e) {
        logError("Error parsing JSON: %s", e.what());
    }

    try {
        const std::string stopTime = jsonObject["stop_time"];
        if (stopTime.empty())
            return;

        m_stopTime = utils::stringToUtcTimeMs(stopTime);
        m_complete = true;
    } catch (...) {
        // Ignore exceptions for missing stop_time
    }
}

/*
    Returns the consumer info this object collects usage statistics for.
*/
ConsumerAppInfo UsageStatistics::consumerInfo() const
{
    return m_consumerInfo;
}

/*
    Returns the JSON representation of this usage statistic. Returns
    an empty JSON object in case of failure.
*/
nlohmann::json UsageStatistics::toJson() const
{
    try {
        nlohmann::json object;
        object["consumer_id"] = m_consumerInfo.id;
        object["consumer_version"] = m_consumerInfo.version;
        object["start_time"] = utils::utcTimeMsToString(startTime());
        if (isComplete())
            object["stop_time"] = utils::utcTimeMsToString(stopTime());

        return object;
    } catch (const nlohmann::json::exception &e) {
        logError("Error constucting JSON: %s", e.what());
        return nlohmann::json();
    }
}

/*
    If \a complete is \c true sets this usage statistic object as complete,
    meaning the usage of the associated consumer was finished. Optionally
    set the stopping time of the usage statistic as \a stopTime. Otherwise
    current UTC time (with milliseconds) is used.

    If \a complete is \c false, sets this statistic as incomplete.
*/
void UsageStatistics::setComplete(bool complete, time_point stopTime)
{
    if (m_complete == complete)
        return;

    if ((m_complete = complete))
        m_stopTime = (stopTime == time_point{}) ? utils::utcTimeMs() : stopTime;
    else
        m_stopTime = time_point{};
}

/*
    Returns whether this usage statistic is complete.
*/
bool UsageStatistics::isComplete() const
{
    return m_complete;
}

/*
    Returns \c true if \a currentTime is within allowed leeway time for
    this usage statistic, \c false otherwise.
*/
bool UsageStatistics::isWithinAllowedLeeway(time_point currentTime) const
{
    if (!m_complete)
        return true;

    constexpr std::chrono::milliseconds sc_zeroMs(0);
    if (s_allowedLeeway == sc_zeroMs)
        return false;

    auto leeway = currentTime - m_stopTime;
    return (leeway <= s_allowedLeeway);
}

/*
    Returns the start time of the usage.
*/
UsageStatistics::time_point UsageStatistics::startTime() const
{
    return m_startTime;
}

/*
    Returns the stop time of the usage.
*/
UsageStatistics::time_point UsageStatistics::stopTime() const
{
    return m_stopTime;
}

/*
    Returns the global allowed leeway time for all usage statistics, in milliseconds.

    The leeway time determines if another application can continue using an already
    completed usage statistics. For this to happen, the application must have been
    started during the allowed leeway time since the statistic was marked completed.

    The leeway time allows concatenating statistics, i.e. for applications that
    start and stop frequently.
*/
std::chrono::milliseconds UsageStatistics::allowedLeeway()
{
    return s_allowedLeeway;
}

/*
    Sets the global allowed leeway time for all usage statistics to \a ms.
*/
void UsageStatistics::setAllowedLeeway(std::chrono::milliseconds ms)
{
    s_allowedLeeway = ms;
}

} // namespace QLicenseService
