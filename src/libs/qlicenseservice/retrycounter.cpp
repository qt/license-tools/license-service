/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "retrycounter.h"

#include "commonsetup.h"

namespace QLicenseService {

/*
    Constructs a retry timer with minimum and maximium interval, and increment added
    between failed operations.
*/
RetryCounter::RetryCounter(uint64_t minInterval, uint64_t maxInterval, uint64_t increment)
    : m_minInterval(minInterval)
    , m_maxInterval(maxInterval)
    , m_increment(increment)
    , m_delay(QUEUE_MIN_RETRY_TIME)
    , m_timesTried(0)
{
}

/*
    Returns the next delay for operation, adding the configured increment
    per times already tried.
*/
uint64_t RetryCounter::nextRetryDelay()
{
    m_delay = m_minInterval + (m_timesTried * m_increment);
    if (m_delay > m_maxInterval)
        m_delay = m_maxInterval;

    m_timesTried++;

    // TODO: do we need to allow clients to time out once max retry time is exceeded?
    return m_delay;
}

/*
    Resets the counter. This should be called after a successful operation.
*/
void RetryCounter::reset()
{
    m_delay = QUEUE_MIN_RETRY_TIME;
    m_timesTried = 0;
}

} // namespace QLicenseService
