/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
 */

#include "reservationjsonparser.h"

#include "reservation.h"

#include <utils.h>
#include <jsonhandler.h>

using namespace QLicenseCore;

namespace QLicenseService {

Status ReservationJsonParser::parse(const std::string &data, Reservation &reservation)
{
    m_error.clear();
    JsonHandler json(data);

    if (!json.get("reservation.reservation_id", reservation.m_id) || reservation.m_id.empty())
        return Status::INVALID_RESPONSE;

    bool localOnly = false;
    if (json.get("reservation.local_only", localOnly) || localOnly)
        reservation.m_flags |= Reservation::Flags::LocalOnly;

    std::string leaseValidTo;
    if (!json.get("reservation.lease_valid_to", leaseValidTo)) {
        logError("reservation.lease_valid_to missing from reservation: %s", reservation.m_id.c_str());
        return Status::INVALID_RESPONSE;
    }
    reservation.m_leaseValidToUtc = utils::stringToUtcTime(leaseValidTo.c_str(), "%Y-%m-%dT%H:%M:%S");

    std::string leaseValidFrom;
    if (!json.get("reservation.lease_valid_from", leaseValidFrom)) {
        logError("reservation.lease_valid_from missing from reservation: %s", reservation.m_id.c_str());
        return Status::INVALID_RESPONSE;
    }
    reservation.m_leaseValidFromUtc = utils::stringToUtcTime(leaseValidFrom.c_str(), "%Y-%m-%dT%H:%M:%S");

    if (reservation.m_leaseValidFromUtc >= reservation.m_leaseValidToUtc) {
        logError("The \"valid from\" value %s is greater than \"valid to\" %s for reservation: %s",
            leaseValidFrom.c_str(), leaseValidTo.c_str(), reservation.m_id.c_str());
        return Status::INVALID_RESPONSE; // Something is off with the lease time
    }

    // optional
    json.get("reservation.lease_renewal_time", reservation.m_leaseRenewalSecsUntilExpiry);

    return Status::SUCCESS;
}

std::string ReservationJsonParser::error() const
{
    return m_error;
}

} // namespace QLicenseService
