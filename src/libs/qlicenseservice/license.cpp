/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "license.h"

#include <errors.h>
#include <utils.h>
#include <crypto.h>

#include <vector>

#include <openssl/evp.h>
#include <openssl/pem.h>
#include <openssl/opensslv.h>

#if defined(OPENSSL_VERSION_NUMBER) && OPENSSL_VERSION_NUMBER < 0x30000000L
#define OPENSSL_LEGACY 1
#endif

using namespace QLicenseCore;

namespace QLicenseService {

/*
    Constructs an empty license for \a user
*/
License::License(const std::string &user)
    : m_verified(false)
    , m_model(License::Model::None)
    , m_perpetual(false)
    , m_reservationType(License::ReservationType::None)
    , m_renewalPolicy(License::RenewalPolicy::Active)
    , m_priority(0)
    , m_licenseValidToDate(0)
    , m_licenseValidFromDate(0)
    , m_leaseTimeSecs(0)
    , m_localUser(user)
{
}

/*
    Constructs a license from JSON \a data, for local \a user
*/
License::License(const std::string &user, const std::string &data)
    : m_verified(false)
    , m_model(License::Model::None)
    , m_perpetual(false)
    , m_reservationType(License::ReservationType::None)
    , m_renewalPolicy(License::RenewalPolicy::Active)
    , m_priority(0)
    , m_licenseValidToDate(0)
    , m_licenseValidFromDate(0)
    , m_leaseTimeSecs(0)
    , m_localUser(user)
    , m_data(JsonHandler(data))
{
    const Status result = m_parser.parse(data, *this);

    if (result == Status::UNKNOWN_LICENSE_MODEL)
        throw Error("Unknown model while parsing license data for license id" + m_id);

    if (result == Status::INVALID_RESPONSE) {
        throw Error("Error while parsing license data for license id: "
            + m_id + ": " + m_parser.error());
    }

    if (!verifySignature())
        throw Error("Invalid signature for license id: " + m_id);

    // Allow other results. This may be a local license, then we just
    // construct a license without valid reservation information.
}

License::~License()
{
    for (auto &reservation : m_reservations)
        delete reservation.second;
}

Status License::parseFromJson(const std::string &data)
{
    m_data = JsonHandler(data);

    const Status result = m_parser.parse(data, *this);
    if (result != Status::SUCCESS) {
        logError(m_parser.error().c_str());
        return result;
    }

    if (!verifySignature()) {
        logError("Invalid signature for license id: %s", m_id.c_str());
        return Status::LICENSE_REJECTED;
    }

    return result;
}

/*
    Copies license data from \a other to this license. This only affects data members
    present in the JSON representation of the license, internal data such as attached
    reservations are unmodified.

    Returns \c true on success, \c false otherwise.
*/
bool License::copyLicenseData(const License *const other)
{
    if (!other) {
        logError("Cannot copy license data from empty object");
        return false;
    } else if (!other->isVerified()) {
        logError("Cannot copy license data from unverified license");
        return false;
    }

    // If the license was verified, we know the parsed data is valid
    m_verified = true;
    m_data = *other->data();

    m_model = other->model();
    m_reservationType = other->reservationType();
    m_renewalPolicy = other->renewalPolicy();
    m_id = other->id();
    m_priority = other->priority();
    m_perpetual = other->isPerpetual();

    m_consumers = other->consumers();

    m_cs_p = other->m_cs_p;
    m_cs_s = other->m_cs_s;

    m_leaseTimeSecs = other->m_leaseTimeSecs;
    m_licenseValidToDate = other->m_licenseValidToDate;
    m_licenseValidFromDate = other->m_licenseValidFromDate;

    return true;
}

bool License::verifySignature() const
{
    m_verified = false;

    // stringify license
    nlohmann::json licenseJson;
    if (!m_data.get("license", licenseJson)) {
        logError("Cannot parse license object from JSON for license id: %s", m_id.c_str());
        return false;
    }
    const std::string licenseString = licenseJson.dump();

    // decode signature
    const std::vector<unsigned char> signature = Crypto::base64Decode(m_cs_s);

    // create openssl context and initialize it
#if OPENSSL_LEGACY
    EVP_MD_CTX *mdctx = EVP_MD_CTX_create();
#else
    EVP_MD_CTX* mdctx = EVP_MD_CTX_new();
#endif
    if (!mdctx) {
        logError("EVP_MD_CTX_new failed.");
        return false;
    }

    BIO* bio = BIO_new_mem_buf(m_cs_p.c_str(), m_cs_p.length());
    EVP_PKEY* pkey = PEM_read_bio_PUBKEY(bio, nullptr, nullptr, nullptr);
    BIO_free(bio);

    try {
        if (!pkey)
            throw Error("Failed to load public key for license id: " + m_id);

        // initialize verification
        if (EVP_DigestVerifyInit(mdctx, nullptr, EVP_sha256(), nullptr, pkey) != 1)
            throw Error("EVP_DigestVerifyInit failed.");

        // add license string to verification
        if (EVP_DigestVerifyUpdate(mdctx, licenseString.c_str(), licenseString.length()) != 1)
            throw Error("EVP_DigestVerifyUpdate failed.");

    } catch (const QLicenseCore::Error &e) {
        logError(e.what());
        EVP_PKEY_free(pkey);
#if OPENSSL_LEGACY
        EVP_MD_CTX_destroy(mdctx);
#else
        EVP_MD_CTX_free(mdctx);
#endif
        return false;
    }

    // verify
    const int result = EVP_DigestVerifyFinal(mdctx, signature.data(), signature.size());

    // cleanup
    EVP_PKEY_free(pkey);
#if OPENSSL_LEGACY
    EVP_MD_CTX_destroy(mdctx);
#else
    EVP_MD_CTX_free(mdctx);
#endif

    if (result != 1) // signature NOT valid
        return false;

    m_verified = true;
    return m_verified;
}

bool License::isVerified() const
{
    return m_verified;
}

/*
    Returns \c true if this license is valid and can be used by client
    applications, \c false otherwise.
*/
bool License::isUsable() const
{
    if (!m_verified)
        return false;

    if (!isValidFromStarted())
        return false;

    if (isExpired() && !isPerpetual())
        return false;

    return true;
}

bool License::operator==(const License &other) const
{
    // Reservation intentionally left out
    return m_id == other.m_id &&
        m_model == other.m_model &&
        m_perpetual == other.m_perpetual &&
        m_reservationType == other.m_reservationType &&
        m_priority == other.m_priority &&
        utils::constTimeCompare(m_localUser, other.m_localUser) &&
        utils::constTimeCompare(m_jwt, other.m_jwt) &&
        m_cs_p == other.m_cs_p &&
        m_cs_s == other.m_cs_s &&
        m_consumers == other.m_consumers &&
        m_licenseValidToDate == other.m_licenseValidToDate &&
        m_licenseValidFromDate == other.m_licenseValidFromDate &&
        m_leaseTimeSecs == other.m_leaseTimeSecs;
}

std::string License::id() const
{
    return m_id;
}

License::Model License::model() const
{
    return m_model;
}

License::ReservationType License::reservationType() const
{
    return m_reservationType;
}

License::RenewalPolicy License::renewalPolicy() const
{
    return m_renewalPolicy;
}

int License::priority() const
{
    return m_priority;
}

std::string License::localUser() const
{
    return m_localUser;
}

void License::setJwt(const std::string &jwt)
{
    // TODO: for testing - this may not be a sensible thing to do...

    // The problem is that renew and release need the token for authentication,
    // but from where to get it?
    // Read logged in information from QtAccount per local user of license?
    m_data.set("reservation.jwt", jwt);
}

std::string License::jwt()
{
    if (!m_data.get("reservation.jwt", m_jwt))
        return std::string();

    return m_jwt;
}

bool License::isValidFromStarted() const
{
    return (utils::utcTime() > m_licenseValidFromDate);
}

bool License::isExpired() const
{
    return (utils::utcTime() > m_licenseValidToDate);
}

bool License::isPerpetual() const
{
    return m_perpetual;
}

std::string License::validFromDateString() const
{
    return utils::utcTimeToString(m_licenseValidFromDate, "%Y-%m-%d");
}

time_t License::validToDate() const
{
    return m_licenseValidToDate;
}

std::string License::validToDateString() const
{
    return utils::utcTimeToString(m_licenseValidToDate, "%Y-%m-%d");
}

uint64_t License::leaseTimeSeconds() const
{
    return m_leaseTimeSecs;
}

/*
    Returns the current reservations for this license.
*/
const std::set<Reservation *> License::reservations() const
{
    std::set<Reservation *> reservations;
    for (const auto& i : m_reservations)
        reservations.insert(i.second);

    return reservations;
}

Reservation *License::reservation(const std::string &reservationId)
{
    auto i = m_reservations.find(reservationId);
    if (i == m_reservations.end())
        return nullptr;

    return i->second;
}

void License::addReservation(Reservation *reservation)
{
    if (!reservation)
        return;

    if (reservation->id().empty())
        return;

    m_reservations.emplace(reservation->id(), reservation);
}

bool License::removeReservation(const std::string &reservationId)
{
    auto i = m_reservations.find(reservationId);
    if (i == m_reservations.end())
        return false;

    delete i->second;
    m_reservations.erase(i);
    return true;
}

const std::set<License::Consumer> &License::consumers() const
{
    return m_consumers;
}

const License::Consumer *License::consumer(const std::string &consumerId, const std::string &consumerVersion,
    const std::string &installationSchema) const
{
    for (auto &consumer : m_consumers) {
        // Look for a matching consumer
        if (consumer.id == consumerId) {
            // TODO: we might want to enable (semantic) version checks:
            //     && (consumerVersion.empty() || utils::compareVersion(consumer.version, consumerVersion, consumerVersion.size()) == 0)) {
            // TODO: no schema check yet
            return &consumer;
        }
    }

    return nullptr;
}

const JsonHandler *License::data() const
{
    return &m_data;
}


bool License::Consumer::operator<(const Consumer &other) const
{
    return id < other.id || (id == other.id && version < other.version);
}

bool License::Consumer::operator==(const Consumer &other) const
{
    return id == other.id && version == other.version;
}

} // namespace QLicenseService
