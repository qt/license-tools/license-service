/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "licenseservice.h"

#include <errors.h>
#include <utils.h>

using namespace QLicenseCore;

namespace QLicenseService {

LicenseService::LicenseService(DaemonRunMode daemonMode, uint16_t tcpPort, const std::string &workDir)
    : AsyncTask<void>()
    , m_daemonRunMode(daemonMode)
    , m_tcpPort(tcpPort)
    , m_workDir(workDir)
{
    m_properties |= Properties::Cancelable;
}

void LicenseService::run()
{
    utils::setBlockSignalsMask();
    try {
        Licenser licenser(m_daemonRunMode, m_tcpPort, m_workDir);
        updateAndNotifyStatus(Status::Running);

        while (1) {
            licenser.listen();

            if (isCancelRequested() || licenser.isFinished()) {
                licenser.shutdown();
                // Wait until all pending requests are handled
                while (licenser.hasPendingRequests())
                    licenser.listen();

                updateAndNotifyStatus(Status::Canceled);
                return;
            }
        }
    } catch (const QLicenseCore::Error &e) {
        setError("Caught exception: " + std::string(e.what()));
        updateAndNotifyStatus(Status::Error);
        return;
    } catch (const std::exception &e) {
        setError("Caught exception: " + std::string(e.what()));
        updateAndNotifyStatus(Status::Error);
        return;
    } catch (...) {
        setError("Caught unknown exception");
        updateAndNotifyStatus(Status::Error);
        return;
    }

    // Never reached
    updateAndNotifyStatus(Status::Finished);
}

} // namespace QLicenseService
