/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "request.h"
#include "license.h"
#include "licensecache.h"
#include "status.h"
#include "licenser.h"

#include <licdsetup.h>

#include <string>

namespace QLicenseService {

class ClientHandler
{
public:
    explicit ClientHandler(const RequestInfo &request, const QLicenseCore::LicdSetup &settings,
         LicenseCache &cache);
    ~ClientHandler();

    QLicenseCore::Status parseRequest(const std::string &input, std::string &reply);
    QLicenseCore::Status parseResponse(std::string &response);

    Reservation *findCachedReservation(std::string &response, Licenser::DaemonState state);
    void revokeCurrentReservation();

    RequestInfo request();
    ConsumerAppInfo consumerInfo() const;
    int socketId() const;
    std::string reservationId() const;
    License *license() const;

private:
    bool buildRequestJson();
    QLicenseCore::Status buildResponseJson(Reservation *reservation, std::string &response);
    QLicenseCore::Status preParseResponse(std::string &response);

    License* updateExistingLicense(std::string &jsonData);
    QLicenseCore::Status addNewReservation(License *license, std::string &jsonData);

private:
    LicenseCache &m_cache;

    License *m_license;
    std::string m_reservationId;

    RequestInfo m_request;
    QLicenseCore::LicdSetup m_settings;
};

} // namespace QLicenseService
