/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/
#pragma once

#include "status.h"

#include <map>
#include <unordered_map>
#include <future>

#if _WIN32
    #include "curl/curl.h"
#else
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <unistd.h>
    #include <netdb.h>
    #include <curl/curl.h>
#endif

namespace QLicenseService {

class HttpRequest;

struct HttpResponse
{
    std::string body;
    std::map<std::string, std::string> headers;
    long code = 0;
};

class HttpResult
{
public:
    HttpResult(HttpRequest *request);
    ~HttpResult();

    HttpRequest *request() const;
    HttpResponse response() const;
    long code() const;

    CURLcode error() const;
    std::string errorString() const;

    bool deleteRequest() const;
    void setDeleteRequest(bool which);

protected:
    std::map<std::string, std::string> parseHeaders(const std::string &headers) const;

private:
    friend class HttpRequest;

private:
    HttpRequest *const m_request;
    bool m_deleteRequest;

    long m_code;
    std::string m_reply;
    std::string m_headers;
    CURLcode m_error;
};

class HttpRequest
{
public:
    enum class CertRevokeOption {
        Strict = 0,
        BestEffort = 1,
        NoRevoke = 2,
        Default = Strict
    };

    HttpRequest(const std::string &url, const std::string &authKey="");
    ~HttpRequest();

    HttpResult *doRequest(const std::string &payload);
    void setDelay(uint32_t msecs);
    void setForceHttps(bool https);
    void setCaBundlePath(const std::string &path);
    void setRequestTimeout(long timeoutSecs);
    void setCertRevokeBehavior(CertRevokeOption option);

public: // event handlers / callbacks
    std::function<void(std::string)> onResultReady;
    std::function<void(std::string)> onError;

private:
   static size_t writeCallback(char *contents, size_t size, size_t nmemb, void *userp);
   static int debugCallback(CURL *handle, curl_infotype type, char *data, size_t size, void *userp);

private:
    const std::string m_url;
    uint32_t m_delay;
    bool m_forceHttps;
    long m_requestTimeout;
    std::string m_caBundlePath;
    CertRevokeOption m_certRevokeBehavior;

    CURL *m_curl;
    curl_slist *m_headers;
};

class HttpClient
{
public:
    explicit HttpClient(const std::string &serverUrl);
    ~HttpClient();

    void send(uint16_t &clientId, const std::string &payload, const std::string &accessPoint,
        const std::string &server, const std::string &authKey="", uint32_t delay = 0);
    bool sendReceive(HttpResponse &response, const std::string &payload, const std::string &accessPoint,
        const std::string &server, const std::string &authKey="");

    QLicenseCore::Status receive(uint16_t &clientId, HttpResponse &response);

    void setServerUrl(const std::string &serverUrl);
    void setForceHttps(bool https);
    void setCABundlePath(const std::string &path);
    void setRequestTimeout(long timeoutSecs);
    void setCertRevokeBehavior(HttpRequest::CertRevokeOption option);

private:
    bool m_forceHttps;
    long m_requestTimeout;
    std::string m_caBundlePath;
    HttpRequest::CertRevokeOption m_certRevokeBehavior;

    std::string m_serverUrl;
    std::unordered_map<uint16_t, std::future<HttpResult *>> m_futures;
};

} // namespace QLicenseService
