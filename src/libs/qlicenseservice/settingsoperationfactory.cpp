/* Copyright (C) 2025 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "settingsoperationfactory.h"

#include <constants.h>

using namespace QLicenseCore;

namespace QLicenseService {

SettingsOperationFactory &SettingsOperationFactory::instance()
{
    static SettingsOperationFactory factory;
    return factory;
}

void SettingsOperationFactory::registerDefaultCallback(const std::string &key
    , SettingsOperation::Callback callback)
{
    m_callbacks.insert(std::make_pair<const std::string &, SettingsOperation::Callback &>(key, callback));
}

SettingsOperation *SettingsOperationFactory::create(const std::string &key
    , QLicenseCore::LicdSetup &settings) const
{
    return AbstractFactory::create(key, settings);
}

SettingsOperation::Callback SettingsOperationFactory::defaultCallback(const std::string &key) const
{
    auto it = m_callbacks.find(key);
    if (it == m_callbacks.cend())
        return SettingsOperation::Callback();

    return it->second;
}

} // nomespace QLicenseService
