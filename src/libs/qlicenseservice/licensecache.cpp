/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "licensecache.h"

#include "commonsetup.h"
#include "version.h"
#include "crypto.h"

#include <errors.h>
#include <utils.h>
#include <licdsetup.h>

using namespace QLicenseCore;

namespace QLicenseService {

static const char sc_lockFile[] = "cache.lock";


LicenseCache::LicenseCache(Context context)
    : m_valid(false)
{
    if (context == Context::System) {
        m_path = std::string(SYSTEM_WORK_DIR) + ROOT_CACHE_DIR;
    } else if (context == Context::User) {
        m_path = QLicenseCore::LicdSetup::getQtAppDataLocation() + USER_CACHE_DIR;
    } else {
        // should never be reached
        throw Error("Unknown cache context.");
    }
}

LicenseCache::~LicenseCache()
{
    if (!m_valid)
        return;

    // Flush all licenses to make any new data persistent, i.e. unsent statistics
    for (auto &i : m_licenses) {
        for (auto &j : i.second)
            flush(j.second.get());
    }

    invalidate();
}

bool LicenseCache::init()
{
    if (!m_licenses.empty())
        throw Error("Cannot initialize already initialized cache.");

    if (!utils::fileExists(m_path) && !utils::createDir(m_path)) {
        m_error = "Cannot create directory \"" + m_path + "\" for cache.";
        return false;
    }

    if (m_lock && !m_lock->tryUnlock()) {
        m_error = "Cannot initialize cache: " + m_lock->error();
        return false;
    }

    m_lock.reset(new QLicenseCore::LockFile(m_path + DIR_SEPARATOR + sc_lockFile));
    if (!m_lock->tryLock()) {
        m_error = "Cannot initialize cache: " + m_lock->error();
        return false;
    }

    readFromDisk();

    m_valid = true;
    return true;
}

bool LicenseCache::clear()
{
    if (!m_valid) {
        m_error = "Cannot clear invalid cache.";
        return false;
    }

    invalidate();
    if (!utils::removeDir(m_path))
        return false;

    return true;
}

/*
    Flushes current contents of the \a license to disk. This updates any
    added or removed reservations for the license, after the initial inserting
    to cache.

    Returns \c true on success. Otherwise aborts on first error and returns \c false.
*/
bool LicenseCache::flush(const License *const license)
{
    if (!m_valid) {
        m_error = "Cannot flush invalid cache.";
        return false;
    }

    const std::string userDir = m_path + DIR_SEPARATOR + license->localUser();
    const std::string licenseDir = userDir + DIR_SEPARATOR + license->id();

    // 1. Delete existing files for this license
    if (utils::fileExists(licenseDir) && (!utils::removeDir(licenseDir))) {
        m_error = "Cannot remove license directory: " + licenseDir;
        return false;
    }

    // 2. Then populate the file structure again with current data
    if (!writeLicense(license))
        return false;

    for (auto *reservation : license->reservations()) {
        if (!writeReservation(reservation))
            return false;
    }

    return true;
}

/*
    Flushes contents of the \a reservation and its associated license to disk. This
    will add persistent data for new reservation if required or update an existing reservation.

    Returns \c true on success. Otherwise aborts on first error and returns \c false.
*/
bool LicenseCache::flush(const Reservation *const reservation)
{
    if (!m_valid) {
        m_error = "Cannot flush invalid cache.";
        return false;
    }

    License *license = reservation->license();
    if (!license) {
        m_error = "Could not find license attached to reservation " + reservation->id();
        return false;
    }

    const std::string userDir = m_path + DIR_SEPARATOR + license->localUser();
    const std::string licenseDir = userDir + DIR_SEPARATOR + license->id();
    const std::string reservationDir = licenseDir + DIR_SEPARATOR + reservation->id();

    // 1. Delete existing files for this reservation
    if (utils::fileExists(reservationDir) && (!utils::removeDir(reservationDir))) {
        m_error = "Cannot remove reservation directory: " + reservationDir;
        return false;
    }

    // 2. Then populate the file structure again with current data
    if (!writeLicense(license)) // overrides existing contents
        return false;

    return writeReservation(reservation);
}

bool LicenseCache::isValid() const
{
    return m_valid;
}

std::string LicenseCache::error() const
{
    return m_error;
}

std::string LicenseCache::path() const
{
    return m_path;
}

/*
    Overrides context specific path.
*/
bool LicenseCache::setPath(const std::string &path)
{
    // do not allow changing path for already initialized cache
    if (m_valid)
        return false;

    m_path = path;
    return true;
}

License *LicenseCache::license(const std::string &user, const std::string &licenseId) const
{
    auto i = m_licenses.find(user);
    if (i == m_licenses.end()) {
        m_error = "No license user found with id: " + user;
        return nullptr;
    }

    auto j = i->second.find(licenseId);
    if (j == i->second.end()) {
        m_error = "No cached license found with id: " + licenseId;
        return nullptr;
    }

    return j->second.get();
}

/*
    Returns licenses for given \a user, if not empty, or all licenses in cache.
*/
std::vector<License *> LicenseCache::licenses(const std::string &user) const
{
    std::vector<License *> licenses;

    if (!user.empty()) {
        auto i = m_licenses.find(user);
        if (i == m_licenses.end()) {
            m_error = "No license user found with id: " + user;
            return licenses;
        }

        for (auto &j : i->second)
            licenses.push_back(j.second.get());
    } else {
        for (auto &i : m_licenses) {
            for (auto &j : i.second)
                licenses.push_back(j.second.get());
        }
    }

    return licenses;
}

uint64_t LicenseCache::licenseCount() const
{
    uint64_t count = 0;
    for (auto &i : m_licenses)
        count += i.second.size();

    return count;
}

License *LicenseCache::licenseByConsumer(const std::string &user, const ConsumerAppInfo &consumer) const
{
    auto i = m_licenses.find(user);
    if (i == m_licenses.end()) {
        m_error = "No license user found with id: " + user;
        return nullptr;
    }

    std::vector<License *> orderedLicenses;
    for (auto &j : i->second) {
        License *license = j.second.get();
        if (license->consumer(consumer.id, consumer.version, consumer.installationSchema)) {
            if (license->isPerpetual() && license->isExpired()
                    && !consumer.buildTimestamp.empty()) {
                time_t buildTime = utils::stringToUtcTime(consumer.buildTimestamp.c_str(), "%Y-%m-%d");
                if (buildTime == -1) {
                    logError("Failed to convert consumer build time stamp \"%s\" to epoch time. "
                        "Cannot use license with ID %s for this consumer.",
                        consumer.buildTimestamp.c_str(), license->id().c_str());
                    continue;
                }
                // Check whether this consumer was "released" before the perpetual license expired
                if (buildTime > license->validToDate())
                    continue;
            }

            orderedLicenses.push_back(license);
        }
    }

    if (orderedLicenses.empty())
        return nullptr;

    std::sort(orderedLicenses.begin(), orderedLicenses.end(),
        [](const License* lhs, const License* rhs) {
            return lhs->priority() > rhs->priority();
        }
    );

    // Return license with highest priority
    return orderedLicenses.front();
}

License *LicenseCache::licenseByReservation(const std::string &reservationId) const
{
    for (const auto &licenseMap : m_licenses) {
        auto it = std::find_if(licenseMap.second.begin(), licenseMap.second.end(),
            [reservationId](const decltype(licenseMap.second)::value_type &pair) {
                return pair.second->reservation(reservationId);
            }
        );

        if (it != licenseMap.second.end())
            return it->second.get();
    }

    return nullptr;
}

/*
    Inserts a \a license into cache. The cache takes ownership of the pointed
    license object.
*/
bool LicenseCache::insert(License *license)
{
    if (!m_valid) {
        m_error = "Cannot insert license to invalid cache.";
        return false;
    }

    if (LicenseCache::license(license->localUser(), license->id())) {
        if (!remove(license->localUser(), license->id())) {
            m_error = "Cannot remove existing license from cache.";
            return false;
        }
    }

    if (!writeLicense(license))
        return false;

    for (auto *reservation : license->reservations()) {
        if (!writeReservation(reservation))
            return false;
    }

    m_licenses[license->localUser()].insert(std::make_pair(license->id(), std::move(std::unique_ptr<License>(license))));
    return true;
}

/*
    Inserts a license from \a filePath to cache for \a localUser. Returns a pointer to
    the inserted license on success, \c nullptr on failure.
*/
License *LicenseCache::insertLocalLicense(const std::string &filePath, const std::string &localUser)
{
    if (!m_valid) {
        m_error = "Cannot insert local license to invalid cache.";
        return nullptr;
    }

    std::string jsonData;
    if (!utils::readFile(jsonData, filePath)) {
        m_error = "Unable to read license file: " + filePath;
        return nullptr;
    }

    License *license = new License(localUser);
    std::unique_ptr<License> guard(license);

    Status result = license->parseFromJson(jsonData);
    if (result != Status::SUCCESS) {
        m_error = "Invalid license format: " + filePath;
        return nullptr;
    }

    if (!license->isValidFromStarted()) {
        m_error = "License valid from date is in future: " + license->validFromDateString();
        return nullptr;
    }

    time_t validToDate = license->validToDate();
    if (license->isExpired()) {
        if (!license->isPerpetual()) {
            m_error = "License is expired on: " + license->validToDateString();
            return nullptr;
        }
        // validToDate is in past, so we need to add our own for reservation
        validToDate = utils::utcTime() + license->leaseTimeSeconds();
    }

    // Generate local reservation for the license
    std::unique_ptr<Reservation> reservation(new Reservation(license, utils::generateUuid(),
        utils::utcTime(), validToDate, Reservation::Flags::LocalOnly));

    license->addReservation(reservation.release());

    if (!insert(license)) {
        m_error = "Error while storing license " + license->id() + ": " + error();
        return nullptr;
    }

    // Cache now manages the object
    guard.release();

    return license;
}

bool LicenseCache::remove(const std::string &user, const std::string &licenseId)
{
    if (!m_valid) {
        m_error = "Cannot remove item from invalid cache.";
        return false;
    }

    auto i = m_licenses.find(user);
    if (i == m_licenses.end()) {
        m_error = "No license user found with id: " + user;
        return false;
    }

    auto j = i->second.find(licenseId);
    if (j == i->second.end()) {
        m_error = "No cached license found with id: " + licenseId;
        return false;
    }

    const License *license = j->second.release();
    // Remove from license tree
    i->second.erase(j);

    const std::string userDir = m_path + DIR_SEPARATOR + license->localUser();
    const std::string licensePath = userDir + DIR_SEPARATOR + license->id();

    delete license;

    if (utils::fileExists(licensePath) && (!utils::removeDir(licensePath))) {
        m_error = "Cannot remove license directory: " + licensePath;
        return false;
    }

    if (utils::getDirListing(userDir, "", DT_DIR).empty()) {
        if (!utils::removeDir(userDir)) // not fatal if fails
            logWarn("Could not remove directory %s", userDir.c_str());
    }

    return true;
}

/*
    Removes the persistent cached files for \a reservationId, owned by \a user and \a licenseId.
    This does not delete any reservation object itself, which are managed by their parent license.

    Returns \c true on success, \c false otherwise.
*/
bool LicenseCache::remove(const std::string &user, const std::string &licenseId, const std::string &reservationId)
{
    if (!m_valid) {
        m_error = "Cannot remove reservation from invalid cache.";
        return false;
    }

    auto i = m_licenses.find(user);
    if (i == m_licenses.end()) {
        m_error = "No license user found with id: " + user;
        return false;
    }

    auto j = i->second.find(licenseId);
    if (j == i->second.end()) {
        m_error = "No cached license found with id: " + licenseId;
        return false;
    }

    const std::string userDir = m_path + DIR_SEPARATOR + user;
    const std::string licenseDir = userDir + DIR_SEPARATOR + licenseId;
    const std::string reservationDir = licenseDir + DIR_SEPARATOR + reservationId;

    if (utils::fileExists(reservationDir) && (!utils::removeDir(reservationDir))) {
        m_error = "Cannot remove reservation directory: " + reservationDir;
        return false;
    }

    return true;
}

/*
    Returns all reservations in cache.
*/
std::set<Reservation *> LicenseCache::reservations() const
{
    std::set<Reservation *> allReservations;

    for (const auto &licenseMap : m_licenses) {
        for (const auto &license : licenseMap.second) {
            const std::set<Reservation *> reservations = license.second->reservations();
            for (auto *res : reservations) {
                if (!res)
                    continue;

                allReservations.insert(res);
            }
        }
    }

    return allReservations;
}

Reservation *LicenseCache::reservation(const std::string &reservationId)
{
    for (const auto &licenseMap : m_licenses) {
        for (const auto &license : licenseMap.second) {
            const std::set<Reservation *> reservations = license.second->reservations();
            for (auto *res : reservations) {
                if (!res)
                    continue;

                if (res->id() == reservationId)
                    return res;
            }
        }
    }

    return nullptr;
}

void LicenseCache::invalidate()
{
    m_licenses.clear();

    if (m_lock && !m_lock->tryUnlock())
        m_error = "Error while invalidating cache: " + m_lock->error();

    m_valid = false;
}

void LicenseCache::readFromDisk()
{
    const std::vector<std::string> cacheUserDirs = utils::getDirListing(m_path, "", DT_DIR);
    for (auto &userDir : cacheUserDirs) {
        const std::string userPath = m_path + DIR_SEPARATOR + userDir;
        const std::vector<std::string> licenseDirs
            = utils::getDirListing(userPath, "", DT_DIR);
        for (auto &licenseDir : licenseDirs) {
            const std::string licensePath = userPath + DIR_SEPARATOR + licenseDir;
            readLicenses(licensePath, userDir);
        }
    }
}

void LicenseCache::readLicenses(const std::string &path, const std::string &user)
{
    const std::vector<std::string> licenseFiles
        = utils::getDirListing(path, "license_");

    for (auto &licenseFile : licenseFiles) {
        const std::string licenseFilePath = path + DIR_SEPARATOR + licenseFile;

        std::string licenseData;
        if (!utils::readFile(licenseData, licenseFilePath)) {
            logWarn("Cannot read license file. Deleting: %s", licenseFilePath.c_str());
            if (!utils::removeDir(path))
                logWarn("Could not remove directory %s", path.c_str());
            continue;
        }

        try {
            // insert to license tree, transfer ownership from method
            std::unique_ptr<License> license(new License(user, licenseData));
            License *ptr = license.get();

            std::vector<std::string> reservationDirs
                = utils::getDirListing(path, "", DT_DIR);

            for (auto &reservationDir: reservationDirs) {
                const std::string reservationPath = path + DIR_SEPARATOR + reservationDir;
                const std::string reservationFilePath = reservationPath + DIR_SEPARATOR
                    + "reservation_" + reservationDir + ".json";

                std::string reservationData;
                if (!utils::readFile(reservationData, reservationFilePath)) {
                    logWarn("Cannot read reservation file. Deleting: %s", reservationFilePath.c_str());
                    if (!utils::deleteFile(reservationFilePath))
                        logError("Unable to remove file: %s", reservationFilePath.c_str());
                    continue;
                }

                std::unique_ptr<Reservation> reservation(new Reservation(license.get()));
                Status result = reservation->parseFromJson(reservationData);
                // Allow other error results, as this can be an expired reservation
                // that we could try releasing.
                if (result == Status::INVALID_RESPONSE) {
                    logError("Ignoring and removing invalid reservation data: \"%s\"", reservationFilePath.c_str());
                    if (!utils::deleteFile(reservationFilePath))
                        logError("Unable to remove file: %s", reservationFilePath.c_str());
                    continue;
                }

                const std::string reservationSHAPath = reservationPath + DIR_SEPARATOR + reservation->id() + ".sha256";
                std::string error;
                if (!verifyReservation(*reservation.get(), reservationFilePath, reservationSHAPath, &error)) {
                    logError("Ignoring and removing invalid reservation data: \"%s\": %s", reservationFilePath.c_str(), error.c_str());
                    if (!utils::deleteFile(reservationFilePath))
                        logError("Unable to remove file: %s", reservationFilePath.c_str());
                    if (!utils::deleteFile(reservationSHAPath))
                        logError("Unable to remove file: %s", reservationSHAPath.c_str());
                    continue;
                }

                const std::string consumersPath = reservationPath + DIR_SEPARATOR + "attached_consumers_" + reservation->id() + ".json";
                if (!readAttachedConsumers(*reservation.get(), consumersPath, &error))
                    logWarn("Could not read attached consumers data: \"%s\": %s", consumersPath.c_str(), error.c_str());

                const std::string statisticsPath = reservationPath + DIR_SEPARATOR + "statistics_" + reservation->id() + ".json";
                if (!readStatistics(*reservation.get(), statisticsPath, &error))
                    logWarn("Could not read statistics data: \"%s\": %s", statisticsPath.c_str(), error.c_str());

                license->addReservation(reservation.release());
            }

            m_licenses[ptr->localUser()].insert(std::make_pair(ptr->id(), std::move(license)));
        } catch (const Error &e) {
            logError("Ignoring and removing invalid license: %s", licenseFilePath.c_str());
            logError("Reason: %s", e.what());
            if (!utils::removeDir(path))
                logError("Unable to remove folder: %s", path.c_str());
        }
    }
}

bool LicenseCache::writeLicense(const License *const license)
{
    const std::string subdir = m_path + DIR_SEPARATOR + license->localUser()
        + DIR_SEPARATOR + license->id();

    if (!utils::createDir(subdir)) {
        m_error = "Cannot create subdirectory " + subdir + " for cache.";
        return false;
    }

    const std::string licensePath = subdir + DIR_SEPARATOR + "license_" + license->id() + ".json";
    const std::string licenseData = license->data()->dump(4);
    if (licenseData.empty()) {
        m_error = "Could not stringify license data for license id: " + license->id();
        return false;
    }

    if (!utils::writeToFile(licensePath, licenseData)) {
        m_error = "Could not write to license file:" + licensePath;
        return false;
    }

    return true;
}

bool LicenseCache::writeReservation(const Reservation *const reservation)
{
    License *license = reservation->license();
    const std::string subdir = m_path + DIR_SEPARATOR + license->localUser()
        + DIR_SEPARATOR + license->id() + DIR_SEPARATOR + reservation->id();

    if (!utils::createDir(subdir)) {
        m_error = "Cannot create subdirectory " + subdir + " for cache.";
        return false;
    }

    const std::string reservationPath = subdir + DIR_SEPARATOR
        + "reservation_" + reservation->id() + ".json";
    const std::string reservationSHAPath = subdir + DIR_SEPARATOR
        + reservation->id() + ".sha256";
    const std::string consumersPath = subdir + DIR_SEPARATOR
        + "attached_consumers_" + reservation->id() + ".json";
    const std::string statisticsPath = subdir + DIR_SEPARATOR
        + "statistics_" + reservation->id() + ".json";

    const std::string reservationData = reservation->toString(4);
    if (!utils::writeToFile(reservationPath, reservationData)) {
        // Remove license directory to avoid inconsistent state, though
        // it means persistent data for current license is lost.
        if (!utils::removeDir(subdir))
            logWarn("Could not remove directory %s", subdir.c_str());
        m_error = "Could not write to reservation file:" + reservationPath;
        return false;
    }

    nlohmann::json reservationJson;
    if (!reservation->data()->get("reservation", reservationJson)) {
        if (!utils::removeDir(subdir))
            logWarn("Could not remove directory %s", subdir.c_str());
        m_error = "Cannot parse reservation object from JSON for reservation file: " + reservationPath
            + " License id: " + license->id() + " Reservation id: " + reservation->id();
        return false;
    }

    const std::string reservationString = reservationJson.dump();
    if (!utils::writeToFile(reservationSHAPath, Crypto::sha256(reservationString))) {
        if (!utils::removeDir(subdir))
            logWarn("Could not remove directory %s", subdir.c_str());
        m_error = "Could not write to reservation SHA:" + reservationSHAPath
            + " License id: " + license->id() + " Reservation id: " + reservation->id();
        return false;
    }

    std::string error;
    if (!writeAttachedConsumers(*reservation, consumersPath, &error)) {
        // Failure here means clients perform additional license request, not fatal
        logWarn("Could not write attached consumer consumers data: %s License id: %s "
            "Reservation id: %s Error: %s", consumersPath.c_str(), license->id().c_str(),
            reservation->id().c_str(), error.c_str());
    }

    if (!writeStatistics(*reservation, statisticsPath, &error)) {
        // Not fatal enough to yield
        logWarn("Could not write reservation statistics: %s License id: %s "
            "Reservation id: %s Error: %s", statisticsPath.c_str(), license->id().c_str(),
            reservation->id().c_str(), error.c_str());
    }

    return true;
}

bool LicenseCache::verifyReservation(const Reservation &reservation, const std::string &filePath, const std::string &shaPath, std::string *error)
{
    try {
        nlohmann::json reservationJson;
        if (!reservation.data()->get("reservation", reservationJson))
            throw Error("Could not parse reservation JSON object. License id: "
                + reservation.license()->id() + " Reservation id: " + reservation.id());

        std::string expectedSHA256;
        if (!utils::readFile(expectedSHA256, shaPath))
            throw Error("Could not read checksum file. License id: "
                + reservation.license()->id() + " Reservation id: " + reservation.id());

        utils::trimStr(expectedSHA256);
        const std::string reservationString = reservationJson.dump();
        if (expectedSHA256 != Crypto::sha256(reservationString))
            throw Error("Reservation checksum mismatch. License id: "
                + reservation.license()->id() + " Reservation id: " + reservation.id());

    } catch (const QLicenseCore::Error &e) {
        *error = e.what();
        return false;
    }

    return true;
}

bool LicenseCache::readStatistics(Reservation &reservation, const std::string &filePath, std::string *error)
{
    try {
        if (reservation.isLocalOnly())
            return true;

        if (!utils::fileExists(filePath))
            return true;

        std::string statisticsData;
        if (!utils::readFile(statisticsData, filePath)) {
            throw Error("Could not read statistics file. License id: "
                + reservation.license()->id() + " Reservation id: " + reservation.id());
        }

        nlohmann::json json = nlohmann::json::parse(statisticsData);
        nlohmann::json consumerRecordArray = json["consumer_runtime_records"];
        for (const auto &record : consumerRecordArray) {
            UsageStatistics statistics(record);
            // Mark completed to avoid leaving incomplete statistics hanging in cache in cases
            // of crashes, etc. If the daemon is (re)started, we know any previous client
            // application usage was interrupted as well.
            if (!statistics.isComplete()) {
                logWarn("Found incomplete usage statistics from cache:\n%s", record.dump(4).c_str());
                statistics.setComplete(true);
            }

            if (statistics.startTime() != UsageStatistics::time_point{})
                reservation.addStatistic(statistics);
            else
                logWarn("Corrupted usage statistic for reservation %s", reservation.id().c_str());
        }
    } catch (const nlohmann::json::exception &e) {
        *error = e.what();
        return false;
    } catch (const QLicenseCore::Error &e) {
        *error = e.what();
        return false;
    }

    return true;
}

bool LicenseCache::writeStatistics(const Reservation &reservation, const std::string &filePath, std::string *error)
{
    try {
        const std::vector<UsageStatistics> usageStatistics = reservation.statistics();
        if (usageStatistics.empty())
            return true;

        nlohmann::json consumerRecordArray;
        for (const auto &statistic : usageStatistics)
            consumerRecordArray.push_back(statistic.toJson());

        nlohmann::json json;
        json["consumer_runtime_records"] = consumerRecordArray;

        const std::string data = json.dump(4);
        if (!utils::writeToFile(filePath, data)) {
            *error = "Failed to write file %s", filePath.c_str();
            return false;
        }
    } catch (const nlohmann::json::exception &e) {
        *error = e.what();
        return false;
    }

    return true;
}

bool LicenseCache::readAttachedConsumers(Reservation &reservation, const std::string &filePath, std::string *error)
{
    try {
        if (!utils::fileExists(filePath))
            return true;

        std::string consumersData;
        if (!utils::readFile(consumersData, filePath)) {
            throw Error("Could not read consumers file. License id: "
                + reservation.license()->id() + " Reservation id: " + reservation.id());
        }

        nlohmann::json json = nlohmann::json::parse(consumersData);
        nlohmann::json consumersArray = json["attached_consumers"];
        for (const auto &consumerJsonObject : consumersArray) {
            ConsumerAppInfo consumer(consumerJsonObject);
            if (!consumer.id.empty() && !consumer.version.empty())
                reservation.attachConsumer(consumer);
            else
                logWarn("Corrupted consumer application information for reservation %s", reservation.id().c_str());
        }
    } catch (const nlohmann::json::exception &e) {
        *error = e.what();
        return false;
    } catch (const QLicenseCore::Error &e) {
        *error = e.what();
        return false;
    }

    return true;
}

bool LicenseCache::writeAttachedConsumers(const Reservation &reservation, const std::string &filePath, std::string *error)
{
    try {
        const std::vector<ConsumerAppInfo> consumers = reservation.attachedConsumers();
        if (consumers.empty())
            return true;

        nlohmann::json consumerArray;
        for (const auto &consumer : consumers)
            consumerArray.push_back(consumer.toJson());

        nlohmann::json json;
        json["attached_consumers"] = consumerArray;

        const std::string data = json.dump(4);
        if (!utils::writeToFile(filePath, data)) {
            *error = "Failed to write file %s", filePath.c_str();
            return false;
        }
    } catch (const nlohmann::json::exception &e) {
        *error = e.what();
        return false;
    }

    return true;
}

} // namespace QLicenseService
