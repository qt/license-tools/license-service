/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "cipcommandparser.h"

#include <utils.h>
#include <errors.h>
#include <logger.h>

using namespace QLicenseCore;

namespace QLicenseService {

CipCommandParser::CipCommandParser(const std::string &input, const std::string &daemonVersion)
    : m_inputStr(input)
    , m_daemonVersion(daemonVersion)
{
}

bool CipCommandParser::parseInput()
{
    if (!findProtocolVersion())
        return false;

    try {
        ParseCipFunction parseMessage = findParser();
        return parseMessage();
    } catch (const QLicenseCore::Error &e) {
        m_error = e.what();
        return false;
    }
}

std::string CipCommandParser::error() const
{
    return m_error;
}

std::string CipCommandParser::command() const
{
    return m_params.command;
}

std::string CipCommandParser::protocolVersion() const
{
    return m_params.protocolVersion;
}

std::string CipCommandParser::clientLibraryVersion() const
{
    return m_params.clientLibraryVersion;
}

std::string CipCommandParser::consumerId() const
{
    return m_params.consumerId;
}

std::string CipCommandParser::consumerVersion() const
{
    return m_params.consumerVersion;
}

std::string CipCommandParser::consumerInstallationSchema() const
{
    return m_params.consumerInstallationSchema;
}

std::string CipCommandParser::consumerSupportedFeatures() const
{
    return m_params.consumerSupportedFeatures;
}

std::string CipCommandParser::consumerProcessId() const
{
    return m_params.consumerProcessId;
}

std::string CipCommandParser::consumerBuildTimestamp() const
{
    return m_params.consumerBuildTimestamp;
}

std::string CipCommandParser::jwtToken() const
{
    return m_params.jwtToken;
}

std::string CipCommandParser::userId() const
{
    return m_params.userId;
}

std::string CipCommandParser::settingsKey() const
{
    return m_params.settingsKey;
}

std::string CipCommandParser::settingsValue() const
{
    return m_params.settingsValue;
}

QLicenseCore::SettingsType CipCommandParser::settingsType() const
{
    return static_cast<SettingsType>(utils::strToInt(m_params.settingsType));
}

bool CipCommandParser::findProtocolVersion()
{
    std::vector<std::string> paramList = utils::splitStr(m_inputStr, ' ');
    if (paramList.size() < 2) {
        m_error = "Not enough parameters to be able to parse the request";
        return false;
    }
    if (paramList[0] != "-pv") {
        m_error = "Invalid request: Protocol version must be first";
        return true;
    }

    const std::string pVersion = paramList[1];
    // 12 characters should be enough for version number (semantic) max length in this context
    if (pVersion.length() < 3 || pVersion.length() > 12) {
        m_error = "Invalid request: Protocol version length does not meet the requirements: "
                + pVersion;
        return false;
    }

    if (!utils::isVersionNum(pVersion)) {
        m_error = "Invalid request: Protocol version not formatted correctly: " + pVersion;
        return false;
    }

    m_params.protocolVersion = paramList[1];
    return true;
}

/*
    Tries to find the least major version parser supporting the message protocol
    and returns the parser method. Throws QLicenseService::Error on failure.
*/
ParseCipFunction CipCommandParser::findParser() const
{
    for (auto &parser : m_cipParsers) {
        if (utils::compareVersion(m_params.protocolVersion, parser.first, 1) == 0)
            return parser.second;
    }

    throw QLicenseCore::Error("No message parser found for CIP protocol version: " + m_params.protocolVersion);
}

bool CipCommandParser::parseCipV10()
{
    static const std::unordered_set<std::string> sc_acceptedParams {
        "-pv", "-c", "-o", "-a", "-v", "-i", "-f", "-u", "-jwt",
        "-pid","-bts", "-key", "-value", "-settings-type"
    };


    /* Exapmple input:
        -pv <protocol_version> -c <CIP_version> -o license -u <user_id> -jwt <jwt_token> -pid <process_id>
        -a <consumer_id> -v <consumer_version> -i <commercial|opensource>
    */

    auto splitParameters = [](const std::string &input) {
        std::map<std::string, std::string> parameters;

        size_t pos = 0;
        while (pos < input.size()) {
            // Find the start of the next option
            size_t optionStart = input.find('-', pos);
            if (optionStart == std::string::npos)
                break;

            // Find the end of the current option
            size_t optionEnd = input.find(' ', optionStart);
            if (optionEnd == std::string::npos)
                optionEnd = input.size();

            const std::string option = input.substr(optionStart, optionEnd - optionStart);

            // Find the start of the value
            size_t valueStart = optionEnd + 1;
            while (valueStart < input.size() && input[valueStart] == ' ')
                valueStart++;

            if (valueStart >= input.size())
                break;

            if (input[valueStart] == '-') {
                // Malformed request, found another option instead of value
                parameters.emplace(option, "");
                pos = (valueStart - 1);
            }

            // Find the end of the value
            size_t valueEnd = input.find(" -", valueStart);
            if (valueEnd == std::string::npos)
                valueEnd = input.size();

            const std::string value = input.substr(valueStart, valueEnd - valueStart);

            parameters.emplace(option, value);
            pos = valueEnd;
        }

        return parameters;
    };

    const auto parameters = splitParameters(m_inputStr);

    for (auto &param : parameters) {
        if (sc_acceptedParams.find(param.first) == sc_acceptedParams.end()) {
            logWarn("Ignoring unknown parameter: %s", param.first.c_str());
            continue;
        }

        if (param.second.empty()) {
            m_error = "No value for parameter: " + param.first;
            return false;
        }

        if (param.first == "-o") {
            if (supportedCommands.find(param.second) != supportedCommands.end()) {
                m_params.command = param.second;
            } else {
                m_error = "Invalid or unknown operation: " + param.second;
                return false;
            }
        } else if (param.first == "-jwt") {
            m_params.jwtToken = param.second;
        } else if (param.first == "-pid") {
            m_params.consumerProcessId = param.second;
        } else if (param.first == "-bts") {
            if (utils::stringToUtcTime(param.second.c_str(), "%Y-%m-%d") == -1) {
                logWarn("Failed to convert consumer build time stamp \"%s\" to epoch time. "
                    "The value must follow the format '%%Y-%%m-%%d'", param.second.c_str());
            } else {
                m_params.consumerBuildTimestamp = param.second;
            }
        } else if (param.first == "-pv") {
            m_params.protocolVersion = param.second;
        } else if (param.first == "-c") {
            m_params.clientLibraryVersion = param.second;
        } else if (param.first == "-a") {
            m_params.consumerId = param.second;
        } else if (param.first == "-v") {
            m_params.consumerVersion = param.second;
        } else if (param.first == "-i") {
            m_params.consumerInstallationSchema = param.second;
        } else if (param.first == "-f") {
            m_params.consumerSupportedFeatures = param.second;
        } else if (param.first == "-u") {
            m_params.userId = param.second;
        } else if (param.first == "-key") {
            m_params.settingsKey = param.second;
        } else if (param.first == "-value") {
            m_params.settingsValue = param.second;
        } else if (param.first == "-settings-type") {
            m_params.settingsType = param.second;
        }
    }

    if (m_params.consumerId.empty()) {
        m_error = "No consumer_id found";
        return false;
    }
    if (m_params.consumerVersion.empty()) {
        m_error = "No consumer_version found";
        return false;
    }
    if (m_params.userId.empty()) {
        m_error = "No username found";
        return false;
    }

    return true;
}

} // namespace QLicenseService
