/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "license.h"

#include <lockfile.h>

#include <string>
#include <map>
#include <set>
#include <memory>
#include <vector>

namespace QLicenseService {

class LicenseCache
{
public:
    enum class Context {
        User = 0,
        System = 1
    };

    explicit LicenseCache(Context context);
    ~LicenseCache();

    bool init();
    bool clear();
    bool flush(const License *const license);
    bool flush(const Reservation *const reservation);

    bool isValid() const;
    std::string error() const;

    std::string path() const;
    bool setPath(const std::string &path);

    License *license(const std::string &user, const std::string &licenseId) const;
    std::vector<License *> licenses(const std::string &user = std::string()) const;
    uint64_t licenseCount() const;

    License *licenseByConsumer(const std::string &user, const ConsumerAppInfo &consumer) const;
    License *licenseByReservation(const std::string &reservationId) const;

    bool insert(License *license);
    License *insertLocalLicense(const std::string &filePath, const std::string &localUser);
    bool remove(const std::string &user, const std::string &licenseId);
    bool remove(const std::string &user, const std::string &licenseId, const std::string &reservationId);

    std::set<Reservation *> reservations() const;
    Reservation *reservation(const std::string &reservationId);

private:
    void invalidate();
    void readFromDisk();

    void readLicenses(const std::string &path, const std::string &user);
    bool writeLicense(const License *const license);
    bool writeReservation(const Reservation *const reservation);

    bool verifyReservation(const Reservation &reservation, const std::string &filePath,
        const std::string &shaPath, std::string *error);

    bool readStatistics(Reservation &reservation, const std::string &filePath, std::string *error);
    bool writeStatistics(const Reservation &reservation, const std::string &filePath, std::string *error);

    bool readAttachedConsumers(Reservation &reservation, const std::string &filePath, std::string *error);
    bool writeAttachedConsumers(const Reservation &reservation, const std::string &filePath, std::string *error);

    using LicenseTree = std::map<std::string, std::map<std::string, std::unique_ptr<License>>>;

private:
    std::unique_ptr<QLicenseCore::LockFile> m_lock;

    LicenseTree m_licenses; // <user, <licenseId, LocalLicense>>

    std::string m_path;

    bool m_valid;
    mutable std::string m_error;
};

} // namespace QLicenseService
