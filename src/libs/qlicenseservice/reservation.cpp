/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "reservation.h"

#include "clienthandler.h"
#include "commonsetup.h"
#include "license.h"

#include <pipe.h>

using namespace QLicenseCore;

namespace QLicenseService {

Reservation::Reservation(License *const license)
    : m_license(license)
    , m_flags(Flags::Default)
    , m_pendingForRelease(false)
    , m_leaseValidFromUtc(0)
    , m_leaseValidToUtc(0)
    , m_nextRenewTimeUtc(0)
    , m_leaseRenewalSecsUntilExpiry(0)
{}

Reservation::Reservation(License *const license, const std::string &id, time_t validFrom, time_t validTo,
                         Flags flags, uint64_t leaseRenewalSecsUntilExpiry)
    : m_license(license)
    , m_flags(flags)
    , m_pendingForRelease(false)
    , m_id(id)
    , m_leaseValidFromUtc(validFrom)
    , m_leaseValidToUtc(validTo)
    , m_nextRenewTimeUtc(0)
    , m_leaseRenewalSecsUntilExpiry(leaseRenewalSecsUntilExpiry)
{
    m_data = license->data()->dump();
    m_data.set("reservation.reservation_id", id);
    m_data.set("reservation.lease_valid_from", utils::utcTimeToString(validFrom, "%Y-%m-%dT%H:%M:%S"));
    m_data.set("reservation.lease_valid_to", utils::utcTimeToString(validTo, "%Y-%m-%dT%H:%M:%S"));
    m_data.set("reservation.lease_renewal_time", leaseRenewalSecsUntilExpiry);
    if (static_cast<int>(m_flags & Flags::LocalOnly))
        m_data.set("reservation.local_only", true);
}

Status Reservation::parseFromJson(const std::string &data)
{
    m_data = JsonHandler(data);

    const Status result = m_parser.parse(data, *this);
    if (result != Status::SUCCESS)
        logError(m_parser.error().c_str());

    return result;
}

/*
    Copies reservation data from \a other to this reservation. This only affects data members
    present in the JSON representation of the reservation, internal data such as attached
    client applications are unmodified.

    Returns \c true on success, \c false otherwise.
*/
bool Reservation::copyReservationData(const Reservation *const other)
{
    if (!other) {
        logError("Cannot copy reservation data from empty object");
        return false;
    }

    m_data = *other->data();

    m_id = other->id();
    m_flags = other->m_flags;

    m_leaseValidToUtc = other->m_leaseValidToUtc;
    m_leaseValidFromUtc = other->m_leaseValidFromUtc;
    m_leaseRenewalSecsUntilExpiry = other->m_leaseRenewalSecsUntilExpiry;

    return true;
}

std::string Reservation::toString(uint8_t indent) const
{
    return m_data.dump(indent);
}

std::string Reservation::id() const
{
    return m_id;
}

License *Reservation::license() const
{
    return m_license;
}

/*
    Updates the renewal and expiry time if supported by reservation flags
*/
void Reservation::resetRenewAndExpiryTimers()
{
    const time_t now = utils::utcTime();

    if (static_cast<int>(m_flags & Flags::Renewable) && (m_leaseValidToUtc - now) > 0) {
        uint64_t renewSecs = 0;
        if (m_license && m_license->renewalPolicy() == License::RenewalPolicy::Passive) {
            // In passive mode renewal time is defined as seconds until lease expiration
            renewSecs = ((m_leaseValidToUtc - now) - m_leaseRenewalSecsUntilExpiry);
        } else {
            // In active mode renewal time is half of the *remaining* lease time,
            // but at most *one* day
            renewSecs = ((m_leaseValidToUtc - now) / 2);
            if (renewSecs > SECS_IN_DAY)
                renewSecs = SECS_IN_DAY;
        }

        if (renewSecs < RESERVATION_MIN_RENEW_SECS) {
            logDebug("Skip starting renewal timer for reservation id %s due to renew time %d s "
                "being below supported minimum.", m_id.c_str(), renewSecs);
        } else if (now + renewSecs >= m_leaseValidToUtc) {
            logDebug("Skip starting renewal timer for reservation id %s due to renew time %d s "
                "being equal or past the lease expiry time.", m_id.c_str(), renewSecs);
        } else {
            m_nextRenewTimeUtc = now + renewSecs;

            m_renewalWatchdog.reset(new Watchdog(std::chrono::seconds(renewSecs)));
            m_renewalWatchdog->setCallback([this] {
                Pipe::globalObject()->write(PIPE_MSG_LICENSE_RENEWAL,
                                            Pipe::Type::Watchdog, m_id);
            });

            m_renewalWatchdog->start();
        }
    }

    if (static_cast<int>(m_flags & Flags::Releasable)) {
        const time_t releaseSeconds = (m_leaseValidToUtc - now > 0) ? m_leaseValidToUtc - now : 1;

        m_expiryWatchdog.reset(new Watchdog(std::chrono::seconds(releaseSeconds)));
        m_expiryWatchdog->setCallback([this] {
            Pipe::globalObject()->write(PIPE_MSG_LICENSE_EXPIRED,
                                        Pipe::Type::Watchdog, m_id);
        });

        m_expiryWatchdog->start();
    }
}

bool Reservation::isLeaseExpired() const
{
    return (utils::utcTime() > m_leaseValidToUtc);
}

bool Reservation::isRenewDue() const
{
    return (utils::utcTime() > m_nextRenewTimeUtc);
}

std::string Reservation::validFromUtcString() const
{
    return utils::utcTimeToString(m_leaseValidFromUtc);
}

std::string Reservation::validToUtcString() const
{
    return utils::utcTimeToString(m_leaseValidToUtc);
}

bool Reservation::isRenewable() const
{
    return static_cast<int>(m_flags & Flags::Renewable);
}

bool Reservation::isReleasable() const
{
    return static_cast<int>(m_flags & Flags::Releasable);
}

bool Reservation::requiresReleaseOnDisconnect() const
{
    if (!m_license)
        return false;

    bool release = false;
    switch (m_license->model()) {
    case License::Model::Float:
        // floating license reservation can be released after last client disconnects
        if (clients().size() <= 1)
            release = true;
        break;
    default:
        break;
    }

    return release;
}

bool Reservation::isLocalOnly() const
{
    return static_cast<int>(m_flags & Flags::LocalOnly);
}

/*
    Marks this reservation as pending for release. Also stops timers
    for renewal and expiration events.
*/
void Reservation::setPendingForRelease()
{
    m_pendingForRelease = true;

    m_renewalWatchdog.reset(nullptr);
    m_expiryWatchdog.reset(nullptr);
}

/*
    Returns \c true if this reservation is pending for release, \c false
    otherwise. Such reservations should not be assigned for new consumers.
*/
bool Reservation::isPendingForRelease() const
{
    return m_pendingForRelease;
}

/*
    Inserts the \a client to set of clients using this reservation. Updates
    the usage statistics for this reservation, if the client refers to a unique
    license consumer.
*/
void Reservation::addClient(ClientHandler *client)
{
    m_clients.insert(client);

    // No need to collect statistics for local reservations
    if (static_cast<int>(m_flags & Flags::LocalOnly))
        return;

    const ConsumerAppInfo consumer = client->consumerInfo();
    // If it's a unique non-finished usage for this consumer, insert new statistics.
    // Otherwise we are already collecting the usage for this consumer.
    if (findExistingStatistic(consumer) == m_usageStatistics.end())
        m_usageStatistics.push_back(UsageStatistics(consumer));
}

/*
    Erases \c client from the set of clients using this reservation. Updates
    the usage statistics for this reservation, if the client is the last user
    of a unique license consumer.
*/
void Reservation::removeClient(ClientHandler *client)
{
    if (m_clients.count(client) == 0)
        return;

    m_clients.erase(client);

    // No need to collect statistics for local reservations
    if (static_cast<int>(m_flags & Flags::LocalOnly))
        return;

    completeStatistics(client);
}

/*
    Returns the clients currently consuming the reservation.
*/
std::set<ClientHandler *> Reservation::clients() const
{
    return m_clients;
}

/*
    Adds new \a statistic to list of usage statistics for this reservation.
*/
void Reservation::addStatistic(const UsageStatistics &statistic)
{
    m_usageStatistics.push_back(statistic);
}

/*
    Returns all usage statistics for this reservation.
*/
std::vector<UsageStatistics> Reservation::statistics() const
{
    return m_usageStatistics;
}

/*
    Returns a vector of usage statistics for this reservation. Already
    completed statistics are removed on invoking this method.
*/
std::vector<UsageStatistics> Reservation::takeStatistics()
{
    std::vector<UsageStatistics> statistics;

    auto it = m_usageStatistics.begin();
    while (it != m_usageStatistics.end()) {
        statistics.push_back(*it);
        if (it->isComplete())
            it = m_usageStatistics.erase(it);
        else
            ++it;
    }

    return std::move(statistics);
}

/*
    Attaches a \a consumer info to this license reservation, if not
    already attached. Multiple consumers can be attached to single reservation.
*/
void Reservation::attachConsumer(const ConsumerAppInfo &consumer)
{
    if (!isAttachedToConsumer(consumer))
        m_attachedConsumers.push_back(consumer);
}

/*
    Returns all consumers attached to this reservation.
*/
std::vector<ConsumerAppInfo> Reservation::attachedConsumers() const
{
    return m_attachedConsumers;
}

/*
    Returns \c true if this reservation has been attached to \a consumer
    information, \c false otherwise.
*/
bool Reservation::isAttachedToConsumer(const ConsumerAppInfo &consumer) const
{
    auto it = std::find(m_attachedConsumers.begin(), m_attachedConsumers.end(), consumer);
    return (it != m_attachedConsumers.end());
}

const JsonHandler *Reservation::data() const
{
    return &m_data;
}

void Reservation::completeStatistics(ClientHandler *client)
{
    {
        // Check if still have a connected client that matches the same consumer,
        // meaning there's no need to finish usage statistic collection.
        auto it = std::find_if(m_clients.begin(), m_clients.end(),
            [client](ClientHandler *c) {
                return (client->consumerInfo() == c->consumerInfo());
            }
        );

        if (it != m_clients.end())
            return;
    }

    {
        // If this was the last client using a unique consumer, we can
        // mark the statistics collection completed for reporting
        auto it = findExistingStatistic(client->consumerInfo());
        if (it != m_usageStatistics.end())
            it->setComplete(true);
    }
}

/*
    Attempts to find an existing usage statistic for \a consumer. A statistic is usable
    by this consumer if has been previosly used by consumer with matching properties, it's
    not marked complete, or is a complete statistic which allowed leeway time falls within
    the start time of \a consumer.
*/
std::vector<UsageStatistics>::iterator Reservation::findExistingStatistic(const ConsumerAppInfo &consumer)
{
    auto it = std::find_if(m_usageStatistics.begin(), m_usageStatistics.end(),
        [consumer](UsageStatistics &statistics) {
            if (!(statistics.consumerInfo() == consumer))
                return false; // Skip if different consumer
            else if (statistics.isComplete()) {
                // Check if there's leeway for continuing using an existing statistic
                if (statistics.isWithinAllowedLeeway(utils::utcTimeMs())) {
                    statistics.setComplete(false); // re-open for usage
                    return true;
                }

                return false; // Skip if an already completed usage statistic with exceeded leeway
            }

            return true;
        }
    );

    return it;
}

} // namespace QLicenseService
