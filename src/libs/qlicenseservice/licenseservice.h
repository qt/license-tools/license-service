/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "licenser.h"

#include <asynctask.h>

namespace QLicenseService {

class LicenseService : public QLicenseCore::AsyncTask<void>
{
public:
    explicit LicenseService(DaemonRunMode daemonMode, uint16_t tcpPort = 0, const std::string &workDir = "");

protected:
    void run() override;

private:
    uint16_t m_tcpPort = 0;
    std::string m_workDir;
    DaemonRunMode m_daemonRunMode;
};

} // namespace QLicenseService
