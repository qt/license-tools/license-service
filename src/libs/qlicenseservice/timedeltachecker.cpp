/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "timedeltachecker.h"

#include "commonsetup.h"

namespace QLicenseService {

TimeDeltaChecker::TimeDeltaChecker()
    : m_localTimeUtc(0)
    , m_serverTimeUtc(0)
{}

void TimeDeltaChecker::reset()
{
    setTimes(0, 0);
}

void QLicenseService::TimeDeltaChecker::setTimes(time_t localTimeUtc, time_t serverTimeUtc)
{
    m_localTimeUtc = localTimeUtc;
    m_serverTimeUtc = serverTimeUtc;
}

bool TimeDeltaChecker::allowedTimeDelta(std::string &error) const
{
    if (m_localTimeUtc > m_serverTimeUtc + SECS_IN_HOUR) {
        error = "Invalid host configuration: local time is ahead of the server time. "
                "Make sure that the system time is set correctly.";
        return false;
    } else if (m_localTimeUtc < m_serverTimeUtc - SECS_IN_HOUR) {
        error = "Invalid host configuration: local time is behind the server time. "
                "Make sure that the system time is set correctly.";
        return false;
    }

    return true;
}

} // namespace QLicenseService
