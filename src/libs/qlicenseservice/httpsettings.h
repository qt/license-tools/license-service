/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "httpclient.h"
#include "licdsetup.h"

namespace QLicenseService {

struct HttpSettings
{
    HttpSettings(QLicenseCore::LicdSetup *settings);

    std::string serverAddress;
    std::string caBundlePath;
    HttpRequest::CertRevokeOption certRevokeBehavior;
    long requestTimeout;
};

} // namespace QLicenseService
