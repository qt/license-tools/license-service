# Copyright (C) 2023 The Qt Company Ltd.
#
# SPDX-License-Identifier: BSD-3-Clause
#

add_subdirectory(3rdparty)
add_subdirectory(qlicensecore)

if (BUILD_SERVICE_LIB)
    add_subdirectory(qlicenseservice)
endif ()

if (BUILD_CLIENT_LIB)
    add_subdirectory(qlicenseclient)
endif ()

if (BUILD_PRECHECK_LIB)
    add_subdirectory(qlicenseprecheck)
endif ()
