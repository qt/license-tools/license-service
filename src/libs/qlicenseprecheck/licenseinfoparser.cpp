/* Copyright (C) 2025 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "licenseinfoparser.h"

#include <errors.h>
#include <logger.h>

using namespace QLicenseClient;
using namespace QLicenseCore;

namespace QLicensePrecheck {

static const std::map<std::string, LicenseInfoType> sc_licenseInfoMap = {
    {"license_id",       LicenseInfoType::Id},
    {"model",            LicenseInfoType::Model},
    {"priority",         LicenseInfoType::Priority},
    {"type",             LicenseInfoType::Type},
    {"schema",           LicenseInfoType::Schema},
    {"hosting",          LicenseInfoType::Hosting},
    {"valid_from",       LicenseInfoType::LicenseValidFrom},
    {"valid_to",         LicenseInfoType::LicenseValidTo},
};


LicenseInfoParser::LicenseInfoParser(const std::string &input)
    : m_json(new QLicenseCore::JsonHandler(input))
{
}

bool LicenseInfoParser::parse(LicenseInfo &licenseInfo)
{
    const std::string version = preParseVersion();
    if (version.empty()) {
        logError("Could not parse version from license JSON");
        return false;
    }

    try {
        ParseLicenseFunction parseLicense = findParser(version);
        return parseLicense(licenseInfo);
    } catch (const QLicenseCore::Error &e) {
        logError("%s", e.what());
        return false;
    }
}

std::string LicenseInfoParser::preParseVersion() const
{
    std::string version;
    m_json->get("version", version);
    return version;
}

ParseLicenseFunction LicenseInfoParser::findParser(const std::string &version) const
{
    for (auto &parser : m_licenseParsers) {
        if (QLicenseCore::utils::compareVersion(version, parser.first, 2) <= 0)
            return parser.second;
    }

    throw QLicenseCore::Error("No suitable parser found for license format version: " + version);
}

bool LicenseInfoParser::parseLicenseV10(LicenseInfo &licenseInfo)
{
    LicenseeInfo licensee;
    m_json->get("licensee.name", licensee.name);
    m_json->get("licensee.description", licensee.description);
    m_json->get("licensee.contact", licensee.contact);
    licenseInfo.setLicensee(licensee);

    LicenseeContacts contacts;
    m_json->get("contacts.sales", contacts.sales);
    m_json->get("contacts.support", contacts.support);
    m_json->get("contacts.admin", contacts.admin);
    licenseInfo.setContacts(contacts);

    std::vector<ConsumerInfo> consumers;
    JsonData::array_t consumerArray;
    m_json->get("consumers", consumerArray);

    try {
        for (const auto &obj : consumerArray) {
            ConsumerInfo consumer;
            consumer.consumerId = obj["consumer_id"];
            consumer.consumerVersion = obj["consumer_version"];

            try {
                consumer.consumerPriority = utils::strToInt(obj.at("consumer_priority"));
            } catch (...) {} // ignore exceptions for missing optional values

            for (const auto &feature : obj["consumer_features"].items()) {
                const std::string &value = feature.value().is_string()
                    ? feature.value().get<std::string>()
                    : to_string(feature.value());

                consumer.consumerFeatures[feature.key()] = value;
            }

            consumers.push_back(consumer);
        }
    } catch (const nlohmann::json::exception &e) {
        logError("Cannot parse element from JSON: %s", e.what());
        return false;
    }

    licenseInfo.setConsumers(consumers);

    QLicenseCore::JsonData info;
    if (!m_json->get("license_info", info)) {
        logError("Could not access \"license_info\" object");
        return false;
    }

    for (const auto &item : info.items()) {
        const std::string &key = item.key();
        if (sc_licenseInfoMap.find(key) == sc_licenseInfoMap.end()) {
            // Not found in the map: Probably new informative value?
            continue;
        }
        const std::string &value = item.value().is_string()
           ? item.value().get<std::string>()
           : to_string(item.value());

        licenseInfo.setLicenseInfo(sc_licenseInfoMap.at(key), value);
    }

    return true;
}

} // namespace QLicensePrecheck
