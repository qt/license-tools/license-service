/* Copyright (C) 2025 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "licenseprecheck_p.h"

#include "licenseinfoparser.h"

#include <commonsetup.h>
#include <constants.h>
#include <errors.h>
#include <httpsettings.h>
#include <logger.h>
#include <utils.h>
#include <crypto.h>

using namespace QLicenseCore;
using namespace QLicenseClient;
using namespace QLicenseService;

namespace QLicensePrecheck {

constexpr char sc_operationList[] = "list";
constexpr char sc_operationCheck[] = "check";

LicensePrecheckPrivate::LicensePrecheckPrivate(const std::string &serverAddr)
    : m_http(nullptr)
{
    Logger::getInstance()->disableAll();

    if (Logger::clientPrintToStdoutFromEnv())
        Logger::getInstance()->setPrintToStdout(true);

    std::string logFilePath;
    if (Logger::clientPrintToFileFromEnv(logFilePath)) {
        Logger::getInstance()->setLogFile(logFilePath);
        Logger::getInstance()->setPrintToFile(true);
    }

    m_settings.reset(new LicdSetup(LicdSetup::getQtAppDataLocation()
        + std::string(USER_SETTINGS_FOLDER_NAME) + DIR_SEPARATOR
        + std::string(DAEMON_SETTINGS_FILE)));

    if (!m_settings->init())
        throw QLicenseCore::Error("Not able to read settings file required for license precheck");

    if (!serverAddr.empty())
        m_settings->set(sc_serverAddr, serverAddr);

    HttpSettings httpSettings(m_settings.get());
    m_http.reset(new HttpClient(httpSettings.serverAddress));
    m_http->setCABundlePath(httpSettings.caBundlePath);
    m_http->setCertRevokeBehavior(httpSettings.certRevokeBehavior);
    m_http->setRequestTimeout(httpSettings.requestTimeout);

    if (!m_account.readFromDisk())
        logWarn("Could not initialize Qt Account settings: %s", m_account.error().c_str());
}

bool LicensePrecheckPrivate::listLicenses(std::vector<QLicenseClient::LicenseInfo> &licenses)
{
    if (m_username.empty()) {
        if (!utils::getCurrentUserName(m_username) || m_username.empty()) {
            setError("Not able to determine the username");
            return false;
        }
    }

    const std::string payload = prepareListRequestPayload();
    if (payload.empty())
        return false;

    HttpResponse response;
    if (!m_http->sendReceive(response, payload, m_settings->get(sc_licensePrecheckAccessPoint),
            m_settings->get(sc_serverAddr), authToken())) {
        setError("Error while performing network request");
        return false;
    }

    if (!isSuccessResponse(response))
        return false;

    return parseListRequestResponse(response.body, licenses);
}

bool LicensePrecheckPrivate::precheck(std::vector<Component> &components)
{
    if (m_username.empty()) {
        if (!utils::getCurrentUserName(m_username) || m_username.empty()) {
            setError("Not able to determine the username");
            return false;
        }
    }

    for (auto &component : components) {
        if (!isValidProperties(component.clientProperties)) {
            setError("Invalid properties provided for the client");
            return false;
        }
    }

    const std::string payload = preparePrecheckRequestPayload(components);
    if (payload.empty())
        return false;

    HttpResponse response;
    if (!m_http->sendReceive(response, payload, m_settings->get(sc_licensePrecheckAccessPoint),
            m_settings->get(sc_serverAddr), authToken())) {
        setError("Error while performing network request");
        return false;
    }

    if (!isSuccessResponse(response))
        return false;

    return parsePrecheckRequestResponse(response.body, components);
}

std::string LicensePrecheckPrivate::error() const
{
    return m_error;
}

std::string LicensePrecheckPrivate::prepareListRequestPayload()
{
    try {
        nlohmann::json json;
        json["operation"] = sc_operationList;
        json["user_id"] = m_username;
        json["hw_id"] = m_settings->get(sc_hwId);

        std::string dumped = json.dump(-1, ' ', false, nlohmann::json::error_handler_t::replace);
        return dumped;

    } catch (const nlohmann::json::exception &e) {
        setError("Error while building list licenses request: " + std::string(e.what()));
        return std::string();
    }
}

std::string LicensePrecheckPrivate::preparePrecheckRequestPayload(const std::vector<Component> &components)
{
    try {
        nlohmann::json json;
        json["operation"] = sc_operationCheck;
        json["user_id"] = m_username;
        json["hw_id"] = m_settings->get(sc_hwId);

        nlohmann::json consumersArray;
        for (const auto &component: components) {
            if (component.componentId.empty()) {
                throw QLicenseCore::Error("No component id provided for consumer: "
                    + component.clientProperties.consumerId);
            }
            consumersArray.push_back(componentToJson(component));
        }
        json["consumers"] = consumersArray;

        return json.dump(-1, ' ', false, nlohmann::json::error_handler_t::replace);

    } catch (const nlohmann::json::exception &e) {
        setError("Error while building precheck request: " + std::string(e.what()));
        return std::string();
    }
}

bool LicensePrecheckPrivate::parseListRequestResponse(const std::string response,
                                               std::vector<QLicenseClient::LicenseInfo> &licenses)
{
    try {
        const nlohmann::json jsonDoc = nlohmann::json::parse(response);
        const nlohmann::json::array_t licenseArray = jsonDoc["licenses"];
        for (const auto &licenseObj : licenseArray) {
            LicenseInfo info;
            info.setJsonData(licenseObj.dump());
            LicenseInfoParser parser(info.jsonData());
            if (!parser.parse(info)) {
                logWarn("Failed to parser license object from response, ignoring invalid license.");
                continue;
            }
            licenses.push_back(info);
        }

    } catch (const nlohmann::json::exception &e) {
        setError("Error while parsing list licenses response: " + std::string(e.what()));
        return false;
    }

    return true;
}

bool LicensePrecheckPrivate::parsePrecheckRequestResponse(const std::string response,
                                                   std::vector<Component> &components)
{
    try {
        const nlohmann::json jsonDoc = nlohmann::json::parse(response);
        const nlohmann::json::array_t dataArray = jsonDoc.at("data");
        for (const auto &obj : dataArray) {
            const std::string componentId = obj.at("component_id");
            auto it = std::find_if(components.begin(), components.end(),
                [componentId](Component &component) {
                    return component.componentId == componentId;
                }
            );

            if (it != components.end())
                it->installationAllowed = obj.at("installation_allowed").get<bool>();
        }
    } catch (const nlohmann::json::exception &e) {
        setError("Error while parsing precheck response: " + std::string(e.what()));
        return false;
    }

    return true;
}

bool LicensePrecheckPrivate::isSuccessResponse(const HttpResponse &response)
{
    if (response.code >= 400) {
        setError("Error while performing request. Received HTTP status code: "
            + std::to_string(response.code));
        return false;
    }

    try {
        nlohmann::json responseJson = nlohmann::json::parse(response.body);
        if (responseJson.contains("status")) {
            if (responseJson.at("status").get<bool>())
                return true;

            if (responseJson.contains("message")) {
                setError(responseJson.at("message").get<std::string>());
                return false;
            }
        }
        setError("Unknown error in server response");
        return false;

    } catch (const nlohmann::json::exception &e) {
        setError("Error parsing response JSON: " + std::string(e.what()));
        return false;
    }
}

std::string LicensePrecheckPrivate::authToken() const
{
    std::string auth;
    if (!m_account.jwt().empty()) {
        auth += "Bearer ";
        auth += m_account.jwt();
    }

    return auth;
}

void LicensePrecheckPrivate::setError(const std::string &error)
{
    logError("%s", error.c_str());
    m_error = error;
}

/*
    Returns a JSON object representation of the \a component, or empty JSON
    object in case of failure.
*/
nlohmann::json LicensePrecheckPrivate::componentToJson(const Component &component) const
{
    try {
        nlohmann::json object;
        object[sc_componentId] = component.componentId;
        object[sc_consumerId] = component.clientProperties.consumerId;
        object[sc_consumerVersion] = component.clientProperties.consumerVersion;
        object[sc_consumerProcessId] = component.clientProperties.consumerProcessId;

        if (!component.clientProperties.consumerInstallationSchema.empty())
            object[sc_consumerInstallationSchema] = component.clientProperties.consumerInstallationSchema;

        if (!component.clientProperties.consumerSupportedFeatures.empty()) {
            auto it = component.clientProperties.consumerSupportedFeatures.begin();
            std::string supportedFeaturesImploded = *it++;
            for (; it != component.clientProperties.consumerSupportedFeatures.end(); ++it)
                supportedFeaturesImploded += "," + *it;

            object[sc_consumerSupportedFeatures] = supportedFeaturesImploded;
        }

        return object;
    } catch (const nlohmann::json::exception &e) {
        logError("Error constucting JSON: %s", e.what());
        return nlohmann::json();
    }
}

/*
    Returns \c true if the mandatory fields in \a properties are set, \c false otherwise.
*/
bool LicensePrecheckPrivate::isValidProperties(const QLicenseClient::ClientProperties &properties) const
{
    return !properties.consumerId.empty() && !properties.consumerVersion.empty();
}

} // namespace QLicensePrecheck
