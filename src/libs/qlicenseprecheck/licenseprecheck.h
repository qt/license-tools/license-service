/* Copyright (C) 2025 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <licenseinfo.h>
#include <licenseclient.h>

#include <vector>

namespace QLicensePrecheck {

class LicensePrecheckPrivate;

struct Component
{
    std::string componentId;
    bool installationAllowed = false;

    QLicenseClient::ClientProperties clientProperties;
};

class LicensePrecheck
{
public:
    LicensePrecheck(const std::string &serverAddr = std::string());
    ~LicensePrecheck();

    std::string error() const;

    bool listLicenses(std::vector<QLicenseClient::LicenseInfo> &licenses);
    bool precheck(std::vector<Component> &components);

private:
    LicensePrecheckPrivate *const d;
};

} // namespace QLicensePrecheck
