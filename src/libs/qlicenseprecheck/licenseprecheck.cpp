/* Copyright (C) 2025 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "licenseprecheck.h"

#include "licenseprecheck_p.h"

namespace QLicensePrecheck {

/*
    \struct Component

    \brief The Component represents a single installable component of a consumer application,
           module, or library.
*/

/*
    \property Component::componentId

    \brief The identifier of this installable component.
*/

/*
    \property Component::installationAllowed

    \brief Indicates if this component is allowed for installation.
*/

/*
    \property Component::clientProperties

    \brief Properties of a license client or consumer that belongs to this installable component.
*/


/*
    \class LicensePrecheck

    \brief The LicensePrecheck class provides methods for performing installation
           time precheck for licenses. This includes listing available licenses for the
           user, and checking whether some components can be installed or not based on
           the license properties.
*/

/*
    Constructs a new license precheck instance, with optional \a serverAddr that takes
    precedense over the address defined in local settings. Reads the service settings file
    from disk and initializes the HTTP client settings.

    Throws \c QLicenseCore::Error on initialization failure.
*/
LicensePrecheck::LicensePrecheck(const std::string &serverAddr)
    : d(new LicensePrecheckPrivate(serverAddr))
{
}

/*
    Destructs the license precheck instance.
*/
LicensePrecheck::~LicensePrecheck()
{
    delete d;
}

/*
    Fetches and lists all \a licenses for current user.

    Returns \a true if the request was performed successfully, \c false otherwise.
    A description about a failure can be retrieved with \l error().
*/
bool LicensePrecheck::listLicenses(std::vector<QLicenseClient::LicenseInfo> &licenses)
{
    return d->listLicenses(licenses);
}

/*
    Performs a precheck request for a list of installable \a components, and shows
    whether they may be installed or not indicated by the value of
    \c Component::installationAllowed.

    Returns \a true if the request was performed successfully, \c false otherwise.
    A description about a failure can be retrieved with \l error().
*/
bool LicensePrecheck::precheck(std::vector<Component> &components)
{
    return d->precheck(components);
}

/*
    Returns a string description for the last error.
*/
std::string LicensePrecheck::error() const
{
    return d->error();
}

} // namespace QLicensePrecheck
