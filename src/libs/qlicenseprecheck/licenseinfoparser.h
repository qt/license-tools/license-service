/* Copyright (C) 2025 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "licenseinfo.h"

#include <jsonhandler.h>

#include <functional>
#include <memory>

namespace QLicensePrecheck {

using ParseLicenseFunction = std::function<bool(QLicenseClient::LicenseInfo &)>;

class LicenseInfoParser
{
public:
    LicenseInfoParser(const std::string &input);

    bool parse(QLicenseClient::LicenseInfo &licenseInfo);

private:
    std::string preParseVersion() const;
    ParseLicenseFunction findParser(const std::string &version) const;

    bool parseLicenseV10(QLicenseClient::LicenseInfo &licenseInfo);

private:
    std::unique_ptr<QLicenseCore::JsonHandler> m_json;

    const std::map<std::string, ParseLicenseFunction> m_licenseParsers {
        {"1.0", std::bind(&LicenseInfoParser::parseLicenseV10, this, std::placeholders::_1)}
    };
};

} // namespace QLicensePrecheck
