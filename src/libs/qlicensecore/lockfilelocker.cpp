/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "lockfilelocker.h"

#include "lockfile.h"
#include "logger.h"
#include "errors.h"

#include <chrono>
#include <thread>

QLicenseCore::LockFileLocker::LockFileLocker(LockFile *file)
    : m_file(file)
{
    if (!m_file)
        throw Error("Cannot create locker for null file");

    if (m_file->tryLock())
        return;

    // If locking doesn't succeed on the first try, we will block
    // and try again until it succeeds.
    while (true) {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        if (m_file->tryLock())
            break;
    }
}

QLicenseCore::LockFileLocker::~LockFileLocker()
{
    if (!m_file->tryUnlock()) {
        logWarn("Could not unlock file \"%s\": %s",
            m_file->filename().c_str(), m_file->error().c_str());
    }
}
