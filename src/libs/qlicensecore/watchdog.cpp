/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "watchdog.h"

#include "utils.h"

namespace QLicenseCore {

/*
    Constructs a new watchdog timer with given timeout
*/
Watchdog::Watchdog(const std::chrono::seconds &timeout)
    : m_id(utils::generateUniqueShortId())
    , m_timeout(timeout)
    , m_completed(false)
    , m_reset(false)
{
}

/*
    Stops the timer and destroys the instance
*/
Watchdog::~Watchdog()
{
    stop();
}

/*
    Returns an identifier for this watchdog
*/
uint16_t Watchdog::id() const
{
    return m_id;
}

/*
    Start the watchdog timer
*/
void Watchdog::start()
{
    m_completed.store(false);
    m_thread = std::thread(&Watchdog::runTimer, this);
}

/*
    Stop the watchdog timer, indicating an operation was completed before the timeout
*/
void Watchdog::stop()
{
    m_completed.store(true);
    if (m_thread.joinable())
        m_thread.join();
}

/*
    Resets the watchdog's timer. Calling this from the watchdog's notify callback is safe.
*/
void Watchdog::reset()
{
    m_completed.store(false);
    m_reset.store(true);
}

/*
    Performs a "hard" restart of the watchdog's timer, which means stopping the
    previous internal timer and creating a new one. Unlike \l{reset()}, this can
    be called to restart a completed or stopped watchdog.
*/
void Watchdog::restart()
{
    stop();
    start();
}

/*
    Sets the notify callback to be invoked on timeout
*/
void Watchdog::setCallback(std::function<void()> callback)
{
    m_callback = std::move(callback);
}

/*
    Runs the watchdog timer. If timeout is reached, the callback gets invoked.
*/
void Watchdog::runTimer()
{
    auto start = std::chrono::steady_clock::now();
    const auto interval = std::chrono::milliseconds(100);
    const auto timeout_ms = std::chrono::duration_cast<std::chrono::milliseconds>(m_timeout);

    while (!m_completed.load()) {
        // Store new starting time since reset
        if (m_reset.load()) {
            start = std::chrono::steady_clock::now();
            m_reset.store(false);
        }

        std::this_thread::sleep_for(interval);

        if ((std::chrono::steady_clock::now() - start) >= timeout_ms) {
            if (m_callback)
                m_callback(); // Notify the caller

            // Check if we should still continue running
            if (!m_reset.load())
                break;
        }
    }
}

} // namespace QLicenseCore
