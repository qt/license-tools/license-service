/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "licdsetup.h"

#include "constants.h"
#include "commonsetup.h"
#include "crypto.h"
#include "utils.h"
#include "logger.h"

namespace QLicenseCore {

LicdSetup::LicdSetup(const std::string &settingsFilePath)
    : Settings()
    , m_settingsFilePath(settingsFilePath)
    , m_parser(settingsFilePath)
{
}

bool LicdSetup::init()
{
    if (m_initialized)
        return true;

    if (!getServiceInfo())
        return false;

    return Settings::init();
}

bool LicdSetup::set(const std::string &key, const std::string &value, SettingsType type)
{
    if (type == SettingsType::Persistent) {
        if (!m_parser.writeKeyValue(std::string(sc_qtlicdIniFileSection), {std::make_pair(key, value)})) {
            logError("Persistent settings update failed for key: '%s' with value: '%s'",
                key.c_str(), value.c_str());
            return false;
        }
    }

    Settings::set(key, value);
    logInfo("Settings updated for key: '%s' with value: '%s'", key.c_str(), value.c_str());
    return true;
}

void LicdSetup::setHardwareId(const std::string &hwId)
{
    logInfo("Setting new hardware id value to '%s'", hwId.c_str());

    std::string settingsString;
    utils::readFile(settingsString, m_settingsFilePath);
    std::vector<std::string> lines = utils::splitStr(settingsString, '\n');

    settingsString = "";
    std::string wanted = "hw_id=";
    for (std::string line : lines) {
        if (line.substr(0, wanted.length()) == wanted) {
            line = wanted + hwId;
        }
        line += '\n';
        settingsString += line;
    }
    utils::writeToFile(m_settingsFilePath, settingsString);
    set(sc_hwId, hwId);
}

bool LicdSetup::getServiceInfo()
{
    if (!utils::fileExists(m_settingsFilePath)) {
        logInfo("Settings file does not exist, creating new default settings");
        if (!writeDefaultSettings())
            return false;
    }

    if (!m_parser.readSection("QtLicenseDaemon", m_settings)) {
        logError("Failed to parse settings from: %s", m_settingsFilePath.c_str());
        return false;
    }

    m_settings["host_os"] = utils::getOsName() + "-" + utils::getOsArchitecture();
    logInfo("Host: %s", m_settings["host_os"].c_str());

    if (Logger::getInstance()->logLevel() >= LogLevel::LOGLEVEL_DEBUG) {
        for (const auto &setting : m_settings)
            logDebug("Setting '%s' to value '%s'", setting.first.c_str(), setting.second.c_str());
    }

    // Check if our setup already has some hw id - generate one if not
    if (get(sc_hwId).empty()) {
        const std::string uuid = QLicenseCore::utils::generateUuid();
        setHardwareId(Crypto::sha256(uuid));
    }

    return true;
}

bool LicdSetup::writeDefaultSettings()
{
    const std::string settingsPath = utils::getPathWithoutFilename(m_settingsFilePath);
    if (!utils::fileExists(settingsPath) && !utils::createDir(settingsPath)) {
        logError("Failed to create settings directory %s", settingsPath.c_str());
        return false;
    }

    if (!utils::writeToFile(m_settingsFilePath, std::string(DEFAULT_QTLICD_SETTINGS_FILE_CONTENT), false)) {
        logError("Failed to create settings file %s", m_settingsFilePath.c_str());
        return false;
    }

    logDebug("New default settings written to: %s", m_settingsFilePath.c_str());
    return true;
}

std::string LicdSetup::getQtAppDataLocation()
{
    std::string retVal = utils::appdataPath();
    retVal += DIR_SEPARATOR;
    retVal += "Qt";
    retVal += DIR_SEPARATOR;
    return retVal;
}

} // namespace QLicenseCore
