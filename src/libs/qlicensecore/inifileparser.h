/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
 */

#pragma once

#include <fstream>
#include <string>
#include <map>
#include <unordered_map>

namespace QLicenseCore {

class IniFileParser
{
public:
    IniFileParser(const std::string &filename) : m_filename(filename) { }

    bool readKeyValue(const std::string &section, const std::string &key, std::string &value) const;
    template <typename MapType>
    bool readSection(const std::string &section, MapType &result) const;
    template <typename MapType>
    bool readSections(std::map<std::string, MapType> &results) const;

    bool writeKeyValue(const std::string &section,
                       const std::map<std::string, std::string> &keyValuePairs) const;
    bool removeSection(const std::string &section) const;

private:
    void writeKeyValuePairs(std::ofstream &file,
                            const std::map<std::string, std::string> &keyValuePairs) const;
    bool skipLine(const std::string &line) const;
    bool isSection(const std::string &line) const;

private:
    std::string m_filename;
};

} // namespace QLicenseCore
