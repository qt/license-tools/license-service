/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "localqtaccount.h"

#include "commonsetup.h"

#include <constants.h>
#include <qtaccountsetup.h>
#include <licdsetup.h>
#include <jsonhandler.h>
#include <utils.h>

namespace QLicenseCore {

LocalQtAccount::LocalQtAccount()
    : m_loggedIn(false)
{
}

LocalQtAccount::~LocalQtAccount()
{
}

bool LocalQtAccount::loggedIn() const
{
    return m_loggedIn;
}

std::string LocalQtAccount::email() const
{
    return m_email;
}

std::string LocalQtAccount::jwt() const
{
    return m_jwt;
}

bool LocalQtAccount::readFromDisk()
{
    const std::string settingsFilePath = LicdSetup::getQtAppDataLocation() + QT_ACCOUNT_SETTINGS_FILE;
    logDebug("Reading Qt Account settings from disk: %s", settingsFilePath.c_str());

    if (!utils::fileExists(settingsFilePath)) {
        m_error = "Qt Account settings file " + settingsFilePath + " does not exist.";
        return false;
    }

    QtAccountSetup settings(settingsFilePath);
    if (!settings.init()) {
        m_error = "Not able to read Qt Account file: " + settingsFilePath;
        return false;
    }

    m_email = settings.get(sc_email);
    m_jwt = settings.get(sc_jwt);

    if (m_email.empty() || m_jwt.empty()) {
        m_error = "Invalid Qt Account settings: " + settingsFilePath;
        return false;
    }

    // TODO: existence of JWT won't likely be enough to determine if the
    // user has logged in. Once renewals are supported, renew login first?
    m_loggedIn = true;

    return true;
}

std::string LocalQtAccount::error() const
{
    return m_error;
}

} // namespace QLicenseService
