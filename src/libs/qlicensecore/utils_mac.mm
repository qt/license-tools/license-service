/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "utils.h"

#include <objc/objc.h>
#include <Foundation/Foundation.h>
#include <string>

namespace QLicenseCore { namespace utils {

/*
   Returns the same path as QStandardPaths::writableLocation(QStandardPaths::AppDataLocation),
   minus the automatic addition of QCoreApplication::organizationName,
   QCoreApplication::applicationName.
*/
std::string appdataPath()
{
    @autoreleasepool {
        NSFileManager *fm = [NSFileManager defaultManager];
        NSArray *appSupportDir = [fm URLsForDirectory:NSApplicationSupportDirectory
                inDomains:NSUserDomainMask];
        if ([appSupportDir count] < 1)
            return std::string();

        NSString *dir = [[appSupportDir objectAtIndex:0] path];
        return std::string(dir.UTF8String);
    }
    return std::string();
}

} } // namespace QLicenseCore::utils
