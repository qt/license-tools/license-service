/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "version.h"

#include <string>

namespace QLicenseCore {

class ServiceResponse
{
public:
    ServiceResponse() = default;
    ServiceResponse(bool success, const std::string &message, int code);

    void setServiceResponse(bool success, const std::string &message, int code);

    std::string toString() const;
    void fromString(const std::string &response);

    bool success() const;
    void setSuccess(bool success);

    std::string message() const;
    void setMessage(const std::string &message);

    int statusCode() const;
    void setStatusCode(int code);

    std::string version() const;

    void setLicenseInfo(const std::string &license);
    std::string licenseInfo() const;

private:
    std::string m_version = DAEMON_VERSION;
    bool m_success = false;
    std::string m_message;
    int m_code = -1;

    std::string m_licenseInfo;
};

} // namespace QLicenseCore
