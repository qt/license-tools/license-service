/* Copyright (C) 2025 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <map>
#include <string>

namespace QLicenseCore {

class InstallationManager
{
public:
    InstallationManager(const std::string &configPath = std::string());

    std::string configFilePath() const;
    std::string installationForVersion(const std::string &version) const;

    bool registerInstallation(const std::string &path, const std::string &version,
        const std::string &source, const std::map<std::string, std::string> &properties = {}) const;
    bool unregisterInstallation(const std::string &path) const;

    void purgeObsoleteInstallations() const;

private:
    const std::string m_configFilePath;
};

} // namespace QLicenseCore
