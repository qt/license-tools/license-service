/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "crypto.h"

#include "logger.h"

#include <openssl/sha.h>
#include <openssl/evp.h>

namespace QLicenseCore { namespace Crypto {

std::string sha256(const std::string &input)
{
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256(reinterpret_cast<const unsigned char*>(input.data()), input.size(), hash);

    std::string result;
    result.reserve(2 * SHA256_DIGEST_LENGTH);

    for (int i = 0; i < SHA256_DIGEST_LENGTH; ++i) {
        // Convert each byte to a two-character hex string
        char hex_byte[3];
        snprintf(hex_byte, sizeof(hex_byte), "%02x", hash[i]);
        result.append(hex_byte, 2);
    }

    return result;
}

std::vector<unsigned char> base64Decode(const std::string &input)
{
    if (input.empty()) {
        logError("Cannot base64 decode an empty input string.");
        return std::vector<unsigned char>();
    }

    BIO *b64 = BIO_new(BIO_f_base64());
    if (!b64) {
        logError("Failed to create BIO_f_base64() BIO object.");
        return std::vector<unsigned char>();
    }

    BIO *bio = BIO_new_mem_buf(input.data(), input.length());
    if (!bio) {
        logError("Failed to create BIO_new_mem_buf() BIO object.");
        BIO_free_all(b64);
        return std::vector<unsigned char>();
    }

    bio = BIO_push(b64, bio);
    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL); // Set flags after pushing to chain

    std::vector<unsigned char> out(input.length());
    int len = BIO_read(bio, out.data(), input.length());
    if (len <= 0) {
        logError("Failed to read BIO object.");
        BIO_free_all(b64);
        return std::vector<unsigned char>();
    }

    out.resize(len);
    BIO_free_all(b64);

    return out;
}

} } // namespace QLicenseService::Crypto
