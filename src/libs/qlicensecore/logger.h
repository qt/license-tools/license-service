/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "utils.h"

#include <stdarg.h>
#include <fstream>
#include <string>
#include <mutex>

// Helper macros
#define logError(...) Logger::getInstance()->error(utils::getFilenameWithoutExtension(__FILE__), __VA_ARGS__)
#define logWarn(...) Logger::getInstance()->warn(utils::getFilenameWithoutExtension(__FILE__), __VA_ARGS__)
#define logInfo(...) Logger::getInstance()->info(utils::getFilenameWithoutExtension(__FILE__), __VA_ARGS__)
#define logDebug(...) { Logger::getInstance()->debug(__FILE__, __FUNCTION__, __LINE__, __VA_ARGS__); }
#define logSilly(...) { Logger::getInstance()->silly(__FILE__, __FUNCTION__, __LINE__, __VA_ARGS__); }

#define HANDLE_VARGS(inputStr, msg)                                 \
do {                                                                \
    va_list args;                                                   \
    va_list args_copy;                                              \
    va_start(args, msg);                                            \
    va_copy(args_copy, args);                                       \
    std::string formattedMsg;                                       \
    int size = std::vsnprintf(nullptr, 0, msg, args);               \
    if (size > 0) {                                                 \
        formattedMsg.resize(size + 1);                              \
        std::vsnprintf(&formattedMsg[0], size + 1, msg, args_copy); \
        formattedMsg.resize(size); /* remove terminating \0 */      \
    }                                                               \
    va_end(args_copy);                                              \
    va_end(args);                                                   \
    inputStr = formattedMsg;                                        \
} while (0)

namespace QLicenseCore {

enum class LogLevel {
    LOGLEVEL_NONE = 0,
    LOGLEVEL_ERROR,
    LOGLEVEL_WARNING,
    LOGLEVEL_INFO,
    LOGLEVEL_DEBUG,
    LOGLEVEL_SILLY
};

class Logger
{
public:
    static Logger *getInstance();

    void info(const std::string &file, const char *msg, ...);
    void warn(const std::string &file, const char *msg, ...);
    void error(const std::string &file, const char *msg, ...);
    void debug(const std::string &file, const char * function, int line, const char *msg, ...);
    void silly(const std::string &file, const char *function, int line, const char *msg, ...);

    void setLogLevel(LogLevel level);
    LogLevel logLevel() const;

    void setPrintToStdout(bool onOff);
    void setPrintToFile(bool onOff);
    void disableAll();
    void setLogFile(std::string logFile);

public:
    static bool clientPrintToStdoutFromEnv();
    static bool clientPrintToFileFromEnv(std::string &filename);

    static bool outputRedirected();

private:
    Logger();
    ~Logger();

    int print(std::string text);
    const char *logLevelAsCString();

private:
    std::string m_logFile;
    LogLevel m_logLevel;
    mutable std::mutex m_mutex;
    std::ofstream m_logStream;
    bool m_printToStdout;
    bool m_printToFile;
};

} // namespace QLicenseCore
