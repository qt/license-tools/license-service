/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <string>

namespace QLicenseCore {

class LocalQtAccount
{
public:
    LocalQtAccount();
    virtual ~LocalQtAccount();

    bool loggedIn() const;

    std::string email() const;
    std::string jwt() const;

    bool readFromDisk();

    std::string error() const;

protected:
    bool m_loggedIn;
    std::string m_error;

    std::string m_email;
    std::string m_jwt;
};

} // namespace QLicenseCore
