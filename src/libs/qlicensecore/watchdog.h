/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <atomic>
#include <chrono>
#include <functional>
#include <thread>

namespace QLicenseCore {

class Watchdog
{
public:
    explicit Watchdog(const std::chrono::seconds &timeout);
    ~Watchdog();

    uint16_t id() const;

    void start();
    void stop();
    void reset();
    void restart();

    void setCallback(std::function<void()> callback);

private:
    void runTimer();

private:
    uint16_t m_id;

    std::chrono::seconds m_timeout;
    std::atomic<bool> m_completed;
    std::atomic<bool> m_reset;

    std::thread m_thread;
    std::function<void()> m_callback;
};

} // namespace QLicenseCore
