/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/
#pragma once

#include "nlohmann/json.hpp"

#include "logger.h"

#include <sstream>
#include <iostream>

namespace QLicenseCore {

typedef nlohmann::json JsonData ;

class JsonHandler  {
private:
    JsonData m_json;

public:

    JsonHandler(const std::string& jsonString = "")
    {
        if (!jsonString.empty()) {
            try {
                m_json = JsonData::parse(jsonString);
            } catch (const nlohmann::json::exception& e) {
                logError("Error parsing JSON: %s", e.what());
            }
        }
    }

    // Generic get function for simple access
    template <typename T>
    bool get(const std::string& key, T& value) const {
        std::vector<std::string> keys;
        std::istringstream iss(key);
        std::string token;
        while (std::getline(iss, token, '.')) {
            keys.push_back(token);
        }

        return getNested(keys, value, m_json);
    }

    // Overloaded get function for direct access to nested keys
    template <typename T>
    bool get(const std::string& key, T& value, const JsonData& jsonData) const {
        std::vector<std::string> keys;
        std::istringstream iss(key);
        std::string token;
        while (std::getline(iss, token, '.')) {
            keys.push_back(token);
        }

        return getNested(keys, value, jsonData);
    }

    // Set function for direct access to nested keys
    template <typename T>
    void set(const std::string& key, const T& value) {
        std::vector<std::string> keys;
        std::istringstream iss(key);
        std::string token;
        while (std::getline(iss, token, '.')) {
            keys.push_back(token);
        }

        setNested(keys, value);
    }

    std::string dump(uint8_t indent = 0) const
    {
        try {
            // Stringify a JSON. Parameter value > 0 returns pretty-printed JSON with given
            // indentation, and indent of 0 (default) returns raw, stringified JSON.
            if (indent == 0) {
                return m_json.dump(-1, ' ', false, nlohmann::json::error_handler_t::replace);
            }
            return m_json.dump(indent);
        } catch (const nlohmann::json::exception& e) {
            logError("Error dumping JSON: %s", e.what());
            return std::string();
        }
    }

    void clear()
    {
        // Clears the contents
        m_json.clear();
    }

     // Recursive helper function to traverse nested keys
    template <typename T>
    bool getNested(const std::vector<std::string>& keys, T& value, const JsonData& jsonData) const {
        const JsonData *currentJson = &jsonData;
        for (const auto& key : keys) {
            if (currentJson->find(key) != currentJson->end()) {
                currentJson = &(*currentJson)[key];
            } else {
                return false;
            }
        }

        try {
            value = currentJson->get<T>();
            return true;
        } catch (const nlohmann::json::exception& e) {
            logError("Error getting entry from JSON: %s", e.what());
            return false;
        }
    }

    // Overloaded get function for direct access to nested keys
    template <typename T>
    bool getNested(const std::vector<std::string>& keys, T& value) const {
        return getNested(keys, value, m_json);
    }

    // Helper function to set nested keys
    template <typename T>
    void setNested(const std::vector<std::string> &keys, const T &value) {
        nlohmann::json  *currentJson = &m_json;
        for (size_t i = 0; i < keys.size() - 1; ++i) {
            const std::string& key = keys[i];
            if (!currentJson->contains(key) || !currentJson->at(key).is_object()) {
                (*currentJson)[key] = nlohmann::json::object();
            }
            currentJson = &(*currentJson)[key];
        }
        // Set the final key
        (*currentJson)[keys.back()] = value;
    }

    JsonData *data()
    {
        return &m_json;
    }
};

} // namespace QLicenseCore
