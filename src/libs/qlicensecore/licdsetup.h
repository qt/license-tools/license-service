/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "settings.h"
#include "inifileparser.h"

#include <string>

namespace QLicenseCore {

enum class SettingsType {
    Temporary = 0,
    Persistent = 1
};

class LicdSetup : public Settings
{
public:
    explicit LicdSetup(const std::string &settingsFilePath);

    bool init() override;
    bool set(const std::string &key, const std::string &value,
        SettingsType type = SettingsType::Temporary);

public:
    static std::string getQtAppDataLocation();

private:
    bool getServiceInfo();
    bool writeDefaultSettings();
    void setHardwareId(const std::string &hwId);

private:
    std::string m_settingsFilePath;
    IniFileParser m_parser;
};

} // namespace QLicenseCore
