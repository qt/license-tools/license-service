/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "commonsetup.h"

#include <cstdint>
#include <vector>
#include <string>

namespace QLicenseCore {

constexpr char sc_true[] = "true";
constexpr char sc_false[] = "false";

// Constants used through the LicdSetup and consuming classes
constexpr char sc_termsAndConditionsAccepted[] = "terms_and_conditions_accepted";

constexpr char sc_reservationAccessPoint[] = "reservation_access_point";
constexpr char sc_reservationReleaseAccessPoint[] = "reservation_release_access_point";
constexpr char sc_reservationRenewalAccessPoint[] = "reservation_renewal_access_point";
constexpr char sc_usageAccessPoint[] = "usage_access_point";
constexpr char sc_versionQueryAccessPoint[] = "version_query_access_point";
constexpr char sc_loginAccessPoint[] = "login_access_point";
constexpr char sc_licensePrecheckAccessPoint[] = "license_precheck_access_point";

constexpr char sc_serverAddr[] = "server_addr";
constexpr char sc_tcpListeningPort[] = "tcp_listening_port";

constexpr char sc_caBundlePath[] = "ca_bundle_path";
constexpr char sc_certificateRevokeCheck[] = "certificate_revoke_check";

constexpr char sc_hwId[] = "hw_id";
constexpr char sc_hostOs[] = "host_os";
constexpr char sc_email[] = "email";
constexpr char sc_jwt[] = "jwt";

constexpr char sc_queueMinRetryInterval[] = "queue_min_retry_interval";
constexpr char sc_queueIntervalIncrement[] = "queue_interval_increment";
constexpr char sc_queueMaxRetryInterval[] = "queue_max_retry_interval";
constexpr char sc_onDemandShutdownTimeout[] = "on_demand_shutdown_timeout";
constexpr char sc_requestTimeout[] = "request_timeout";
constexpr char sc_executionStatLeewayTime[] = "execution_stat_leeway_time";
constexpr char sc_executionStatUploadInterval[] = "execution_stat_upload_interval";

constexpr uint64_t sc_maxExecutionStatLeeway = (1000 * SECS_IN_DAY);

constexpr char sc_iso8601TimeFormat[] = "%Y-%m-%dT%H:%M:%S.708Z";

constexpr char sc_qtlicdIniFileSection[] = "QtLicenseDaemon";

constexpr char sc_termsAndConditionsUrl[] = "https://www.qt.io/terms-conditions";
constexpr char sc_termsAndConditionsTitle[]
    = "This Agreement applies to your download and installation of Qt software as a "
      "Designated User of a Customer under a valid agreement for the licensing of Qt "
      "software (each as defined in the Agreement).";
constexpr char sc_termsAndConditionsDesignatedUserConfirmation[]
    = "I confirm that I am a Designated User of a Customer.";
constexpr char sc_termsAndConditionsConfirmation[]
    = "I acknowledge, accept, and will abide by the terms of this Agreement as a "
      "Designated User. In the event of any discrepancy between this Agreement and the "
      "agreement between the Customer for whom I am a Designated User, that agreement "
      "with the Customer governs my use and installation of Qt software to the extent "
      "of the discrepancy.";

// Constants used in the ConsumerAppInfo class
constexpr char sc_consumerId[] = "consumer_id";
constexpr char sc_consumerVersion[] = "consumer_version";
constexpr char sc_consumerProcessId[] = "consumer_process_id";
constexpr char sc_consumerInstallationSchema[] = "consumer_installation_schema";
constexpr char sc_consumerSupportedFeatures[] = "consumer_supported_features";
constexpr char sc_consumerBuildTimestamp[] = "consumer_build_timestamp";
constexpr char sc_componentId[] = "component_id";

static const std::vector<std::string> sc_caBundleSearchPaths {
    // from SEARCH_CA_BUNDLE_PATHS in 3rdparty/curl/CMakeList.txt
    "/etc/ssl/certs/ca-certificates.crt",
    "/etc/pki/tls/certs/ca-bundle.crt",
    "/usr/share/ssl/certs/ca-bundle.crt",
    "/usr/local/share/certs/ca-root-nss.crt",
    "/etc/ssl/cert.pem"
};

// Other

} // namespace QLicenseCore
