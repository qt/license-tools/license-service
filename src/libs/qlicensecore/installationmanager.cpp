/* Copyright (C) 2025 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "installationmanager.h"

#include "utils.h"
#include "inifileparser.h"
#include "logger.h"
#include "version.h"
#include "licdsetup.h"
#include "commonsetup.h"

namespace QLicenseCore {

/*
    \class InstallationManager

    \brief The InstallationManager class provides an interface for maintaining records
           of installed Qt License Service versions.
*/

/*
    Constructs a new installation manager, which operates on a local user specific
    configuration file. An optional \a configPath parameter can be provided to
    instead use the absolute path to a caller specified configuration file.
*/
InstallationManager::InstallationManager(const std::string &configPath)
    : m_configFilePath(!configPath.empty() ? configPath : LicdSetup::getQtAppDataLocation()
        + USER_SETTINGS_FOLDER_NAME + DIR_SEPARATOR + DAEMON_INSTALLATIONS_FILE)
{
}

/*
    Returns the configuration file path this installation manager operates on.
*/
std::string InstallationManager::configFilePath() const
{
    return m_configFilePath;
}

/*
    Returns the absolute path to a Qt License Service installation suitable for use with
    client library \a version, or empty string if no suitable installation was found.

    Installation with latest version, that matches the major version of the client is
    preferred. If an installation entry contains the \c preview property, it will be
    only returned if no non-preview installations of the same version exist.
*/
std::string InstallationManager::installationForVersion(const std::string &version) const
{
    IniFileParser parser(m_configFilePath);
    std::map<std::string, std::map<std::string, std::string>> sections;
    if (!parser.readSections(sections)) {
        logError("Unable to find License Service installation from: %s", m_configFilePath.c_str());
        return "";
    }
    std::map<std::string, std::string> suitableMatches;
    std::map<std::string, std::string> suitablePreviewMatches;
    for (const auto &section : sections) {
        const std::string &foundVersion = sections[section.first]["version"];
        const std::string &foundPath = sections[section.first]["path"];
        const std::string &foundPreview = sections[section.first]["preview"];
        if (foundVersion.empty()) {
            logDebug("Misconfiguration in '%s'. Version not found for: %s", m_configFilePath.c_str(),
                     foundPath.c_str());
            continue;
        }

        if (utils::compareVersion(version, foundVersion, 1) != 0) {
            // the parsed major version is smaller than the asked minimum version
            continue;
        }
        // we can replace existing match if the version number matches as if the versions match
        // then they should be identical
        if (foundPreview == "true")
            suitablePreviewMatches[foundVersion] = foundPath;
        else
            suitableMatches[foundVersion] = foundPath;
    }

    // Insert preview elements, if the container doesn't already contain the same version key
    suitableMatches.insert(suitablePreviewMatches.begin(), suitablePreviewMatches.end());

    if (!suitableMatches.empty()) {
        auto bestMatch = *suitableMatches.rbegin();
        const std::string foundVersion = bestMatch.first;
        const std::string foundPath = bestMatch.second;
        logInfo("Service version matched the version requirement of client library "
                "%s version, found service installation %s from: %s",
                version.c_str(), foundVersion.c_str(), foundPath.c_str());
        return foundPath;
    }

    logWarn("Service version matching the version requirement of the "
            "client library %s version could not be found from: %s",
            version.c_str(), m_configFilePath.c_str());

    return std::string();
}

/*
    Register a new installation in \a path, with the installation \a source. An optional
    map of key-value \a properties can be provided as a parameter.

    Returns \c true if the installation was registered successfully, \c false otherwise.
*/
bool InstallationManager::registerInstallation(const std::string &path, const std::string &version,
    const std::string &source, const std::map<std::string, std::string> &properties) const
{
    const std::string currentTime = utils::localTimeToString(utils::localTime());
    const std::string formattedProgramPath = utils::replaceCharacters(path, ":/\\", '_');

    std::map<std::string, std::string> installationContent = {
        { "path", path },
        { "source", source },
        { "version", version },
        { "timestamp", currentTime }
    };

    if (!properties.empty())
        installationContent.insert(properties.begin(), properties.end());

    IniFileParser parser(m_configFilePath);
    if (!parser.writeKeyValue(formattedProgramPath, installationContent)) {
        logError("Failed to register '%s' into: %s by: %s", path.c_str(),
            m_configFilePath.c_str(), source.c_str());
        return false;
    }

    logInfo("%s registered into: %s by: %s", path.c_str(), m_configFilePath.c_str(),
        source.c_str());

    return true;
}

/*
    Unregisters an installation in \a path. Returns \c true on success, \c false otherwise.
*/
bool InstallationManager::unregisterInstallation(const std::string &path) const
{
    IniFileParser parser(m_configFilePath);
    const std::string formattedProgramPath = utils::replaceCharacters(path, ":/\\", '_');

    if (!parser.removeSection(formattedProgramPath)) {
        logError("Failed to unregister '%s' from: %s", path.c_str(), m_configFilePath.c_str());
        return false;
    }

    logInfo("%s unregistered from: %s", path.c_str(), m_configFilePath.c_str());
    return true;
}

/*
    Removes installation entries which point to a non-existing path from the
    installation configuration file.
*/
void InstallationManager::purgeObsoleteInstallations() const
{
    if (!utils::fileExists(m_configFilePath))
        return;

    IniFileParser parser(m_configFilePath);
    std::map<std::string, std::map<std::string, std::string>> sections;
    if (!parser.readSections(sections))
        return;

    for (const auto &section : sections) {
        const std::string &foundPath = sections[section.first]["path"];
        if (!utils::fileExists(foundPath)) {
            parser.removeSection(section.first);
            logDebug("Purged non-existent service installation '%s' from: %s",
                     section.first.c_str(), m_configFilePath.c_str());
        }
    }
}

} // namespace QLicenseCore
