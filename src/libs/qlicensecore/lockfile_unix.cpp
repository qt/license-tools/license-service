/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "lockfile.h"

#include "utils.h"
#include "logger.h"

#include <cerrno>
#include <sys/file.h>
#include <unistd.h>

namespace QLicenseCore {

LockFile::LockFile(const std::string &filename)
    : m_filename(filename)
    , m_handle(-1)
    , m_locked(false)
{
}

LockFile::~LockFile()
{
    if (m_handle != -1 && m_locked) {
        if (!tryUnlock())
            logError("Could not unlock lock file %s: %s", m_filename.c_str(), m_error.c_str());
    }
}

bool LockFile::tryLock(const std::string &fileData)
{
    m_error.clear();
    if (m_locked)
        return true;

    std::string directoryPath = m_filename.substr(0, m_filename.find_last_of('/'));
    if (!directoryPath.empty()) {
        struct stat st = {0};
        if (stat(directoryPath.c_str(), &st) == -1) {
            if (mkdir(directoryPath.c_str(), 0755) == -1 && errno != EEXIST) {
                m_error = "Cannot create directory for lock file \"" + m_filename + "\": " + strerror(errno);
                return false;
            }
        }
    }

    errno = 0;
    m_handle = open(m_filename.c_str(), O_CREAT | O_RDWR | O_NONBLOCK, 0600);
    if (m_handle == -1) {
        m_error = "Cannot open lock file \"" + m_filename + "\": " + strerror(errno);
        return false;
    }

    int flags = fcntl(m_handle, F_GETFD);
    if (flags == -1) {
        m_error = "Cannot get file descriptor flags for lock file \"" + m_filename + "\": " + strerror(errno);
        closeHandle();
        return false;
    }

    // Add FD_CLOEXEC flag to prevent the file descriptor from being inherited by child processes
    if (fcntl(m_handle, F_SETFD, flags | FD_CLOEXEC) == -1) {
        m_error = "Cannot set FD_CLOEXEC on lock file \"" + m_filename + "\": " + strerror(errno);
        closeHandle();
        return false;
    }

    errno = 0;
    m_locked = flock(m_handle, LOCK_NB | LOCK_EX) != -1;
    if (!m_locked) {
        m_error = "Cannot apply lock \"" + m_filename + "\": " + strerror(errno);
        closeHandle();
        return false;
    }

    const uint64_t pid = utils::getPID();
    const std::string data = fileData.empty() ? std::to_string(pid) : fileData;

    errno = 0;
    uint64_t totalWritten = 0;
    while (totalWritten < data.size()) {
        const uint64_t written = write(m_handle, data.c_str() + totalWritten, data.size() - totalWritten);
        if (written < 0) {
            tryUnlock();
            m_error = "Cannot write to lock file \"" + m_filename + "\": " + strerror(errno);
            return false;
        }
        totalWritten += written;
    }

    return m_locked;
}

bool LockFile::tryUnlock()
{
    m_error.clear();
    if (!m_locked || m_handle == -1)
        return true;

    errno = 0;
    m_locked = flock(m_handle, LOCK_UN | LOCK_NB) == -1;
    if (m_locked)
        m_error = "Cannot remove lock \"" + m_filename + "\": " + strerror(errno);
    else {
        unlink(m_filename.c_str());
        closeHandle();
    }

    return !m_locked;
}

bool LockFile::isLocked() const
{
    return m_locked;
}

std::string LockFile::error() const
{
    return m_error;
}

std::string LockFile::filename() const
{
    return m_filename;
}

void LockFile::closeHandle()
{
    if (m_handle != -1) {
        close(m_handle);
        m_handle = -1;
    }
}

} // namespace QLicenseCore
