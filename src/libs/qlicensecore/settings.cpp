/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "settings.h"

#include "logger.h"

namespace QLicenseCore {

/*
    \brief
    A basic key-value based storage for string setting values.
*/

/*
    Default constructor for the settings.
*/
Settings::Settings()
    : m_initialized(false)
{
}

/*
    A virtual initialization function for the settings. Derived classes may
    want to override this to perform some initialization step, like reading
    a configuration file to set the default values.

    The default implementation sets the settings as initialized and returns \c true.
*/
bool Settings::init()
{
    m_initialized = true;
    return true;
}

/*
    Sets a value of new or existing setting \a key to \a value.
*/
void Settings::set(const std::string &key, const std::string &value)
{
    m_settings[key] = value;
}

/*
    Returns a setting value for \a key, or empty string if no such setting was found.
*/
std::string Settings::get(const std::string &key) const
{
    if (!contains(key)) {
        logWarn("No settings value found for key \"%s\"", key.c_str());
        return std::string();
    }

    return m_settings.at(key);
}

/*
    Returns \c true if a setting matching \a key exists, \c false otherwise.
*/
bool Settings::contains(const std::string &key) const
{
    auto it = m_settings.find(key);
    return (it != m_settings.end());
}

} // namespace QLicenseCore
