/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <unordered_map>
#include <string>

namespace QLicenseCore {

class Settings
{
public:
    Settings();
    virtual ~Settings() = default;

    virtual bool init();
    virtual void set(const std::string &key, const std::string &value);
    virtual std::string get(const std::string &key) const;
    bool contains(const std::string &key) const;

protected:
    bool m_initialized;
    std::unordered_map<std::string, std::string> m_settings;
};

} // namespace QLicenseCore
