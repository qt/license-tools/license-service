/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/


#include <algorithm>
#include <array>
#include <iomanip>
#include <atomic>
#include <cctype>
#include <random>
#include <stdlib.h>
#include <sstream>
#include <fstream>
#include <iostream>

#include "utils.h"
#include "commonsetup.h"
#include "logger.h"

#if __linux__ || __APPLE__ || __MACH__
#include <csignal>
#include <fcntl.h>
#include <sys/resource.h>
#include <sys/utsname.h>
#endif

#if __linux__
#include <uuid/uuid.h>
#endif

#if __APPLE__ || __MACH__
#include <limits.h>
#include <mach-o/dyld.h>
#include <iostream>
#include <CoreFoundation/CoreFoundation.h>
#endif

#if _WIN32
#include <windows.h>
#include <libloaderapi.h>
#include <string>
#include <strsafe.h>
#include <rpc.h>
#include <Shlobj.h>
#endif

namespace QLicenseCore { namespace utils {

// Trim whitespaces from start of a string
void trimLeft(std::string &s)
{
    s.erase(s.begin(), std::find_if (s.begin(), s.end(), [](unsigned char ch)
                                    { return !(std::isspace(ch)); }));
}

// Trim whitespaces from end of a string
void trimRight(std::string &s)
{
    s.erase(std::find_if (s.rbegin(), s.rend(), [](unsigned char ch)
                         { return !std::isspace(ch); })
                .base(),
            s.end());
}

// Trim whitespaces from both ends of a string
void trimStr(std::string &s)
{
    trimLeft(s);
    trimRight(s);
}

// Split a string by a char (Defaults to a space)
std::vector<std::string> splitStr(const std::string &str, const char delimiter)
{
    std::vector<std::string> strings;
    std::istringstream ss(str);
    std::string tmpStr;
    while (getline(ss, tmpStr, delimiter)) {
        trimStr(tmpStr);
        strings.push_back(tmpStr);
    }
    return strings;
}

// Convert string to lower case
std::string strToLower(const std::string &str)
{
    std::string ret = str;
    std::transform(ret.begin(), ret.end(), ret.begin(), ::tolower);
    return ret;
}

// Convert string to upper case
std::string strToUpper(const std::string &str)
{
    std::string ret = str;
    std::transform(ret.begin(), ret.end(), ret.begin(), ::toupper);
    return ret;
}

// String to int: Because stoi() raises an exception upon failing
int strToInt(const std::string &str)
{
    int retVal = 0;
    try {
        retVal = stoi(str);
    }
    catch (...) {
        logError("Unable to convert '%s' to an int", str.c_str());
    }
    return retVal;
}

bool strToUint64(const std::string &input, uint64_t &output) {
    try {
        output = stoull(input);
    }
    catch (...) {
        logError("Unable to convert '%s' to an uint64_t", input.c_str());
        return false;
    }
    return true;
}

std::string generateRandomString(size_t length)
{
    static const char characters[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    std::random_device rd;
    std::mt19937 engine(rd());
    // subtract the null terminator '\0'
    std::uniform_int_distribution<> dis(0, sizeof(characters) - 2);

    std::string str;
    str.reserve(length);
    for (size_t i = 0; i < length; ++i)
        str += characters[dis(engine)];

    return str;
}

std::string replaceCharacters(const std::string &input, const std::string &charactersToReplace, const char &replacement)
{
    std::string result = input;
    for (char c : charactersToReplace)
        std::replace(result.begin(), result.end(), c, replacement);
    return result;
}

bool constTimeCompare(const std::string &a, const std::string &b)
{
    if (a.size() != b.size())
        return false;

    const volatile unsigned char *_a = reinterpret_cast<const volatile unsigned char *>(a.c_str());
    const volatile unsigned char *_b = reinterpret_cast<const volatile unsigned char *>(b.c_str());

    const size_t size = a.size();

    unsigned char result = 0;
    for (size_t i = 0; i < size; i++)
        result |= _a[i] ^ _b[i];

    // 0 if equal, nonzero otherwise
    return (result == 0);
}

std::string getUserHomeDir()
{
    // Try to find current user's home
    std::string retVal = "";
    const char* homedir;
#if __linux__ || __APPLE__ || __MACH__
    // Linux and Mac
    homedir = std::getenv("HOME");
    if (homedir == nullptr) {
        homedir = getpwuid(getuid())->pw_dir;
    }
#else
    // Windows
    const std::string narrowStr = narrow(_wgetenv(widen("USERPROFILE").c_str()));
    homedir = narrowStr.c_str();
#endif
    if (homedir == nullptr) {
        logError("Not able to determine user's home directory");
        return std::string();
    }
    retVal += std::string(homedir);
    return retVal;
}

std::string getTempDir()
{
#if __linux__ || __APPLE__ || __MACH__
    const char* tempDir = nullptr;
    tempDir = std::getenv("TMPDIR");
    if (tempDir != nullptr)
        return std::string(tempDir);

    tempDir = std::getenv("TEMP");
    if (tempDir != nullptr)
        return std::string(tempDir);

    // Default to /tmp if neither variable is set
    return "/tmp";
#else
    std::array<wchar_t, MAX_PATH> buffer;
    DWORD result = GetTempPathW(buffer.size(), buffer.data());

    if (result == 0 || result > buffer.size())
        return std::string();

    return std::string(narrow(buffer.data()));
#endif
}

// Write data to the file
bool writeToFile(const std::string &filepath, const std::string &data, bool append)
{
    std::ofstream file;
#if _WIN32
    if (append) {
        file.open(utils::widen(filepath), std::ios::out | std::ios::app);
    } else {
        file.open(utils::widen(filepath), std::ios::out);
    }
#else
    if (append) {
        file.open(filepath, std::ios::out | std::ios::app);
    } else {
        file.open(filepath, std::ios::out);
    }
#endif
    if (file.fail()) {
        logError("Failed to write data in file %s", filepath.c_str());
        return false;
    }
    try {
        file << data << std::endl;
    } catch (...) {
        file.close();
        logError("Failed to write data in file %s", filepath.c_str());
        return false;
    }
    file.close();
    return true;
}

/*
    Creates a directory, including any leading directories, for filepath.
*/
bool createDir(const std::string &filepath)
{
    std::vector<std::string> pathVector = utils::splitStr(filepath, DIR_SEPARATOR);
    std::string path;
#if __linux__ || __APPLE__ || __MACH__
    // Restore root directory separator
    path += SYSTEM_ROOT;
#endif
    for (const std::string &dir : pathVector) {
        path += dir;
        path += DIR_SEPARATOR;
        if (!utils::fileExists(path)) {
#if __linux__ || __APPLE__ || __MACH__
            errno = 0;
            if (mkdir(path.c_str(), 0755) != 0) {
                logError("mkdir failed for directory \"%s\": %s", path.c_str(), strerror(errno));
                return false;
            }

            if (chmod(path.c_str(), 0755) != 0) {
                logError("chmod failed for directory \"%s\": %s", path.c_str(), strerror(errno));
                return false;
            }
#else
            if (CreateDirectoryW(widen(path).c_str(), NULL) == 0)
                return false;
#endif
        }
    }
    return true;
}

bool removeDir(const std::string &path)
{
    // do sanity checks for given path
    if (path.empty()) {
        logWarn("Refusing to remove empty path");
        return false;
    } else if (path == SYSTEM_ROOT) {
        logWarn("Refusing to remove root directory");
        return false;
    } else if (path == getUserHomeDir()) {
        logWarn("Refusing to remove home directory");
        return false;
    } else if ((path.rfind(".", 0) == 0) || (path.rfind(".") == path.length() - 1)) {
        logWarn("Refusing to remove relative directory");
        return false;
    }

#if __linux__ || __APPLE__ || __MACH__
    errno = 0;
    DIR* dir = opendir(path.c_str());
    if (!dir) {
        logWarn("Cannot open directory %s: %s", path.c_str(), strerror(errno));
        return false;
    }

    struct dirent* ent;
    while ((ent = readdir(dir)) != nullptr) {
        // do not allow dot and dot-dot directories
        if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0)
            continue;

        errno = 0;
        const std::string filePath = path + DIR_SEPARATOR + ent->d_name;
        struct stat fileStat;
        if (lstat(filePath.c_str(), &fileStat) == -1) {
            closedir(dir);
            logWarn("Failed to get file status for path %s: %s", filePath.c_str(), strerror(errno));
            return false;
        }

        if (S_ISDIR(fileStat.st_mode)) {
            if (!removeDir(filePath)) {
                closedir(dir);
                return false;
            }
        } else {
            // regular file, delete it
            errno = 0;
            if (unlink(filePath.c_str()) != 0) {
                closedir(dir);
                logWarn("Failed to remove file %s: %s", filePath.c_str(), strerror(errno));
                return false;
            }
        }
    }

    errno = 0;
    closedir(dir);
    if (rmdir(path.c_str()) != 0) {
        logWarn("Failed to remove directory %s: %s", path.c_str(), strerror(errno));
        return false;
    }

    return true;
#else
    WIN32_FIND_DATAW findFileData;
    HANDLE hFind = FindFirstFileW(widen((path + "\\*")).c_str(), &findFileData);

    if (hFind == INVALID_HANDLE_VALUE) {
        logWarn("Failed to open directory %s", path.c_str());
        return false;
    }

    do {
        const std::string fileName = narrow(findFileData.cFileName);
        if (fileName != "." && fileName != "..") {
            const std::string filePath = path + "\\" + fileName;

            if (findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
                if (!removeDir(filePath)) {
                    FindClose(hFind);
                    return false;
                }
            } else {
                if (!DeleteFileW(widen(filePath).c_str())) {
                    FindClose(hFind);
                    logWarn("Failed to remove file %s: %s", filePath.c_str(), getLastWin32Error().c_str());
                    return false;
                }
            }
        }
    } while (FindNextFileW(hFind, &findFileData) != 0);

    FindClose(hFind);

    if (!RemoveDirectoryW(widen(path).c_str())) {
        logWarn("Failed to remove directory %s: %s", path.c_str(), getLastWin32Error().c_str());
        return false;
    }

    return true;
#endif
}

bool readFile(std::string &str, const std::string &filepath)
{
    std::ifstream file;
    std::stringstream ss;

#if _WIN32
    file.open(widen(filepath), std::ios::in);
#else
    file.open(filepath, std::ios::in);
#endif
    if (file.is_open()) {
        std::string line;
        while (getline(file, line)) {
            ss << line << std::endl;
        }
        file.close();
    } else {
        return false;
    }
    str = ss.str();
    return true;
}

#if _WIN32
bool readFileWin32(std::string &str, const std::string &filepath)
{
    // Set share mode flags to allow reading even if the file is opened for writing
    // by another process, or marked for deletion (like our QLicenseCore::LockFile does)
    HANDLE hFile = CreateFileW(
        utils::widen(filepath).c_str(),
        GENERIC_READ,
        FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
        NULL,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        NULL
    );

    if (hFile == INVALID_HANDLE_VALUE) {
        logError("Error opening file: %s", utils::getLastWin32Error().c_str());
        return false;
    }

    std::vector<char> buffer;
    const DWORD chunkSize = 1024;
    DWORD bytesRead;
    char tempBuffer[chunkSize];

    do {
        if (!ReadFile(hFile, tempBuffer, chunkSize, &bytesRead, NULL)) {
            logError("Error reading file: %s", utils::getLastWin32Error().c_str());
            CloseHandle(hFile);
            return false;
        }

        buffer.insert(buffer.end(), tempBuffer, tempBuffer + bytesRead);

    } while ((bytesRead > 0));

    CloseHandle(hFile);
    str = std::string(buffer.begin(), buffer.end());
    return true;
}
#endif

bool fileExists(const std::string &name)
{
#if __linux__ || __APPLE__ || __MACH__
    struct stat buffer;
    return (stat (name.c_str(), &buffer) == 0);
#else
    if (_waccess(widen(name).c_str(), 0) == -1)
        return false;

    return true;
#endif
}

std::string getFileOwnerName(const std::string &filename)
{
    std::string retVal;
#if __linux__ || __APPLE__ || __MACH__
    struct stat info;
    stat(filename.c_str(), &info);  // Error check omitted
    struct passwd *pw = getpwuid(info.st_uid);
    if (pw != 0) retVal = pw->pw_name; // contains the user name

#else
    logWarn("Uniplemented: Win version of getFileOwnerName()");
    retVal = "NULL";
#endif
    return retVal;
}

void getFileOwner(const std::string &filename, uint16_t &owner, uint16_t &group)
{
#if __linux__ || __APPLE__ || __MACH__
    struct stat info;
    stat(filename.c_str(), &info);  // Error check omitted
    owner = info.st_uid;
    group = info.st_gid;
#else
   logWarn("Uniplemented: Win version of getFileOwner()");
#endif
}

std::vector<std::string> getDirListing(const std::string &directory, const std::string &filter, int typeMask)
{
    /* Open directory stream */
    DIR *dir = opendir(directory.c_str());
    std::vector<std::string> files;
    if (!dir) {
        /* Could not open directory */
        logWarn("Cannot open directory %s,", directory.c_str());
        return files; // Return an empty vector
    }
    /* Gather files and directories within the directory */
    struct dirent *ent;
    while ((ent = readdir(dir)) != nullptr) {
        if (ent->d_type == typeMask) {
            // do not allow dot and dot-dot directories
            if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0)
                continue;

            std::string name = ent->d_name;
            if (name.substr(0, filter.length()) == filter || filter.empty()) {
                files.push_back(name);
            }
        }
    }
    closedir(dir);
    return files;
}

bool deleteFile(const std::string &filepath)
{
    if (fileExists(filepath)) {
        std::remove(filepath.c_str()); // delete file
        return true;
    }
    logWarn("No file %s found to delete", filepath.c_str());

    return false;
}

std::string getFilenameWithoutPath(std::string path)
{
    // Removes path, leaving filename only (with extension
    return path.substr(path.find_last_of(DIR_SEPARATOR) + 1);
}

std::string getFilenameWithoutExtension(std::string path)
{
    // Removes path and 4 last chars (.cpp), leaving filename only.
    // Extension removal is not generic, but is enough here for logging purposes.
    std::string str = getFilenameWithoutPath(path);
    return str.substr(0, str.find_last_of('.'));
}

std::string getPathWithoutFilename(std::string path)
{
    return path.substr(0, path.find_last_of("\\/"));
}

std::string getProgramPath()
{
    const size_t bufferSize = 1024;
#if __linux__
    char buffer[bufferSize];
    ssize_t bytesRead = readlink("/proc/self/exe", buffer, bufferSize - 1);
    if (bytesRead != -1) {
        buffer[bytesRead] = '\0';
        return std::string(buffer);
    } else {
        logError("Unable to read program path!");
        return "";
    }
#elif __APPLE__ || __MACH__
    char buffer[bufferSize];
    uint32_t size = sizeof(buffer);
    if (_NSGetExecutablePath(buffer, &size) != 0) {
        logError("Unable to read program path!");
        return "";
    } else {
        return std::string(buffer);
    }
#else
    wchar_t path[bufferSize];
    if (!GetModuleFileNameW(NULL, path, bufferSize)) {
        logError("Unable to read program path!");
        return "";
    }
    return narrow(path);
#endif
}

uint16_t generateUniqueShortId()
{
    static std::atomic<uint16_t> uid(0);
    return ++uid;
}

bool getCurrentUserName(std::string &username) {
    if (!getUsernameFromEnv(username)) {
        if (!getUsernameFromOS(username))
            return false;
    }
    return true;
}

bool getUsernameFromEnv(std::string &username)
{
#if _WIN32
    username = narrow(_wgetenv(widen("QTLICD_CLIENT_USERNAME").c_str()));
    if (username.empty())
        return false;

    return true;
#else
    const char* user = std::getenv("QTLICD_CLIENT_USERNAME");

    if (!user) {
        return false;
    }
    username = std::string(user);
    return true;
#endif
}

bool getUsernameFromOS(std::string &username)
{
#ifdef _WIN32
    // Windows
    #include <lmcons.h>
    wchar_t user[UNLEN + 1];
    DWORD size = UNLEN + 1;
    if (!GetUserNameW(user, &size))
        return false;
    username = std::string(narrow(user));
#elif __APPLE__ || __MACH__ || __linux__
    // Mac / Linux
    uid_t uid = geteuid();  // Get the effective user ID

    struct passwd pw;
    struct passwd *pw_result;

    size_t bufsize = sysconf(_SC_GETPW_R_SIZE_MAX);
    if (bufsize == -1)   // Value was indeterminate
        bufsize = 16384; // Should be more than enough

    std::vector<char> buf(bufsize);
    int ret = getpwuid_r(uid, &pw, buf.data(), buf.size(), &pw_result);
    if (ret != 0 || !pw_result) {
        logError("Cannot get local user account record: %s", strerror(ret));
        return false;
    }

    username = pw.pw_name;
#endif
    return true;
}

std::string getOsName()
{
#if _WIN32
    return "Windows";
#elif __APPLE__ || __MACH__
    return "macOS";
#elif __linux__
    return "Linux";
#elif __FreeBSD__
    return "FreeBSD";
#elif __unix || __unix__
    return "Unix";
#else
    return "Unknown OS";
#endif
}

std::string getOsArchitecture()
{
    std::string osArch;
#if __linux__ || __APPLE__ || __MACH__
    struct utsname sys_info;
    if (uname(&sys_info) == 0) {
        osArch = sys_info.machine;
    } else {
        osArch = "N/A";
        logError("Failed to get system information (uname failed)");
    }

#elif _WIN32
    SYSTEM_INFO sysInfo;
    GetSystemInfo(&sysInfo);

    switch (sysInfo.wProcessorArchitecture) {
    case PROCESSOR_ARCHITECTURE_AMD64:
        osArch = "x86_64";
        break;
    case PROCESSOR_ARCHITECTURE_INTEL:
        osArch = "x86";
        break;
#ifdef PROCESSOR_ARCHITECTURE_ARM64
    case PROCESSOR_ARCHITECTURE_ARM64:
        osArch = "arm64";
        break;
#endif
#ifdef PROCESSOR_ARCHITECTURE_ARM
    case PROCESSOR_ARCHITECTURE_ARM:
        osArch = "arm";
        break;
#endif
    default:
        osArch = "N/A";
        logError("Unknown architecture");
        break;
    }
#else
    logError("Unsupported platform");
#endif
    return osArch;
}

std::string getHostname()
{
#if __APPLE__ || __MACH__ || __linux__
    char hostname[256];
    if (::gethostname(hostname, sizeof(hostname)) == 0)
        return std::string(hostname);

    return std::string();
#elif _WIN32
    wchar_t hostname[MAX_COMPUTERNAME_LENGTH + 1];
    DWORD size = sizeof(hostname);
    if (GetComputerNameW(hostname, &size))
        return std::string(narrow(hostname));

    return std::string();
#else
    // unsupported platform
    return std::string();
#endif
}

int getHostWordSize()
{
    // Size of a pointer, multiplied by 8, to convert to bits
    return 8 * sizeof(void *);
}

uint64_t getPID() {
#ifdef _WIN32
    return GetCurrentProcessId();
#else
    return ::getpid();
#endif

}


#if _WIN32
/*
 * Windows  implementation of missing POSIX strptime()
*/
char* strptime(const char* s,
                          const char* f,
                          struct tm* tm) {
  std::istringstream input(s);
  input.imbue(std::locale(setlocale(LC_ALL, nullptr)));
  input >> std::get_time(tm, f);
  if (input.fail()) {
    return nullptr;
  }
  return (char*)(s + input.tellg());
}
#endif

std::string localTimeToString(time_t epochTime, const char* format)
{
    char timestamp[64] = {0};
    strftime(timestamp, sizeof(timestamp), format, localtime(&epochTime));
    return timestamp;
}

time_t stringToLocalTime(const char* time, const char* format)
{
    if (time == nullptr || time[0] == '\0') {
        time = "1980-01-01 00:00:00";
    }
    std::tm tmTime;
    memset(&tmTime, 0, sizeof(tmTime));
    strptime(time, format, &tmTime);
    return mktime(&tmTime);
}

void setBlockSignalsMask()
{
#if __linux__ || __APPLE__ || __MACH__
    // block termination signals in thread invoking this
    sigset_t signal_set;
    sigemptyset(&signal_set);
    sigaddset(&signal_set, SIGINT);
    sigaddset(&signal_set, SIGTERM);
    pthread_sigmask(SIG_BLOCK, &signal_set, NULL);
#endif
}

// Adler32 hash tool

#define MOD_ADLER 65521

std::string unsignedLongToString(unsigned long v)
{
    std::stringstream out;
    out << v;
    return out.str();
}

void update_adler32(unsigned long &chksum, const unsigned char *data, size_t len)
{
    unsigned long a=chksum&0xFFFF;
    unsigned long b=(chksum>>16)&0xFFFF;

    while (len > 0)
    {
        size_t tlen = len > 4096 ? 4096 : len;
        len -= tlen;
        do
        {
            a += *data++;
            b += a;
        } while (--tlen);

        a %= MOD_ADLER;
        b %= MOD_ADLER;
    }

    chksum = (b << 16) | a;
}

unsigned long adler32( const unsigned char *data, size_t len )
{
    unsigned long chksum = 0;
    update_adler32( chksum, data, len );
    return chksum;
}

void sleepSecs(uint32_t timeInSecs)
{
#if __linux__ || __APPLE__ || __MACH__
    sleep(timeInSecs); // secs
#else
    Sleep(timeInSecs * 1000); // millisecs in Windows
#endif
}

void sleepMillisecs(uint32_t timeInMillisecs)
{
#if __linux__ || __APPLE__ || __MACH__
    usleep(timeInMillisecs * 1000); // microsecs in *nix
#else
    Sleep(timeInMillisecs); // millisecs in Windows
#endif
}

bool startDetached(const std::string &program, const std::vector<std::string> &args)
{
#if __linux__ || __APPLE__ || __MACH__
    std::vector<char *> argv;
     // First argument is excepted to be the program name
    argv.push_back(const_cast<char *>(program.c_str()));
    for (const auto &arg : args)
        argv.push_back(const_cast<char *>(arg.c_str()));

    // Last argument for exec must be a null pointer
    argv.push_back(nullptr);

    errno = 0;
    pid_t pid = fork();
    if (pid < 0) {
        logError("Failed to fork process: %s", strerror(errno));
        return false;
    } else if (pid > 0) {
        // Parent process, return
        return true;
    } else {
        if (setsid() < 0) {
            logError("Failed to create a new session: %s", strerror(errno));
            return false;
        }

        int devNull = open("/dev/null", O_RDWR);
        if (devNull < 0) {
            logError("Failed to open /dev/null: %s", strerror(errno));
            return false;
        }

        // Redirect stdin, stdout, stderr
        if (dup2(devNull, STDIN_FILENO) < 0
                || dup2(devNull, STDOUT_FILENO) < 0
                || dup2(devNull, STDERR_FILENO) < 0) {
            logError("Failed to redirect standard I/O streams: %s", strerror(errno));
            close(devNull);
            return false;
        }

        close(devNull);

        // Child, replace the process image
        errno = 0;
        execv(program.c_str(), argv.data());
        logError("Failed to execute program %s: %s", program.c_str(), strerror(errno));
        // execv only returns if there's an error
        return false;
    }
#elif _WIN32
    // First argument is excepted to be the program name
    std::string cmdLine = program;
    for (const auto &arg : args)
        cmdLine += " " + arg;

    STARTUPINFOW si;
    PROCESS_INFORMATION pi;

    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);

    // Set standard handles to NULL to detach them from the parent process
    si.dwFlags = STARTF_USESTDHANDLES;
    si.hStdInput = NULL;
    si.hStdOutput = NULL;
    si.hStdError = NULL;

    ZeroMemory(&pi, sizeof(pi));

    // Start the child process
    if (!CreateProcessW(NULL, const_cast<wchar_t *>(widen(cmdLine).c_str()),
            NULL, NULL, FALSE, CREATE_NEW_PROCESS_GROUP | CREATE_NO_WINDOW, NULL, NULL, &si, &pi)) {
        logError("Failed to create process %s: %s", program.c_str(), getLastWin32Error().c_str());
        return false;
    }

    // Close process and thread handles
    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);

    return true;
#else
    logError("Unsupported platfrom for startDetached");
    return false;
#endif
}

#if _WIN32
std::string getLastWin32Error()
{
    DWORD error = ::GetLastError();
    if (error == 0)
        return std::string();

    LPWSTR buffer = nullptr;

    FormatMessageW(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL, error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPWSTR)&buffer, 0, NULL
    );

    std::string errorString(narrow(buffer));
    LocalFree(buffer);

    return errorString;
}
#endif

/*
    Returns 1 if rhs is smaller, -1 if lhs is smaller, 0 if equal. Does not
    support versions containing non-numeric parts. If parts is not 0, compares
    only the number of given parts from left to right, otherwise compares as
    many parts as the version with most parts has.
*/
int compareVersion(const std::string &lhs, const std::string &rhs, int maxParts)
{
    // variables to store each numeric part of version string
    int lhsPart = 0;
    int rhsPart = 0;
    int currentParts = 0;

    // loop until both string are processed or maxParts is reached
    for (int i = 0, j = 0; (i < lhs.length() || j < rhs.length()); i++, j++) {
        if ((maxParts != 0) && currentParts >= maxParts)
            break;

        // store numeric part of version lhs in lhsPart
        while (i < lhs.length() && !isVersionNumSeparator(lhs[i])) {
            lhsPart = lhsPart * 10 + (lhs[i] - '0');
            i++;
        }
        // store numeric part of version rhs in rhsPart
        while (j < rhs.length() && !isVersionNumSeparator(rhs[j])) {
            rhsPart = rhsPart * 10 + (rhs[j] - '0');
            j++;
        }

        if (lhsPart > rhsPart)
            return 1;
        if (rhsPart > lhsPart)
            return -1;

        // if equal, reset variables and go for next numeric part
        lhsPart = 0;
        rhsPart = 0;
        currentParts++;
    }

    return 0;
}

bool isVersionNumSeparator(const char &c)
{
    return c == '.' || c == '-' || c == '_';
}

bool isVersionNum(const std::string& str)
{
    bool allValid = std::all_of(str.begin(), str.end(), [](char c) {
        return std::isdigit(c) || isVersionNumSeparator(c);
    });

    bool hasDigit = std::any_of(str.begin(), str.end(), [](char c) {
        return std::isdigit(c);
    });

    return allValid && hasDigit;
}

/*
*   App-specific utils here (not generic)
*/

std::string stripNonAscii(const std::string &str) {
    // Strip all special chars expect those required by protocol
    std::string output;
    // Reserve memory to avoid reallocations
    output.reserve(str.size());
    for (char c : str) {
        if (c == ':' || c == '.' || c == '-' || c == ' ' || c == '_' || c == '@') {
            output += c;
        } else if (std::isalnum(c) != 0) {
            output += c;
        }

    }
    return output;
}

time_t utcTime()
{
    // Get the current time point (in UTC timezone)
    auto now = std::chrono::system_clock::now();
    return std::chrono::system_clock::to_time_t(now);
}

time_t localTime()
{
    // Get the current time point (in UTC timezone)
    std::time_t nowTime = utcTime();

    // Convert to local timezone time
    std::tm* localTime = std::localtime(&nowTime);
    return std::mktime(localTime);
}

std::chrono::system_clock::time_point utcTimeMs()
{
    return std::chrono::system_clock::now();
}

std::string utcTimeToString(time_t utcTime, const char *format)
{
    if (!format)
        return std::string();

    std::tm *timeInfo = std::gmtime(&utcTime);
    if (!timeInfo)
        return std::string();

    // Format the time as a string
    std::ostringstream oss;
    oss << std::put_time(timeInfo, format);

    return oss.str();
}

time_t stringToUtcTime(const char *time, const char *format)
{
    if (!time || !format)
        return -1;

    // Parse the input string
    std::tm timeInfo = {};
    std::istringstream iss(time);
    iss >> std::get_time(&timeInfo, format);
    if (iss.fail())
        return -1;

    // Set to indicate that DST is not in effect
    timeInfo.tm_isdst = 0;

    // Convert to time_t (UTC), returns -1 on failure
    return (std::mktime(&timeInfo) - getTimezoneOffset());
}

/*
    Converts a time point \a utcTime to a \c std::string, and returns the result
    string, or an empty string on failure. The result string is of ISO 8601 format.
*/
std::string utcTimeMsToString(std::chrono::system_clock::time_point utcTime)
{
    if (utcTime == std::chrono::system_clock::time_point{})
        return std::string();

    // Truncate the time_point to seconds precision to ensure no rounding occurs.
    auto utcTimeSeconds = std::chrono::duration_cast<std::chrono::seconds>(utcTime.time_since_epoch());
    std::chrono::system_clock::time_point truncatedTime{utcTimeSeconds};

    std::time_t utcTime_t = std::chrono::system_clock::to_time_t(truncatedTime);

    // Extract the ms part
    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(
        utcTime.time_since_epoch()) % 1000;

    std::tm *timeInfo = std::gmtime(&utcTime_t);
    if (!timeInfo)
        return std::string();

    // Format the time as a string (up to seconds)
    std::ostringstream oss;
    oss << std::put_time(timeInfo, "%Y-%m-%dT%H:%M:%S");

    // Append the ms part and the 'Z' (marking UTC)
    oss << '.' << std::setw(3) << std::setfill('0') << ms.count() << 'Z';

    return oss.str();
}

/*
    Converts a \a time string to \c std::chrono::system_clock::time_point, and
    returns the resultant time point, or empty time point on failure. The \a time
    string must be of ISO 8601 format.
*/
std::chrono::system_clock::time_point stringToUtcTimeMs(const std::string &time)
{
    if (time.empty())
        return std::chrono::system_clock::time_point{};

    std::string timeWithoutMs;
    std::string millisecondsStr;

    // Find if there is a '.' indicating milliseconds
    size_t dotPos = time.find('.');
    if (dotPos != std::string::npos) {
        timeWithoutMs = time.substr(0, dotPos);
        millisecondsStr = time.substr(dotPos + 1);

        // Remove excess digits after milliseconds (like 'Z')
        if (millisecondsStr.size() > 3)
            millisecondsStr = millisecondsStr.substr(0, 3);
    } else {
        // Not found, take the entire time string
        timeWithoutMs = time;
        if (timeWithoutMs.back() == 'Z')
            timeWithoutMs.pop_back();
    }

    std::time_t utcTime = stringToUtcTime(timeWithoutMs.c_str(), "%Y-%m-%dT%H:%M:%S");
    if (utcTime == -1)
        return std::chrono::system_clock::time_point{};

    long milliseconds = 0;
    if (!millisecondsStr.empty())
        milliseconds = std::stoi(millisecondsStr);

    auto timePoint = std::chrono::system_clock::from_time_t(utcTime);
    timePoint += std::chrono::milliseconds(milliseconds);

    return timePoint;
}

long getTimezoneOffset()
{
#if __APPLE__ || __MACH__ || __linux__
    // Convert to time_t (UTC), returns -1 on failure
    return timezone;
#elif _WIN32
    TIME_ZONE_INFORMATION tzi;
    DWORD result = GetTimeZoneInformation(&tzi);
    if (result == TIME_ZONE_ID_INVALID) {
        return 0; // Default to no offset
    } else {
        // The formula in Win32 API docs is a bit confusing because we
        // actually need to subtract the bias from the local time:
        // https://learn.microsoft.com/en-us/windows/win32/api/timezoneapi/nf-timezoneapi-gettimezoneinformation#remarks

        // The bias is the difference in *minutes* between UTC and local time
        return (tzi.Bias * SECS_IN_MINUTE);
    }
#endif
}

std::string generateUuid()
{
#if __APPLE__ || __MACH__
    CFUUIDRef uuidRef = CFUUIDCreate(NULL);
    CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);

    const char* uuidCharPtr = CFStringGetCStringPtr(uuidStringRef, kCFStringEncodingUTF8);

    std::string uuidStr;
    if (uuidCharPtr) {
        uuidStr = uuidCharPtr;
    }

    CFRelease(uuidRef);
    CFRelease(uuidStringRef);

    return uuidStr;
#elif __linux__
    uuid_t uuid;
    char uuidStr[37]; // 36-byte plus trailing '\0'

    uuid_generate(uuid);
    // Convert binary representation to string
    uuid_unparse(uuid, uuidStr);

    return std::string(uuidStr);
#elif _WIN32
    UUID uuid;
    RPC_WSTR uuidCStr;

    if (UuidCreate(&uuid) != RPC_S_OK)
        logError("Failed to create UUID, errno: %d", GetLastError());
    // Convert to string
    if (UuidToStringW(&uuid, &uuidCStr) != RPC_S_OK) {
        logError("Could not convert UUID to string, errno: %d", GetLastError());
        return std::string();
    }
    std::string uuidStr(utils::narrow(reinterpret_cast<wchar_t *>(uuidCStr)));

    RpcStringFreeW(&uuidCStr);

    return uuidStr;
#endif
}

#if _WIN32 || __linux__
std::string appdataPath()
{
#if _WIN32
    wchar_t wstring[MAX_PATH];
    // SHGetSpecialFolderPath is deprecated, but replacement SHGetFolderPath is not supported on XP
    if (SHGetSpecialFolderPath(0, wstring, CSIDL_APPDATA, FALSE)) {
        std::string utf8str = narrow(wstring);
        return utf8str;
    }

    return narrow(_wgetenv(widen("APPDATA").c_str()));
#else
    if (const char *xdg_home = getenv("XDG_DATA_HOME"))
        return std::string(xdg_home);

    std::string homePath = getUserHomeDir();
    if (!homePath.empty()) {
        homePath.append("/.local/share");
        return homePath;
    }

    return std::string();
#endif
}
#endif

void setMaxOpenFileDescriptrors()
{
#if _WIN32
    if (_setmaxstdio(WIN32_MAX_STDIO) == -1)
        logWarn("Could not increase the maximum number of simultaneously open files: %s", strerror(errno));
#else
    struct rlimit rl;
    if (getrlimit(RLIMIT_NOFILE, &rl) == -1) {
        logWarn("Could not get resource limits for current process: %s", strerror(errno));
        return;
    }
#if __APPLE__ || __MACH__
    rl.rlim_cur = std::min(static_cast<rlim_t>(OPEN_MAX), rl.rlim_max);
#else // linux
    rl.rlim_cur = rl.rlim_max;
#endif
    if (setrlimit(RLIMIT_NOFILE, &rl) == -1)
        logWarn("Could not increase the maximum number of simultaneously open files: %s", strerror(errno));
#endif
}

/*
    Returns the working directory of the process, or empty string on failure.
*/
std::string workingDirectory()
{
#ifdef _WIN32
    wchar_t buffer[MAX_PATH];
    if (GetCurrentDirectoryW(MAX_PATH, buffer))
        return utils::narrow(buffer);
#else
    char buffer[PATH_MAX];
    if (getcwd(buffer, sizeof(buffer)))
        return std::string(buffer);
#endif
    return std::string();
}

/*
    Sets the working directory of the process to \a dir. Returns \c true
    on success, \c false otherwise.
*/
bool setWorkingDirectory(const std::string &dir)
{
#ifdef _WIN32
    return SetCurrentDirectoryW(utils::widen(dir).c_str());
#else
    return (chdir(dir.c_str()) == 0);
#endif
}

#if _WIN32
/*
    Converts a wide char string \a s (UTF-16) to narrow string (UTF-8)
    and returns the converted string. Returns an empty string on failure.

    This should be used for narrow string conversion close to Windows API calls,
    to avoid holding wide string data in the application code.
*/
std::string narrow(const wchar_t *s)
{
    if (s == nullptr)
        return std::string();

    const int len = WideCharToMultiByte(CP_UTF8, 0, s, -1, nullptr, 0, nullptr, nullptr);
    if (len == 0) {
        logError("Unable to convert wchar_t * to std::string");
        return std::string();
    }

    std::string result(len - 1, '\0');
    WideCharToMultiByte(CP_UTF8, 0, s, -1, &result[0], len, nullptr, nullptr);

    return result;
}

/*
    Converts a narrow char string \a s (UTF-8) to wide string (UTF-16)
    and returns the converted string. Returns an empty string on failure.

    This should be used for wide string conversion close to Windows API calls,
    to avoid holding wide string data in the application code.
*/
std::wstring widen(const char *s)
{
    if (s == nullptr)
        return std::wstring();

    const int len = MultiByteToWideChar(CP_UTF8, 0, s, -1, nullptr, 0);
    if (len == 0) {
        logError("Unable to convert char * to std::wstring");
        return std::wstring();
    }

    std::wstring result(len - 1, L'\0');
    MultiByteToWideChar(CP_UTF8, 0, s, -1, &result[0], len);

    return result;
}

std::string narrow(const std::wstring &s)
{
    return narrow(s.c_str());
}

std::wstring widen(const std::string &s)
{
    return widen(s.c_str());
}
#endif

} } // namespace QLicenseCore::utils
