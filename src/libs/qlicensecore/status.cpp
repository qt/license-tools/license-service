/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "status.h"

#include "constants.h"

#include <map>

#define RESPONSE_CODE_SIZE 7

namespace QLicenseCore {

// String equivalents for reply codes
static const std::map<Status, std::string> sc_statusStringMap
    = {{Status::UNKNOWN_ERROR, "Unknown error."},
       {Status::SUCCESS, "Request successful."},
       {Status::LICENSE_REJECTED, "No valid license could be acquired."},
       {Status::LICENSE_POOL_FULL, "There are no free licenses available in the license pool."},
       {Status::UNKNOWN_LICENSE_MODEL, "The license model is unknown."},
       {Status::UNKNOWN_LICENSE_TYPE, "The license type is unknown."},
       {Status::UNKNOWN_RESERVATION_TYPE, "The reservation type is unknown."},
       {Status::RESERVATION_NOT_FOUND, "The server cannot find the requested reservation id."},
       {Status::BETTER_LICENSE_AVAILABLE, "The server found a better license for current application and refused renewal of the previous license."},
       {Status::TERMS_AND_CONDITIONS_NOT_ACCEPTED, "Cannot verify user acceptance of the terms and conditions available in '" + std::string(sc_termsAndConditionsUrl) + "'."},
       {Status::SERVER_HAS_NO_LICENSES, "Reservation or renewal cannot proceed as the server has no licenses in use."},
       {Status::BAD_REQUEST, "The request to the server was invalid."},
       {Status::INVALID_RESPONSE, "The server response was invalid."},
       {Status::BAD_CONNECTION, "No connection to server. Try again later."},
       {Status::UNAUTHORIZED, "Unauthorized. Please login with your Qt Account."},
       {Status::SERVER_ERROR, "Internal server error."},
       {Status::SERVER_BUSY, "The server is busy."},
       {Status::SSL_VERIFY_ERROR, "Could not verify the remote server's SSL certificate."},
       {Status::SSL_LOCAL_CERTIFICATE_ERROR, "There was a problem with the local SSL certificate."},
       {Status::BAD_CLIENT_REQUEST, "The request from the client application was invalid."},
       {Status::REQUEST_TIMEOUT, "Timeout was reached when performing the request."},
       {Status::SERVICE_VERSION_TOO_LOW,
        "The client library major version is newer than the Qt License Service major version."},
       {Status::SERVICE_VERSION_TOO_NEW,
        "The Qt License Service major version is newer than the server or client library major version."},
       {Status::MISSING_SERVICE_VERSION, "Incorrect or missing service version in request."},
       {Status::NO_CLIENT_FOUND, "The client was not found in cache."},
       {Status::SETTINGS_ERROR, "Invalid service settings request."},
       {Status::INCOMPATIBLE_LICENSE_FORMAT_VERSION, "The license format version is incompatible with the Qt License Service version."}};

std::string statusString(Status status)
{
    auto it = sc_statusStringMap.find(status);
    if (it == sc_statusStringMap.end())
        return std::string();

    return it->second;
}


namespace ServerResponseCode {

static const std::map<std::string, Status> sc_responseInfoMap = {
    {"2000000", Status::SUCCESS}, // Request ok (default)
    {"2100001", Status::SUCCESS}, // Reservation successfully created
    {"2100011", Status::SUCCESS}, // Already reserved -> renew existing reservation
    {"2100002", Status::SUCCESS}, // Reservation successfully extended
    {"2100003", Status::SUCCESS}, // Reservation successfully released
    {"4001001", Status::BAD_REQUEST}, // Invalid payload
    {"4101001", Status::BAD_REQUEST}, // Bad input parameters
    {"4001002", Status::BAD_REQUEST}, // Username invalid
    {"4101002", Status::BAD_REQUEST}, // Username invalid
    {"4010000", Status::UNAUTHORIZED}, // Authentication failed
    {"4100000", Status::UNAUTHORIZED}, // Not authorized
    {"4001000", Status::UNAUTHORIZED}, // Not authorized
    {"4100012", Status::LICENSE_POOL_FULL}, // License pool fully reserved
    {"4100010", Status::LICENSE_REJECTED}, // License not found
    {"4100011", Status::RESERVATION_NOT_FOUND}, // Reservation not found for renew or release
    {"4004000", Status::SERVICE_VERSION_TOO_NEW}, // Service version is newer than Server version
    {"4104000", Status::SERVICE_VERSION_TOO_NEW}, // Service version is newer than Server version
    {"4001005", Status::MISSING_SERVICE_VERSION}, // Incorrect or missing Service version in request body
    {"4101005", Status::MISSING_SERVICE_VERSION}, // Incorrect or missing Service version in request body
    {"4100016", Status::BETTER_LICENSE_AVAILABLE}, // There is a better license available when renewing
    {"4105002", Status::SERVER_BUSY}, // Server is busy when making new reservation
    {"5005002", Status::SERVER_BUSY}, // Server is busy
    {"5005004", Status::SERVER_BUSY}, // Request timed out
    {"5105003", Status::SERVER_HAS_NO_LICENSES}, // Reservation cannot proceed as the server has no license in use
    {"5105004", Status::SERVER_BUSY}, // Request timed out when making new reservation
    {"5105000", Status::SERVER_ERROR}, // Internal server error when making new reservation
    {"9999999", Status::UNKNOWN_ERROR} // Unknown error
};

bool isSuccessCode(const std::string &code)
{
    if (code.length() != RESPONSE_CODE_SIZE)
        return false;

    return (code.rfind("2", 0) == 0);
}

Status codeToStatus(const std::string &code)
{
    if (code.length() != RESPONSE_CODE_SIZE)
        return Status::UNKNOWN_ERROR;

    auto it = sc_responseInfoMap.find(code);
    if (it == sc_responseInfoMap.end()) {
        if (isSuccessCode(code))
            return Status::SUCCESS;
        else
            return Status::UNKNOWN_ERROR;
    }

    return it->second;
}

}

bool isNetworkError(Status status)
{
    return (status == Status::BAD_CONNECTION
         || status == Status::SSL_VERIFY_ERROR
         || status == Status::SSL_LOCAL_CERTIFICATE_ERROR);
}

// namespace ServerResponseCode

} // namespace QLicenseCore
