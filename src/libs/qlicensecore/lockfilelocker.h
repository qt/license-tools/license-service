/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

namespace QLicenseCore {

class LockFile;

class LockFileLocker
{
public:
    explicit LockFileLocker(LockFile *file);
    ~LockFileLocker();

private:
    LockFile *const m_file;
};

} // namespace QLicenseCore
