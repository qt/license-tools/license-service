/* Copyright (C) 2025 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "qtaccountsetup.h"

#include "logger.h"
#include "utils.h"

#include <sstream>

namespace QLicenseCore {

QtAccountSetup::QtAccountSetup(const std::string &settingsFilePath)
    : Settings()
    , m_settingsFilePath(settingsFilePath)
    , m_parser(settingsFilePath)
{
}

bool QtAccountSetup::init()
{
    if (m_initialized)
        return true;

    if (!getQtAccountInfo())
        return false;

    return Settings::init();
}

bool QtAccountSetup::createSettingsFile(const std::string &path, const std::string &email,
    const std::string &jwt)
{
    std::stringstream ss;
    ss << "[QtAccount]" << std::endl;
    ss << "email=" << email << std::endl;
    ss << "jwt=" << jwt << std::endl;
    ss << "u=" << std::endl;

    const std::string settingsPath = utils::getPathWithoutFilename(path);
    if (!utils::fileExists(settingsPath) && !utils::createDir(settingsPath)) {
        logError("Failed to create settings directory %s", settingsPath.c_str());
        return false;
    }

    if (!utils::writeToFile(path, ss.str(), false)) {
        logError("Failed to create file %s", path.c_str());
        return false;
    }

    return true;
}

bool QtAccountSetup::getQtAccountInfo()
{
    if (!utils::fileExists(m_settingsFilePath))
        return false;

    if (!m_parser.readSection("QtAccount", m_settings)) {
        logError("Failed to parse Qt Account settings from: %s", m_settingsFilePath.c_str());
        return false;
    }

    return true;
}

} // namespace QLicenseCore
