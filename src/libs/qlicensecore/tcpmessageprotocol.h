/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <cstdint>
#include <string>

#if __APPLE__ || __MACH__ || __linux__
#include <sys/socket.h>
#include <netinet/in.h>
typedef int type_socket;
#else
#include <BaseTsd.h>
#include <winsock2.h>
typedef SOCKET type_socket;
typedef SSIZE_T ssize_t;
#endif

namespace QLicenseCore {

class TcpMessageProtocol
{
public:
    enum Capabilities : uint32_t {
        None = 0,
        // Can be extended to include the last 4-bits per byte (or 3-bits for the last byte)
        ResponseLengthHeader = 1 << 0,       // 0x00000001
        ReservedHeaderCapability2 = 1 << 1,  // 0x00000002
        ReservedHeaderCapability3 = 1 << 2,  // 0x00000004
        ReservedHeaderCapability4 = 1 << 3,  // 0x00000008

        ReservedGroup2Capability1 = 1 << 8,  // 0x00000100
        ReservedGroup2Capability2 = 1 << 9,  // 0x00000200
        ReservedGroup2Capability3 = 1 << 10, // 0x00000400
        ReservedGroup2Capability4 = 1 << 11, // 0x00000800

        ReservedGroup3Capability1 = 1 << 16, // 0x00010000
        ReservedGroup3Capability2 = 1 << 17, // 0x00020000
        ReservedGroup3Capability3 = 1 << 18, // 0x00040000
        ReservedGroup3Capability4 = 1 << 19, // 0x00080000

        ReservedGroup4Capability1 = 1 << 24, // 0x01000000
        ReservedGroup4Capability2 = 1 << 25, // 0x02000000
        ReservedGroup4Capability3 = 1 << 26, // 0x04000000
        ReservedGroup4Capability4 = 1 << 27, // 0x08000000

        // Do not use, reserved to indicate the presence of the capabilities header:
        CapabilitiesMagicMarker = 1u << 31 // 0x80000000
    };

    TcpMessageProtocol(Capabilities capabilities = Capabilities::None);
    virtual ~TcpMessageProtocol() = 0;

    void setCapabilities(uint32_t capabilities);
    uint32_t capabilities() const;

    uint32_t supportedCapabilities(uint32_t capabilities) const;

protected:
    bool readPeerCapabilities(type_socket socketFd, uint32_t &capabilities) const;

    std::string createRequestPayload(const std::string &message);
    std::string createResponsePayload(uint32_t peerCapabilities, const std::string &message);

private:
    uint32_t m_capabilities;
};

} // namespace QLicenseCore
