/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <condition_variable>
#include <string>
#include <mutex>
#include <deque>
#include <atomic>

namespace QLicenseCore {

class Pipe
{
public:
    enum class Type {
       TcpServer = 0,
       LegacyTcpServer = 1,
       HttpClient = 2,
       Watchdog = 3
    };

    Pipe();
    ~Pipe();

    void write(const std::string &message, Type type, const std::string &identifier);
    std::string read(Type *type = nullptr, std::string *identifier = nullptr);

    bool waitForReadyRead(int timeoutSecs = -1);
    int queuedMessagesCount() const;

    static Pipe *globalObject();

private:
    struct Message
    {
        Type type;
        std::string identifier;
        std::string message;
    };

    std::deque<Message> m_messages;
    std::condition_variable m_cv;
    mutable std::mutex m_mutex;
    std::atomic_int m_messageCount;
};

} // namespace QLicenseCore
