/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "tcpmessageprotocol.h"

#include <cstring>

namespace QLicenseCore {

/*
    \brief
    An abstract class to define common capabilities of the messaging protocol used
    in the TCP socket communication in different parts of Qt License Service.
*/

/*
    Constructs a new consumer of the TCP protocol with optional \a capabilties bitmask.
*/
TcpMessageProtocol::TcpMessageProtocol(Capabilities capabilities)
    : m_capabilities(Capabilities::None)
{
    setCapabilities(capabilities);
}

/*
    Virtual destructor for \c TcpProtocol.
*/
TcpMessageProtocol::~TcpMessageProtocol()
{
}

/*
    Sets the capabilities bitmask of this protocol consumer to \a capabilities. If \a capabilities
    is not \c Capabilities::None, the \c Capabilities::CapabilitiesMagicMarker is automatically
    added to the capabilities bitmask to indicate the precense of the capabililites header.
*/
void TcpMessageProtocol::setCapabilities(uint32_t capabilities)
{
    m_capabilities = capabilities;

    if (m_capabilities != Capabilities::None)
        m_capabilities |= Capabilities::CapabilitiesMagicMarker;
}

/*
    Returns the capabilities bitmask of this protocol consumer.
*/
uint32_t TcpMessageProtocol::capabilities() const
{
    return m_capabilities;
}

/*
    Returns a new bitmask of capabilities set for both this protocol
    consumer, and the original \a capabilities input.
*/
uint32_t TcpMessageProtocol::supportedCapabilities(uint32_t capabilities) const
{
    return capabilities & m_capabilities;
}

/*
    Attempts to read the capabilities header from a peer TCP \a socketFd. Returns \c true
    if the capabilties header was found and read successfully, \c false otherwise. The
    resultant capabilities bitmask can be retrieved with the \a capabilities return parameter.
*/
bool TcpMessageProtocol::readPeerCapabilities(type_socket socketFd, uint32_t &capabilities) const
{
    uint32_t capabilitiesNet;
#if _WIN32
    ssize_t bytesRead = recv(socketFd, reinterpret_cast<char *>(&capabilitiesNet), sizeof(capabilitiesNet), MSG_PEEK);
#else
    ssize_t bytesRead = recv(socketFd, &capabilitiesNet, sizeof(capabilitiesNet), MSG_PEEK);
#endif

    if (bytesRead == sizeof(capabilitiesNet)) {
        capabilities = ntohl(capabilitiesNet); // network to host byte order

        // check if the highest bit is set, which we use as a marker for capabilities.
        if ((capabilities & TcpMessageProtocol::Capabilities::CapabilitiesMagicMarker)) {
            // now that we confirmed the header, actually read and discard it
#if _WIN32
            recv(socketFd, reinterpret_cast<char *>(&capabilitiesNet), sizeof(capabilitiesNet), 0);
#else
            recv(socketFd, &capabilitiesNet, sizeof(capabilitiesNet), 0);
#endif
            return true;
        }
    }
    return false;
}

/*
    Creates a payload in the context of a request, containing an appropriate capabilities header
    based on the capabilities bitmask of this protocol consumer, any additional header data,
    and followed by \a message. Returns the resultant payload.
*/
std::string TcpMessageProtocol::createRequestPayload(const std::string &message)
{
    if (m_capabilities == Capabilities::None)
        return message;

    uint32_t capabilitiesNet = htonl(m_capabilities); // convert to network byte order

    std::string payload(sizeof(m_capabilities) + message.size(), '\0');
    std::memcpy(&payload[0], &capabilitiesNet, sizeof(capabilitiesNet));
    std::memcpy(&payload[sizeof(capabilitiesNet)], message.data(), message.size());

    return payload;
}

/*
    Creates a payload in the context of a response, containing an appropriate capabilities header
    based on the \a peerCapabilities bitmask and the capabilities set for this protocol consumer,
    any additional header data, and followed by \a message. Returns the resultant payload.

*/
std::string TcpMessageProtocol::createResponsePayload(uint32_t peerCapabilities, const std::string &message)
{
    uint32_t capabilities = supportedCapabilities(peerCapabilities);
    // If the client does not support any capabilities (old client version),
    // then just return the plain response message
    if (capabilities == Capabilities::None)
        return message;

    std::string payload(sizeof(capabilities), '\0');
    // Reserve enough space for capabilities + length header + message, to avoid reallocation
    const uint32_t expectedMaxSize = sizeof(capabilities) + sizeof(uint32_t) + message.size();
    payload.reserve(expectedMaxSize);

    uint32_t capabilitiesNet = htonl(capabilities); // convert to network byte order
    std::memcpy(&payload[0], &capabilitiesNet, sizeof(capabilitiesNet));

    if (capabilities & TcpMessageProtocol::Capabilities::ResponseLengthHeader) {
        uint32_t length = message.size();
        uint32_t lengthNet = htonl(length); // convert to network byte order

        size_t currentSize = payload.size();
        payload.resize(currentSize + sizeof(length));
        std::memcpy(&payload[currentSize], &lengthNet, sizeof(lengthNet));
    }

    size_t currentSize = payload.size();
    payload.resize(currentSize + message.size());
    std::memcpy(&payload[currentSize], message.data(), message.size());

    return payload;
}

} // namespace QLicenseCore
