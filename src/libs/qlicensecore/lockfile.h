/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <string>

#ifdef _WIN32
#include <windows.h>
#endif

namespace QLicenseCore {

class LockFile
{
public:
    explicit LockFile(const std::string &filename);
    ~LockFile();

    bool tryLock(const std::string &fileData = std::string());
    bool tryUnlock();

    bool isLocked() const;
    std::string error() const;

    std::string filename() const;

private:
#ifdef _WIN32
    HANDLE m_handle;
    DWORD m_bytesWritten;
    OVERLAPPED m_overlapped;
#else
    void closeHandle();
    int m_handle;
#endif
    bool m_locked;

    std::string m_error;
    std::string m_filename;
};

} // namespace QLicenseCore
