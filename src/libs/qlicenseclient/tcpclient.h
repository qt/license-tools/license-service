/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <tcpmessageprotocol.h>

#include <string>
#include <map>

#if __APPLE__ || __MACH__ || __linux__
    #include <sys/types.h>
    #include <arpa/inet.h>
    #include <unistd.h>
    #include <netdb.h>
#else
    #define WIN32_LEAN_AND_MEAN
    #define NOMINMAX
    #include <WinSock2.h>
    #include <windows.h>
    #include <ws2tcpip.h>
    #include <process.h>
    #include <cstdint>
    // Need to link with Ws2_32.lib
    #pragma comment (lib, "Ws2_32.lib")
#endif

namespace QLicenseClient {

class TcpClient : public QLicenseCore::TcpMessageProtocol
{
public:
    TcpClient(const std::string &connAddr, uint16_t port);
    ~TcpClient();

    bool init(std::string &error, int timeoutSecs = 30);

    bool sendReceive(const std::string &message, std::string &reply);
    bool send(const std::string &message, std::string &error);
    bool receive(std::string &reply);

    bool receiveMessage(std::string &reply);
    bool receiveMessage(uint32_t length, std::string &reply);
    bool receiveLengthHeader(uint32_t &length, std::string &error);

private:
    void doCloseSocket();
    void doHardCloseSocket();

    std::string errorString(int errCode);
    std::string getLastSocketError() const;

    enum TcpReturnValues {
        e_tcp_success           = 0,
        e_tcp_error_conn        = 1,
        e_tcp_error_send        = 2,
        e_tcp_error_recv        = 3,
        e_tcp_error_hostname    = 4,
        e_tcp_error_socket      = 5,
        e_tcp_error_init        = 6,
        e_tcp_disconnected      = 7,
        e_tcp_msg_out_of_bounds = 8,
        e_tcp_msg_zero_length   = 9
    };

private:
    std::string m_address;
    uint16_t m_port;

    type_socket m_socketFD;
    sockaddr_in m_server;

    std::map<int, std::string> m_tcpReturnStr = {
        {e_tcp_success,         "TCP: Ok"},
        {e_tcp_error_conn,      "TCP: Error while connecting"},
        {e_tcp_error_hostname,  "TCP: Error while resolving the hostname"},
        {e_tcp_error_recv,      "TCP: Error while receiving data"},
        {e_tcp_error_send,      "TCP: Error while sending data"},
        {e_tcp_error_socket,    "TCP: Error while setting up the socket"},
        {e_tcp_error_init,      "TCP: Error while initializing socket subsystem"},
        {e_tcp_disconnected,    "TCP: Remote side disconnected socket"},
        {e_tcp_msg_out_of_bounds, "TCP: Received message length is out of bounds"},
        {e_tcp_msg_zero_length, "TCP: Length of message to receive is zero"}
    };
};

} // namespace QLicenseClient
