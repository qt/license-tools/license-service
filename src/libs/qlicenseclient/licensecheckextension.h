/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "licenseinfo.h"

namespace QLicenseClient {

class LicenseCheckExtension
{
public:
    LicenseCheckExtension(LicenseReservationInfo *licenseInfo);

    bool verifyLicense();

private:
    inline void setError(StatusCode code, const std::string &error)
    {
        m_licenseInfo->setStatus(code);
        m_licenseInfo->setMessage(error);
    }

private:
    LicenseReservationInfo *m_licenseInfo;
};

} // namespace QLicenseClient
