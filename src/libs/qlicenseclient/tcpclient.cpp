/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "tcpclient.h"

#include <utils.h>
#include <commonsetup.h>
#include <logger.h>

#include <cstring>
#include <errno.h>
#include <algorithm>

using namespace QLicenseCore;

namespace QLicenseClient {

TcpClient::TcpClient(const std::string &connAddr, uint16_t port)
    : QLicenseCore::TcpMessageProtocol(TcpMessageProtocol::Capabilities::ResponseLengthHeader)
    , m_address(connAddr)
    , m_port(port)
#if _WIN32
    , m_socketFD(INVALID_SOCKET)
#else
    , m_socketFD(-1)
#endif
{
}

TcpClient::~TcpClient()
{
    doCloseSocket();
}

bool TcpClient::init(std::string &error, int timeoutSecs)
{
#ifdef _WIN32
    // Initialize Winsock
    WSADATA wsaData;
    int iResult = 0;
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != NO_ERROR) {
        error = errorString(e_tcp_error_init) + ": " + getLastSocketError();
        return false;
    }
    const int inetSuccess = inet_pton(AF_INET, m_address.c_str(), &m_server.sin_addr.s_addr);
#else
    const int inetSuccess = inet_aton(m_address.c_str(), &m_server.sin_addr);
#endif
    if (!inetSuccess) {
        // Try to resolve hostname
        struct hostent *host;
        struct in_addr **addrList;
        if ((host = gethostbyname(m_address.c_str())) == nullptr) {
#ifdef _WIN32
            error = errorString(e_tcp_error_hostname) + ": " + getLastSocketError();
#else
            // We cannot use the default error retrieval mechanism for the obsolete method:
            error = errorString(e_tcp_error_hostname) + ": \"" + hstrerror(h_errno)
                + "\". Error code: " + std::to_string(h_errno);
#endif
            return false;
        }
        addrList = (struct in_addr **) host->h_addr_list;
        m_server.sin_addr = *addrList[0];
    }
    m_server.sin_port = htons(m_port);

    // Create socket
    m_server.sin_family = AF_INET;
    m_socketFD = (socket(AF_INET , SOCK_STREAM, 0));
#ifdef _WIN32
    if (m_socketFD == INVALID_SOCKET) {
#else
    if (m_socketFD < 0) {
#endif
        error = errorString(e_tcp_error_socket) + ": " + getLastSocketError();
        doHardCloseSocket();
        return false;
    }

    int retries = 0;
    // Connect to daemon. In case the connection was refused due to exceeded
    // pending connections, try again.
    while (connect(m_socketFD, (struct sockaddr *)&m_server, sizeof(m_server)) == -1) {
#ifdef _WIN32
        int lastError = WSAGetLastError();
        if (lastError == WSAECONNREFUSED) {
#else
        if (errno == ECONNREFUSED) {
#endif
            if ((timeoutSecs == -1) || ++retries < timeoutSecs) {
                QLicenseCore::utils::sleepSecs(1);
                continue;
            }
        }
        error = errorString(e_tcp_error_conn) + ": " + getLastSocketError();
        doHardCloseSocket();
        return false;
    }

    return true;
}

/*
    Sends \a message through the client socket to the server socket. The \c reply
    contains the server response on success, or an error string in case of failure.

    Returns \c true on success, \c false on failure.
*/
bool TcpClient::sendReceive(const std::string &message, std::string &reply)
{
    if (!send(message, reply))
        return false;

    return receive(reply);
}

/*
    Sends \a message to peer. Returns \c true on success, \c false otherwise.
    An error message in case of failure can be retrieved with the \a error parameter.
    If the client has any \c ClientCapabilities set, those are sent as a fixed length
    header before the actual message.
*/
bool TcpClient::send(const std::string &message, std::string &error)
{
    std::string payload = createRequestPayload(message);
    // send optional capabilities bitmask (4 bytes), followed by the actual message
    if (::send(m_socketFD, payload.c_str(), (int)payload.length(), 0) == -1) {
        error = errorString(e_tcp_error_send) + ": " + getLastSocketError();
        doHardCloseSocket();
        return false;
    }

    return true;
}

/*
    Receives a capabilities header, any additional header data, and message from peer.
    Returns \c true on success, \c false otherwise. The reply or error message in case of
    failure can be retrieved with \a reply parameter.
*/
bool TcpClient::receive(std::string &reply)
{
    uint32_t capabilities;
    if (readPeerCapabilities(m_socketFD, capabilities)) {
        if (capabilities & QLicenseCore::TcpMessageProtocol::ResponseLengthHeader) {
            uint32_t length;
            if (!receiveLengthHeader(length, reply))
                return false;

            return receiveMessage(length, reply);
        }
    }
    // default
    return receiveMessage(reply);
}

/*
    Receives a message from peer. Returns \c true on success, \c false otherwise.
    The reply or error message in case of failure can be retrieved with \a reply parameter.

    This method is provided for compatibility with older service versions, that do not
    send the response length header as part of the response payload.
*/
bool TcpClient::receiveMessage(std::string &reply)
{
    // Reserve buffer size based on best guess, since we don't know the actual message size
    reply = std::string(DEFAULT_TCP_CHUNK_SIZE * 4, '\0');
    ssize_t bytes_recv = recv(m_socketFD, &reply.front(), (int)reply.size(), 0);

    // Could be that remote side closed gracefully, so check for 0 as well
    if (bytes_recv == -1) {
        reply = errorString(e_tcp_error_recv) + ": " + getLastSocketError();
        doHardCloseSocket();
        return false;
    } else if (bytes_recv == 0) {
        reply = errorString(e_tcp_disconnected);
        doHardCloseSocket();
        return false;
    }

    reply = reply.substr(0, bytes_recv); // remove extra bytes
    return true;
}

/*
    Receives a message of maximum \a length bytes from peer. Returns \c true on
    success, \c false otherwise. The reply or error message in case of
    failure can be retrieved with \a reply parameter.
*/
bool TcpClient::receiveMessage(uint32_t length, std::string &reply)
{
    reply = std::string(length, '\0');
    size_t totalBytesReceived = 0;

    while (totalBytesReceived < length) {
        ssize_t bytesReceived = recv(m_socketFD, &reply[totalBytesReceived],
            std::min<size_t>(DEFAULT_TCP_CHUNK_SIZE, length - totalBytesReceived), 0);
        if (bytesReceived == -1) {
            reply = errorString(e_tcp_error_recv) + ": " + getLastSocketError();
            doHardCloseSocket();
            return false;
        } else if (bytesReceived == 0) {
            reply = errorString(e_tcp_disconnected);
            doHardCloseSocket();
            return false;
        }
        totalBytesReceived += bytesReceived;
    }

    return true;
}

/*
    Receives the header indicating subsequent message payload \a length from peer.
    Returns \c true on success, \c false otherwise. An error message in case of
    failure can be retrieved with the \a error parameter.
*/
bool TcpClient::receiveLengthHeader(uint32_t &length, std::string &error)
{
    // read the 4-byte length header
#if _WIN32
    ssize_t bytesReceived = recv(m_socketFD, reinterpret_cast<char *>(&length), sizeof(length), 0);
#else
    ssize_t bytesReceived = recv(m_socketFD, &length, sizeof(length), 0);
#endif
    // Could be that remote side closed gracefully, so check for 0 as well
    if (bytesReceived == -1) {
        error = errorString(e_tcp_error_recv) + ": " + getLastSocketError();
        doHardCloseSocket();
        return false;
    } else if (bytesReceived == 0) {
        error = errorString(e_tcp_disconnected);
        doHardCloseSocket();
        return false;
    }

    length = ntohl(length);  // convert from network to host byte order
    if (length == 0) {
        error = errorString(e_tcp_msg_zero_length);
        doHardCloseSocket();
        return false;
    } else if (length > MAX_TCP_MESSAGE_LENGTH) {
        error = errorString(e_tcp_msg_out_of_bounds);
        doHardCloseSocket();
        return false;
    }

    return true;
}

void TcpClient::doCloseSocket()
{
#if __APPLE__ || __MACH__  || __linux__
    close(m_socketFD);
    m_socketFD = -1;
#else
    if (m_socketFD == INVALID_SOCKET)
        return;

    if (shutdown(m_socketFD, SD_SEND) == SOCKET_ERROR) {
        logError("Failed to shutdown socket: %s", getLastSocketError().c_str());
        closesocket(m_socketFD);
        m_socketFD = INVALID_SOCKET;
        return;
    }

    std::string buffer(DEFAULT_TCP_CHUNK_SIZE, ' ');
    size_t bytesRead = 0;
    do {
        // any pending data from remote side is discarded at this point
        bytesRead = recv(m_socketFD, &buffer.front(), (int)buffer.size(), 0);
        if (bytesRead < 0)
            logWarn("Failed to close socket gracefully: %s", getLastSocketError().c_str());

    } while (bytesRead > 0);

    closesocket(m_socketFD);
    m_socketFD = INVALID_SOCKET;
#endif
}

void TcpClient::doHardCloseSocket()
{
#if __APPLE__ || __MACH__  || __linux__
    close(m_socketFD);
    m_socketFD = -1;
#else
    closesocket(m_socketFD);
    m_socketFD = INVALID_SOCKET;
#endif
}

std::string TcpClient::errorString(int errCode)
{
    return m_tcpReturnStr[errCode];
}

std::string TcpClient::getLastSocketError() const
{
#if _WIN32
    int errorCode = WSAGetLastError();
    LPWSTR buffer = nullptr;

    FormatMessageW(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL, (DWORD)errorCode, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPWSTR)&buffer, 0, NULL
        );

    std::string errorString(QLicenseCore::utils::narrow(buffer));
    LocalFree(buffer);

    if (errorString.empty())
        errorString = "Unknown error";

    return errorString;
#else
    return std::string(strerror(errno));
#endif
}

} // namespace QLicenseClient
