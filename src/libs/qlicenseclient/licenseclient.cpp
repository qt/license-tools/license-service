/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "licenseclient.h"
#include "licenseclient_p.h"

#include "commonsetup.h"

using namespace QLicenseCore;

namespace QLicenseClient {

/*
    \class LicenseClient

    \brief The LicenseClient class provides an interface for performing license requests
           by the client application to a running Qt License Daemon.
*/

/*
    Returns \c true if a license service installation matching the client
    library version was found, or the service is already running, indicated
    by a valid TCP port.

    Returns \c false otherwise.

    If the service is not already running, this method parses the installations.ini
    file for suitable service installation. Invalid sections are removed from the file.
*/
bool LicenseClient::serviceAvailable()
{
    return LicenseClientPrivate::serviceAvailable();
}

/*
    Constructs a new license client
*/
LicenseClient::LicenseClient()
    : d(new LicenseClientPrivate(this))
{
}

/*
    Disconnects the client socket and destroys the license client.
*/
LicenseClient::~LicenseClient()
{
    delete d;
}

/*
    Connects the client socket to license daemon. The socket connection is kept open for the
    duration of the lifetime of the license client.

    The daemon can refuse the connection, if it already has too many pending connections from
    other clients. In this case the initialization attempts to retry establishing the connection
    automatically. The \a timeoutSecs parameter can be used to specify the (estimate) maximum time
    of attempting to connect the client socket.

    Defaults to 0 (no retries). Optionally set to positive value in seconds, or -1 for infinite.

    Returns \c true on success, \c false otherwise. Error message can be retrieved
    with \a error parameter.
*/
bool LicenseClient::init(std::string &error, int timeoutSecs)
{
    return d->init(error, timeoutSecs);
}

/*
    Requests a license reservation. The \a clientProperties contain the necessary
    information about the client requesting the license.

    The \a callback is used to notify the caller about changes to the license reservation
    after a reply from the service is received, like successful or failed initial reservation,
    license data was updated after renewing, or license was revoked.

    Calling this will invoke \c{init()} automatically, if the license client is uninitialized.

    Returns \c true if the request could be sent, \c false otherwise.
*/
bool LicenseClient::reserve(const ClientProperties &clientProperties,
    ReservationStatusCallback callback)
{
    return d->reserve(clientProperties, callback);
}

/*
    Informs the license service that the application is no longer using the license reservation.

    Removes the license reservation from this client application and disconnects
    the local socket communicating with the license service. If there's a license
    request that is still pending response, this function blocks until that is processed.

    The license service will decide if the reservation should be released
    from the remote server and local cache based on the license's properties.

    This method is invoked automatically on object destruction.
*/
void LicenseClient::release()
{
    d->release();
}

/*
    Sends a status request to the license daemon with \a operation as the status operation
    to perform. The status reply from the daemon, or error
    string in case of failure can be retrieved with the \a status parameter.

    Calling this will invoke \c{init()} automatically, if the license client is uninitialized.

    Returns \c true on success, \c false otherwise.
*/
bool LicenseClient::status(StatusOperation operation, std::string &status)
{
    return d->status(operation, status);
}

/*
    Removes all reservations from the license service's local cache for the current user.
    If network connection is available, an attempt to release the reservations from
    the remove server is made.

    Returns \c true on success, \c false otherwise.

    A message string representing the result of the operation can be retrieved
    with \a result parameter.
*/
bool LicenseClient::clearReservationCache(std::string &result)
{
    return d->requestOperation(CLEAR_CACHE_CMD, result);
}

/*
    Sets the value of setting \a key of this license client to \a value. Overrides
    any old value for an existing \a key. The settings can be used to control some
    behavior of the license client.

    Currently supported settings values are:

    Key                   Value       Default  Description
    --------------------------------------------------
    ExtendedLicenseCheck  true|false  false    Enables additional checks for the license returned by service
*/
void LicenseClient::setClientSetting(const std::string &key, const std::string &value)
{
    d->setClientSetting(key, value);
}

/*
    Sends a get settings request to the license daemon with key as the .ini property
    to perform. The requested .ini property will reply from the daemon, or error
    string in case of failure can be retrieved with the key parameter.

    Returns \c true on success, \c false otherwise.
*/
bool LicenseClient::getServiceSetting(const std::string &key, std::string &reply)
{
    return d->getServiceSetting(key, reply);
}

/*
    Sends a set settings request to the license daemon with key as the .ini property, value and setting type
    to update .ini properties. The request will update the .ini property from the daemon, or error
    string in case of failure cannot be set with the key and value parameters.

    Returns \c true on success, \c false otherwise.
*/
bool LicenseClient::setServiceSetting(const std::string &key, const std::string &value, const SettingsType &settingsType, std::string &reply)
{
    return d->setServiceSetting(key, value, settingsType, reply);
}

} // namespace QLicenseClient
