/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "licenseinfo.h"

#include <functional>
#include <string>
#include <list>

namespace QLicenseClient {

namespace ClientSettings {

constexpr char sc_true[] = "true";
constexpr char sc_false[] = "false";

// Supported setting keys:
constexpr char sc_extendedLicenseCheck[] = "ExtendedLicenseCheck";

} // namespace ClientSettings

struct ClientProperties
{
    std::string consumerId;
    std::string consumerVersion;
    std::string consumerInstallationSchema;
    std::list<std::string> consumerSupportedFeatures;
    std::string consumerProcessId;
    std::string consumerBuildTimestamp;
};

class LicenseClientPrivate;

class LicenseClient
{
public:
    static bool serviceAvailable();

    enum class StatusOperation {
        DaemonVersion = 0,
        ServerVersion = 1,
        Reservations
    };

    enum class SettingsType {
        Temporary = 0,
        Persistent = 1
    };

    using ReservationStatusCallback = std::function<void(LicenseReservationInfo *info)>;

    LicenseClient();
    LicenseClient(const LicenseClient &) = delete;

    virtual ~LicenseClient();

    bool init(std::string &error, int timeoutSecs = 0);

    bool reserve(const ClientProperties &clientProperties, ReservationStatusCallback);
    void release();

    bool status(StatusOperation operation, std::string &status);
    bool clearReservationCache(std::string &result);

    void setClientSetting(const std::string &key, const std::string &value);

    bool getServiceSetting(const std::string &key, std::string &reply);
    bool setServiceSetting(const std::string &key, const std::string &value, const SettingsType &settingsType, std::string &reply);

private:
    LicenseClientPrivate *const d;
};

} // namespace QLicenseClient
