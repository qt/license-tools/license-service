/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "licenseresponseparser.h"

#include <utils.h>
#include <errors.h>

namespace QLicenseClient {

static const std::map<std::string, LicenseInfoType> sc_licenseInfoMap = {
    {"license_id",       LicenseInfoType::Id},
    {"model",            LicenseInfoType::Model},
    {"priority",         LicenseInfoType::Priority},
    {"type",             LicenseInfoType::Type},
    {"schema",           LicenseInfoType::Schema},
    {"hosting",          LicenseInfoType::Hosting},
    {"valid_from",       LicenseInfoType::LicenseValidFrom},
    {"valid_to",         LicenseInfoType::LicenseValidTo},
    {"lease_valid_from", LicenseInfoType::LeaseValidFrom},
    {"lease_valid_to",   LicenseInfoType::LeaseValidTo},
};


LicenseResponseParser::LicenseResponseParser(const std::string &input)
    : m_json(new QLicenseCore::JsonHandler(input))
{
}

bool LicenseResponseParser::parse(LicenseReservationInfo *reservationInfo)
{
    const std::string version = preParseVersion();
    if (version.empty()) {
        reservationInfo->setMessage("Could not parse version from response JSON");
        return false;
    }

    try {
        ParseResponseFunction parseResponse = findParser(version);
        return parseResponse(reservationInfo);
    } catch (const QLicenseCore::Error &e) {
        reservationInfo->setMessage(e.what());
        return false;
    }
}

std::string LicenseResponseParser::preParseVersion() const
{
    std::string version;
    m_json->get("license.version", version);
    return version;
}

ParseResponseFunction LicenseResponseParser::findParser(const std::string &version) const
{
    for (auto &parser : m_responseParsers) {
        if (QLicenseCore::utils::compareVersion(version, parser.first, 2) <= 0)
            return parser.second;
    }

    throw QLicenseCore::Error("No suitable parser found for license response version: " + version);
}

bool LicenseResponseParser::parseResponseV10(LicenseReservationInfo *reservationInfo)
{
    m_json->get("license.licensee.name", reservationInfo->m_licensee.name);
    m_json->get("license.licensee.description", reservationInfo->m_licensee.description);
    m_json->get("license.licensee.contact", reservationInfo->m_licensee.contact);

    m_json->get("license.contacts.sales", reservationInfo->m_contacts.sales);
    m_json->get("license.contacts.support", reservationInfo->m_contacts.support);
    m_json->get("license.contacts.admin", reservationInfo->m_contacts.admin);

    std::map<std::string, std::string> features;
    m_json->get("license.consumer.consumer_features", features);
    reservationInfo->setFeatures(features);

    // Access the "license_info" object
    QLicenseCore::JsonData info;
    if (!m_json->get("license.license_info", info)) {
        reservationInfo->setMessage( "Could not access \"license_info\" object");
        return false;
    }

    // Loop through all items in the "license_info" object
    for (const auto &item : info.items()) {
        const std::string &key = item.key();
        if (sc_licenseInfoMap.find(key) == sc_licenseInfoMap.end()) {
            // Not found in the map: Probably new informative value?
            continue;
        }
        const std::string &value = item.value().is_string()
            ? item.value().get<std::string>()
            : to_string(item.value());

        reservationInfo->m_licenseInfo[sc_licenseInfoMap.at(key)] = value;
    }

    // Set additional time info
    std::string validFrom;
    std::string validTo;
    if (!m_json->get("reservation.lease_valid_from", validFrom)) {
        reservationInfo->setMessage("reservation.lease_valid_from missing from reservation");
        return false;
    }
    if (!m_json->get("reservation.lease_valid_to", validTo)) {
        reservationInfo->setMessage("reservation.lease_valid_to missing from reservation");
        return false;
    }
    reservationInfo->m_licenseInfo[LicenseInfoType::LeaseValidFrom] = validFrom;
    reservationInfo->m_licenseInfo[LicenseInfoType::LeaseValidTo] = validTo;

    return true;
}

} // namespace QLicenseClient
