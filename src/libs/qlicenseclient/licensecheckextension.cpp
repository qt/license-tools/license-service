/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "licensecheckextension.h"

namespace QLicenseClient {

LicenseCheckExtension::LicenseCheckExtension(LicenseReservationInfo *licenseInfo)
    : m_licenseInfo(licenseInfo)
{
}

bool LicenseCheckExtension::verifyLicense()
{
    return m_licenseInfo->status() == StatusCode::Success;
}

} // namespace QLicenseClient
