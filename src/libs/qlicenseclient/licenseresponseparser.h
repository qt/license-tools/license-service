/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "licenseclient.h"

#include <jsonhandler.h>

#include <string>
#include <memory>

namespace QLicenseClient {

using ParseResponseFunction = std::function<bool(LicenseReservationInfo *)>;

class LicenseResponseParser
{
public:
    LicenseResponseParser(const std::string &input);

    bool parse(LicenseReservationInfo *reservationInfo);

private:
    std::string preParseVersion() const;
    ParseResponseFunction findParser(const std::string &version) const;

    bool parseResponseV10(LicenseReservationInfo *reservationInfo);

private:
    std::unique_ptr<QLicenseCore::JsonHandler> m_json;

    const std::map<std::string, ParseResponseFunction> m_responseParsers {
        {"1.0", std::bind(&LicenseResponseParser::parseResponseV10, this, std::placeholders::_1)}
    };
};

} // namespace QLicenseClient
