/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "licenseclient.h"

#include "tcpclient.h"

#include <lockfile.h>
#include <watchdog.h>
#include <response.h>
#include <settings.h>

#include <memory>
#include <mutex>

namespace QLicenseClient {

class LicenseClientPrivate
{
public:
    LicenseClientPrivate(LicenseClient *qq);
    ~LicenseClientPrivate();

    bool init(std::string &error, int timeoutSecs = 0, bool isRetry = false);

    bool reserve(const ClientProperties &clientProperties, LicenseClient::ReservationStatusCallback);
    void release();

    bool status(LicenseClient::StatusOperation operation, std::string &status);
    bool getServiceSetting(const std::string &key, std::string &status);
    bool setServiceSetting(const std::string &key, const std::string &value, const LicenseClient::SettingsType &settingsType, std::string &reply);
    bool requestOperation(const std::string &operation, std::string &status, const std::string &key = "", const std::string &value = "", const LicenseClient::SettingsType &settingsType = LicenseClient::SettingsType::Temporary);

    StatusCode statusCode(int status) const;

    void setClientSetting(const std::string &key, const std::string &value);
    std::string clientSetting(const std::string &key) const;

public:
    static bool serviceAvailable();
    static void setMandatoryRequestParameters(std::stringstream &ss,
        const std::string &protocolVersion, const std::string &operation,
        const std::string &appName, const std::string &appVersion,
        const std::string &username);

private:
    static bool getDaemonPort(uint16_t &port);

    bool requestLicense(const std::string &operation, const std::string &jwt = "");
    bool sendReceive(const std::string &message, std::string &reply, std::string &error);

    bool parseServiceResponse(QLicenseCore::ServiceResponse &response);
    bool parseLicenseResponse(QLicenseCore::ServiceResponse &response);
    bool validateLicense();

    bool removePortFile();

    bool getJwt(std::string &jwt);

    bool startService(uint16_t &port, std::string &error);
    bool waitServiceStart(uint16_t &port, std::string &error);

    void startWatchdog();

    void setError(LicenseReservationInfo *info, const std::string &error, StatusCode code) const;

    friend class LicenseClient;

    struct LicenseRequestParameters
    {
        std::string protocolVersion;
        std::string consumerId;
        std::string consumerVersion;
        std::string consumerInstallationSchema;
        std::string consumerSupportedFeatures;
        std::string consumerProcessId;
        std::string consumerBuildTimestamp;
        std::string userId;
        std::string command; // Obsolete?
    };

private:
    LicenseClient *const q;

    std::unique_ptr<TcpClient> m_tcpClient;
    QLicenseCore::LockFile m_lock;
    std::thread m_thread;
    mutable std::mutex m_mutex;

    std::string m_jwt;

    LicenseClient::ReservationStatusCallback m_onReservationStatusChanged;
    std::unique_ptr<QLicenseCore::Watchdog> m_reservationStatusWatchdog;

    LicenseRequestParameters m_requestParams;
    LicenseReservationInfo *m_reservationInfo;

    QLicenseCore::Settings m_settings;
};

} // namespace QLicenseClient
