/* Copyright (C) 2025 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <string>
#include <map>
#include <vector>

namespace QLicenseClient {

struct LicenseeInfo
{
    std::string name;
    std::string description;
    std::string contact;
};

struct LicenseeContacts
{
    std::string sales;
    std::string support;
    std::string admin;
};

struct ConsumerInfo
{
    std::string consumerId;
    std::string consumerVersion;
    int consumerPriority = 0;
    std::map<std::string, std::string> consumerFeatures;
};

enum class LicenseInfoType {
    Id                = 0,
    Model             = 1,
    Priority          = 2,
    Type              = 3,
    Schema            = 4,
    Hosting           = 5,
    LicenseValidFrom  = 6,
    LicenseValidTo    = 7,
    LeaseValidFrom    = 8,
    LeaseValidTo      = 9
};

enum class StatusCode {
    UnknownError           = -1,
    // Success codes       [0-99]
    Success                = 0,
    // Rejection codes     [100-199]
    LicenseRejected        = 100,
    LicensePoolFull        = 101,
    UnknownLicenseModel    = 102,
    UnknownReservationType = 103,
    ReservationNotFound    = 104,
    UnknownLicenseType     = 105,
    BetterLicenseAvailable = 106,
    TermsAndConditionsNotAccepted = 107,
    ServerHasNoLicenses    = 108,
    // Request error codes [200-299]
    BadRequest             = 200,
    InvalidResponse        = 201,
    BadConnection          = 202,
    Unauthorized           = 203,
    ServerError            = 204,
    ServerBusy             = 205,
    TimeMismatch           = 206,
    TcpSocketError         = 207,
    SslVerifyError         = 208,
    SslLocalCertificateError = 209,
    BadClientRequest       = 210,
    RequestTimeout         = 211,
    SettingsError          = 212,
    // Version error codes [300-399]
    ServiceVersionTooLow   = 300,
    ServiceVersionTooNew   = 301,
    MissingServiceVersion  = 302,
    IncompatibleLicenseFormatVersion = 303
};

class LicenseInfo
{
public:
    LicenseInfo() = default;
    virtual ~LicenseInfo() = default;

    virtual void clear();

    const LicenseeInfo &licenseeInfo() const;
    const LicenseeContacts &contactsInfo() const;
    bool licenseInfo(const LicenseInfoType type, std::string &value) const;
    std::vector<ConsumerInfo> consumers() const;

    const std::string &jsonData() const;

    void setLicensee(const LicenseeInfo &licensee);
    void setContacts(const LicenseeContacts &contacts);
    void setLicenseInfo(LicenseInfoType type, const std::string &value);
    void setConsumers(const std::vector<ConsumerInfo> &consumers);
    void setJsonData(const std::string &json);

private:
    friend class LicenseResponseParser;

    LicenseeInfo m_licensee;
    LicenseeContacts m_contacts;
    std::map<LicenseInfoType, std::string> m_licenseInfo;

    std::vector<ConsumerInfo> m_consumers;

    std::string m_jsonData;
};

class LicenseReservationInfo : public LicenseInfo
{
public:
    LicenseReservationInfo();

    void clear() override;

    const std::map<std::string, std::string> &licenseFeatures() const;

    std::string message() const;
    StatusCode status() const;
    std::string statusToString(StatusCode status) const;

    void setFeatures(const std::map<std::string, std::string> &features);
    void setMessage(const std::string &message);
    void setStatus(StatusCode status);

private:
    std::map<std::string, std::string> m_features;

    std::string m_message;
    StatusCode m_status;
};

} // namespace QLicenseClient
