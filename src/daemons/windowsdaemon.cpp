/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "daemon.h"

#include "licenseservice.h"
#include "utils.h"

#include <strsafe.h>
#include <cstdio>
#include <iostream>

#define SVCNAME TEXT("qtlicd")

using namespace QLicenseService;

SERVICE_STATUS          gSvcStatus;
SERVICE_STATUS_HANDLE   gSvcStatusHandle;
HANDLE                  ghSvcStopEvent = NULL;

VOID SvcInstall(void);
VOID __stdcall DoDeleteSvc(void);
VOID WINAPI SvcCtrlHandler( DWORD );
VOID WINAPI SvcMain( DWORD, LPWSTR * );

VOID ReportSvcStatus( DWORD, DWORD, DWORD );
VOID SvcInit( DWORD, LPWSTR * );
VOID SvcReportEvent( LPWSTR );

std::unique_ptr<LicenseService> service;

BOOL WINAPI consoleHandler(DWORD signal)
{
    if (signal == CTRL_C_EVENT)
        service->cancel();

    return TRUE;
}

bool handleArgs(int argc, wchar_t* argv[], uint16_t& tcpPort, DaemonRunMode& daemonRunMode, std::string &workDir, std::string& message)
{
    // Convert Unicode UTF-16 command-line arguments to UTF-8
    char** args = new char* [argc];
    for (int i = 0; i < argc; ++i) {
        std::string narrowStr = utils::narrow(argv[i]);
        args[i] = new char[narrowStr.length() + 1];
        std::copy(narrowStr.begin(), narrowStr.end(), args[i]);
        args[i][narrowStr.length()] = '\0'; // Ensure null-termination
    }

    const bool success = handleArgs(argc, args, tcpPort, daemonRunMode, workDir, message);

    for (int i = 0; i < argc; ++i)
        delete[] args[i];

    delete[] args;

    return success;
}

//
// Purpose:
//   Entry point for the process
//
// Parameters:
//   None
//
// Return value:
//   None, defaults to 0 (zero)
//
int __cdecl wmain(int argc, wchar_t *argv[])
{
    QLicenseCore::utils::setMaxOpenFileDescriptrors();

    // First check if we must install/uninstall the service
    if (lstrcmpiW(argv[1], L"--install") == 0) {
        // If command-line parameter is "install", install the service
        printf("Installing service\n");
        SvcInstall();
        return 0;
    }
    else if (lstrcmpiW(argv[1], L"--delete") == 0) {
        printf("Deleting service\n");
        DoDeleteSvc();
        return 0;
    }

    setLogFile();

    // Parse the rest of the arguments
    uint16_t tcpPort = 0;
    std::string workDir = SYSTEM_WORK_DIR;
    DaemonRunMode daemonMode = DaemonRunMode::Daemon; // default
    std::string message;
    if (!handleArgs(argc, argv, tcpPort, daemonMode, workDir, message)) {
        logError("Arguments: %s", message.c_str());
        printHelp();
        return EXIT_FAILURE;
    }

    if (daemonMode != DaemonRunMode::Daemon) {
        // Fire it up
        service.reset(new LicenseService(daemonMode, tcpPort, workDir));

        if (!(service->start() && service->waitForStarted())) {
            logError("Error while starting licenser: %s", service->errorString().c_str());
            return -1;
        }

        logInfo("Started in mode #%d: Listening...", (int)daemonMode);

        if (!service->waitForFinished()) {
           logError("Error while stopping licenser: %s", service->errorString().c_str());
            return -1;
        }

        // Licenser runs infinitely if not interrupted
        if (service->status() != LicenseService::Status::Canceled
                && service->status() != LicenseService::Status::Finished) {
            logError("Unexpected status for stopped licenser: ", service->errorString().c_str());
            return -1;
        }

        return 0;
    }

    // Otherwise, the service is probably being started by the SCM.

    // Set logger to print to file only by default, as system service does not need stdout
    Logger::getInstance()->setLogFile(SYSTEM_WORK_DIR + std::string(LOG_FILE));
    Logger::getInstance()->setPrintToStdout(false);
    Logger::getInstance()->setPrintToFile(true);

    SERVICE_TABLE_ENTRYW DispatchTable[] =
    {
        { SVCNAME, (LPSERVICE_MAIN_FUNCTION) SvcMain },
        { NULL, NULL }
    };

    // This call returns when the service has stopped.
    // The process should simply terminate when the call returns.

    if (!StartServiceCtrlDispatcherW( DispatchTable ))
    {
        SvcReportEvent(TEXT("StartServiceCtrlDispatcher"));
    }
}

//
// Purpose:
//   Installs a service in the SCM database
//
// Parameters:
//   None
//
// Return value:
//   None
//
VOID SvcInstall()
{
    SC_HANDLE schSCManager;
    SC_HANDLE schService;
    wchar_t szUnquotedPath[MAX_PATH];
    SERVICE_DESCRIPTIONW sd;
    LPWSTR szDesc = TEXT(LICD_SERVICE_DESCRIPTION);
    if ( !GetModuleFileNameW( NULL, szUnquotedPath, MAX_PATH ) )
    {
        printf("Cannot install service (%d)\n", GetLastError());
        return;
    }

    // In case the path contains a space, it must be quoted so that
    // it is correctly interpreted. For example,
    // "d:\my share\myservice.exe" should be specified as
    // ""d:\my share\myservice.exe"".
    wchar_t szPath[MAX_PATH];
    StringCbPrintfW(szPath, MAX_PATH, TEXT("\"%s\""), szUnquotedPath);

    // Get a handle to the SCM database.

    schSCManager = OpenSCManagerW(
        NULL,                    // local computer
        NULL,                    // ServicesActive database
        SC_MANAGER_ALL_ACCESS);  // full access rights

    if (NULL == schSCManager)
    {
        printf("OpenSCManager failed (%d)\n", GetLastError());
        return;
    }

    // Create the service

    schService = CreateServiceW(
        schSCManager,              // SCM database
        SVCNAME,                   // name of service
        SVCNAME,                   // service name to display
        SERVICE_ALL_ACCESS,        // desired access
        SERVICE_WIN32_OWN_PROCESS, // service type
        SERVICE_AUTO_START,      // start type
        SERVICE_ERROR_NORMAL,      // error control type
        szPath,                    // path to service's binary
        NULL,                      // no load ordering group
        NULL,                      // no tag identifier
        NULL,                      // no dependencies
        NULL,                      // LocalSystem account
        NULL);                     // no password

    if (schService == NULL)
    {
        printf("CreateService failed (%d)\n", GetLastError());
        CloseServiceHandle(schSCManager);
        return;
    }
    else printf("Service installed successfully\n");

    // Change the service description.
    sd.lpDescription = szDesc;
    if ( !ChangeServiceConfig2W(
        schService,                 // handle to service
        SERVICE_CONFIG_DESCRIPTION, // change: description
        &sd) )                      // new description
    {
        printf("ChangeServiceConfig2 failed\n");
    }

    CloseServiceHandle(schService);
    CloseServiceHandle(schSCManager);
}

//
// Purpose:
//   Entry point for the service
//
// Parameters:
//   dwArgc - Number of arguments in the lpszArgv array
//   lpszArgv - Array of strings. The first string is the name of
//     the service and subsequent strings are passed by the process
//     that called the StartService function to start the service.
//
// Return value:
//   None.
//
VOID WINAPI SvcMain( DWORD dwArgc, LPWSTR *lpszArgv )
{
    QLicenseCore::utils::setMaxOpenFileDescriptrors();

    // Register the handler function for the service
    gSvcStatusHandle = RegisterServiceCtrlHandler(
        SVCNAME,
        SvcCtrlHandler);

    if ( !gSvcStatusHandle )
    {
        SvcReportEvent(TEXT("RegisterServiceCtrlHandler"));
        return;
    }

    // These SERVICE_STATUS members remain as set here

    gSvcStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
    gSvcStatus.dwServiceSpecificExitCode = 0;

    // Report initial status to the SCM

    ReportSvcStatus( SERVICE_START_PENDING, NO_ERROR, 3000 );

    // Perform service-specific initialization and work.

    SvcInit( dwArgc, lpszArgv );
}

//
// Purpose:
//   The service code
//
// Parameters:
//   dwArgc - Number of arguments in the lpszArgv array
//   lpszArgv - Array of strings. The first string is the name of
//     the service and subsequent strings are passed by the process
//     that called the StartService function to start the service.
//
// Return value:
//   None
//
VOID SvcInit( DWORD dwArgc, LPWSTR *lpszArgv)
{
    // TO_DO: Declare and set any required variables.
    //   Be sure to periodically call ReportSvcStatus() with
    //   SERVICE_START_PENDING. If initialization fails, call
    //   ReportSvcStatus with SERVICE_STOPPED.

    // Create an event. The control handler function, SvcCtrlHandler,
    // signals this event when it receives the stop control code.

    ghSvcStopEvent = CreateEvent(
                         NULL,    // default security attributes
                         TRUE,    // manual reset event
                         FALSE,   // not signaled
                         NULL);   // no name

    if ( ghSvcStopEvent == NULL)
    {
        ReportSvcStatus( SERVICE_STOPPED, GetLastError(), 0 );
        return;
    }

    // Report running status when initialization is complete.

    ReportSvcStatus( SERVICE_RUNNING, NO_ERROR, 0);

    printf("Starting daemon\n");

    service.reset(new LicenseService(DaemonRunMode::Daemon, 0, SYSTEM_WORK_DIR));

    DWORD error = NO_ERROR;
    // Fire it up
    if (service->start() && service->waitForStarted()) {
        std::cout << "Started: listening..\n";

        while (true) {
            // Do work until signaled to stop
            if (WaitForSingleObject(ghSvcStopEvent, 10) == WAIT_OBJECT_0)
                break;
        }

        service->cancel();
        if (!service->waitForFinished()) {
            std::cout << "Error while stopping licenser: " << service->errorString() << std::endl;
            error = ERROR_SERVICE_SPECIFIC_ERROR;
        }

        // Licenser runs infinitely if not interrupted
        if (service->status() != LicenseService::Status::Canceled
                && service->status() != LicenseService::Status::Finished) {
            std::cout << "Unexpected status for stopped licenser: " << service->errorString() << std::endl;
            error = ERROR_SERVICE_SPECIFIC_ERROR;
        }
    } else {
        std::cout << "Error while starting licenser: " << service->errorString() << std::endl;
        error = ERROR_SERVICE_SPECIFIC_ERROR;
    }

    ReportSvcStatus( SERVICE_STOP_PENDING, error, 0 );
    CloseHandle(ghSvcStopEvent);
    ReportSvcStatus( SERVICE_STOPPED, error, 0 );
    return;
}

//
// Purpose:
//   Sets the current service status and reports it to the SCM.
//
// Parameters:
//   dwCurrentState - The current state (see SERVICE_STATUS)
//   dwWin32ExitCode - The system error code
//   dwWaitHint - Estimated time for pending operation,
//     in milliseconds
//
// Return value:
//   None
//
VOID ReportSvcStatus( DWORD dwCurrentState,
                      DWORD dwWin32ExitCode,
                      DWORD dwWaitHint)
{
    static DWORD dwCheckPoint = 1;

    // Fill in the SERVICE_STATUS structure.

    gSvcStatus.dwCurrentState = dwCurrentState;
    gSvcStatus.dwWin32ExitCode = dwWin32ExitCode;
    gSvcStatus.dwWaitHint = dwWaitHint;

    if (dwCurrentState == SERVICE_START_PENDING)
        gSvcStatus.dwControlsAccepted = 0;
    else gSvcStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP;

    if ( (dwCurrentState == SERVICE_RUNNING) ||
           (dwCurrentState == SERVICE_STOPPED) )
        gSvcStatus.dwCheckPoint = 0;
    else gSvcStatus.dwCheckPoint = dwCheckPoint++;

    // Report the status of the service to the SCM.
    SetServiceStatus( gSvcStatusHandle, &gSvcStatus );
}

//
// Purpose:
//   Called by SCM whenever a control code is sent to the service
//   using the ControlService function.
//
// Parameters:
//   dwCtrl - control code
//
// Return value:
//   None
//
VOID WINAPI SvcCtrlHandler( DWORD dwCtrl )
{
   // Handle the requested control code.

   switch (dwCtrl)
   {
      case SERVICE_CONTROL_STOP:
         ReportSvcStatus(SERVICE_STOP_PENDING, NO_ERROR, 0);

         // Signal the service to stop.

         SetEvent(ghSvcStopEvent);
         ReportSvcStatus(gSvcStatus.dwCurrentState, NO_ERROR, 0);

         return;

      case SERVICE_CONTROL_INTERROGATE:
         break;

      default:
         break;
   }

}

//
// Purpose:
//   Logs messages to the event log
//
// Parameters:
//   szFunction - name of function that failed
//
// Return value:
//   None
//
// Remarks:
//   The service must have an entry in the Application event log.
//
VOID SvcReportEvent(LPWSTR szFunction)
{
    HANDLE hEventSource;
    LPCWSTR lpszStrings[2];
    wchar_t Buffer[80];

    hEventSource = RegisterEventSourceW(NULL, SVCNAME);

    if ( NULL != hEventSource )
    {
        StringCchPrintfW(Buffer, 80, TEXT("%s failed with %d"), szFunction, GetLastError());

        lpszStrings[0] = SVCNAME;
        lpszStrings[1] = Buffer;

        ReportEventW(hEventSource,        // event log handle
                    EVENTLOG_ERROR_TYPE, // event type
                    0,                   // event category
                    0,           // event identifier
                    NULL,                // no security identifier
                    2,                   // size of lpszStrings array
                    0,                   // no binary data
                    lpszStrings,         // array of strings
                    NULL);               // no binary data

        DeregisterEventSource(hEventSource);
    }
}

//
// Purpose:
//   Deletes a service from the SCM database
//
// Parameters:
//   None
//
// Return value:
//   None
//
VOID __stdcall DoDeleteSvc()
{
    SC_HANDLE schSCManager;
    SC_HANDLE schService;
    // Get a handle to the SCM database.

    schSCManager = OpenSCManagerW(
        NULL,                    // local computer
        NULL,                    // ServicesActive database
        SC_MANAGER_ALL_ACCESS);  // full access rights

    if (NULL == schSCManager)
    {
        printf("OpenSCManager failed (%d)\n", GetLastError());
        return;
    }

    // Get a handle to the service.

    schService = OpenServiceW(
        schSCManager,       // SCM database
        SVCNAME,          // name of service
        DELETE);            // need delete access

    if (schService == NULL)
    {
        printf("OpenService failed (%d)\n", GetLastError());
        CloseServiceHandle(schSCManager);
        return;
    }

    // Delete the service.

    if (! DeleteService(schService) )
    {
        printf("DeleteService failed (%d)\n", GetLastError());
    }
    else printf("Service deleted successfully\n");

    CloseServiceHandle(schService);
    CloseServiceHandle(schSCManager);
}
