/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/
#pragma once

#include "commonsetup.h"
#include "licenser.h"
#include "version.h"
#include "logger.h"
#include "licdsetup.h"
#include "installationmanager.h"

using namespace QLicenseCore;

namespace QLicenseService {

bool parseLogLevel(const char* logLevel, std::string)
{
    if (strcmp(logLevel, "silly") == 0) {
        Logger::getInstance()->setLogLevel(LogLevel::LOGLEVEL_SILLY);
    } else if (strcmp(logLevel, "debug") == 0) {
        Logger::getInstance()->setLogLevel(LogLevel::LOGLEVEL_DEBUG);
    } else if (strcmp(logLevel, "info") == 0) {
        Logger::getInstance()->setLogLevel(LogLevel::LOGLEVEL_INFO);
    } else if (strcmp(logLevel, "warning") == 0) {
        Logger::getInstance()->setLogLevel(LogLevel::LOGLEVEL_WARNING);
    } else if (strcmp(logLevel, "error") == 0) {
        Logger::getInstance()->setLogLevel(LogLevel::LOGLEVEL_ERROR);
    } else if (strcmp(logLevel, "none") == 0) {
        Logger::getInstance()->setLogLevel(LogLevel::LOGLEVEL_NONE);
    } else {
        return false;
    }
    return true;
}

void setLogFile()
{
    std::string path = utils::getProgramPath();
    size_t pos = path.find_last_of(DIR_SEPARATOR);
    if (pos != std::string::npos)
        path = path.substr(0, pos) + DIR_SEPARATOR + LOG_FILE;  // next to the program executable
    else
        path = LOG_FILE;  // is the program running in root folder?
    Logger::getInstance()->setLogFile(path);
}

bool parseRunMode(const char *mode, DaemonRunMode& runMode, std::string &workDir, std::string &message)
{
    if (strcmp(mode, "daemon") == 0) {
        // This is default, but can be given as an argument
        runMode = DaemonRunMode::Daemon;
        workDir = SYSTEM_WORK_DIR;
    } else if (strcmp(mode, "cli") == 0) {
        runMode = DaemonRunMode::CLIMode;
        workDir = LicdSetup::getQtAppDataLocation()
                + std::string(USER_SETTINGS_FOLDER_NAME)
                + DIR_SEPARATOR;
    } else if (strcmp(mode, "on-demand") == 0) {
        runMode = DaemonRunMode::OnDemand;
        workDir = LicdSetup::getQtAppDataLocation()
                + std::string(USER_SETTINGS_FOLDER_NAME)
                + DIR_SEPARATOR;
    } else if (strcmp(mode, "test") == 0) {
        runMode = DaemonRunMode::TestMode;
        workDir = "./";
    } else {
        message = "Unknown mode: " + std::string(mode);
        return false;
    }

    logInfo("Working directory is now set to '%s'", workDir.c_str());
    return true;
}

void printHelp()
{
    printf("\nUsage: qtlicd.exe [option] [value]\n");
    printf("Supported options are:\n");
    printf("  --version              : Version info\n");
    printf("  --register <source>    : Registers itself in the 'installations.ini' file\n");
    printf("       The <source> defines the entity calling this. E.g. 'installer'.\n");
    printf("  --unregister           : Unregister itself from the 'installations.ini' file\n");
    printf("  --add-key-value <key=value>\n");
    printf("                         : Add additional key-value pair to the installations.ini on register\n");
    printf("  --help                 : This help\n");
#ifdef _WIN32
    printf("  --install              : Installs the daemon executable as system service\n");
    printf("  --delete               : Removes the daemon executable from system services\n");
#endif
    printf("  --port <port number>   : Specify TCP/IP server port. If omitted, default is used.\n");
    printf("  --log-level <loglevel> : Set logging level to be one of: none|info|warning|error|debug|silly\n");
    printf("  --nostdout             : Do not print logs to stdout\n");
    printf("  --nologfile            : Do not print logs into file\n");
    printf("  --mode <mode>          : Set operating mode, required value must be one of:\n");
    printf("       on-demand -  Run in on-demand mode (in user space)\n");
    printf("       cli       -  Run in non-daemon mode (in console like any other CLI app)\n");
    printf("       test      -  Run in test mode (for devs, not operable in real environment)\n");
#ifndef _WIN32
    // CMD line parameter "--mode daemon" does nothing in windows:
    // Only windows service dispatcher can start qtlicd in daemon mode
    printf("       daemon    -  Run as system service (default)\n");
#else
    printf("    Note: If launched from windows cmd line, mode is mandatory.");
#endif
    printf("Example: Run as CLI app, setting log level to 'debug'\n");
    printf("> qtlicd --log-level debug --mode cli:\n\n");
    printf("Supported environment variables are:\n");
    printf("  QTLICD_LOG_LEVEL <loglevel>\n");
    printf("                         : Set logging level to be one of: none|info|warning|error|debug|silly\n");
    printf("  QTLICD_LICENSE_IMPORT_PATH <path>\n");
    printf("                         : Set the path to import local licenses from (defaults to user's home)\n");
}


bool handleArgs(int argc, char *argv[], uint16_t& tcpPort, DaemonRunMode &daemonRunMode, std::string &workDir, std::string &message)
{
    // Parse env vars
    if (const char* logLevel = std::getenv("QTLICD_LOG_LEVEL")) {
        if (!parseLogLevel(logLevel, message)) {
            message = ("invalid log level; " + std::string(logLevel));
            return false;
        }
    }

    // Parse the cmd-line args
    for (int i = 1; i < argc; i++) {
        std::string arg = std::string(argv[i]);
        if (arg  == "--version") {
            //Show the version and exit.
            std::cout << "Qt License Daemon (qtlicd) v" << DAEMON_VERSION << " "
                    << COPYRIGHT_TEXT << std::endl;
#ifdef GIT_COMMIT_SHA
            std::cout << "From revision: " << GIT_COMMIT_SHA << std::endl;
#endif
            std::cout << "Build date: " << BUILD_DATE << std::endl;
            exit(EXIT_SUCCESS);
        } else if (arg == "--register") {
            static const std::string sc_optionPrefix = "--";

            std::string installSource;
            if ((argc == i + 1) || std::string(argv[i + 1]).substr(0, sc_optionPrefix.size()) == sc_optionPrefix)
                installSource = "undefined";
            else
                installSource = argv[i + 1];

            // Second iteration to find the contextual option
            std::string valuesString;
            for (int j = 1; j < argc; ++j) {
                std::string arg = argv[j];
                if (arg == "--add-key-value" && (j + 1 < argc)) {
                    valuesString = argv[j + 1];
                    break;
                }
            }

            std::map<std::string, std::string> keyValues;
            if (!valuesString.empty()) {
                std::istringstream iss(valuesString);
                std::string pair;

                while (std::getline(iss, pair, ',')) {
                    std::istringstream pairIss(pair);
                    std::string key;
                    std::string value;
                    if (std::getline(pairIss, key, '=') && std::getline(pairIss, value))
                        keyValues[key] = value;
                }
            }

            InstallationManager manager;
            if (manager.registerInstallation(utils::getProgramPath(), DAEMON_VERSION, installSource, keyValues))
                exit(EXIT_SUCCESS);
            else
                exit(EXIT_FAILURE);
         } else if (arg == "--unregister") {
            InstallationManager manager;
            if (manager.unregisterInstallation(utils::getProgramPath()))
                exit(EXIT_SUCCESS);
            else
                exit(EXIT_FAILURE);
        } else if (arg == "--help") {
            printHelp();
            exit(EXIT_SUCCESS);
        } else if (arg == "--mode") {
            if (argc == i + 1) {
                message = "No run mode given";
                return false;
            }
            if (!parseRunMode(argv[i + 1], daemonRunMode, workDir, message))
                return false;
            i++;
        } else if (arg == "--log-level") {
            if (argc == i + 1) {
                message = "No log level given";
                return false;
            } else if (!parseLogLevel(argv[i + 1], message)) {
                message = ("invalid log level; " + std::string(argv[i + 1]));
                return false;
            }
            i++;
        } else if (arg == "--port") {
            if (argc == i + 1) {
                message = "No port number given";
                return false;
            }
            try {
                tcpPort = std::stoi(argv[i+1]);
                i++;
            }
            catch (...) {
               message = "Invalid port - must be an integer";
               return false;
            }
        } else if (arg == "--nostdout") {
            Logger::getInstance()->setPrintToStdout(false);
        } else if (arg == "--nologfile") {
            Logger::getInstance()->setPrintToFile(false);
        }
        else {
            message = "Invalid argument: " + arg;
            return false;
        }
    }
    return true;
}

} // namespace QLicenseService
