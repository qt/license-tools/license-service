/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "daemon.h"

#include "licenseservice.h"
#include "utils.h"

#include <sys/stat.h>
#include <unistd.h>
#include <csignal>

using namespace QLicenseService;

std::unique_ptr<LicenseService> service;

void signalHandler(int signal)
{
    if (!service)
        exit(signal);

    switch (signal) {
    case SIGINT:
        service->cancel();
        break;
    case SIGTERM:
        service->cancel();
        break;
    default:
        exit(signal);
    }
}

int startProcess(uint16_t tcpPort, DaemonRunMode &mode, const std::string &workDir) {
    // Fire it up
    service.reset(new LicenseService(mode, tcpPort, workDir));

    if (!(service->start() && service->waitForStarted())) {
        logError("Failed to start licenser: %s", service->errorString().c_str());
        return EXIT_FAILURE;
    }

    logInfo("Started in mode #%d: Listening...", (int)mode);

    if (!service->waitForFinished()) {
        logError("Failed to stop licenser: %s", service->errorString().c_str());
        return EXIT_FAILURE;
    }

    // Licenser runs infinitely if not interrupted
    if (service->status() != LicenseService::Status::Canceled
            && service->status() != LicenseService::Status::Finished) {
        logError("Unexpected status for stopped licenser: %s", service->errorString().c_str());
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}


int main(int argc, char *argv[])
{
    QLicenseCore::utils::setMaxOpenFileDescriptrors();

    signal(SIGINT, signalHandler);
    signal(SIGTERM, signalHandler);

    uint16_t tcpPort = 0;
    DaemonRunMode daemonMode = DaemonRunMode::Daemon; // default
    std::string workDir = SYSTEM_WORK_DIR; // default
    std::string message;

    setLogFile();

    // Parse the arguments
    if (!handleArgs(argc, argv, tcpPort, daemonMode, workDir, message)) {
        logError("Arguments: %s", message.c_str());
        printHelp();
        return EXIT_FAILURE;
    }

    if (daemonMode != DaemonRunMode::Daemon) {
        return startProcess(tcpPort, daemonMode, workDir);
    }

    /*
    *   Daemon mode
    */

    // Set logger to print to file only by default, as system service does not need stdout
    Logger::getInstance()->setPrintToStdout(false);
    Logger::getInstance()->setPrintToFile(true);

#ifdef __linux__
    // Fork process in linux. (macOS launchd doesn't like forking)
    pid_t pid, sid;
    pid = fork();
    if (pid > 0) {
        return EXIT_SUCCESS;
    }
    else if (pid < 0) {
        logError("Daemon process fork failed!");
        return EXIT_FAILURE;
    }
    umask(0);
    sid = setsid();
    // Check session ID for the child process
    if (sid < 0)
    {
        logError("Invalid session id for a child process");
        return EXIT_FAILURE;
    }
#endif

    int result = startProcess(tcpPort, daemonMode, workDir);
    logInfo("Daemon stopped with return code: %d", result);
    return result;
}
