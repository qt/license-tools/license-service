# Copyright (C) 2024 The Qt Company Ltd.
#
# SPDX-License-Identifier: BSD-3-Clause
#

string(TIMESTAMP BUILD_DATE "%Y-%m-%d")

execute_process(
    COMMAND git rev-list --abbrev-commit -n1 HEAD
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    OUTPUT_VARIABLE GIT_COMMIT_SHA
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

# If git command failed or resulting variable is empty, try to read from .tag file
if (NOT GIT_COMMIT_SHA)
    file(READ "${CMAKE_SOURCE_DIR}/.tag" TAG_CONTENT)
    string(STRIP ${TAG_CONTENT} GIT_COMMIT_SHA)
endif()

message("Commit SHA: ${GIT_COMMIT_SHA}")

file(READ "${CMAKE_SOURCE_DIR}/dist/qtlicd.ini" QTLICD_INI_FILE_CONTENT)
# Escape double quotes and newline
string(REPLACE "\"" "\\\"" QTLICD_INI_FILE_CONTENT "${QTLICD_INI_FILE_CONTENT}")
string(REPLACE "\n" "\\n" QTLICD_INI_FILE_CONTENT "${QTLICD_INI_FILE_CONTENT}")

set(DEFAULT_QTLICD_SETTINGS_FILE_CONTENT "${QTLICD_INI_FILE_CONTENT}")

configure_file(commonsetup.h.in ${CMAKE_BINARY_DIR}/include/commonsetup.h)
configure_file(version.h.in ${CMAKE_BINARY_DIR}/include/version.h)
