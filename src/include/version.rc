/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include <windows.h>

#include "version.h"

VS_VERSION_INFO VERSIONINFO
FILEVERSION     DAEMON_VERSION_WIN32
PRODUCTVERSION  DAEMON_VERSION_WIN32
FILEFLAGSMASK   VS_FFI_FILEFLAGSMASK
FILEFLAGS       0x0L
FILEOS          VOS_NT_WINDOWS32
FILETYPE        VFT_APP
FILESUBTYPE     VFT2_UNKNOWN

BEGIN
    BLOCK "StringFileInfo"
    BEGIN
        BLOCK "040904B0" // Language and codepage (U.S. English and Unicode)
        BEGIN
            VALUE "CompanyName", COMPANY_NAME
            VALUE "FileDescription", LICD_SERVICE_DESCRIPTION
            VALUE "FileVersion", DAEMON_VERSION
            VALUE "InternalName", "qtlicd\0"
            VALUE "LegalCopyright", COPYRIGHT_TEXT
            VALUE "OriginalFilename", "qtlicd.exe\0"
            VALUE "ProductName", LICD_SERVICE_DESCRIPTION
            VALUE "ProductVersion", DAEMON_VERSION
        END
    END

    BLOCK "VarFileInfo"
    BEGIN
        // English language (0x409) in the Unicode (UTF-16) codepage (0x04B0)
        VALUE "Translation", 0x0409, 0x04B0
    END
END
