/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "qtlicensetool.h"
#include "licenseclient.h"

#include <constants.h>
#include <logger.h>
#include <qtaccount.h>
#include <version.h>
#include <utils.h>

#include <iostream>

#if __APPLE__ || __MACH__ || __linux__
#include <termios.h>
#elif _WIN32
#include <conio.h>
#endif

using namespace QLicenseClient;
using namespace QLicenseCore;

void showVersion()
{
    std::cout << "qtlicensetool v" << QTLICENSETOOL_VERSION
              << " - CLI tool for Qt Licensing - " << COPYRIGHT_TEXT << std::endl;
#ifdef GIT_COMMIT_SHA
    std::cout << "From revision: " << GIT_COMMIT_SHA << std::endl;
#endif
    std::cout << "Build date: " << BUILD_DATE << std::endl;

    return;
}

bool askStatus(LicenseClient::StatusOperation operation)
{
    LicenseClient client;

    std::string error;
    if (!client.init(error)) {
        std::cout << error << std::endl;
        return false;
    }

    std::string reply;
    const bool success = client.status(operation, reply);

    std::cout << reply << std::endl;
    return success;
}

bool getServiceSetting(const std::string &key, std::string &value)
{
    LicenseClient client;
    std::string error;

    if (!client.init(error)) {
        std::cout << error << std::endl;
        return false;
    }

    return client.getServiceSetting(key, value);
}

bool setServiceSetting(const std::string &key, const std::string &value,
    const LicenseClient::SettingsType &settingsType, std::string &reply)
{
    LicenseClient client;
    std::string error;

    if (!client.init(error)) {
        std::cout << error << std::endl;
        return false;
    }

    return client.setServiceSetting(key, value, settingsType, reply);
}

bool clearReservations()
{
    LicenseClient client;

    std::string error;
    if (!client.init(error)) {
        std::cout << error << std::endl;
        return false;
    }

    std::string reply;
    const bool success = client.clearReservationCache(reply);

    std::cout << reply << std::endl;
    return success;
}

void performLogin()
{
    QLicenseService::QtAccount account;

    std::string email;
    std::string password;

    std::cout << "Qt Account email: ";
    std::cin >> email;

    std::cout << "Qt Account password (will not echo): ";
#if __APPLE__ || __MACH__ || __linux__
    termios oldTerm;
    termios term;

    // Turn off echoing
    tcgetattr(STDIN_FILENO, &oldTerm);
    term = oldTerm;
    term.c_lflag &= ~ECHO;
    tcsetattr(STDIN_FILENO, TCSANOW, &term);

    std::cin >> password;

    // Restore old attributes
    tcsetattr(STDIN_FILENO, TCSANOW, &oldTerm);
#elif _WIN32
    std::wstring wpassword;
    wchar_t ch;
    while ((ch = _getwch()) != L'\r') { // Return key
        if (ch == L'\b') { // Backspace key
            if (!wpassword.empty())
                wpassword.pop_back();
        } else {
            wpassword.push_back(ch);
        }
    }
    password = QLicenseCore::utils::narrow(wpassword);
#endif
    std::cout << "\n";

    if (!account.performLogin(email, password))
        errorAndExit("Login failed with error: " + account.error());

    if (!account.writeToDisk())
        errorAndExit(account.error());

    std::cout << "Successfully logged in for account " << email << std::endl;
}

void errorAndExit(const std::string &reason)
{
    if (!reason.empty()) {
        std::cout << reason << std::endl;
    }
    exit(EXIT_FAILURE);
}

bool showTermsAndConditions()
{
    std::cout << "The full Terms and Conditions are available in: '" << sc_termsAndConditionsUrl << "'\n" << std::endl;
    std::cout << sc_termsAndConditionsTitle << "\n" << std::endl;

    std::cout << "[x] " << sc_termsAndConditionsDesignatedUserConfirmation << std::endl;
    std::cout << "[x] " << sc_termsAndConditionsConfirmation << "\n" << std::endl;

    std::string value;
    if (getServiceSetting(sc_termsAndConditionsAccepted, value) && (value == sc_true)) {
        std::cout << "Terms and Conditions already accepted by user." << std::endl;
        return true;
    }

    if (Logger::outputRedirected()) {
        std::cout << "Output device does not appear to be associated with a terminal. "
            "The Terms and Conditions can be accepted with the '--accept-terms-and-conditions' "
            "option." << std::endl;
        return true;
    }

    std::cout << "Do you accept the above terms? [y|n]" << std::endl;
    std::getline(std::cin, value);
    utils::trimStr(value);
    value = utils::strToLower(value);

    std::string setting;
    if (value == "yes" || value == "y") {
        setting = sc_true;
    } else if (value == "no" || value == "n") {
        setting = sc_false;
    } else {
        std::cout << "Invalid value '" << value << "'" << std::endl;
        return false;
    }

    std::string reply;
    if (!setServiceSetting(sc_termsAndConditionsAccepted, setting,
            LicenseClient::SettingsType::Persistent, reply)) {
        std::cout << "Failed to store Terms and Conditions acceptance: " << reply << std::endl;
        return false;
    }

    if (setting == sc_true)
        std::cout << "Terms and Conditions accepted by user." << std::endl;
    else
        std::cout << "Terms and Conditions rejected by user." << std::endl;

    return true;
}

bool acceptTermsAndConditions()
{
    std::string reply;
    if (!setServiceSetting(sc_termsAndConditionsAccepted, sc_true,
            LicenseClient::SettingsType::Persistent, reply)) {
        std::cout << "Failed to store Terms and Conditions acceptance: " << reply << std::endl;
        return false;
    }

    std::cout << "The full Terms and Conditions are available in: '" << sc_termsAndConditionsUrl << "'\n" << std::endl;
    std::cout << sc_termsAndConditionsTitle << "\n" << std::endl;

    std::cout << "[x] " << sc_termsAndConditionsDesignatedUserConfirmation << std::endl;
    std::cout << "[x] " << sc_termsAndConditionsConfirmation << "\n" << std::endl;

    std::cout << "Terms and Conditions accepted by user." << std::endl;
    return true;
}


void helpAndExit()
{
    std::cout << "Usage:\n"
        << "     qtlicensetool <Action> [params]\n"
        << "   Where 'Action' is:\n"
        << "     -h or --help                  : Prints out the tool's help text\n"
        << "     -v or --version               : Prints out qtlicensetool version\n"
        << "     -S or --serverversion         : Prints out License Server version\n"
        << "     -D or --daemonversion         : Prints out the License Service (daemon) version\n"
        << "     -l or --login                 : Login with your Qt Account for cloud usage\n"
        << "     -r or --reservations          : Prints out the list of active reservations\n"
        << "     -c or --clear-reservations    : Releases license reservations for the current user\n"
        << "     --settings                    : Set or get settings values\n"
        << "     --get-server-address          : Get address of the license server\n"
        << "     --set-server-address <URL>    : Set address of the license server\n"
        << "     --show-terms-and-conditions   : Shows the terms and conditions of the service\n"
        << "     --accept-terms-and-conditions : Accepts the terms and conditions of the service\n"
        << "   Example:\n"
        << "     > qtlicensetool --daemonversion                         : Prints out a daemon version number\n"
        << "     > qtlicensetool --settings set request_timeout 60 Y     : Update the request_timeout value\n"
        << "     > qtlicensetool --settings get request_timeout          : Prints out the request_timeout value\n";
    exit(EXIT_SUCCESS);
}
