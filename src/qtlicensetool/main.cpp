/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "qtlicensetool.h"

#include <constants.h>
#include <licenseclient.h>
#include <logger.h>

#include <algorithm>
#include <iostream>

using namespace QLicenseClient;
using namespace QLicenseCore;


int main(int argc, char *argv[])
{
    Logger::getInstance()->setLogLevel(LogLevel::LOGLEVEL_WARNING);
    Logger::getInstance()->setPrintToFile(false);

    if (argc < 2) {
        errorAndExit("No arguments given");
    }
    std::string command = argv[1];
    // Get rid of commands which don't require further actions
    if (command == "--version" || command == "-v") {
        showVersion();
        return EXIT_SUCCESS;
    } else if (command == "--help" || command == "-h") {
        helpAndExit();
    } else if (command == "--login" || command == "-l") {
        performLogin();
        return EXIT_SUCCESS;
    }

    // Then those requiring connection
    if (command == "--serverversion" || command == "-S") {
        if (!askStatus(LicenseClient::StatusOperation::ServerVersion))
            return EXIT_FAILURE;
    } else if (command == "--daemonversion" || command == "-D") {
        if (!askStatus(LicenseClient::StatusOperation::DaemonVersion))
            return EXIT_FAILURE;
    } else if (command == "--reservations" || command == "-r") {
        if (!askStatus(LicenseClient::StatusOperation::Reservations))
            return EXIT_FAILURE;
    } else if (command == "--show-terms-and-conditions") {
        if (!showTermsAndConditions())
            return EXIT_FAILURE;
    } else if (command == "--accept-terms-and-conditions") {
        acceptTermsAndConditions();
    } else if (command == "--settings") {
        if (argc < 4)
            errorAndExit("Too few arguments for option --settings");

        const std::string action = argv[2];
        const std::string key = argv[3];
        std::string value;

        if (!action.empty() && action == "get" && !key.empty()) {
            getServiceSetting(key, value);
            std::cout << value << std::endl;
        } else if (!action.empty() && action == "set" && !key.empty()) {
            std::string isPersistenceSetting;
            LicenseClient::SettingsType settingsType;
            if (argc > 4)
                value = argv[4];
            if (argc > 5)
                isPersistenceSetting = argv[5];

            isPersistenceSetting = utils::strToLower(isPersistenceSetting);
            if (isPersistenceSetting == "yes" || isPersistenceSetting == "y") {
                settingsType = LicenseClient::SettingsType::Persistent;
            } else if (isPersistenceSetting == "no" || isPersistenceSetting == "n"
                    || isPersistenceSetting.empty()) {
                settingsType = LicenseClient::SettingsType::Temporary;
            } else {
                errorAndExit("Invalid argument for settings persistence. "
                    "Supported values are 'y|yes' and 'n|no'.");
            }
            std::string reply;
            const bool success = setServiceSetting(key, value, settingsType, reply);
            std::cout << reply << std::endl;
            if (!success)
                return EXIT_FAILURE;
        } else {
            errorAndExit("Invalid arguments for option --settings");
        }
    } else if (command == "--get-server-address") {
        std::string value;
        const bool success = getServiceSetting(sc_serverAddr, value);
        std::cout << value << std::endl;
        if (!success)
            return EXIT_FAILURE;
    } else if (command == "--set-server-address") {
        if (argc < 3)
            errorAndExit("Too few arguments for option --set-server-address");

        const std::string value = argv[2];
        std::string reply;
        const bool success = setServiceSetting(sc_serverAddr, value, LicenseClient::SettingsType::Persistent, reply);
        std::cout << reply << std::endl;
        if (!success)
            return EXIT_FAILURE;
    } else if (command == "--clear-reservations" || command == "-c"){
        if (!clearReservations())
            return EXIT_FAILURE;
    } else {
        errorAndExit("Invalid parameters");
    }
}
