/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "mocwrapper.h"

#include "licenseclient.h"
#include "version.h"
#include "commonsetup.h"
#include "utils.h"
#include "installationmanager.h"
#include "logger.h"

#include <condition_variable>
#include <sstream>
#include <iostream>

#if __APPLE__ || __MACH__ || __linux__
#include <unistd.h>
#else
#include <process.h>
#include <windows.h>
#endif

using namespace QLicenseClient;
using namespace QLicenseCore;

static const char sc_tab[] = "    ";
static const char sc_mocBypassLicenseCheck[] = "QTFRAMEWORK_BYPASS_LICENSE_CHECK";
static const char sc_qConfigPath[] = "../mkspecs/" QCONFIG_FILE;
static const char sc_mocVersionPath[] = MOC_QT_VERSION_FILE;

static std::condition_variable s_cv;
static std::mutex s_mutex;
static bool s_bypassLicenseCheck = false;

int forward_call(int argc, char *argv[])
{
    // Even though server will see something like "qtframework" within
    // license request, the binary name still is "moc"
    static const std::string sc_origCmd = "moc";

    std::string executable = ORIGINAL_MOC_PREFIX + sc_origCmd;
#ifdef _WIN32
    (void)argc;
    (void)argv;

    LPCWSTR originalCmdLine = GetCommandLineW();
    std::string cmd = utils::narrow(originalCmdLine);

    size_t pos = utils::strToLower(cmd).find(utils::strToLower(sc_origCmd));
    if (pos == std::string::npos) {
        fprintf(stderr, "Could not find the executable name \"%s\" from command line: %s\n",
            executable.c_str(), cmd.c_str());
        return 1;
    }
    cmd.replace(pos, sc_origCmd.length(), executable);

    STARTUPINFOW si;
    PROCESS_INFORMATION pi;

    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    ZeroMemory(&pi, sizeof(pi));

    // Start the child process
    if (!CreateProcessW(NULL, const_cast<wchar_t *>(QLicenseCore::utils::widen(cmd).c_str()),
            NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) {
        fprintf(stderr, "Unable to create process %s: %s", executable.c_str(),
            QLicenseCore::utils::getLastWin32Error().c_str());
        return 1;
    }

    WaitForSingleObject(pi.hProcess, INFINITE);

    // Close process and thread handles
    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);

    return 0;
#else
    std::string cmd = std::string(argv[0]) + '\0';
    cmd.replace(cmd.find(sc_origCmd), sc_origCmd.length(), executable);

    for (int i = 1; i < argc; i++) {
        cmd += " ";
        cmd += argv[i];
        cmd += '\0';
    }

    int retval = execv(cmd.c_str(), argv);
    if (retval != 0) {
        fprintf(stderr, "Unable to call '%s': %s\nCommand: \n%s\n",
            executable.c_str(), strerror(errno), cmd.c_str());
    }
    return retval;
#endif
}

int requestLicense()
{
    std::unique_lock<std::mutex> lock(s_mutex);

    LicenseClient client;

    std::string error;
    if (!client.init(error)) {
        std::cout << "Could not initialize license client: " << error << std::endl;
        return -1;
    }

    int status = 0;
    auto onReservationStatusChanged = [&](LicenseReservationInfo *info) {
        if (info->status() == StatusCode::Success) {
            std::string licenseId;
            info->licenseInfo(LicenseInfoType::Id, licenseId);
            logInfo("Got a valid license for %s, license id: %s", MOCWRAPPER_CONSUMER_NAME, licenseId.c_str());
        } else {
            std::cout << "Could not request license for " << MOCWRAPPER_CONSUMER_NAME << ":" << std::endl;
            std::cout << sc_tab << "Message: " << info->message() << std::endl;
            std::cout << sc_tab << "Status code: " << static_cast<int>(info->status()) << std::endl;
            status = -1;
        }

        s_cv.notify_all();
    };

    ClientProperties props;
    props.consumerId = MOCWRAPPER_CONSUMER_NAME;
    props.consumerVersion = getQtFrameworkVersion();
    props.consumerSupportedFeatures = {};
    props.consumerInstallationSchema = "commercial";

    if (!client.reserve(props, onReservationStatusChanged)) {
        std::cout << "Could not send license reservation request" << std::endl;
        return -1;
    }

    s_cv.wait(lock);
    return status;
}

bool bypassLicenseCheck()
{
    if (s_bypassLicenseCheck)
        return true;

    const char *value = std::getenv(sc_mocBypassLicenseCheck);
    if (!value)
        return false;

    const std::string valueStr = QLicenseCore::utils::strToLower(value);
    if (valueStr == "1" || valueStr == "true" || valueStr == "yes")
        return true;
    else if (valueStr == "0" || valueStr == "false" || valueStr == "no")
        return false;

    // default if unrecognized value
    return false;
}

void printFailureInstructions()
{
    std::cout << "If you have a valid commercial license, please create a bug report to Qt Bugtracker:" << std::endl;
    std::cout << "https://bugreports.qt.io/projects/QLS" << std::endl << std::endl;
    std::cout << "Attach the above details to the bug report." << std::endl << std::endl;
    std::cout << "Additionally, attach the " << LOG_FILE << " file of the Qt License Service that "
                 "can be found from the directory of this Qt installation." << std::endl;

    QLicenseCore::InstallationManager manager;
    std::string logFilePath = manager.installationForVersion(DAEMON_VERSION);
    if (!logFilePath.empty()) {
        logFilePath = QLicenseCore::utils::getPathWithoutFilename(logFilePath);
        logFilePath += DIR_SEPARATOR;
        logFilePath += LOG_FILE;

        std::cout << "On this system the log file path appears to be: \"" << logFilePath << "\"." << std::endl;
    }

    std::cout << std::endl;
    std::cout << "To bypass the license check entirely, set the environment variable "
              << sc_mocBypassLicenseCheck << " with value 1." << std::endl;
}

std::string getQtFrameworkVersion()
{
    std::string programPath = utils::getProgramPath();
    if (programPath.empty())
        return std::string("0.0.0");

    programPath = utils::getPathWithoutFilename(programPath);
    programPath += DIR_SEPARATOR;

    std::string version;
    if (!getVersionFromMocVersionFile(programPath + sc_mocVersionPath, version)
            && !getVersionFromQtConfFile(programPath + sc_qConfigPath, version)) {
        return std::string("0.0.0");
    }

    return version;
}

bool getVersionFromMocVersionFile(const std::string &filename, std::string &version)
{
    if (!utils::fileExists(filename))
        return false;

    if (!utils::readFile(version, filename))
        return false;

    utils::trimStr(version);
    if (version.empty() || version.size() > 30) {
        logWarn("Ignoring unexpected Qt version: \"%s\"", version.c_str());
        return false;
    }
    return true;
}

bool getVersionFromQtConfFile(const std::string &filename, std::string &version)
{
    if (!utils::fileExists(filename))
        return false;

    std::string contents;
    if (!utils::readFile(contents, filename))
        return false;

    std::istringstream iss;
    iss.str(contents);

    static const std::string sc_key = "QT_VERSION =";

    std::string line;
    while (std::getline(iss, line)) {
        // trim leading/trailing whitespace
        utils::trimStr(line);
        if (line.compare(0, sc_key.size(), sc_key) == 0) {
            version = line.substr(sc_key.size());
            utils::trimStr(version);
            // 30 chars ought to be enough for the version
            if (version.empty() || version.size() > 30) {
                logWarn("Ignoring unexpected Qt version: \"%s\"", version.c_str());
                return false;
            }
            return true;
        }
    }

    return false;
}

void setBypassLicenseCheck(bool bypass)
{
    s_bypassLicenseCheck = bypass;
}
