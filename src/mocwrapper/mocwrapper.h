/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <string>

int requestLicense();
int forward_call(int argc, char *argv[]);
bool bypassLicenseCheck();
void setBypassLicenseCheck(bool bypass);
void printFailureInstructions();

std::string getQtFrameworkVersion();
bool getVersionFromMocVersionFile(const std::string &filename, std::string &version);
bool getVersionFromQtConfFile(const std::string &filename, std::string &version);
