/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "mocwrapper.h"
#include "commonsetup.h"
#include "version.h"

#include <iostream>

#include <stdlib.h>

int main(int argc, char *argv[])
{
    if (argc > 1) {
        const std::string arg = argv[1];
        if (arg == "--mocwrapper-version") {
            std::cout << "MOC-wrapper v" << MOCWRAPPER_VERSION << " "
                      << COPYRIGHT_TEXT << std::endl;
#ifdef GIT_COMMIT_SHA
            std::cout << "From revision: " << GIT_COMMIT_SHA << std::endl;
#endif
            std::cout << "Build date: " << BUILD_DATE << std::endl;
            return EXIT_SUCCESS;
        } else if ((arg == "-h") || (arg == "--help") || (arg == "--help-all") || (arg == "-?")
                || (arg == "-v") || (arg == "--version")) {
            // Skip license check for options that do not necessitate making a license reservation
            setBypassLicenseCheck(true);
        }
    }

    if (!bypassLicenseCheck() && (requestLicense() != 0)) {
        std::cout << "Cannot acquire license to use " << MOCWRAPPER_CONSUMER_NAME
                  << "." << std::endl << std::endl;
        printFailureInstructions();
        // We allow running without license for the time being
    }

    // Continue by running MOC...
    if (forward_call(argc, argv) != 0) {
        std::cerr << "Unable to start moc" << std::endl;
        return EXIT_FAILURE;
    }
}
