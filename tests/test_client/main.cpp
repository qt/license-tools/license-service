/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
*/

#include "testapp.h"

#include <utils.h>

#include <atomic>
#include <iostream>
#include <thread>

using namespace QLicenseCore;

class DurationAverage
{
public:
    DurationAverage()
        : m_sum(0)
        , m_count(0)
    {}

    void add(uint64_t duration)
    {
        if (duration == 0)
            return;

        m_sum.fetch_add(duration, std::memory_order_relaxed);
        m_count.fetch_add(1, std::memory_order_relaxed);
    }

    double average() const
    {
        uint64_t total_sum = m_sum.load(std::memory_order_relaxed);
        uint64_t total_count = m_count.load(std::memory_order_relaxed);

        return total_count > 0 ? static_cast<double>(total_sum) / total_count : 0.0;
    }

private:
    std::atomic<uint64_t> m_sum;
    std::atomic<uint64_t> m_count;
};

void printHelp()
{
    std::cout << "Usage:\n"
        << " testapp <application name> <version> [options]\n"
        << "     Application name and version are mandatory\n\n";
        std::cout << "   Possible options are:\n"
        << "     -h or --help                     : This help\n"
        << "     -o or --operation                : Operation to perform - possible values are:\n"
        << "                                          reservation (default) | list | precheck\n"
        << "   Options for operation \"reservation\":\n"
        << "     -j or --parallel-jobs <number>   : Number of parallel license requests to do\n"
        << "     -r or --rounds <number>          : Number of sequential rounds for license requests\n"
        << "     -t or --max-time <milliseconds>  : Maximum time to simulate work after succesfull reservation\n"
        << "                                          The actual time is randomized for each client, between 0...<max-time>.\n"
        << "                                          The default maximum work time is 1 second.\n"
        << "     -ex or --extended-license-check  : Enforce extended license check.\n"
        << "     -bts or --build-timestamp <date> : Use given consumer build timestamp for license requests.\n"
        << "   Examples:\n"
        << "     > testapp squish_tester 8.0 -j 1 -r 2     : Make a single squish tester license reservation 2 times in a row\n"
        << "     > testapp squish_tester 8.0 -o precheck   : Check if given consumer can be installed\n"
        << "     > testapp squish_tester 8.0 -o list       : List all licenses for current user\n"
    << std::endl;
}

int runReservationsTestBench(int rounds, int jobs, uint32_t maxTime
     , const std::string &consumerId, const std::string &consumerVersion
     , const std::string &consumerBuildTimestamp, bool extendedLicenseCheck)
{
    const unsigned time = utils::utcTime();
    srand(time);

    std::atomic<int> errors(0);
    DurationAverage avg;

    for (int i = 0; i < rounds; ++i) {
        std::cout << "### Start request round " << i << " ###" << std::endl;
        std::vector<std::unique_ptr<std::thread>> threads;
        for (int j = 0; j < jobs; ++j) {
            threads.push_back(std::unique_ptr<std::thread>(new std::thread([=, &errors, &avg]() -> void {
#ifdef _WIN32
                uint32_t seed = (uint32_t)time ^ (uint32_t)GetCurrentThreadId();
                srand(seed);
#endif
                // Add some randomness to the starting time
                uint32_t msecs = rand() % 1000;
                utils::sleepMillisecs(msecs);

                auto start = std::chrono::high_resolution_clock::now();

                TestApp app(consumerId, consumerVersion, consumerBuildTimestamp, extendedLicenseCheck);
                if (!app.requestLicense()) {
                    // License not granted
                    std::cout << "[Job " << j << "] No luck with license, bailing out" << std::endl;
                    ++errors;
                    return;
                }

                auto end = std::chrono::high_resolution_clock::now();

                auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
                avg.add(duration.count());

                std::cout << "[Job " << j << "] took " << duration.count() << " milliseconds to get license." << std::endl;
                // Do the job here:
                msecs = (maxTime == 0) ? rand() % 1000 : rand() % maxTime;
                std::cout << "[Job " << j << "] Doing my work for "
                          << (float)((float)msecs / (float)1000) << " secs" << std::endl;
                utils::sleepMillisecs(msecs);

                std::cout << "[Job " << j << "] Done. (disconnected)" << std::endl;
            })));
        }

        for (auto &thread : threads)
            thread->join();
    }

    if (errors > 0) {
        std::cout << "### Finished with errors! Failed requests: " << errors << " ###" << std::endl;
        return 1;
    } else {
        std::cout << "### Finished ###" << std::endl;
        std::cout << "### Average duration for license acquirement was: " << avg.average() << " ms" << std::endl;
        return 0;
    }
}

int main(int argc, char *argv[])
{
    utils::setMaxOpenFileDescriptrors();

    std::string consumerId;
    std::string consumerVersion;
    std::string consumerBuildTimestamp;
    std::string operation = "reservation";
    bool extendedLicenseCheck = false;

    int rounds = 1;
    int jobs = 1;
    uint32_t maxTime = 0;

    if (argc < 3)  {
        std::cout << "Too few arguments" << std::endl;
        printHelp();
        return 1;
    }
    // Find and application name, which should be 1st argument
    consumerId = argv[1];
    consumerVersion = argv[2];

    // Parse rest of the arguments; First index is the command itself, second
    // is the app name, third is the version, so we start from 3
    for (int i = 3; i < argc; i++) {
        const std::string arg = argv[i];
        std::string val;

        if (arg == "--help" || arg == "-h") {
            printHelp();
            return 0;
        }

        // It's an option, check if it has value
        if (arg[0] == '-' && argc > i + 1) {
            val = argv[i + 1];
            utils::trimStr(val);
            if (val[0] == '-')// Do not accept another switch as a value
                val.clear();
            else
                i++;
        }

        if (val.empty()) {
            std::cout <<"No value for option: " << arg << std::endl;
            return 1;
        }

        if (arg == "--parallel-jobs" || arg == "-j") {
            jobs = utils::strToInt(val);
        } else if (arg == "--rounds" || arg == "-r") {
            rounds = utils::strToInt(val);
        } else if (arg == "--max-time" || arg == "-t") {
            maxTime = utils::strToInt(val);
        } else if (arg == "--operation" || arg == "-o") {
            operation = val;
        } else if (arg == "--extended-license-check" || arg == "-ex") {
            extendedLicenseCheck = true;
        } else if (arg == "--build-timestamp" || arg == "-bts") {
            consumerBuildTimestamp = val;
        } else {
            std::cout << "Unknown option: " << arg << std::endl;
            return 1;
        }
    }

    if (operation == "reservation") {
        return runReservationsTestBench(rounds, jobs, maxTime,
            consumerId, consumerVersion, consumerBuildTimestamp, extendedLicenseCheck);
    } else if (operation == "list") {
        TestApp app(consumerId, consumerVersion, consumerBuildTimestamp, extendedLicenseCheck);
        return (app.listLicenses() ? 0 : 1);
    } else if (operation == "precheck") {
        TestApp app(consumerId, consumerVersion, consumerBuildTimestamp, extendedLicenseCheck);
        return (app.precheckInstallation() ? 0 : 1);
    } else {
        std::cout << "Unknown operation: " << operation << std::endl;
        return 1;
    }
}
