/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
*/
#pragma once

#include "licenseclient.h"
#include "licenseprecheck.h"
#include "version.h"

#include <memory>
#include <condition_variable>

#define VERSION_MAJOR       DAEMON_VERSION_MAJOR
#define VERSION_MINOR       DAEMON_VERSION_MINOR
#define TEST_APP_VERSION    VERSION_MAJOR "." VERSION_MINOR
#define TEST_APP_NAME       "Test_Application"
#define WORK_DIR            "./"

using namespace QLicenseClient;
using namespace QLicensePrecheck;

class TestApp
{
public:
    TestApp(const std::string &consumerId, const std::string &consumerVersion,
        const std::string &consumerBuildTimestamp = std::string(), bool extendedLicenseCheck = false);
    ~TestApp();

    bool requestLicense();
    bool listLicenses();
    bool precheckInstallation();

private:
    int getUserInfo();
    ClientProperties getClientProperties() const;

    void printLicenseInfo(LicenseInfo &info);
    void printReservationInfo(LicenseReservationInfo &info);

private:
    std::string m_path;
    std::string m_consumerId;
    std::string m_consumerVersion;
    std::string m_consumerBuildTimestamp;
    bool m_extendedLicenseCheck;

    std::unique_ptr<LicenseClient> m_client;
    std::unique_ptr<LicensePrecheck> m_precheck;

    std::condition_variable m_cv;
    std::mutex m_mutex;
};

class TcpClient;
