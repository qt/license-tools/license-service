/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
*/

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <initcleanupfixture.h>

#include "commonsetup.h"

#include "qtlicensetool.h"
#include "licenseservice.h"
#include "licenseclient.h"

using namespace QLicenseService;
using namespace QLicenseClient;

TEST_CASE_METHOD(InitCleanupFixture, "Version query", "[qtlicensetool]")
{
    LicenseService service(DaemonRunMode::TestMode, 0, DAEMON_WORK_DIR_TEST);
    // 1. Verify starting of license "service" succeeds
    REQUIRE((service.start() && service.waitForStarted()));
    // 2. Verify send version command & reply status ok
    REQUIRE(askStatus(LicenseClient::StatusOperation::DaemonVersion));
    // 3. Verify stop service
    REQUIRE((service.cancel() && service.waitForFinished(30000)));
    REQUIRE(service.status() == LicenseService::Status::Canceled);
}
