/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
*/

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <initcleanupfixture.h>

#include "testdata.h"

#include <string>

#include <qtaccountsetup.h>
#include <commonsetup.h>

using namespace QLicenseCore;

TEST_CASE_METHOD(InitCleanupFixture, "Initialize Qt Account settings from file", "[licdsetup]")
{
    QtAccountSetup settings(QT_ACCOUNT_INI_PATH);
    REQUIRE(settings.init());

    REQUIRE(settings.get("email") == "foo@bar.com");
    REQUIRE(settings.get("jwt") == "123456789");
}
