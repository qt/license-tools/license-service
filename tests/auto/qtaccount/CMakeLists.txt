# Copyright (C) 2025 The Qt Company Ltd.
#
# SPDX-License-Identifier: BSD-3-Clause
#

set(QT_ACCOUNT_INI_PATH ${CMAKE_CURRENT_LIST_DIR}/qtaccount.ini)
configure_file(testdata.h.in testdata.h)

add_executable(tst_qtaccount tst_qtaccount.cpp)
target_link_libraries(tst_qtaccount PRIVATE Catch2 qlicensecore)

add_test(NAME tst_qtaccount COMMAND tst_qtaccount)

include_directories("${CMAKE_BINARY_DIR}/tests/auto/qtaccount")
