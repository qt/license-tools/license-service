/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
*/

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <initcleanupfixture.h>

#include "commonsetup.h"
#include "testdata.h"

#include <string>

#include <inifileparser.h>
#include <installationmanager.h>
#include <licdsetup.h>

using namespace QLicenseCore;

const char TEST_INI_FILE[] = "installations.ini";
const std::string TEST_QTINI_PATH = utils::workingDirectory() + DIR_SEPARATOR + DAEMON_SETTINGS_FILE;

class InitCleanupIniFIle: public InitCleanupFixture
{
public:
    InitCleanupIniFIle()
    {
        std::string iniFileContent;
        utils::readFile(iniFileContent, MOCK_QTLICDINI_PATH);
        utils::writeToFile(TEST_QTINI_PATH, iniFileContent);
    }

    ~InitCleanupIniFIle()
    {
        utils::deleteFile(TEST_QTINI_PATH);
    }
};

static bool checkIniContent(
    std::map<int, std::string> &initialFileContent,
    std::map<int, std::string> &finalFileContent)
{
    std::string line;
    int count = 0;
    int total = initialFileContent.size();
    if (initialFileContent.size() >= finalFileContent.size()) {
        return false;
    }
    for (const auto &keyPare : initialFileContent) {
        count++;
        if (count == total) {
            if (finalFileContent[count] != "new_property=new_value") {
                return false;
            }
        } else {
            size_t separatorPos = keyPare.second.find('=');
            if (separatorPos != std::string::npos) {
                std::string currentKey = keyPare.second.substr(0, separatorPos);
                if (currentKey == "server_addr") {
                    if (currentKey + "=" + "http://test.qls:8080" != finalFileContent[count]) {
                        return false;
                    }
                } else if (initialFileContent[count] != finalFileContent[count]) {
                    return false;
                }
            }
        }
    }
    return true;
}

TEST_CASE_METHOD(InitCleanupFixture, "Read matching service installation from installations.ini", "[installationmanager]")
{
    std::remove(TEST_INI_FILE);
    IniFileParser iniHandler(TEST_INI_FILE);
    InstallationManager manager(TEST_INI_FILE);

    REQUIRE(
    iniHandler.writeKeyValue("_home_foo_bar1_bin_qtlicd", {
            {"path", "/home/foo/bar1/bin/qtlicd"},
            {"version", "3.1.0"}
        }));

    REQUIRE(
    iniHandler.writeKeyValue("_home_foo_bar2_bin_qtlicd", {
            {"path", "/home/foo/bar2/bin/qtlicd"},
            {"version", "3.0.1"}
        }));

    REQUIRE(
    iniHandler.writeKeyValue("_home_foo_bar6_bin_qtlicd", {
            {"path", "/home/foo/bar6/bin/qtlicd"},
            {"version", "3.1.0"},
            {"preview", "true"}
        }));

    // Test non-preview is preferred when same version is available
    REQUIRE(manager.installationForVersion("3.1.0")
            == "/home/foo/bar1/bin/qtlicd");

    REQUIRE(
    iniHandler.writeKeyValue("_home_foo_bar4_bin_qtlicd", {
            {"path", "/home/foo/bar4/bin/qtlicd"},
            {"version", "3.2.0"}
        }));

    REQUIRE(
    iniHandler.writeKeyValue("_home_foo_bar5_bin_qtlicd", {
            {"path", "/home/foo/bar5/bin/qtlicd"},
            {"version", "2.1.0"}
        }));

    // Test latest minor version is preferred
    REQUIRE(manager.installationForVersion("3.0.0")
            == "/home/foo/bar4/bin/qtlicd");

    REQUIRE(
    iniHandler.writeKeyValue("_home_foo_bar3_bin_qtlicd", {
            {"path", "/home/foo/bar3/bin/qtlicd"},
            {"version", "4.1.0"}
        }));

    // Test correct major version is preferred
    REQUIRE(manager.installationForVersion("2.2.0")
            == "/home/foo/bar5/bin/qtlicd");

    REQUIRE(
        iniHandler.writeKeyValue("_home_foo_bar7_bin_qtlicd", {
            {"path", "/home/foo/bar7/bin/qtlicd"},
            {"version", "4.2.0"},
            {"preview", "true"}
        }));

    // Test preview is preferred when greater version is available
    REQUIRE(manager.installationForVersion("4.1.0")
            == "/home/foo/bar7/bin/qtlicd");

    // Test older major version is not accepted
    REQUIRE(manager.installationForVersion("5.2.0") == "");

    // Test newer major version is not accepted
    REQUIRE(manager.installationForVersion("1.2.0") == "");

    // Test older minor version, but same major version is accepted
    REQUIRE(manager.installationForVersion("4.9.0")
            == "/home/foo/bar7/bin/qtlicd");
}

TEST_CASE_METHOD(InitCleanupFixture, "Remove orphans from installations.ini", "[installationmanager]")
{
    INFO("TEST_INI_FILE: " << TEST_INI_FILE);
    std::remove(TEST_INI_FILE);

    {
        IniFileParser iniHandler(TEST_INI_FILE);

        REQUIRE(
        iniHandler.writeKeyValue("path_1", {
                {"path", "/home/foo/bar1/bin/qtlicd"},
                {"version", "3.1.0"}
            }));

        REQUIRE(
        iniHandler.writeKeyValue("path_2", {
                {"path", MOCK_QTLICD_PATH},
                {"version", "3.0.1"}
            }));

        REQUIRE(
        iniHandler.writeKeyValue("path_3", {
                {"path", "/home/foo/bar4/bin/qtlicd"},
                {"version", "3.2.0"}
            }));
    }

    InstallationManager(TEST_INI_FILE).purgeObsoleteInstallations();

    IniFileParser iniHandler(TEST_INI_FILE);
    std::map<std::string, std::map<std::string, std::string>> results;
    REQUIRE(iniHandler.readSections(results));
    REQUIRE(results["path_1"].empty() == true);
    REQUIRE(results["path_2"].empty() == false);
    REQUIRE(results["path_3"].empty() == true);
}

TEST_CASE_METHOD(InitCleanupIniFIle, "Ensure keys retain their original positions after update qtlicd.ini properties", "[installationmanager]")
{
    IniFileParser iniHandler(TEST_QTINI_PATH);

#if _WIN32
    std::ifstream initialInputFile(utils::widen(MOCK_QTLICDINI_PATH));
#else
    std::ifstream initialInputFile(MOCK_QTLICDINI_PATH);
#endif

    std::map<int, std::string> initialFileContent;
    if (utils::fileExists(MOCK_QTLICDINI_PATH) && initialInputFile.is_open()) {
        std::string line;
        int lineNumber = 1;
        while (std::getline(initialInputFile, line)) {
            utils::trimStr(line);
            initialFileContent[lineNumber] = line;
            lineNumber++;
        }
    }

   REQUIRE(iniHandler.writeKeyValue(
        "QtLicenseDaemon",
        {{"server_addr", "http://test.qls:8080"}, {"new_property", "new_value"}}));

#if _WIN32
    std::ifstream finalOutputFile(utils::widen(TEST_QTINI_PATH));
#else
    std::ifstream finalOutputFile(TEST_QTINI_PATH);
#endif

    std::map<int, std::string> finalFileContent;
    if (utils::fileExists(TEST_QTINI_PATH) && finalOutputFile.is_open()) {
        std::string line;
        int lineNumber = 1;
        while (std::getline(finalOutputFile, line)) {
            utils::trimStr(line);
            finalFileContent[lineNumber] = line;
            lineNumber++;
        }
    }
    REQUIRE(checkIniContent(initialFileContent, finalFileContent));
}

