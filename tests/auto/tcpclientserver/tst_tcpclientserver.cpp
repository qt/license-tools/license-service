/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
*/

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <initcleanupfixture.h>

#include "tcpclient.h"
#include "tcpserver.h"
#include "commonsetup.h"
#include "constants.h"
#include "licenseclient_p.h"
#include "cipcommandparser.h"

using namespace QLicenseClient;
using namespace QLicenseService;

bool waitDataAvailable(TcpServer &server)
{
    bool dataAvailable = false;
    for (int i = 0; i < 10; ++i) {
        dataAvailable = server.dataAvailable();
        if (dataAvailable)
            break;

        utils::sleepSecs(1);
    }

    return dataAvailable;
}

TEST_CASE_METHOD(InitCleanupFixture, "Test receive large response packet", "[tcpclientserver]")
{
    uint16_t port = 0;

    TcpServer server;
    REQUIRE(server.init(port));
    server.start();

    {
        TcpClient client(DAEMON_DEFAULT_ADDR, port);

        std::string error;
        REQUIRE(client.init(error));

        const std::string payload = "a test message";
        REQUIRE(client.send(payload, error));
        REQUIRE(waitDataAvailable(server));

        uint16_t socketId = 1;
        const std::string received = server.receive(socketId);
        REQUIRE(received == payload);

        const std::string responsePayload = utils::generateRandomString(32384);
        REQUIRE(server.sendResponse(socketId, responsePayload) == 0);

        std::string receivedResponse;
        REQUIRE(client.receive(receivedResponse));
        REQUIRE(receivedResponse == responsePayload);
    }

    server.stop();
}

TEST_CASE_METHOD(InitCleanupFixture, "Test OOB packet size", "[tcpclientserver]")
{
    uint16_t port = 0;

    TcpServer server;
    REQUIRE(server.init(port));
    server.start();

    {
        TcpClient client(DAEMON_DEFAULT_ADDR, port);

        std::string error;
        REQUIRE(client.init(error));

        const std::string payload = "a test message";
        REQUIRE(client.send(payload, error));
        REQUIRE(waitDataAvailable(server));

        uint16_t socketId = 1;
        const std::string received = server.receive(socketId);
        REQUIRE(received == payload);

        const std::string responsePayload = utils::generateRandomString(MAX_TCP_MESSAGE_LENGTH + 1);
        REQUIRE(server.sendResponse(socketId, responsePayload) == 1);

        // Need to do the sending manually to circumvent our size limits:
        std::string completeMessage;
        {
            uint32_t capabilities = TcpMessageProtocol::Capabilities::ResponseLengthHeader;
            capabilities |= TcpMessageProtocol::Capabilities::CapabilitiesMagicMarker;
            uint32_t capabilitiesNet = htonl(capabilities);

            completeMessage.resize(sizeof(capabilities));
            std::memcpy(&completeMessage[0], &capabilitiesNet, sizeof(capabilitiesNet));

            uint32_t length = responsePayload.size();
            uint32_t networkLength = htonl(length);

            completeMessage.resize(completeMessage.size() + sizeof(length) + responsePayload.size());
            std::memcpy(&completeMessage[sizeof(capabilities)], &networkLength, sizeof(networkLength));
            std::memcpy(&completeMessage[sizeof(capabilities) + sizeof(networkLength)], responsePayload.data(), responsePayload.size());

            REQUIRE(::send(server.clientSocket(socketId).fd, completeMessage.c_str(), completeMessage.length(), 0)
                == completeMessage.length());
        }

        const std::string expectedError = "TCP: Received message length is out of bounds";
        std::string receivedResponse;
        REQUIRE(!client.receive(receivedResponse));
        REQUIRE(receivedResponse.compare(0, expectedError.size(), expectedError) == 0);
    }

    server.stop();
}

TEST_CASE_METHOD(InitCleanupFixture, "Test client capabilities", "[tcpclientserver]")
{
    uint16_t port = 0;

    TcpServer server;
    REQUIRE(server.init(port));
    server.start();

    {
        TcpClient client(DAEMON_DEFAULT_ADDR, port);

        std::string error;
        REQUIRE(client.init(error));

        // 1. Test no capabilities (backwards compatibility for pre v3.4.0)
        client.setCapabilities(0);

        std::string payload = "a test message";
        REQUIRE(client.send(payload, error));
        REQUIRE(waitDataAvailable(server));

        uint16_t socketId = 1;
        std::string received = server.receive(socketId);
        REQUIRE(received == payload);

        ClientSocket clientSocket = server.clientSocket(socketId);
        REQUIRE(clientSocket.capabilities == 0);

        const std::string responsePayload = "a test response";
        REQUIRE(server.sendResponse(socketId, responsePayload) == 0);

        std::string receivedResponse;
        REQUIRE(client.receiveMessage((responsePayload.length()), receivedResponse));
        REQUIRE(receivedResponse == responsePayload);

        // 2. Test capabilities combination - assert the server received the set capabilities mask.
        uint32_t capabilities = TcpMessageProtocol::Capabilities::ResponseLengthHeader
            | TcpMessageProtocol::Capabilities::ReservedHeaderCapability2
            | TcpMessageProtocol::Capabilities::ReservedHeaderCapability4;

        client.setCapabilities(capabilities);

        REQUIRE(client.send(payload, error));
        REQUIRE(waitDataAvailable(server));

        received = server.receive(socketId);
        REQUIRE(received == payload);

        clientSocket = server.clientSocket(socketId);
        REQUIRE(clientSocket.capabilities & TcpMessageProtocol::Capabilities::ResponseLengthHeader);
        REQUIRE(clientSocket.capabilities & TcpMessageProtocol::Capabilities::ReservedHeaderCapability2);
        REQUIRE(!(clientSocket.capabilities & TcpMessageProtocol::Capabilities::ReservedHeaderCapability3));
        REQUIRE(clientSocket.capabilities & TcpMessageProtocol::Capabilities::ReservedHeaderCapability4);
        REQUIRE(clientSocket.capabilities & TcpMessageProtocol::Capabilities::CapabilitiesMagicMarker);

        REQUIRE(server.sendResponse(socketId, responsePayload) == 0);

        REQUIRE(client.receive(receivedResponse));
        REQUIRE(receivedResponse == responsePayload);

        // 3. Test client sends unknown capabilties to server - assert the supported
        // capabilities for the client socket is determined by the server ("None" in this case)
        server.setCapabilities(TcpMessageProtocol::Capabilities::None);

        std::stringstream ss;
        LicenseClientPrivate::setMandatoryRequestParameters(ss, CIP_PROTOCOL_VERSION,
            DAEMON_VERSION_CMD, QTLICENSETOOL_APP_NAME, QTLICENSETOOL_VERSION, "foobar");
        ss << " -c " << LIBLICENSECLIENT_VERSION;

        payload = ss.str();

        REQUIRE(client.send(payload, error));
        REQUIRE(waitDataAvailable(server));

        received = server.receive(socketId);
        utils::trimStr(received);
        clientSocket = server.clientSocket(socketId);
        REQUIRE(server.supportedCapabilities(clientSocket.capabilities) == TcpMessageProtocol::Capabilities::None);

        // Assert no information is lost or corrupted by inclusion of the message headers
        CipCommandParser parser(received, DAEMON_VERSION);
        REQUIRE(parser.parseInput());
        REQUIRE(parser.clientLibraryVersion() == LIBLICENSECLIENT_VERSION);
        REQUIRE(parser.protocolVersion() == CIP_PROTOCOL_VERSION);
        REQUIRE(parser.command() == DAEMON_VERSION_CMD);
        REQUIRE(parser.consumerId() == QTLICENSETOOL_APP_NAME);
        REQUIRE(parser.consumerVersion() == QTLICENSETOOL_VERSION);
        REQUIRE(parser.userId() == "foobar");

        REQUIRE(server.sendResponse(socketId, responsePayload) == 0);

        REQUIRE(client.receive(receivedResponse));
        REQUIRE(receivedResponse == responsePayload);
    }

    server.stop();
}

TEST_CASE_METHOD(InitCleanupFixture, "Test supported capabilities mask", "[tcpclientserver]")
{
    TcpClient client(DAEMON_DEFAULT_ADDR, 0);

    // 1. No capabilities
    client.setCapabilities(TcpMessageProtocol::Capabilities::None);
    uint32_t capabilitiesMask = TcpMessageProtocol::Capabilities::None;
    REQUIRE(client.supportedCapabilities(capabilitiesMask) == TcpMessageProtocol::Capabilities::None);

    // 2. Mask contains capabilities, client doesn't
    capabilitiesMask = TcpMessageProtocol::Capabilities::ResponseLengthHeader
        | TcpMessageProtocol::Capabilities::CapabilitiesMagicMarker;
    REQUIRE(client.supportedCapabilities(capabilitiesMask) == TcpMessageProtocol::Capabilities::None);

    // 3. Both contain same capabilities
    client.setCapabilities(TcpMessageProtocol::Capabilities::ResponseLengthHeader);
    REQUIRE(client.supportedCapabilities(capabilitiesMask) == (TcpMessageProtocol::Capabilities::ResponseLengthHeader
        | TcpMessageProtocol::Capabilities::CapabilitiesMagicMarker));

    // Client contains capabilities, mask doesn't
    client.setCapabilities(TcpMessageProtocol::Capabilities::ResponseLengthHeader
        | TcpMessageProtocol::Capabilities::ReservedHeaderCapability2);
    REQUIRE(client.supportedCapabilities(capabilitiesMask) == (TcpMessageProtocol::Capabilities::ResponseLengthHeader
        | TcpMessageProtocol::Capabilities::CapabilitiesMagicMarker));
}
