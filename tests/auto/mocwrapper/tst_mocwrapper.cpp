/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
*/

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <initcleanupfixture.h>

#include "testdata.h"

#include <commonsetup.h>
#include <mocwrapper.h>

TEST_CASE_METHOD(InitCleanupFixture, "Get Qt Framework version", "[mocwrapper]")
{
    std::string version;
    REQUIRE(getVersionFromMocVersionFile(MOCWRAPPER_VERSION_FILE, version));
    REQUIRE(version == "6.8.2");

    REQUIRE(getVersionFromQtConfFile(QCONFIG_VERSION_FILE, version));
    REQUIRE(version == "6.6.2");

    // expected as there's no sanity check for the returned version:
    REQUIRE(getVersionFromMocVersionFile(INVALID_FILE, version));
    REQUIRE(!getVersionFromQtConfFile(INVALID_FILE, version));
}

TEST_CASE_METHOD(InitCleanupFixture, "Get Qt Framework version relative to binary", "[mocwrapper]")
{
    std::string originalWorkDir = utils::workingDirectory();
    REQUIRE(!originalWorkDir.empty());

    std::string newWorkDir = utils::getTempDir() + "/tst_mocwrapper"
        + std::to_string(utils::generateUniqueShortId());

    REQUIRE(utils::createDir(newWorkDir));
    REQUIRE(utils::setWorkingDirectory(newWorkDir));

    // Verify expected relative paths to binary work as well:
    std::string testFilePath = std::string(TST_BIN_DIRECTORY) + "/" + MOC_QT_VERSION_FILE;
    REQUIRE(utils::writeToFile(testFilePath, "1.2.3"));
    REQUIRE(getQtFrameworkVersion() == "1.2.3");
    REQUIRE(utils::deleteFile(testFilePath));

    std::string mkspecsDir = std::string(TST_BIN_DIRECTORY) + "/../mkspecs";
    REQUIRE(utils::createDir(mkspecsDir));

    testFilePath = mkspecsDir + "/" + QCONFIG_FILE;
    REQUIRE(utils::writeToFile(testFilePath, "QT_VERSION = 3.2.1"));
    REQUIRE(getQtFrameworkVersion() == "3.2.1");
    REQUIRE(utils::deleteFile(testFilePath));

    REQUIRE(utils::removeDir(mkspecsDir));

    REQUIRE(utils::setWorkingDirectory(originalWorkDir));
    REQUIRE(utils::removeDir(newWorkDir));
}
