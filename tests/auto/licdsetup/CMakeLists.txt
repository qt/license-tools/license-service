# Copyright (C) 2024 The Qt Company Ltd.
#
# SPDX-License-Identifier: BSD-3-Clause
#

add_executable(tst_licdsetup tst_licdsetup.cpp)
target_link_libraries(tst_licdsetup PRIVATE Catch2 qlicensecore)

add_test(NAME tst_licdsetup COMMAND tst_licdsetup)
