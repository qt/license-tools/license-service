/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
*/

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <initcleanupfixture.h>

#include <licdsetup.h>
#include <commonsetup.h>

TEST_CASE_METHOD(InitCleanupFixture, "Initialize service settings from file", "[licdsetup]")
{
    LicdSetup settings(DAEMON_WORK_DIR_TEST + std::string("/") + DAEMON_SETTINGS_FILE);
    REQUIRE(settings.init());

    // verify we can read some values
    REQUIRE(settings.get("server_addr") == "localhost:8080");
    REQUIRE(settings.get("ca_bundle_path") == "auto");
    REQUIRE(settings.get("tcp_listening_port") == "0");
}
