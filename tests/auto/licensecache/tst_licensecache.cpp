/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
*/

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <initcleanupfixture.h>

#include "testdata.h"

#include "commonsetup.h"
#include "clienthandler.h"
#include "licensecache.h"
#include "licenseimport.h"
#include "utils.h"
#include "licdsetup.h"
#include "request.h"
#include "jsonhandler.h"

using namespace QLicenseService;
using namespace QLicenseCore;

LicdSetup sc_setup("");

std::string getTempCachePath()
{
    return utils::getTempDir() + DIR_SEPARATOR
        + "tst_licensecache." + utils::generateRandomString(5);
}

std::vector<License *> getTestLicenses()
{
    std::vector<License *> licenses;
    std::string jsonData;

    REQUIRE(utils::readFile(jsonData, TEST_JSON1));
    licenses.push_back(new License("johndoe", jsonData));

    REQUIRE(utils::readFile(jsonData, TEST_JSON2));
    licenses.push_back(new License("johndoe", jsonData));

    REQUIRE(utils::readFile(jsonData, TEST_JSON3));
    licenses.push_back(new License("foobar", jsonData));

    REQUIRE(utils::readFile(jsonData, TEST_JSON4));
    licenses.push_back(new License("foobar", jsonData));

    return licenses;
}

TEST_CASE_METHOD(InitCleanupFixture, "Initialize empty cache", "[licensecache]")
{
    LicenseCache cache(LicenseCache::Context::User);
    REQUIRE(cache.setPath(getTempCachePath()));
    REQUIRE(cache.init());
    REQUIRE(cache.isValid());
}

TEST_CASE_METHOD(InitCleanupFixture, "Insert and retrieve licenses from cache", "[licensecache]")
{
    static const std::string testPath = getTempCachePath();
    static std::vector<License *> licenses = getTestLicenses();

    std::string jsonData;
    REQUIRE(utils::readFile(jsonData, TEST_JSON1));
    static License *copiedLicense = new License("johndoe", jsonData);

    static std::unique_ptr<LicenseCache> cache(new LicenseCache(LicenseCache::Context::User));

    SECTION("Init cache") {
        REQUIRE(cache->setPath(testPath));
        REQUIRE(cache->init());
        REQUIRE(cache->isValid());
        REQUIRE(cache->licenseCount() == 0);
    }

    SECTION("Verify insert and retrieve") {
        // 1. Insert the licenses
        REQUIRE(cache->insert(licenses[0]));
        REQUIRE(cache->insert(licenses[1]));
        REQUIRE(cache->insert(licenses[2]));
        REQUIRE(cache->insert(licenses[3]));

        // 2. Verify we get the same data back
        License *licenseFromCache = cache->license("johndoe", "1111111111");
        REQUIRE(*licenseFromCache == *licenses[0]);

        licenseFromCache = cache->license("johndoe", "2222222222");
        REQUIRE(*licenseFromCache == *licenses[1]);

        licenseFromCache = cache->license("foobar", "3333333333");
        REQUIRE(*licenseFromCache == *licenses[2]);

        licenseFromCache = cache->license("foobar", "4444444444");
        REQUIRE(*licenseFromCache == *licenses[3]);

        // 3. Verify license count
        REQUIRE(cache->licenseCount() == 4);

        // 4. Verify insert cached license with same data again succeeds
        REQUIRE(*copiedLicense == *licenses[0]);
        REQUIRE(cache->insert(copiedLicense));
        REQUIRE(cache->licenseCount() == 4);

        // Assign a new pointer for the first license
        // (the original was destructed by the cache, on replace)
        licenses[0] = copiedLicense;
    }

    SECTION("Verify retrieve by consumer") {
        // 1. Verify get license with same consumer, higher priority
        License *licenseFromCache = cache->licenseByConsumer("johndoe",
            ConsumerAppInfo("test_app1", "1.2.3", ""));
        REQUIRE(*licenseFromCache == *licenses[1]);

        // 2. Verify get license with different consumer
        licenseFromCache = cache->licenseByConsumer("foobar",
            ConsumerAppInfo("test_app2", "1.0.0", ""));
        REQUIRE(*licenseFromCache == *licenses[2]);

        // 3. Verify get license with no consumer version
        licenseFromCache = cache->licenseByConsumer("foobar", ConsumerAppInfo("test_app3", "", ""));
        REQUIRE(*licenseFromCache == *licenses[3]);
    }

    SECTION("Verify retrieve all licenses for user") {
        std::vector<License *> licensesFromCache = cache->licenses("johndoe");
        REQUIRE(*licensesFromCache[0] == *licenses[0]);
        REQUIRE(*licensesFromCache[1] == *licenses[1]);

        licensesFromCache = cache->licenses("foobar");
        REQUIRE(*licensesFromCache[0] == *licenses[2]);
        REQUIRE(*licensesFromCache[1] == *licenses[3]);
    }

    SECTION("Verify retrieve with invalid information") {
        // 1. Verify get invalid license id for user
        License *licenseFromCache = cache->license("johndoe", "3333333333");
        REQUIRE(!licenseFromCache);

        // 2. Verify get invalid license user, existing license id
        licenseFromCache = cache->license("tester", "1111111111");
        REQUIRE(!licenseFromCache);

        // 3. Verify get invalid license with matching consumer, non matching version
        // TODO: version check is disabled from the resolution logic for now, enable once re-added
        //licenseFromCache = cache->licenseByConsumer("foobar", "test_app3", "0.0.1");
        //REQUIRE(!licenseFromCache);
    }

    SECTION("Clean the cache directory") {
        // Windows won't allow unlinking open files, destroy cache instance first
        cache.reset(nullptr);
        REQUIRE(utils::removeDir(testPath));
    }
}

TEST_CASE_METHOD(InitCleanupFixture, "Create and read cache from disk", "[licensecache]")
{
    static const std::string testPath = getTempCachePath();

    SECTION("Create \"existing\" cache") {
        LicenseCache cache(LicenseCache::Context::User);
        REQUIRE(cache.setPath(testPath));
        REQUIRE(cache.init());
        REQUIRE(cache.isValid());
        REQUIRE(cache.licenseCount() == 0);

        // 1. Insert test licenses
        std::vector<License *> licenses = getTestLicenses();
        REQUIRE(cache.insert(licenses[0]));
        REQUIRE(cache.insert(licenses[1]));
        REQUIRE(cache.insert(licenses[2]));
        REQUIRE(cache.insert(licenses[3]));
    }

    SECTION("Read exsting cache") {
        LicenseCache cache(LicenseCache::Context::User);
        REQUIRE(cache.setPath(testPath));
        REQUIRE(cache.init());
        REQUIRE(cache.isValid());
        REQUIRE(cache.licenseCount() == 4);

        // 1. Verify we get the same data back
        std::vector<License *> licenses = getTestLicenses();

        License *licenseFromCache = cache.license("johndoe", "1111111111");
        REQUIRE(*licenseFromCache == *licenses[0]);

        licenseFromCache = cache.license("johndoe", "2222222222");
        REQUIRE(*licenseFromCache == *licenses[1]);

        licenseFromCache = cache.license("foobar", "3333333333");
        REQUIRE(*licenseFromCache == *licenses[2]);

        licenseFromCache = cache.license("foobar", "4444444444");
        REQUIRE(*licenseFromCache == *licenses[3]);

        for (auto *license : licenses)
            delete license;
    }

    SECTION("Clean the cache directory") {
        REQUIRE(utils::removeDir(testPath));
    }
}

TEST_CASE_METHOD(InitCleanupFixture, "Verify read existing cache contents from disk", "[licensecache]")
{
    LicenseCache cache(LicenseCache::Context::User);
    REQUIRE(cache.setPath(TEST_EXISTING_CACHE_DIR));
    REQUIRE(cache.init());
    REQUIRE(cache.isValid());
    REQUIRE(cache.licenseCount() == 1);

    License *licenseFromCache = cache.license("johndoe", "1111111111");
    REQUIRE(licenseFromCache);
    REQUIRE(licenseFromCache->isVerified());

    REQUIRE(licenseFromCache->reservations().size() == 1);
    Reservation *reservation = licenseFromCache->reservation("0e03e00e-b65a-4210-97cb-b6f76ab900e9");
    REQUIRE(reservation);

    REQUIRE(reservation->attachedConsumers().size() == 1);
    ConsumerAppInfo consumer = reservation->attachedConsumers().front();
    REQUIRE(consumer.id == "test_app1");
    REQUIRE(consumer.version == "1.2.3");
    REQUIRE(consumer.installationSchema == "commercial");
    REQUIRE(consumer.processId == "1");
    REQUIRE(consumer.supportedFeatures == std::string());

    auto statistics = reservation->statistics();
    REQUIRE(statistics.size() == 2);

    REQUIRE(statistics[0].isComplete());
    REQUIRE(statistics[0].consumerInfo().id == "test_app1");
    REQUIRE(statistics[0].consumerInfo().version == "1.2.3");

    REQUIRE(statistics[1].isComplete());
    REQUIRE(statistics[1].consumerInfo().id == "test_app1");
    REQUIRE(statistics[1].consumerInfo().version == "4.5.6");
}

TEST_CASE_METHOD(InitCleanupFixture, "Initializing multiple caches in same context fails", "[licensecache]")
{
    static const std::string testPath = getTempCachePath();

    LicenseCache cache1(LicenseCache::Context::User);
    REQUIRE(cache1.setPath(testPath));
    REQUIRE(cache1.init());
    REQUIRE(cache1.isValid());

    LicenseCache cache2(LicenseCache::Context::User);
    REQUIRE(cache2.setPath(testPath));
    REQUIRE(!cache2.init());
    REQUIRE(!cache2.isValid());
}

TEST_CASE_METHOD(InitCleanupFixture, "Remove licenses from cache", "[licensecache]")
{
    static const std::string testPath = getTempCachePath();
    static std::vector<License *> licenses = getTestLicenses();

    SECTION("Create an \"existing\" cache") {
        LicenseCache cache(LicenseCache::Context::User);
        REQUIRE(cache.setPath(testPath));
        REQUIRE(cache.init());
        REQUIRE(cache.isValid());
        REQUIRE(cache.licenseCount() == 0);

        // 1. Insert test licenses
        REQUIRE(cache.insert(licenses[0]));
        REQUIRE(cache.insert(licenses[1]));
        REQUIRE(cache.insert(licenses[2]));
        REQUIRE(cache.insert(licenses[3]));
    }

    SECTION("Remove single license from cache") {
        LicenseCache cache(LicenseCache::Context::User);
        REQUIRE(cache.setPath(testPath));
        REQUIRE(cache.init());

        // 1. Remove valid license
        REQUIRE(cache.remove("foobar", "4444444444"));

        // 2. Verify no such license exists in cache after removal
        License *licenseFromCache = cache.license("foobar", "4444444444");
        REQUIRE(!licenseFromCache);

        // 3. Remove non-existent license fails
        REQUIRE(!cache.remove("foobar", "5555555555"));
    }

    SECTION("Clear an existing cache") {
        LicenseCache cache(LicenseCache::Context::User);
        REQUIRE(cache.setPath(testPath));
        REQUIRE(cache.init());
        REQUIRE(cache.isValid());
        // 1. Verify we have expected existing license count
        REQUIRE(cache.licenseCount() == 3);

        REQUIRE(cache.clear());
        REQUIRE(!cache.isValid());
        // 2. Verify license count is zero after clearing
        REQUIRE(cache.licenseCount() == 0);
    }
}

TEST_CASE_METHOD(InitCleanupFixture, "Import local license file into cache", "[licensecache]")
{
    static const std::string testPath = getTempCachePath();
    LicenseCache cache(LicenseCache::Context::User);

    REQUIRE(cache.setPath(testPath));
    REQUIRE(cache.init());
    REQUIRE(cache.isValid());
    REQUIRE(cache.licenseCount() == 0);

    LicenseImport importer(cache, "johndoe");
    importer.sync({TEST_LICENSES_HOME_DIR});

    REQUIRE(cache.licenseCount() == 5);

    License *licenseFromCache = cache.license("johndoe", "1111111111");
    REQUIRE(licenseFromCache);

    // Assert expected files exist
    const std::string basepath = cache.path() + DIR_SEPARATOR + licenseFromCache->localUser()
        + DIR_SEPARATOR + licenseFromCache->id() + DIR_SEPARATOR;

    REQUIRE(utils::fileExists(basepath + "license_" + licenseFromCache->id() + ".json"));
    for (auto *reservation : licenseFromCache->reservations()) {
        const std::string reservationPath = basepath + DIR_SEPARATOR + reservation->id() + DIR_SEPARATOR;
        REQUIRE(utils::fileExists(reservationPath + "reservation_" + reservation->id() + ".json"));
        REQUIRE(utils::fileExists(reservationPath + reservation->id() + ".sha256"));
    }

    // Verify sync to new directory removes obsolete files
    importer.sync({std::string(TEST_LICENSES_HOME_DIR) + "/invalid"});
    REQUIRE(cache.licenseCount() == 0);

    std::vector<License *> licenses = getTestLicenses();
    for (auto *license : licenses)
        cache.insert(license);

    // Verify sync doesnt remove (simulated) licenses from server
    importer.sync({std::string(TEST_LICENSES_HOME_DIR) + "/invalid"});
    REQUIRE(cache.licenseCount() == 4);
}

TEST_CASE_METHOD(InitCleanupFixture, "Import and serve expired perpetual license from cache", "[licensecache]")
{
    static const std::string testPath = getTempCachePath();
    LicenseCache cache(LicenseCache::Context::User);

    REQUIRE(cache.setPath(testPath));
    REQUIRE(cache.init());
    REQUIRE(cache.isValid());
    REQUIRE(cache.licenseCount() == 0);

    LicenseImport importer(cache, "johndoe");
    importer.sync({TEST_LICENSES_HOME_DIR});

    License *licenseFromCache = cache.license("johndoe", "5555555555");
    REQUIRE(licenseFromCache);
    REQUIRE(licenseFromCache->isExpired());
    REQUIRE(licenseFromCache->isPerpetual());
    REQUIRE(licenseFromCache->isUsable());

    const std::string consumerJsonData = "{\"consumer_id\":\"expired_consumer\","
        "\"consumer_version\":\"1.0.0\",\"consumer_process_id\":\"12345\"}";
    nlohmann::json consumerObject;
    try {
        consumerObject = nlohmann::json::parse(consumerJsonData);
    } catch (const nlohmann::json::exception &e) {
        INFO(e.what());
        FAIL();
    }

    RequestInfo info;
    info.consumerInfo = ConsumerAppInfo(consumerObject);
    info.userId = "johndoe";
    ClientHandler client(info, sc_setup, cache);

    std::string response;
    Reservation *reservation = client.findCachedReservation(response, Licenser::DaemonState::Offline);
    if (!reservation) {
        INFO(response);
        FAIL();
    }

    // Verify we got a valid local-only reservation, even if the license was expired
    REQUIRE(reservation == *licenseFromCache->reservations().begin());
    REQUIRE(!reservation->isLeaseExpired());
}

TEST_CASE_METHOD(InitCleanupFixture, "Import expired perpetual license with consumer build timestamp", "[licensecache]")
{
    static const std::string testPath = getTempCachePath();
    LicenseCache cache(LicenseCache::Context::User);

    REQUIRE(cache.setPath(testPath));
    REQUIRE(cache.init());
    REQUIRE(cache.isValid());

    LicenseImport importer(cache, "johndoe");
    importer.sync({TEST_LICENSES_HOME_DIR});

    // 1. Verify no valid reservation where consumer_build_timestamp is greater than valid_to
    ConsumerAppInfo consumer("expired_consumer", "1.0.0", "12345");
    consumer.buildTimestamp = "2030-01-01";

    RequestInfo info;
    info.consumerInfo = consumer;
    info.userId = "johndoe";
    ClientHandler client1(info, sc_setup, cache);

    std::string response;
    Reservation *reservation = client1.findCachedReservation(response, Licenser::DaemonState::Offline);
    REQUIRE(!reservation);

    // 2. Verify valid reservation where consumer_build_timestamp is less than valid_to
    consumer.buildTimestamp = "2020-01-01";
    info.consumerInfo = consumer;
    ClientHandler client2(info, sc_setup, cache);

    reservation = client2.findCachedReservation(response, Licenser::DaemonState::Offline);
    if (!reservation) {
        INFO(response);
        FAIL();
    }
    REQUIRE(reservation->license()->id() == "5555555555");

    // 3. Verify invalid consumer_built_timestamp does not result in valid license
    consumer.buildTimestamp = "asdqwe";
    info.consumerInfo = consumer;
    ClientHandler client3(info, sc_setup, cache);

    reservation = client3.findCachedReservation(response, Licenser::DaemonState::Offline);
    REQUIRE(!reservation);

    // 4. Verify consumer_build_timestamp with time part is accepted
    consumer.buildTimestamp = "2020-01-01 12:30:45";
    info.consumerInfo = consumer;
    ClientHandler client4(info, sc_setup, cache);

    reservation = client4.findCachedReservation(response, Licenser::DaemonState::Offline);
    if (!reservation) {
        INFO(response);
        FAIL();
    }
    REQUIRE(reservation->license()->id() == "5555555555");

    // 5. Verify matching build timestamp and lease valid_to is accepted
    consumer.buildTimestamp = "2024-09-28";
    info.consumerInfo = consumer;
    ClientHandler client5(info, sc_setup, cache);

    reservation = client5.findCachedReservation(response, Licenser::DaemonState::Offline);
    if (!reservation) {
        INFO(response);
        FAIL();
    }
    REQUIRE(reservation->license()->id() == "5555555555");
}

TEST_CASE_METHOD(InitCleanupFixture, "Import and disallow usage of expired subscription license from cache", "[licensecache]")
{
    static const std::string testPath = getTempCachePath();
    LicenseCache cache(LicenseCache::Context::User);

    REQUIRE(cache.setPath(testPath));
    REQUIRE(cache.init());
    REQUIRE(cache.isValid());
    REQUIRE(cache.licenseCount() == 0);

    LicenseImport importer(cache, "johndoe");
    importer.sync({TEST_LICENSES_HOME_DIR});

    License *licenseFromCache = cache.license("johndoe", "6666666666");
    REQUIRE(!licenseFromCache); // expired subscription license not imported

    const std::string consumerJsonData = "{\"consumer_id\":\"expired_subscription_consumer\","
        "\"consumer_version\":\"1.0.0\",\"consumer_process_id\":\"12345\"}";
    nlohmann::json consumerObject;
    try {
        consumerObject = nlohmann::json::parse(consumerJsonData);
    } catch (const nlohmann::json::exception &e) {
        INFO(e.what());
        FAIL();
    }

    RequestInfo info;
    info.consumerInfo = ConsumerAppInfo(consumerObject);
    info.userId = "johndoe";
    ClientHandler client(info, sc_setup, cache);

    std::string response;
    REQUIRE(!client.findCachedReservation(response, Licenser::DaemonState::Offline));
}
