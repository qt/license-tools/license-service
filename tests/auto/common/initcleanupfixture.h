/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
*/

#include <logger.h>

using namespace QLicenseCore;

class InitCleanupFixture
{
public:
    InitCleanupFixture()
    {
        // Initialization code (runs before each test case)
        Logger::getInstance()->setPrintToStdout(true);
        Logger::getInstance()->setLogLevel(LogLevel::LOGLEVEL_DEBUG);
    }

    ~InitCleanupFixture()
    {
        // Cleanup code (runs after each test case)
    }
};
