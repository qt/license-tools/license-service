/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
*/

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <initcleanupfixture.h>

#include <httpclient.h>

using namespace QLicenseService;

class TestHttpResult : public HttpResult
{
public:
    TestHttpResult()
        : HttpResult(nullptr)
    {}

    std::map<std::string, std::string> testParseHeaders(const std::string &headers) const
    {
        return parseHeaders(headers);
    }
};

TEST_CASE_METHOD(InitCleanupFixture, "Parse response headers", "[httpclient]")
{
    static const std::string sc_headerString =
        "Access-Control-Allow-Origin: *\n"
        "Connection: Keep-Alive\n"
        "Content-Encoding: gzip\n"
        "Content-Type: text/html; charset=utf-8\n"
        "Date: Mon, 18 Jul 2016 16:06:00 GMT\n"
        "Etag: \"c561c68d0ba92bbeb8b0f612a9199f722e3a621a\"\n"
        "Keep-Alive: timeout=5, max=997\n"
        "Last-Modified: Mon, 18 Jul 2016 02:36:04 GMT\n"
        "Server: Apache\n"
        "Set-Cookie: mykey=myvalue; expires=Mon, 17-Jul-2017 16:06:00 GMT; Max-Age=31449600; Path=/; secure\n"
        "Transfer-Encoding: chunked\n"
        "Vary: Cookie, Accept-Encoding\n"
        "X-Backend-Server: developer2.webapp.scl3.mozilla.com\n"
        "X-Cache-Info: not cacheable; meta data too large\n"
        "X-kuma-revision: 1085259\n"
        "x-frame-options: DENY\";\n";

    TestHttpResult result;
    auto headerMap = result.testParseHeaders(sc_headerString);

    REQUIRE(headerMap.size() == 16);

    auto it = headerMap.find("Date");
    REQUIRE(it != headerMap.end());
    REQUIRE(it->second == "Mon, 18 Jul 2016 16:06:00 GMT");

    it = headerMap.find("X-Backend-Server");
    REQUIRE(it != headerMap.end());
    REQUIRE(it->second == "developer2.webapp.scl3.mozilla.com");

    it = headerMap.find("Content-Type");
    REQUIRE(it != headerMap.end());
    REQUIRE(it->second == "text/html; charset=utf-8");
}
