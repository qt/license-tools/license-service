/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
*/

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <initcleanupfixture.h>

#include <string>
#include <iostream>
#include <map>

#include <inifileparser.h>

using namespace QLicenseCore;

const char TEST_INI_FILE[] = "test.ini";


TEST_CASE_METHOD(InitCleanupFixture, "Read and write to .ini file", "[inifileparser]")
{
    std::remove(TEST_INI_FILE);
    IniFileParser iniHandler(TEST_INI_FILE);

    std::string temp;
    REQUIRE(!iniHandler.readKeyValue("Section1", "Key1", temp));

    REQUIRE(iniHandler.writeKeyValue("Section1", {{"Key1", "Value1"}}));
    REQUIRE(iniHandler.readKeyValue("Section1", "Key1", temp));
    REQUIRE(temp == "Value1");

    REQUIRE(iniHandler.writeKeyValue("Section2", {{"Key1", "Value1"}}));
    REQUIRE(iniHandler.readKeyValue("Section2", "Key1", temp));
    REQUIRE(temp == "Value1");

    REQUIRE(iniHandler.writeKeyValue("Section1", {{"Key2", "Value2"}}));
    REQUIRE(iniHandler.readKeyValue("Section1", "Key2", temp));
    REQUIRE(temp == "Value2");

    REQUIRE(iniHandler.writeKeyValue("Section1", {{"Key1", "Value3"}}));
    REQUIRE(iniHandler.readKeyValue("Section1", "Key1", temp));
    REQUIRE(temp == "Value3");

    REQUIRE(iniHandler.writeKeyValue("Section3", {{"Key1", "Value1"}, {"Key11", "Value11"}}));
    REQUIRE(iniHandler.readKeyValue("Section3", "Key1", temp));
    REQUIRE(temp == "Value1");
    REQUIRE(iniHandler.readKeyValue("Section3", "Key11", temp));
    REQUIRE(temp == "Value11");

    REQUIRE(iniHandler.removeSection("Section1"));
    REQUIRE(!iniHandler.readKeyValue("Section1", "Key1", temp));
    REQUIRE(iniHandler.readKeyValue("Section2", "Key1", temp));
    REQUIRE(temp == "Value1");
    REQUIRE(iniHandler.readKeyValue("Section3", "Key1", temp));
    REQUIRE(temp == "Value1");

    std::map<std::string, std::string> result;
    REQUIRE(iniHandler.readSection("Section3", result));
    REQUIRE(result["Key1"] == "Value1");
    REQUIRE(result["Key11"] == "Value11");

    std::map<std::string, std::map<std::string, std::string>> results;
    REQUIRE(iniHandler.readSections(results));
    REQUIRE(results.size() == 2);
    REQUIRE(results["Section2"]["Key1"] == "Value1");
    REQUIRE(results["Section3"]["Key1"] == "Value1");
    REQUIRE(results["Section3"]["Key11"] == "Value11");
}
