/* Copyright (C) 2024 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
*/

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <initcleanupfixture.h>

#include "testdata.h"

#include "commonsetup.h"
#include "clienthandler.h"
#include "license.h"
#include "licensecache.h"
#include "pipe.h"
#include "reservation.h"

using namespace QLicenseService;
using namespace QLicenseCore;

LicdSetup sc_setup("");
LicenseCache sc_cache(LicenseCache::Context::User);

TEST_CASE_METHOD(InitCleanupFixture, "Test release user based floating reservation on disconnect", "[reservation]")
{
    std::string jsonData;
    REQUIRE(utils::readFile(jsonData, TEST_JSON1));
    std::unique_ptr<License> floatingLicense(new License("johndoe", jsonData));

    Reservation *reservation = new Reservation(floatingLicense.get(), "12345", utils::utcTime(),
        floatingLicense->validToDate(), Reservation::Flags::Default);

    floatingLicense->addReservation(reservation);

    ClientHandler client1(RequestInfo(), sc_setup, sc_cache);
    ClientHandler client2(RequestInfo(), sc_setup, sc_cache);

    reservation->addClient(&client1);
    reservation->addClient(&client2);

    // User based license with two connected clients, do not release
    REQUIRE(!reservation->requiresReleaseOnDisconnect());

    reservation->removeClient(&client2);
    // User based license with one connected client, release
    REQUIRE(reservation->requiresReleaseOnDisconnect());
}

TEST_CASE_METHOD(InitCleanupFixture, "Test release process based floating reservation on disconnect", "[reservation]")
{
    std::string jsonData;
    REQUIRE(utils::readFile(jsonData, TEST_JSON2));
    std::unique_ptr<License> floatingLicense(new License("johndoe", jsonData));

    Reservation *reservation = new Reservation(floatingLicense.get(), "12345", utils::utcTime(),
        floatingLicense->validToDate(), Reservation::Flags::Default);

    floatingLicense->addReservation(reservation);

    // Process based reservation is released once last client disconnects
    REQUIRE(reservation->requiresReleaseOnDisconnect());

    ClientHandler client1(RequestInfo(), sc_setup, sc_cache);
    ClientHandler client2(RequestInfo(), sc_setup, sc_cache);

    reservation->addClient(&client1);
    reservation->addClient(&client2);
    REQUIRE(!reservation->requiresReleaseOnDisconnect());

    reservation->removeClient(&client2);
    REQUIRE(reservation->requiresReleaseOnDisconnect());
}

TEST_CASE_METHOD(InitCleanupFixture, "Test do not release user based site reservation on disconnect", "[reservation]")
{
    std::string jsonData;
    REQUIRE(utils::readFile(jsonData, TEST_JSON3));
    std::unique_ptr<License> siteLicense(new License("johndoe", jsonData));

    Reservation *reservation = new Reservation(siteLicense.get(), "12345", utils::utcTime(),
        siteLicense->validToDate(), Reservation::Flags::Default);

    siteLicense->addReservation(reservation);

    // Site license is never released on disconnect
    REQUIRE(!reservation->requiresReleaseOnDisconnect());

    ClientHandler client1(RequestInfo(), sc_setup, sc_cache);
    ClientHandler client2(RequestInfo(), sc_setup, sc_cache);

    reservation->addClient(&client1);
    REQUIRE(!reservation->requiresReleaseOnDisconnect());

    reservation->addClient(&client2);
    REQUIRE(!reservation->requiresReleaseOnDisconnect());
}

TEST_CASE_METHOD(InitCleanupFixture, "Test do not release named license reservation on disconnect", "[reservation]")
{
    std::string jsonData;
    REQUIRE(utils::readFile(jsonData, TEST_JSON4));
    std::unique_ptr<License> namedLicense(new License("johndoe", jsonData));

    Reservation *reservation = new Reservation(namedLicense.get(), "12345", utils::utcTime(),
        namedLicense->validToDate(), Reservation::Flags::Default);

    namedLicense->addReservation(reservation);

    // Named license is never released on disconnect
    REQUIRE(!reservation->requiresReleaseOnDisconnect());

    ClientHandler client1(RequestInfo(), sc_setup, sc_cache);
    ClientHandler client2(RequestInfo(), sc_setup, sc_cache);

    reservation->addClient(&client1);
    REQUIRE(!reservation->requiresReleaseOnDisconnect());

    reservation->addClient(&client2);
    REQUIRE(!reservation->requiresReleaseOnDisconnect());
}

TEST_CASE_METHOD(InitCleanupFixture, "Test attach consumers", "[reservation]")
{
    std::string jsonData;
    REQUIRE(utils::readFile(jsonData, TEST_JSON3));
    std::unique_ptr<License> siteLicense(new License("johndoe", jsonData));

    Reservation *reservation = new Reservation(siteLicense.get(), "12345", utils::utcTime(),
        siteLicense->validToDate(), Reservation::Flags::Default);

    siteLicense->addReservation(reservation);

    const std::string consumerJsonData1 = "{\"consumer_id\":\"foobar\",\"consumer_version\":\"1.0\","
        "\"consumer_supported_features\":\"edition,plugins\",\"consumer_process_id\":\"12345\"}";
    const std::string consumerJsonData2 = "{\"consumer_id\":\"foobar\",\"consumer_version\":\"2.0\","
        "\"consumer_installation_schema\":\"opensource\",\"consumer_process_id\":\"67890\"}";

    nlohmann::json consumerObject1;
    nlohmann::json consumerObject2;
    try {
        consumerObject1 = nlohmann::json::parse(consumerJsonData1);
        consumerObject2 = nlohmann::json::parse(consumerJsonData2);
    } catch (const nlohmann::json::exception &e) {
        INFO(e.what());
        FAIL();
    }

    RequestInfo info1;
    info1.consumerInfo = ConsumerAppInfo(consumerObject1);

    RequestInfo info2;
    info2.consumerInfo = ConsumerAppInfo(consumerObject2);

    REQUIRE(info1.consumerInfo != info2.consumerInfo);
    REQUIRE(info1.consumerInfo.toJson() == consumerObject1);
    REQUIRE(info2.consumerInfo.toJson() == consumerObject2);

    ClientHandler client1(info1, sc_setup, sc_cache);
    ClientHandler client2(info2, sc_setup, sc_cache);

    reservation->addClient(&client1);
    REQUIRE(reservation->attachedConsumers().size() == 0);

    reservation->attachConsumer(client1.consumerInfo());
    REQUIRE(reservation->isAttachedToConsumer(client1.consumerInfo()));
    REQUIRE(!reservation->isAttachedToConsumer(client2.consumerInfo()));

    reservation->addClient(&client2);
    reservation->attachConsumer(client2.consumerInfo());
    REQUIRE(reservation->isAttachedToConsumer(client1.consumerInfo()));
    REQUIRE(reservation->isAttachedToConsumer(client2.consumerInfo()));
}

TEST_CASE_METHOD(InitCleanupFixture, "Test usage statistics leeway", "[reservation]")
{
    std::string jsonData;
    REQUIRE(utils::readFile(jsonData, TEST_JSON3));
    std::unique_ptr<License> siteLicense(new License("johndoe", jsonData));

    Reservation *reservation = new Reservation(siteLicense.get(), "12345", utils::utcTime(),
        siteLicense->validToDate(), Reservation::Flags::Default);

    siteLicense->addReservation(reservation);

    const std::string consumerJsonData = "{\"consumer_id\":\"foobar\",\"consumer_version\":\"1.0\","
        "\"consumer_supported_features\":\"edition,plugins\",\"consumer_process_id\":\"12345\"}";

    nlohmann::json consumerObject;
    try {
        consumerObject = nlohmann::json::parse(consumerJsonData);
    } catch (const nlohmann::json::exception &e) {
        INFO(e.what());
        FAIL();
    }

    RequestInfo info;
    info.consumerInfo = ConsumerAppInfo(consumerObject);

    REQUIRE(info.consumerInfo.toJson() == consumerObject);

    ClientHandler client(info, sc_setup, sc_cache);

    // 1. Test no leeway
    reservation->addClient(&client);
    auto statistics = reservation->takeStatistics();
    REQUIRE(!statistics.front().isComplete());
    REQUIRE(!reservation->statistics().empty());

    reservation->removeClient(&client);
    statistics = reservation->takeStatistics();
    REQUIRE(statistics.front().isComplete());
    REQUIRE(reservation->statistics().empty());

    // 2. Test usage of existing statistic is allowed within leeway time
    UsageStatistics::setAllowedLeeway(std::chrono::milliseconds(3000));
    reservation->addClient(&client);
    statistics = reservation->takeStatistics();
    REQUIRE(!statistics.front().isComplete());
    REQUIRE(!reservation->statistics().empty());

    reservation->removeClient(&client);
    statistics = reservation->statistics();
    REQUIRE(statistics.front().isComplete());

    utils::sleepSecs(1);

    reservation->addClient(&client);
    statistics = reservation->takeStatistics();
    REQUIRE(!statistics.front().isComplete());
    REQUIRE(reservation->statistics().size() == 1);
    REQUIRE(statistics.size() == 1);

    // 3. Test usage of existing statistic is not allowed after leeway time
    reservation->addClient(&client);

    reservation->removeClient(&client);
    statistics = reservation->statistics();
    REQUIRE(statistics.front().isComplete());

    utils::sleepSecs(4);

    reservation->addClient(&client);
    statistics = reservation->takeStatistics();
    REQUIRE(statistics.front().isComplete());
    REQUIRE(!statistics.back().isComplete());
    REQUIRE(reservation->statistics().size() == 1);
    REQUIRE(statistics.size() == 2);
}

TEST_CASE_METHOD(InitCleanupFixture, "Test passive renewal policy", "[licensecache]")
{
    REQUIRE(Pipe::globalObject()->queuedMessagesCount() == 0);

    std::string jsonData;
    REQUIRE(utils::readFile(jsonData, TEST_JSON5));

    std::unique_ptr<License> license(new License("johndoe"));
    // Expected rejection due to signature check failure:
    REQUIRE(license->parseFromJson(jsonData) == Status::LICENSE_REJECTED);
    REQUIRE(license->renewalPolicy() == License::RenewalPolicy::Passive);

    // 1. Verify no renewal occurs in case renewal secs until expiry is 0
    time_t now = utils::utcTime();
    Reservation *reservation = new Reservation(license.get(), "12345", now,
        now + license->leaseTimeSeconds(), Reservation::Flags::Renewable, 0);

    reservation->resetRenewAndExpiryTimers();
    utils::sleepSecs(10);

    REQUIRE(Pipe::globalObject()->queuedMessagesCount() == 0);
    delete reservation;

    // 2. Verify no renewal occurs in case renewal secs until expiry results in less than 5s renewal
    now = utils::utcTime();
    reservation = new Reservation(license.get(), "12345", now,
        now + license->leaseTimeSeconds(), Reservation::Flags::Renewable, 7);

    reservation->resetRenewAndExpiryTimers();
    utils::sleepSecs(10);

    REQUIRE(Pipe::globalObject()->queuedMessagesCount() == 0);
    delete reservation;

    // 3. Verify renewal occurs in case renewal secs until expiry is close to expiry
    now = utils::utcTime();
    reservation = new Reservation(license.get(), "12345", now,
        now + license->leaseTimeSeconds(), Reservation::Flags::Renewable, 2);

    reservation->resetRenewAndExpiryTimers();
    utils::sleepSecs(10);

    REQUIRE(Pipe::globalObject()->queuedMessagesCount() == 1);
    REQUIRE(Pipe::globalObject()->read() == PIPE_MSG_LICENSE_RENEWAL);
    delete reservation;
}

TEST_CASE_METHOD(InitCleanupFixture, "Test active renewal policy", "[licensecache]")
{
    REQUIRE(Pipe::globalObject()->queuedMessagesCount() == 0);

    std::string jsonData;
    REQUIRE(utils::readFile(jsonData, TEST_JSON6));

    std::unique_ptr<License> license(new License("johndoe"));
    // Expected rejection due to signature check failure:
    REQUIRE(license->parseFromJson(jsonData) == Status::LICENSE_REJECTED);
    REQUIRE(license->renewalPolicy() == License::RenewalPolicy::Active);

    // 1. Verify renewal occurs halfway of lease time in active mode
    time_t now = utils::utcTime();
    Reservation *reservation = new Reservation(license.get(), "12345", now,
        now + license->leaseTimeSeconds(), Reservation::Flags::Renewable);

    reservation->resetRenewAndExpiryTimers();
    // 6s (halfway of lease time) + a second to give some leeway
    utils::sleepSecs(7);

    REQUIRE(Pipe::globalObject()->queuedMessagesCount() == 1);
    REQUIRE(Pipe::globalObject()->read() == PIPE_MSG_LICENSE_RENEWAL);
    delete reservation;
}
