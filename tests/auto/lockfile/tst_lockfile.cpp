/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
*/

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <initcleanupfixture.h>

#include <lockfile.h>
#include <lockfilelocker.h>
#include <utils.h>

#include <thread>

using namespace QLicenseCore;

static const std::string filename = utils::getUserHomeDir() + "/.tst_qtlicd.lock";

TEST_CASE_METHOD(InitCleanupFixture, "Create lock file", "[lockfile]")
{
    QLicenseCore::LockFile lockFile(filename);

    REQUIRE(lockFile.tryLock());
    REQUIRE(lockFile.tryLock()); // no stacking lock
    REQUIRE(lockFile.isLocked());
    REQUIRE(lockFile.tryUnlock());
    REQUIRE(lockFile.tryUnlock()); // already unlocked
    REQUIRE(!lockFile.isLocked());
}

TEST_CASE_METHOD(InitCleanupFixture, "Try lock already locked file", "[lockfile]")
{
    QLicenseCore::LockFile lockFile1(filename);

    REQUIRE(lockFile1.tryLock());
    REQUIRE(lockFile1.isLocked());

    QLicenseCore::LockFile lockFile2(filename);

    REQUIRE(!lockFile2.tryLock());
    REQUIRE(!lockFile2.isLocked());

    REQUIRE(lockFile1.tryUnlock());
}

TEST_CASE_METHOD(InitCleanupFixture, "LockFileLocker - competing threads succeed eventually", "[lockfile]")
{
    std::vector<std::unique_ptr<std::thread>> threads;
    for (int j = 0; j < 10; ++j) {
        threads.push_back(std::unique_ptr<std::thread>(new std::thread([=]() -> void {
            QLicenseCore::LockFile lock(filename);
            REQUIRE(!lock.isLocked());
            {
                LockFileLocker _(&lock);
                REQUIRE(lock.isLocked());
                // Keep the lock for some time
                std::this_thread::sleep_for(std::chrono::milliseconds(500));
            }
            REQUIRE(!lock.isLocked());
        })));
    }

    for (auto &thread : threads)
        thread->join();
}


TEST_CASE_METHOD(InitCleanupFixture, "Lock file is released from crashed process", "[lockfile]")
{
#if _WIN32
    REQUIRE(utils::startDetached("crash_app.exe", std::vector<std::string>()));
#else
    REQUIRE(utils::startDetached("crash_app", std::vector<std::string>()));
#endif
    // Give the process some time
    std::this_thread::sleep_for(std::chrono::milliseconds(5000));

    QLicenseCore::LockFile lock(filename);
    REQUIRE(!lock.isLocked());
    REQUIRE(lock.tryLock());
    REQUIRE(lock.isLocked());
}
