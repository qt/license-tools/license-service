# Copyright (C) 2023 The Qt Company Ltd.
#
# SPDX-License-Identifier: BSD-3-Clause
#

add_executable(tst_licenseservice tst_licenseservice.cpp)
target_link_libraries(tst_licenseservice PRIVATE Catch2 qlicenseservice)

add_test(NAME tst_licenseservice COMMAND tst_licenseservice)
