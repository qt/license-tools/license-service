/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
*/

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <initcleanupfixture.h>

#include "commonsetup.h"

#include "licenseservice.h"
#include "licdsetup.h"
#include "utils.h"


using namespace QLicenseService;

TEST_CASE_METHOD(InitCleanupFixture, "Start/stop without clients", "[licenseservice]")
{
    LicenseService service(DaemonRunMode::TestMode, 0, DAEMON_WORK_DIR_TEST);
    // 1. Verify starting of license "service" succeeds
    REQUIRE((service.start() && service.waitForStarted()));
    // 2. Verify stop service
    REQUIRE((service.cancel() && service.waitForFinished(30000)));
    REQUIRE(service.status() == LicenseService::Status::Canceled);
}

TEST_CASE_METHOD(InitCleanupFixture, "Start two daemon instances with same port", "[licenseservice]")
{
    LicenseService service(DaemonRunMode::TestMode, 0, DAEMON_WORK_DIR_TEST);
    // 1. Verify starting of license "service" succeeds
    REQUIRE((service.start() && service.waitForStarted()));

    LicenseService service2(DaemonRunMode::TestMode, 0, DAEMON_WORK_DIR_TEST);
    // 2. Verify starting another instance fails gracefully
    REQUIRE((service2.start() && !service2.waitForStarted()));
    REQUIRE(service2.status() == LicenseService::Status::Error);

    // 3. Verify stop original service
    REQUIRE((service.cancel() && service.waitForFinished(30000)));
    REQUIRE(service.status() == LicenseService::Status::Canceled);
}
