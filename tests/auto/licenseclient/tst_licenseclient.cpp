/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only
*/

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <initcleanupfixture.h>

#include "licenseclient.h"
#include "licenseclient_p.h"
#include "licenseservice.h"
#include "commonsetup.h"

using namespace QLicenseClient;
using namespace QLicenseService;

uint16_t getServicePort()
{
    const std::string portFilePath = LicdSetup::getQtAppDataLocation()
        + USER_SETTINGS_FOLDER_NAME + DIR_SEPARATOR + DAEMON_TCP_PORT_FILE;

    std::string portStr;
#if _WIN32
    if (!utils::readFileWin32(portStr, portFilePath) || portStr.empty()) {
#else
    if (!utils::readFile(portStr, portFilePath) || portStr.empty()) {
#endif
        logError("Could not read service port file \"%s\"", portFilePath.c_str());
        return 0;
    }

    utils::trimStr(portStr);
    return utils::strToInt(portStr);
}


TEST_CASE_METHOD(InitCleanupFixture, "Assert all status codes", "[licenseclient]")
{
    // Test that nothing is accidentally removed or altered, only
    // new values can be added to QLicenseClient::StatusCode enum.

    // Note: Update the test accordingly to assert new values.

    LicenseClientPrivate client(nullptr);
    REQUIRE(client.statusCode(-1) == StatusCode::UnknownError);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::UnknownError) == "UnknownError");

    REQUIRE(client.statusCode(0) == StatusCode::Success);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::Success) == "Success");

    REQUIRE(client.statusCode(100) == StatusCode::LicenseRejected);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::LicenseRejected) == "LicenseRejected");
    REQUIRE(client.statusCode(101) == StatusCode::LicensePoolFull);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::LicensePoolFull) == "LicensePoolFull");
    REQUIRE(client.statusCode(102) == StatusCode::UnknownLicenseModel);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::UnknownLicenseModel) == "UnknownLicenseModel");
    REQUIRE(client.statusCode(103) == StatusCode::UnknownReservationType);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::UnknownReservationType) == "UnknownReservationType");
    REQUIRE(client.statusCode(104) == StatusCode::ReservationNotFound);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::ReservationNotFound) == "ReservationNotFound");
    REQUIRE(client.statusCode(105) == StatusCode::UnknownLicenseType);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::UnknownLicenseType) == "UnknownLicenseType");
    REQUIRE(client.statusCode(106) == StatusCode::BetterLicenseAvailable);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::BetterLicenseAvailable) == "BetterLicenseAvailable");
    REQUIRE(client.statusCode(107) == StatusCode::TermsAndConditionsNotAccepted);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::TermsAndConditionsNotAccepted) == "TermsAndConditionsNotAccepted");
    REQUIRE(client.statusCode(108) == StatusCode::ServerHasNoLicenses);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::ServerHasNoLicenses) == "ServerHasNoLicenses");

    REQUIRE(client.statusCode(200) == StatusCode::BadRequest);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::BadRequest) == "BadRequest");
    REQUIRE(client.statusCode(201) == StatusCode::InvalidResponse);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::InvalidResponse) == "InvalidResponse");
    REQUIRE(client.statusCode(202) == StatusCode::BadConnection);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::BadConnection) == "BadConnection");
    REQUIRE(client.statusCode(203) == StatusCode::Unauthorized);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::Unauthorized) == "Unauthorized");
    REQUIRE(client.statusCode(204) == StatusCode::ServerError);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::ServerError) == "ServerError");
    REQUIRE(client.statusCode(205) == StatusCode::ServerBusy);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::ServerBusy) == "ServerBusy");
    REQUIRE(client.statusCode(206) == StatusCode::TimeMismatch);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::TimeMismatch) == "TimeMismatch");
    REQUIRE(client.statusCode(207) == StatusCode::TcpSocketError);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::TcpSocketError) == "TcpSocketError");
    REQUIRE(client.statusCode(208) == StatusCode::SslVerifyError);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::SslVerifyError) == "SslVerifyError");
    REQUIRE(client.statusCode(209) == StatusCode::SslLocalCertificateError);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::SslLocalCertificateError) == "SslLocalCertificateError");
    REQUIRE(client.statusCode(210) == StatusCode::BadClientRequest);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::BadClientRequest) == "BadClientRequest");
    REQUIRE(client.statusCode(211) == StatusCode::RequestTimeout);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::RequestTimeout) == "RequestTimeout");

    REQUIRE(client.statusCode(300) == StatusCode::ServiceVersionTooLow);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::ServiceVersionTooLow) == "ServiceVersionTooLow");
    REQUIRE(client.statusCode(301) == StatusCode::ServiceVersionTooNew);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::ServiceVersionTooNew) == "ServiceVersionTooNew");
    REQUIRE(client.statusCode(302) == StatusCode::MissingServiceVersion);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::MissingServiceVersion) == "MissingServiceVersion");
    REQUIRE(client.statusCode(303) == StatusCode::IncompatibleLicenseFormatVersion);
    REQUIRE(LicenseReservationInfo().statusToString(StatusCode::IncompatibleLicenseFormatVersion) == "IncompatibleLicenseFormatVersion");
}

TEST_CASE_METHOD(InitCleanupFixture, "Unknown code ranges", "[licenseclient]")
{
    LicenseClientPrivate client(nullptr);
    REQUIRE(client.statusCode(-999) == StatusCode::UnknownError);
    REQUIRE(client.statusCode(999) == StatusCode::UnknownError);
    REQUIRE(client.statusCode(199) == StatusCode::UnknownError);
    REQUIRE(client.statusCode(99) == StatusCode::Success);
}

TEST_CASE_METHOD(InitCleanupFixture, "(re)initialize and release", "[licenseclient]")
{
    LicenseService service(DaemonRunMode::TestMode, 0, DAEMON_WORK_DIR_TEST);
    // 1. Verify starting of license "service" succeeds
    REQUIRE((service.start() && service.waitForStarted()));

    {
        LicenseClient client;
        std::string error;
        // 2. Verify client connection
        REQUIRE(client.init(error));
        client.release();

        // 3. Verify client can be reinitialized after revoking
        REQUIRE(client.init(error));

        // 4. Verify client cannot be reinitialized while already initialized
        REQUIRE(!client.init(error));
        client.release();

        // 5. Verify client can be implicitly reinitialized after revoking
        REQUIRE(client.status(LicenseClient::StatusOperation::DaemonVersion, error));
    }

    // 5. Verify stop service
    REQUIRE((service.cancel() && service.waitForFinished(30000)));
    REQUIRE(service.status() == LicenseService::Status::Canceled);
}

TEST_CASE_METHOD(InitCleanupFixture, "CIP protocol version compatibility", "[licenseclient]")
{
    static const std::string sc_newerMinorVersion = "1.9";
    static const std::string sc_newerMajorVersion = "9.0";
    static const std::string sc_olderMajorVersion = "0.1";

    // Assert test data is within acceptable range to the actual protocol version
    REQUIRE(utils::compareVersion(CIP_PROTOCOL_VERSION, sc_newerMinorVersion, 1) == 0);
    REQUIRE(utils::compareVersion(CIP_PROTOCOL_VERSION, sc_newerMinorVersion, 2) == -1);
    REQUIRE(utils::compareVersion(CIP_PROTOCOL_VERSION, sc_newerMajorVersion, 1) == -1);
    REQUIRE(utils::compareVersion(CIP_PROTOCOL_VERSION, sc_olderMajorVersion, 1) == 1);

    uint16_t port = 0;
    LicenseService service(DaemonRunMode::TestMode, port, DAEMON_WORK_DIR_TEST);
    REQUIRE((service.start() && service.waitForStarted()));

    {
        port = getServicePort();
        REQUIRE(port > 0);

        TcpClient tcpClient(DAEMON_DEFAULT_ADDR, port);

        std::string error;
        REQUIRE(tcpClient.init(error));

        // 1. Assert same major version range of the CIP protocol is accepted
        std::stringstream ss;
        LicenseClientPrivate::setMandatoryRequestParameters(ss, sc_newerMinorVersion,
            DAEMON_VERSION_CMD, QTLICENSETOOL_APP_NAME, QTLICENSETOOL_VERSION, "foobar");
        ss << " -c " << LIBLICENSECLIENT_VERSION;

        std::string reply;
        REQUIRE(tcpClient.sendReceive(ss.str(), reply));

        ServiceResponse response;
        response.fromString(reply);
        REQUIRE(response.success());
        REQUIRE(response.statusCode() == static_cast<int>(StatusCode::Success));

        // 2. Assert newer major version is not accepted
        ss.str("");
        LicenseClientPrivate::setMandatoryRequestParameters(ss, sc_newerMajorVersion,
            DAEMON_VERSION_CMD, QTLICENSETOOL_APP_NAME, QTLICENSETOOL_VERSION, "foobar");
        ss << " -c " << LIBLICENSECLIENT_VERSION;

        REQUIRE(tcpClient.sendReceive(ss.str(), reply));

        response.fromString(reply);
        REQUIRE(!response.success());
        REQUIRE(response.statusCode() == static_cast<int>(StatusCode::BadClientRequest));

        // 3. Assert older major version without implemented parser is not accepted
        ss.str("");
        LicenseClientPrivate::setMandatoryRequestParameters(ss, sc_olderMajorVersion,
            DAEMON_VERSION_CMD, QTLICENSETOOL_APP_NAME, QTLICENSETOOL_VERSION, "foobar");
        ss << " -c " << LIBLICENSECLIENT_VERSION;

        REQUIRE(tcpClient.sendReceive(ss.str(), reply));

        response.fromString(reply);
        REQUIRE(!response.success());
        REQUIRE(response.statusCode() == static_cast<int>(StatusCode::BadClientRequest));
    }

    REQUIRE((service.cancel() && service.waitForFinished(30000)));
    REQUIRE(service.status() == LicenseService::Status::Canceled);
}

TEST_CASE_METHOD(InitCleanupFixture, "Unexpected request parameters", "[licenseclient]")
{
    uint16_t port = 0;
    LicenseService service(DaemonRunMode::TestMode, port, DAEMON_WORK_DIR_TEST);
    REQUIRE((service.start() && service.waitForStarted()));

    {
        port = getServicePort();
        REQUIRE(port > 0);

        TcpClient tcpClient(DAEMON_DEFAULT_ADDR, port);

        std::string error;
        REQUIRE(tcpClient.init(error));

        std::stringstream ss;
        LicenseClientPrivate::setMandatoryRequestParameters(ss, CIP_PROTOCOL_VERSION,
            DAEMON_VERSION_CMD, QTLICENSETOOL_APP_NAME, QTLICENSETOOL_VERSION, "foobar");
        ss << " -c " << LIBLICENSECLIENT_VERSION;
        ss << " -invalid_option " << "value";
        ss << " a_value_without_option";

        std::string reply;
        REQUIRE(tcpClient.sendReceive(ss.str(), reply));

        ServiceResponse response;
        response.fromString(reply);
        REQUIRE(response.success());
        REQUIRE(response.statusCode() == static_cast<int>(StatusCode::Success));
    }

    REQUIRE((service.cancel() && service.waitForFinished(30000)));
    REQUIRE(service.status() == LicenseService::Status::Canceled);
}

TEST_CASE_METHOD(InitCleanupFixture, "Client version compatibility", "[licenseclient]")
{
    uint16_t port = 0;
    LicenseService service(DaemonRunMode::TestMode, port, DAEMON_WORK_DIR_TEST);
    REQUIRE((service.start() && service.waitForStarted()));

    {
        port = getServicePort();
        REQUIRE(port > 0);

        TcpClient tcpClient(DAEMON_DEFAULT_ADDR, port);

        std::string error;
        REQUIRE(tcpClient.init(error));

        // 1. Assert old  major version is not accepted
        std::stringstream ss;
        LicenseClientPrivate::setMandatoryRequestParameters(ss, CIP_PROTOCOL_VERSION,
            DAEMON_VERSION_CMD, QTLICENSETOOL_APP_NAME, QTLICENSETOOL_VERSION, "foobar");
        ss << " -c " << "1.0.0";

        std::string reply;
        REQUIRE(tcpClient.sendReceive(ss.str(), reply));

        ServiceResponse response;
        response.fromString(reply);
        REQUIRE(!response.success());
        REQUIRE(response.statusCode() == static_cast<int>(StatusCode::ServiceVersionTooNew));

        // 2. Assert current version is accepted
        ss.str("");
        LicenseClientPrivate::setMandatoryRequestParameters(ss, CIP_PROTOCOL_VERSION,
            DAEMON_VERSION_CMD, QTLICENSETOOL_APP_NAME, QTLICENSETOOL_VERSION, "foobar");
        ss << " -c " << LIBLICENSECLIENT_VERSION;

        REQUIRE(tcpClient.sendReceive(ss.str(), reply));
        response.fromString(reply);
        REQUIRE(response.success());
        REQUIRE(response.statusCode() == static_cast<int>(StatusCode::Success));

        // 3. Assert newer patch level is accepted
        int patchVersion = utils::strToInt(LIBLICENSECLIENT_VERSION_PATCH);
        ++patchVersion;

        const std::string newerPatchVersion = std::string(LIBLICENSECLIENT_VERSION_MAJOR) + "."
            + std::string(LIBLICENSECLIENT_VERSION_MINOR) + "." + std::to_string(patchVersion);

        ss.str("");
        LicenseClientPrivate::setMandatoryRequestParameters(ss, CIP_PROTOCOL_VERSION,
            DAEMON_VERSION_CMD, QTLICENSETOOL_APP_NAME, QTLICENSETOOL_VERSION, "foobar");
        ss << " -c " << newerPatchVersion;

        REQUIRE(tcpClient.sendReceive(ss.str(), reply));
        response.fromString(reply);
        REQUIRE(response.success());
        REQUIRE(response.statusCode() == static_cast<int>(StatusCode::Success));

        // 4. Assert newer minor level version is accepted
        int minorVersion = utils::strToInt(LIBLICENSECLIENT_VERSION_MINOR);
        ++minorVersion;

        const std::string newerMinorVersion = std::string(LIBLICENSECLIENT_VERSION_MAJOR) + "."
            + std::to_string(minorVersion) + "." + std::string(LIBLICENSECLIENT_VERSION_PATCH);

        ss.str("");
        LicenseClientPrivate::setMandatoryRequestParameters(ss, CIP_PROTOCOL_VERSION,
            DAEMON_VERSION_CMD, QTLICENSETOOL_APP_NAME, QTLICENSETOOL_VERSION, "foobar");
        ss << " -c " << newerMinorVersion;

        REQUIRE(tcpClient.sendReceive(ss.str(), reply));
        response.fromString(reply);
        REQUIRE(response.success());
        REQUIRE(response.statusCode() == static_cast<int>(StatusCode::Success));

        // 5. Assert newer major version is not accepted
        ss.str("");
        LicenseClientPrivate::setMandatoryRequestParameters(ss, CIP_PROTOCOL_VERSION,
            DAEMON_VERSION_CMD, QTLICENSETOOL_APP_NAME, QTLICENSETOOL_VERSION, "foobar");
        ss << " -c " << "99.0.0";

        REQUIRE(tcpClient.sendReceive(ss.str(), reply));
        response.fromString(reply);
        REQUIRE(!response.success());
        REQUIRE(response.statusCode() == static_cast<int>(StatusCode::ServiceVersionTooLow));

        // 6. Assert no version is accepted (backwards compatibility)
        ss.str("");
        LicenseClientPrivate::setMandatoryRequestParameters(ss, CIP_PROTOCOL_VERSION,
            DAEMON_VERSION_CMD, QTLICENSETOOL_APP_NAME, QTLICENSETOOL_VERSION, "foobar");

        REQUIRE(tcpClient.sendReceive(ss.str(), reply));
        response.fromString(reply);
        REQUIRE(response.success());
        REQUIRE(response.statusCode() == static_cast<int>(StatusCode::Success));
    }

    REQUIRE((service.cancel() && service.waitForFinished(30000)));
    REQUIRE(service.status() == LicenseService::Status::Canceled);
}

TEST_CASE_METHOD(InitCleanupFixture, "Set and get service setting", "[.skip]") // disabled due to #QLS-1758
{
    {
        LicenseService service(DaemonRunMode::TestMode, 0, DAEMON_WORK_DIR_TEST);
        // Verify starting of license "service" succeeds
        REQUIRE((service.start() && service.waitForStarted()));

        LicenseClient client;
        ServiceResponse response;
        std::string error;
        // Verify client connection
        REQUIRE(client.init(error));

        std::string reply;
        // Verify setting the request_timeout property in persistent mode
        REQUIRE(client.setServiceSetting("request_timeout", "90", LicenseClient::SettingsType::Persistent, reply));
        REQUIRE(client.getServiceSetting("request_timeout", reply));
        REQUIRE(reply == "90");

        // Verify setting the request_timeout property in temporary mode
        REQUIRE(client.setServiceSetting("request_timeout", "120", LicenseClient::SettingsType::Temporary, reply));
        REQUIRE(client.getServiceSetting("request_timeout", reply));
        REQUIRE(reply == "120");

        // Verify setting the server_addr property in persistent mode
        REQUIRE(client.setServiceSetting("server_addr", "localhost:8080", LicenseClient::SettingsType::Persistent, reply));
        REQUIRE(client.getServiceSetting("server_addr", reply));
        REQUIRE(reply == "localhost:8080");

        // Verify setting the server_addr property in temporary mode
        REQUIRE(client.setServiceSetting("server_addr", "http://foo.bar", LicenseClient::SettingsType::Temporary, reply));
        REQUIRE(client.getServiceSetting("server_addr", reply));
        REQUIRE(reply == "http://foo.bar");

        // Verify empty server_addr is accepted
        REQUIRE(client.setServiceSetting("server_addr", "", LicenseClient::SettingsType::Temporary, reply));
        REQUIRE(client.getServiceSetting("server_addr", reply));
        REQUIRE(reply == "");

        // Verify failure when attempting to set a property that is not allowed
        REQUIRE(!client.setServiceSetting("ca_bundle_path", "test value", LicenseClient::SettingsType::Persistent, reply));

        // Attempt to set an invalid value for request_timeout
        REQUIRE(!client.setServiceSetting("request_timeout", "test value", LicenseClient::SettingsType::Persistent, reply));

        // Verify getting a value that exists, but is not part of the subset of settings that can be overwritten by the client
        REQUIRE(client.getServiceSetting("tcp_listening_port", reply));

        // Verify getting a non-existing value results in failure return
        REQUIRE(!client.getServiceSetting("QLS_PORT", reply));

        // Verify when the client is out of scope and re-created, the persistent values can be read from the .ini file
        REQUIRE(client.setServiceSetting("request_timeout", "180", LicenseClient::SettingsType::Persistent, reply));

        REQUIRE((service.cancel() && service.waitForFinished(30000)));
        REQUIRE(service.status() == LicenseService::Status::Canceled);
    }

    {
        LicenseService service(DaemonRunMode::TestMode, 0, DAEMON_WORK_DIR_TEST);
        REQUIRE((service.start() && service.waitForStarted()));

        LicenseClient client;
        ServiceResponse response;
        std::string error;

        REQUIRE(client.init(error));

        std::string reply;
        // Verify persistent settings changes
        REQUIRE(client.getServiceSetting("request_timeout", reply));
        REQUIRE(reply == "180");

        REQUIRE(client.getServiceSetting("server_addr", reply));
        REQUIRE(reply == "localhost:8080");

        REQUIRE((service.cancel() && service.waitForFinished(30000)));
        REQUIRE(service.status() == LicenseService::Status::Canceled);
    }
}
