# Copyright (C) 2023 The Qt Company Ltd.
#
# SPDX-License-Identifier: BSD-3-Clause
#

add_executable(tst_licenseclient tst_licenseclient.cpp)
target_link_libraries(tst_licenseclient PRIVATE Catch2 qlicenseclient qlicenseservice)

add_test(NAME tst_licenseclient COMMAND tst_licenseclient)
