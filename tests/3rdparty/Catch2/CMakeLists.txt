# Copyright (C) 2023 The Qt Company Ltd.
#
# SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
#

if ("${CMAKE_SYSTEM_NAME}" STREQUAL "Windows")
    add_definitions(-DDO_NOT_USE_WMAIN)
endif()

add_library(Catch2 INTERFACE)
target_include_directories(Catch2 INTERFACE ${CMAKE_CURRENT_LIST_DIR})
