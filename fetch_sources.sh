#!/usr/bin/env bash
# Copyright (C) 2023 The Qt Company Ltd.
#
# SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
#

set -ex

function exit_fail {
    echo "ERROR! $1: Aborting installation"
    exit 1
}

coin_ip=$(echo $QT_COIN_GIT_DAEMON | cut -d':' -f1)
url="http://${coin_ip}:8080/coin/api/results/license-tools/license-service/${TESTED_MODULE_REVISION_COIN}/sources_unix.tar.gz"
targetFile="/tmp/sources_unix.tar.gz"

if command -v curl >/dev/null
then
    curl --fail -L --retry 5 --retry-delay 5 -o "$targetFile" "$url"
else
    wget --tries 5 -O "$targetFile" "$url"
fi
if [ $? != 0 ]; then
    exit_fail "Failed to fetch sources"
fi
mv /tmp/sources_unix.tar.gz ${HOME}/work/license-tools/license-service/deploy/license_service_sources_unix.tar.gz
if [ $? != 0 ]; then
    exit_fail "Failed to move sources to deploy folder"
fi
