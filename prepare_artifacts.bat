:: Copyright (C) 2023 The Qt Company Ltd.
::
:: SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
::

@echo off

set repoDir=C:\Users\qt\work\license-tools\license-service
mkdir %repoDir%\artifacts\
call :check_error_level
mkdir %repoDir%\artifacts\service_artifacts
call :check_error_level
mkdir %repoDir%\artifacts\cip_artifacts
call :check_error_level
mkdir %repoDir%\artifacts\precheck_artifacts
call :check_error_level
mkdir %repoDir%\artifacts\corelib_artifacts
call :check_error_level

copy "%repoDir%\deploy\*" "%repoDir%\artifacts\service_artifacts"
call :check_error_level
copy "%repoDir%\build\bin\Release\testapp.exe" "%repoDir%\artifacts\service_artifacts"
call :check_error_level

copy "%repoDir%\build\lib\Release\qlicenseclient*.*" "%repoDir%\artifacts\cip_artifacts"
call :check_error_level
copy "%repoDir%\build\include\licenseclient.h" "%repoDir%\artifacts\cip_artifacts"
call :check_error_level
copy "%repoDir%\build\include\licenseinfo.h" "%repoDir%\artifacts\cip_artifacts"
call :check_error_level

copy "%repoDir%\build\lib\Release\qlicenseprecheck*.*" "%repoDir%\artifacts\precheck_artifacts"
call :check_error_level
copy "%repoDir%\build\lib\Release\libcurl*.*" "%repoDir%\artifacts\precheck_artifacts"
call :check_error_level
copy "%repoDir%\build\include\licenseprecheck.h" "%repoDir%\artifacts\precheck_artifacts"
call :check_error_level
copy "%repoDir%\build\include\licenseclient.h" "%repoDir%\artifacts\precheck_artifacts"
call :check_error_level
copy "%repoDir%\build\include\licenseinfo.h" "%repoDir%\artifacts\precheck_artifacts"
call :check_error_level

copy "%repoDir%\build\lib\Release\qlicensecore*.*" "%repoDir%\artifacts\corelib_artifacts"
call :check_error_level
copy "%repoDir%\src\libs\qlicensecore\*.h" "%repoDir%\artifacts\corelib_artifacts"
call :check_error_level
copy "%repoDir%\src\libs\3rdparty\nlohmann\json.hpp" "%repoDir%\artifacts\corelib_artifacts"
call :check_error_level
copy "%repoDir%\build\include\commonsetup.h" "%repoDir%\artifacts\corelib_artifacts"
call :check_error_level
copy "%repoDir%\build\include\version.h" "%repoDir%\artifacts\corelib_artifacts"
call :check_error_level

:check_error_level
IF %ERRORLEVEL% NEQ 0 (
    echo Error: Operation failed with error code %ERRORLEVEL%.
    exit %ERRORLEVEL%
)
EXIT /B 0
