:: Copyright (C) 2023 The Qt Company Ltd.
::
:: SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
::

@echo off

REM Set default generator if argument is not provided
set "GENERATOR=Visual Studio 17 2022"

REM Check if an argument is provided
if "%~1" neq "" (
    set "GENERATOR=%~1"
)

mkdir build
cd build

cmake -DCONFIG=Release -DCMAKE_BUILD_TYPE=Release .. -G "%GENERATOR%"
IF %ERRORLEVEL% NEQ 0 (
    echo Error: cmake failed with error code %ERRORLEVEL%.
    exit /b %ERRORLEVEL%
)

cmake --build . --config Release
IF %ERRORLEVEL% NEQ 0 (
    echo Error: build failed with error code %ERRORLEVEL%.
    exit /b %ERRORLEVEL%
)

cd ..
mkdir deploy

set files_to_copy= ^
    build\bin\Release\qtlicd.exe ^
    build\bin\Release\licheck.exe ^
    build\bin\Release\mocwrapper.exe ^
    build\bin\Release\qtlicensetool.exe ^
    dist\qtlicd.ini ^
    README.md ^
    CHANGELOG ^
    INSTALL_win.txt

for %%f in (%files_to_copy%) do (
    copy "%%f" deploy\
    IF %ERRORLEVEL% NEQ 0 (
        echo Error: copy "%%f" failed with error code %ERRORLEVEL%.
        exit /b %ERRORLEVEL%
    )
)
